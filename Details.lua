local  BUI = select(2, ...):unpack()

function BUI:ImportDetails()

  local profileName = "Bob's Details"

  --This is basically just a straight copy paste from the Details WTF file
  _detalhes_global["__profiles"][profileName] = {
    ["show_arena_role_icon"] = false,
    ["capture_real"] = {
      ["heal"] = true,
      ["spellcast"] = true,
      ["miscdata"] = true,
      ["aura"] = true,
      ["energy"] = true,
      ["damage"] = true,
    },
    ["row_fade_in"] = {
      "in", -- [1]
      0.2, -- [2]
    },
    ["streamer_config"] = {
      ["faster_updates"] = false,
      ["quick_detection"] = false,
      ["reset_spec_cache"] = false,
      ["no_alerts"] = false,
      ["use_animation_accel"] = true,
      ["disable_mythic_dungeon"] = false,
    },
    ["all_players_are_group"] = false,
    ["use_row_animations"] = true,
    ["report_heal_links"] = false,
    ["remove_realm_from_name"] = true,
    ["minimum_overall_combat_time"] = 10,
    ["event_tracker"] = {
      ["enabled"] = false,
      ["font_color"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
        1, -- [4]
      },
      ["line_height"] = 16,
      ["line_color"] = {
        0.1, -- [1]
        0.1, -- [2]
        0.1, -- [3]
        0.3, -- [4]
      },
      ["font_shadow"] = "NONE",
      ["font_size"] = 10,
      ["font_face"] = "Friz Quadrata TT",
      ["frame"] = {
        ["show_title"] = true,
        ["strata"] = "LOW",
        ["backdrop_color"] = {
          0.16, -- [1]
          0.16, -- [2]
          0.16, -- [3]
          0.47, -- [4]
        },
        ["locked"] = false,
        ["height"] = 300,
        ["width"] = 250,
      },
      ["line_texture"] = "Details Serenity",
      ["options_frame"] = {
      },
    },
    ["report_to_who"] = "Beeware",
    ["class_specs_coords"] = {
      [62] = {
        0.251953125, -- [1]
        0.375, -- [2]
        0.125, -- [3]
        0.25, -- [4]
      },
      [63] = {
        0.375, -- [1]
        0.5, -- [2]
        0.125, -- [3]
        0.25, -- [4]
      },
      [250] = {
        0, -- [1]
        0.125, -- [2]
        0, -- [3]
        0.125, -- [4]
      },
      [251] = {
        0.125, -- [1]
        0.25, -- [2]
        0, -- [3]
        0.125, -- [4]
      },
      [252] = {
        0.25, -- [1]
        0.375, -- [2]
        0, -- [3]
        0.125, -- [4]
      },
      [253] = {
        0.875, -- [1]
        1, -- [2]
        0, -- [3]
        0.125, -- [4]
      },
      [254] = {
        0, -- [1]
        0.125, -- [2]
        0.125, -- [3]
        0.25, -- [4]
      },
      [255] = {
        0.125, -- [1]
        0.25, -- [2]
        0.125, -- [3]
        0.25, -- [4]
      },
      [66] = {
        0.125, -- [1]
        0.25, -- [2]
        0.25, -- [3]
        0.375, -- [4]
      },
      [257] = {
        0.5, -- [1]
        0.625, -- [2]
        0.25, -- [3]
        0.375, -- [4]
      },
      [258] = {
        0.6328125, -- [1]
        0.75, -- [2]
        0.25, -- [3]
        0.375, -- [4]
      },
      [259] = {
        0.75, -- [1]
        0.875, -- [2]
        0.25, -- [3]
        0.375, -- [4]
      },
      [260] = {
        0.875, -- [1]
        1, -- [2]
        0.25, -- [3]
        0.375, -- [4]
      },
      [577] = {
        0.25, -- [1]
        0.375, -- [2]
        0.5, -- [3]
        0.625, -- [4]
      },
      [262] = {
        0.125, -- [1]
        0.25, -- [2]
        0.375, -- [3]
        0.5, -- [4]
      },
      [581] = {
        0.375, -- [1]
        0.5, -- [2]
        0.5, -- [3]
        0.625, -- [4]
      },
      [264] = {
        0.375, -- [1]
        0.5, -- [2]
        0.375, -- [3]
        0.5, -- [4]
      },
      [265] = {
        0.5, -- [1]
        0.625, -- [2]
        0.375, -- [3]
        0.5, -- [4]
      },
      [266] = {
        0.625, -- [1]
        0.75, -- [2]
        0.375, -- [3]
        0.5, -- [4]
      },
      [267] = {
        0.75, -- [1]
        0.875, -- [2]
        0.375, -- [3]
        0.5, -- [4]
      },
      [268] = {
        0.625, -- [1]
        0.75, -- [2]
        0.125, -- [3]
        0.25, -- [4]
      },
      [269] = {
        0.875, -- [1]
        1, -- [2]
        0.125, -- [3]
        0.25, -- [4]
      },
      [270] = {
        0.75, -- [1]
        0.875, -- [2]
        0.125, -- [3]
        0.25, -- [4]
      },
      [70] = {
        0.251953125, -- [1]
        0.375, -- [2]
        0.25, -- [3]
        0.375, -- [4]
      },
      [102] = {
        0.375, -- [1]
        0.5, -- [2]
        0, -- [3]
        0.125, -- [4]
      },
      [71] = {
        0.875, -- [1]
        1, -- [2]
        0.375, -- [3]
        0.5, -- [4]
      },
      [103] = {
        0.5, -- [1]
        0.625, -- [2]
        0, -- [3]
        0.125, -- [4]
      },
      [72] = {
        0, -- [1]
        0.125, -- [2]
        0.5, -- [3]
        0.625, -- [4]
      },
      [104] = {
        0.625, -- [1]
        0.75, -- [2]
        0, -- [3]
        0.125, -- [4]
      },
      [73] = {
        0.125, -- [1]
        0.25, -- [2]
        0.5, -- [3]
        0.625, -- [4]
      },
      [64] = {
        0.5, -- [1]
        0.625, -- [2]
        0.125, -- [3]
        0.25, -- [4]
      },
      [105] = {
        0.75, -- [1]
        0.875, -- [2]
        0, -- [3]
        0.125, -- [4]
      },
      [65] = {
        0, -- [1]
        0.125, -- [2]
        0.25, -- [3]
        0.375, -- [4]
      },
      [256] = {
        0.375, -- [1]
        0.5, -- [2]
        0.25, -- [3]
        0.375, -- [4]
      },
      [261] = {
        0, -- [1]
        0.125, -- [2]
        0.375, -- [3]
        0.5, -- [4]
      },
      [263] = {
        0.25, -- [1]
        0.375, -- [2]
        0.375, -- [3]
        0.5, -- [4]
      },
    },
    ["profile_save_pos"] = true,
    ["tooltip"] = {
      ["header_statusbar"] = {
        0.3, -- [1]
        0.3, -- [2]
        0.3, -- [3]
        0.8, -- [4]
        false, -- [5]
        false, -- [6]
        "WorldState Score", -- [7]
      },
      ["fontcolor_right"] = {
        1, -- [1]
        0.7, -- [2]
        0, -- [3]
        1, -- [4]
      },
      ["line_height"] = 17,
      ["tooltip_max_targets"] = 2,
      ["icon_size"] = {
        ["W"] = 17,
        ["H"] = 17,
      },
      ["tooltip_max_pets"] = 2,
      ["anchor_relative"] = "top",
      ["abbreviation"] = 2,
      ["anchored_to"] = 1,
      ["show_amount"] = false,
      ["header_text_color"] = {
        1, -- [1]
        0.9176, -- [2]
        0, -- [3]
        1, -- [4]
      },
      ["fontsize"] = 10,
      ["background"] = {
        0.196, -- [1]
        0.196, -- [2]
        0.196, -- [3]
        0.8, -- [4]
      },
      ["submenu_wallpaper"] = true,
      ["fontsize_title"] = 10,
      ["icon_border_texcoord"] = {
        ["B"] = 0.921875,
        ["L"] = 0.078125,
        ["T"] = 0.078125,
        ["R"] = 0.921875,
      },
      ["commands"] = {
      },
      ["tooltip_max_abilities"] = 6,
      ["fontface"] = "Friz Quadrata TT",
      ["border_color"] = {
        0, -- [1]
        0, -- [2]
        0, -- [3]
        1, -- [4]
      },
      ["border_texture"] = "Details BarBorder 3",
      ["anchor_offset"] = {
        0, -- [1]
        0, -- [2]
      },
      ["maximize_method"] = 1,
      ["fontshadow"] = false,
      ["border_size"] = 14,
      ["menus_bg_texture"] = "Interface\\SPELLBOOK\\Spellbook-Page-1",
      ["anchor_screen_pos"] = {
        507.7, -- [1]
        -350.5, -- [2]
      },
      ["anchor_point"] = "bottom",
      ["menus_bg_coords"] = {
        0.309777336120606, -- [1]
        0.924000015258789, -- [2]
        0.213000011444092, -- [3]
        0.279000015258789, -- [4]
      },
      ["fontcolor"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
        1, -- [4]
      },
      ["menus_bg_color"] = {
        0.8, -- [1]
        0.8, -- [2]
        0.8, -- [3]
        0.2, -- [4]
      },
    },
    ["ps_abbreviation"] = 3,
    ["world_combat_is_trash"] = false,
    ["update_speed"] = 0.2,
    ["bookmark_text_size"] = 11,
    ["animation_speed_mintravel"] = 0.45,
    ["track_item_level"] = true,
    ["windows_fade_in"] = {
      "in", -- [1]
      0.2, -- [2]
    },
    ["instances_menu_click_to_open"] = false,
    ["overall_clear_newchallenge"] = true,
    ["current_dps_meter"] = {
      ["enabled"] = false,
      ["font_color"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
        1, -- [4]
      },
      ["arena_enabled"] = true,
      ["font_shadow"] = "NONE",
      ["font_size"] = 18,
      ["mythic_dungeon_enabled"] = true,
      ["sample_size"] = 5,
      ["font_face"] = "Friz Quadrata TT",
      ["frame"] = {
        ["show_title"] = false,
        ["strata"] = "LOW",
        ["backdrop_color"] = {
          0, -- [1]
          0, -- [2]
          0, -- [3]
          0.2, -- [4]
        },
        ["locked"] = false,
        ["height"] = 65,
        ["width"] = 220,
      },
      ["update_interval"] = 0.3,
      ["options_frame"] = {
      },
    },
    ["data_cleanup_logout"] = false,
    ["instances_disable_bar_highlight"] = false,
    ["trash_concatenate"] = false,
    ["color_by_arena_team"] = true,
    ["animation_speed"] = 33,
    ["disable_stretch_from_toolbar"] = false,
    ["disable_lock_ungroup_buttons"] = false,
    ["memory_ram"] = 64,
    ["disable_window_groups"] = false,
    ["instances_suppress_trash"] = 0,
    ["options_window"] = {
      ["scale"] = 1,
    },
    ["animation_speed_maxtravel"] = 3,
    ["time_type_original"] = 2,
    ["default_bg_alpha"] = 0.5,
    ["font_faces"] = {
      ["menus"] = "Friz Quadrata TT",
    },
    ["time_type"] = 2,
    ["death_tooltip_width"] = 350,
    ["animate_scroll"] = false,
    ["instances"] = {
      {
        ["__pos"] = {
          ["normal"] = {
            ["y"] = -33.849609375,
            ["x"] = -971.3557052612305,
            ["w"] = 190.6220855712891,
            ["h"] = 187.0004119873047,
          },
          ["solo"] = {
            ["y"] = 2,
            ["x"] = 1,
            ["w"] = 300,
            ["h"] = 200,
          },
        },
        ["hide_in_combat_type"] = 1,
        ["clickthrough_window"] = false,
        ["menu_anchor"] = {
          16, -- [1]
          0, -- [2]
          ["side"] = 2,
        },
        ["bg_r"] = 0.0941176470588235,
        ["hide_out_of_combat"] = false,
        ["color_buttons"] = {
          1, -- [1]
          1, -- [2]
          1, -- [3]
          1, -- [4]
        },
        ["toolbar_icon_file"] = "Interface\\AddOns\\Details\\images\\toolbar_icons",
        ["micro_displays_locked"] = true,
        ["tooltip"] = {
          ["n_abilities"] = 3,
          ["n_enemies"] = 3,
        },
        ["switch_all_roles_in_combat"] = false,
        ["clickthrough_toolbaricons"] = false,
        ["row_info"] = {
          ["textR_outline"] = false,
          ["spec_file"] = "Interface\\AddOns\\Details\\images\\spec_icons_normal",
          ["textL_outline"] = false,
          ["texture_highlight"] = "Interface\\FriendsFrame\\UI-FriendsList-Highlight",
          ["textR_show_data"] = {
            true, -- [1]
            true, -- [2]
            true, -- [3]
          },
          ["textL_enable_custom_text"] = false,
          ["fixed_text_color"] = {
            1, -- [1]
            1, -- [2]
            1, -- [3]
          },
          ["space"] = {
            ["right"] = 0,
            ["left"] = 0,
            ["between"] = 1,
          },
          ["texture_background_class_color"] = false,
          ["textL_outline_small_color"] = {
            0, -- [1]
            0, -- [2]
            0, -- [3]
            1, -- [4]
          },
          ["font_face_file"] = "Interface\\Addons\\Details\\fonts\\Accidental Presidency.ttf",
          ["backdrop"] = {
            ["enabled"] = false,
            ["size"] = 12,
            ["color"] = {
              1, -- [1]
              1, -- [2]
              1, -- [3]
              1, -- [4]
            },
            ["texture"] = "Details BarBorder 2",
          },
          ["models"] = {
            ["upper_model"] = "Spells\\AcidBreath_SuperGreen.M2",
            ["lower_model"] = "World\\EXPANSION02\\DOODADS\\Coldarra\\COLDARRALOCUS.m2",
            ["upper_alpha"] = 0.5,
            ["lower_enabled"] = false,
            ["lower_alpha"] = 0.1,
            ["upper_enabled"] = false,
          },
          ["textL_translit_text"] = false,
          ["height"] = 21,
          ["texture_file"] = "Interface\\AddOns\\BigWigs\\Media\\Textures\\BantoBar",
          ["textR_bracket"] = "(",
          ["font_size"] = 15,
          ["icon_file"] = "Interface\\AddOns\\Details\\images\\classes_small",
          ["icon_grayscale"] = false,
          ["textL_custom_text"] = "{data1}. {data3}{data2}",
          ["use_spec_icons"] = true,
          ["textR_enable_custom_text"] = false,
          ["start_after_icon"] = true,
          ["fixed_texture_color"] = {
            0, -- [1]
            0, -- [2]
            0, -- [3]
          },
          ["textL_show_number"] = true,
          ["percent_type"] = 1,
          ["textL_outline_small"] = true,
          ["textR_custom_text"] = "{data1} ({data2}, {data3}%)",
          ["fixed_texture_background_color"] = {
            0, -- [1]
            0, -- [2]
            0, -- [3]
            0.150228589773178, -- [4]
          },
          ["textR_outline_small"] = true,
          ["texture_background_file"] = "Interface\\AddOns\\Details\\images\\bar4_reverse",
          ["texture_background"] = "Details D'ictum (reverse)",
          ["textR_outline_small_color"] = {
            0, -- [1]
            0, -- [2]
            0, -- [3]
            1, -- [4]
          },
          ["textR_class_colors"] = false,
          ["textL_class_colors"] = false,
          ["alpha"] = 1,
          ["no_icon"] = false,
          ["icon_offset"] = {
            0, -- [1]
            0, -- [2]
          },
          ["texture_custom"] = "",
          ["font_face"] = "Accidental Presidency",
          ["texture_class_colors"] = true,
          ["texture"] = "BantoBar",
          ["fast_ps_update"] = false,
          ["textR_separator"] = ",",
          ["texture_custom_file"] = "Interface\\",
        },
        ["ignore_mass_showhide"] = false,
        ["plugins_grow_direction"] = 1,
        ["menu_icons"] = {
          true, -- [1]
          true, -- [2]
          true, -- [3]
          true, -- [4]
          true, -- [5]
          false, -- [6]
          ["space"] = -2,
          ["shadow"] = false,
        },
        ["switch_damager"] = false,
        ["micro_displays_side"] = 2,
        ["window_scale"] = 1,
        ["hide_icon"] = true,
        ["toolbar_side"] = 1,
        ["bg_g"] = 0.0941176470588235,
        ["bg_b"] = 0.0941176470588235,
        ["switch_healer_in_combat"] = false,
        ["color"] = {
          0.0705882352941177, -- [1]
          0.0705882352941177, -- [2]
          0.0705882352941177, -- [3]
          0.639999985694885, -- [4]
        },
        ["skin"] = "Minimalistic",
        ["following"] = {
          ["enabled"] = false,
          ["bar_color"] = {
            1, -- [1]
            1, -- [2]
            1, -- [3]
          },
          ["text_color"] = {
            1, -- [1]
            1, -- [2]
            1, -- [3]
          },
        },
        ["switch_healer"] = false,
        ["StatusBarSaved"] = {
          ["center"] = "DETAILS_STATUSBAR_PLUGIN_CLOCK",
          ["right"] = "DETAILS_STATUSBAR_PLUGIN_PDPS",
          ["options"] = {
            ["DETAILS_STATUSBAR_PLUGIN_PDPS"] = {
              ["textYMod"] = 1,
              ["textXMod"] = 0,
              ["textFace"] = "Accidental Presidency",
              ["textAlign"] = 0,
              ["textStyle"] = 2,
              ["textSize"] = 10,
              ["textColor"] = {
                1, -- [1]
                1, -- [2]
                1, -- [3]
                1, -- [4]
              },
            },
            ["DETAILS_STATUSBAR_PLUGIN_PSEGMENT"] = {
              ["textColor"] = {
                1, -- [1]
                1, -- [2]
                1, -- [3]
                1, -- [4]
              },
              ["segmentType"] = 2,
              ["textFace"] = "Accidental Presidency",
              ["textXMod"] = 0,
              ["textAlign"] = 0,
              ["textStyle"] = 2,
              ["textSize"] = 10,
              ["textYMod"] = 1,
            },
            ["DETAILS_STATUSBAR_PLUGIN_CLOCK"] = {
              ["textColor"] = {
                1, -- [1]
                1, -- [2]
                1, -- [3]
                1, -- [4]
              },
              ["textFace"] = "Accidental Presidency",
              ["textXMod"] = 6,
              ["textAlign"] = 0,
              ["timeType"] = 1,
              ["textStyle"] = 2,
              ["textSize"] = 10,
              ["textYMod"] = 1,
            },
          },
          ["left"] = "DETAILS_STATUSBAR_PLUGIN_PSEGMENT",
        },
        ["switch_tank_in_combat"] = false,
        ["bg_alpha"] = 0.183960914611816,
        ["__snapV"] = true,
        ["__locked"] = false,
        ["menu_alpha"] = {
          ["enabled"] = false,
          ["onenter"] = 1,
          ["iconstoo"] = true,
          ["ignorebars"] = false,
          ["onleave"] = 1,
        },
        ["show_statusbar"] = false,
        ["__was_opened"] = true,
        ["strata"] = "LOW",
        ["clickthrough_incombatonly"] = true,
        ["__snap"] = {
          [2] = 2,
        },
        ["menu_icons_size"] = 0.850000023841858,
        ["hide_in_combat_alpha"] = 0,
        ["switch_damager_in_combat"] = false,
        ["row_show_animation"] = {
          ["anim"] = "Fade",
          ["options"] = {
          },
        },
        ["libwindow"] = {
          ["y"] = -33.84962463378906,
          ["x"] = 0,
          ["point"] = "LEFT",
          ["scale"] = 1,
        },
        ["statusbar_info"] = {
          ["alpha"] = 0.639999985694885,
          ["overlay"] = {
            0.0705882352941177, -- [1]
            0.0705882352941177, -- [2]
            0.0705882352941177, -- [3]
          },
        },
        ["stretch_button_side"] = 1,
        ["bars_grow_direction"] = 1,
        ["show_sidebars"] = false,
        ["bars_sort_direction"] = 1,
        ["backdrop_texture"] = "Details Ground",
        ["grab_on_top"] = false,
        ["instance_button_anchor"] = {
          -27, -- [1]
          1, -- [2]
        },
        ["clickthrough_rows"] = false,
        ["auto_current"] = true,
        ["menu_anchor_down"] = {
          16, -- [1]
          -3, -- [2]
        },
        ["version"] = 3,
        ["attribute_text"] = {
          ["enabled"] = true,
          ["shadow"] = false,
          ["side"] = 1,
          ["text_size"] = 12,
          ["custom_text"] = "{name}",
          ["text_face"] = "Accidental Presidency",
          ["anchor"] = {
            -18, -- [1]
            3, -- [2]
          },
          ["text_color"] = {
            1, -- [1]
            1, -- [2]
            1, -- [3]
            1, -- [4]
          },
          ["enable_custom_text"] = false,
          ["show_timer"] = {
            true, -- [1]
            true, -- [2]
            true, -- [3]
          },
        },
        ["hide_in_combat"] = false,
        ["posicao"] = {
          ["normal"] = {
            ["y"] = -33.849609375,
            ["x"] = -971.3557052612305,
            ["w"] = 190.6220855712891,
            ["h"] = 187.0004119873047,
          },
          ["solo"] = {
            ["y"] = 2,
            ["x"] = 1,
            ["w"] = 300,
            ["h"] = 200,
          },
        },
        ["auto_hide_menu"] = {
          ["left"] = false,
          ["right"] = false,
        },
        ["desaturated_menu"] = false,
        ["wallpaper"] = {
          ["enabled"] = false,
          ["texcoord"] = {
            0, -- [1]
            1, -- [2]
            0, -- [3]
            0.7, -- [4]
          },
          ["overlay"] = {
            1, -- [1]
            1, -- [2]
            1, -- [3]
            1, -- [4]
          },
          ["anchor"] = "all",
          ["height"] = 114.042518615723,
          ["alpha"] = 0.5,
          ["width"] = 283.000183105469,
        },
        ["total_bar"] = {
          ["enabled"] = false,
          ["only_in_group"] = true,
          ["icon"] = "Interface\\ICONS\\INV_Sigil_Thorim",
          ["color"] = {
            1, -- [1]
            1, -- [2]
            1, -- [3]
          },
        },
        ["switch_tank"] = false,
        ["switch_all_roles_after_wipe"] = false,
        ["skin_custom"] = "",
        ["bars_inverted"] = false,
      }, -- [1]
      {
        ["__pos"] = {
          ["normal"] = {
            ["y"] = -184.8794250488281,
            ["x"] = -971.3557052612305,
            ["w"] = 190.6220855712891,
            ["h"] = 75.05921173095703,
          },
          ["solo"] = {
            ["y"] = 2,
            ["x"] = 1,
            ["w"] = 300,
            ["h"] = 200,
          },
        },
        ["show_statusbar"] = false,
        ["clickthrough_window"] = false,
        ["menu_anchor"] = {
          16, -- [1]
          0, -- [2]
          ["side"] = 2,
        },
        ["bg_r"] = 0.0941176470588235,
        ["hide_out_of_combat"] = false,
        ["color_buttons"] = {
          1, -- [1]
          1, -- [2]
          1, -- [3]
          1, -- [4]
        },
        ["toolbar_icon_file"] = "Interface\\AddOns\\Details\\images\\toolbar_icons",
        ["micro_displays_locked"] = true,
        ["tooltip"] = {
          ["n_abilities"] = 3,
          ["n_enemies"] = 3,
        },
        ["switch_all_roles_in_combat"] = false,
        ["clickthrough_toolbaricons"] = false,
        ["clickthrough_rows"] = false,
        ["switch_tank"] = false,
        ["switch_all_roles_after_wipe"] = false,
        ["menu_icons"] = {
          true, -- [1]
          true, -- [2]
          true, -- [3]
          true, -- [4]
          true, -- [5]
          false, -- [6]
          ["space"] = -2,
          ["shadow"] = false,
        },
        ["switch_damager"] = false,
        ["show_sidebars"] = false,
        ["window_scale"] = 1,
        ["hide_icon"] = true,
        ["toolbar_side"] = 1,
        ["bg_g"] = 0.0941176470588235,
        ["bg_b"] = 0.0941176470588235,
        ["switch_healer_in_combat"] = false,
        ["color"] = {
          0.0705882352941177, -- [1]
          0.0705882352941177, -- [2]
          0.0705882352941177, -- [3]
          0.639999985694885, -- [4]
        },
        ["skin"] = "Minimalistic",
        ["following"] = {
          ["bar_color"] = {
            1, -- [1]
            1, -- [2]
            1, -- [3]
          },
          ["enabled"] = false,
          ["text_color"] = {
            1, -- [1]
            1, -- [2]
            1, -- [3]
          },
        },
        ["switch_healer"] = false,
        ["StatusBarSaved"] = {
          ["center"] = "DETAILS_STATUSBAR_PLUGIN_CLOCK",
          ["right"] = "DETAILS_STATUSBAR_PLUGIN_PDPS",
          ["options"] = {
            ["DETAILS_STATUSBAR_PLUGIN_PDPS"] = {
              ["textYMod"] = 1,
              ["textXMod"] = 0,
              ["textFace"] = "Accidental Presidency",
              ["textAlign"] = 0,
              ["textStyle"] = 2,
              ["textSize"] = 10,
              ["textColor"] = {
                1, -- [1]
                1, -- [2]
                1, -- [3]
                1, -- [4]
              },
            },
            ["DETAILS_STATUSBAR_PLUGIN_PSEGMENT"] = {
              ["textColor"] = {
                1, -- [1]
                1, -- [2]
                1, -- [3]
                1, -- [4]
              },
              ["segmentType"] = 2,
              ["textXMod"] = 0,
              ["textFace"] = "Accidental Presidency",
              ["textAlign"] = 0,
              ["textStyle"] = 2,
              ["textSize"] = 10,
              ["textYMod"] = 1,
            },
            ["DETAILS_STATUSBAR_PLUGIN_CLOCK"] = {
              ["textColor"] = {
                1, -- [1]
                1, -- [2]
                1, -- [3]
                1, -- [4]
              },
              ["textFace"] = "Accidental Presidency",
              ["textXMod"] = 6,
              ["textAlign"] = 0,
              ["timeType"] = 1,
              ["textStyle"] = 2,
              ["textSize"] = 10,
              ["textYMod"] = 1,
            },
          },
          ["left"] = "DETAILS_STATUSBAR_PLUGIN_PSEGMENT",
        },
        ["instance_button_anchor"] = {
          -27, -- [1]
          1, -- [2]
        },
        ["bg_alpha"] = 0.183960914611816,
        ["__snapV"] = true,
        ["__locked"] = false,
        ["menu_alpha"] = {
          ["enabled"] = false,
          ["onleave"] = 1,
          ["ignorebars"] = false,
          ["iconstoo"] = true,
          ["onenter"] = 1,
        },
        ["hide_in_combat_type"] = 1,
        ["__was_opened"] = true,
        ["strata"] = "LOW",
        ["clickthrough_incombatonly"] = true,
        ["__snap"] = {
          [4] = 1,
        },
        ["menu_icons_size"] = 0.850000023841858,
        ["hide_in_combat_alpha"] = 0,
        ["bars_sort_direction"] = 1,
        ["total_bar"] = {
          ["enabled"] = false,
          ["only_in_group"] = true,
          ["icon"] = "Interface\\ICONS\\INV_Sigil_Thorim",
          ["color"] = {
            1, -- [1]
            1, -- [2]
            1, -- [3]
          },
        },
        ["libwindow"] = {
          ["y"] = -184.8794555664063,
          ["x"] = 0,
          ["point"] = "LEFT",
          ["scale"] = 1,
        },
        ["statusbar_info"] = {
          ["alpha"] = 0.639999985694885,
          ["overlay"] = {
            0.0705882352941177, -- [1]
            0.0705882352941177, -- [2]
            0.0705882352941177, -- [3]
          },
        },
        ["backdrop_texture"] = "Details Ground",
        ["row_show_animation"] = {
          ["anim"] = "Fade",
          ["options"] = {
          },
        },
        ["skin_custom"] = "",
        ["bars_inverted"] = false,
        ["switch_tank_in_combat"] = false,
        ["switch_damager_in_combat"] = false,
        ["grab_on_top"] = false,
        ["version"] = 3,
        ["auto_current"] = true,
        ["attribute_text"] = {
          ["show_timer"] = {
            true, -- [1]
            true, -- [2]
            true, -- [3]
          },
          ["shadow"] = false,
          ["side"] = 1,
          ["text_size"] = 12,
          ["custom_text"] = "{name}",
          ["text_face"] = "Accidental Presidency",
          ["anchor"] = {
            -18, -- [1]
            3, -- [2]
          },
          ["enabled"] = true,
          ["enable_custom_text"] = false,
          ["text_color"] = {
            1, -- [1]
            1, -- [2]
            1, -- [3]
            1, -- [4]
          },
        },
        ["menu_anchor_down"] = {
          16, -- [1]
          -3, -- [2]
        },
        ["row_info"] = {
          ["textR_outline"] = false,
          ["spec_file"] = "Interface\\AddOns\\Details\\images\\spec_icons_normal",
          ["textL_outline"] = false,
          ["textR_outline_small"] = true,
          ["textR_show_data"] = {
            true, -- [1]
            true, -- [2]
            true, -- [3]
          },
          ["textL_enable_custom_text"] = false,
          ["fixed_text_color"] = {
            1, -- [1]
            1, -- [2]
            1, -- [3]
          },
          ["space"] = {
            ["right"] = 0,
            ["left"] = 0,
            ["between"] = 1,
          },
          ["texture_background_class_color"] = false,
          ["start_after_icon"] = true,
          ["font_face_file"] = "Interface\\Addons\\Details\\fonts\\Accidental Presidency.ttf",
          ["backdrop"] = {
            ["enabled"] = false,
            ["texture"] = "Details BarBorder 2",
            ["color"] = {
              1, -- [1]
              1, -- [2]
              1, -- [3]
              1, -- [4]
            },
            ["size"] = 12,
          },
          ["font_size"] = 15,
          ["textL_translit_text"] = false,
          ["texture_custom_file"] = "Interface\\",
          ["texture_file"] = "Interface\\AddOns\\BigWigs\\Media\\Textures\\BantoBar",
          ["height"] = 21,
          ["models"] = {
            ["upper_model"] = "Spells\\AcidBreath_SuperGreen.M2",
            ["lower_model"] = "World\\EXPANSION02\\DOODADS\\Coldarra\\COLDARRALOCUS.m2",
            ["upper_alpha"] = 0.5,
            ["lower_enabled"] = false,
            ["lower_alpha"] = 0.1,
            ["upper_enabled"] = false,
          },
          ["icon_file"] = "Interface\\AddOns\\Details\\images\\classes_small",
          ["icon_grayscale"] = false,
          ["textL_custom_text"] = "{data1}. {data3}{data2}",
          ["textR_bracket"] = "(",
          ["texture_custom"] = "",
          ["textL_outline_small_color"] = {
            0, -- [1]
            0, -- [2]
            0, -- [3]
            1, -- [4]
          },
          ["fixed_texture_color"] = {
            0, -- [1]
            0, -- [2]
            0, -- [3]
          },
          ["textL_show_number"] = true,
          ["percent_type"] = 1,
          ["textL_outline_small"] = true,
          ["textR_custom_text"] = "{data1} ({data2}, {data3}%)",
          ["texture"] = "BantoBar",
          ["texture_highlight"] = "Interface\\FriendsFrame\\UI-FriendsList-Highlight",
          ["texture_background_file"] = "Interface\\AddOns\\Details\\images\\bar4_reverse",
          ["textR_class_colors"] = false,
          ["alpha"] = 1,
          ["textL_class_colors"] = false,
          ["texture_background"] = "Details D'ictum (reverse)",
          ["textR_outline_small_color"] = {
            0, -- [1]
            0, -- [2]
            0, -- [3]
            1, -- [4]
          },
          ["no_icon"] = false,
          ["icon_offset"] = {
            0, -- [1]
            0, -- [2]
          },
          ["textR_enable_custom_text"] = false,
          ["font_face"] = "Accidental Presidency",
          ["texture_class_colors"] = true,
          ["fixed_texture_background_color"] = {
            0, -- [1]
            0, -- [2]
            0, -- [3]
            0.150228589773178, -- [4]
          },
          ["fast_ps_update"] = false,
          ["textR_separator"] = ",",
          ["use_spec_icons"] = true,
        },
        ["hide_in_combat"] = false,
        ["posicao"] = {
          ["normal"] = {
            ["y"] = -184.8794250488281,
            ["x"] = -971.3557052612305,
            ["w"] = 190.6220855712891,
            ["h"] = 75.05921173095703,
          },
          ["solo"] = {
            ["y"] = 2,
            ["x"] = 1,
            ["w"] = 300,
            ["h"] = 200,
          },
        },
        ["bars_grow_direction"] = 1,
        ["micro_displays_side"] = 2,
        ["wallpaper"] = {
          ["overlay"] = {
            1, -- [1]
            1, -- [2]
            1, -- [3]
            1, -- [4]
          },
          ["texcoord"] = {
            0, -- [1]
            1, -- [2]
            0, -- [3]
            0.7, -- [4]
          },
          ["enabled"] = false,
          ["anchor"] = "all",
          ["height"] = 114.042518615723,
          ["alpha"] = 0.5,
          ["width"] = 283.000183105469,
        },
        ["stretch_button_side"] = 1,
        ["ignore_mass_showhide"] = false,
        ["plugins_grow_direction"] = 1,
        ["desaturated_menu"] = false,
        ["auto_hide_menu"] = {
          ["left"] = false,
          ["right"] = false,
        },
      }, -- [2]
    },
    ["report_lines"] = 5,
    ["clear_ungrouped"] = true,
    ["memory_threshold"] = 3,
    ["skin"] = "WoW Interface",
    ["override_spellids"] = true,
    ["class_icons_small"] = "Interface\\AddOns\\Details\\images\\classes_small",
    ["force_activity_time_pvp"] = true,
    ["pvp_as_group"] = true,
    ["numerical_system"] = 1,
    ["minimum_combat_time"] = 5,
    ["player_details_window"] = {
      ["scale"] = 1,
      ["bar_texture"] = "Skyline",
      ["skin"] = "ElvUI",
    },
    ["cloud_capture"] = true,
    ["damage_taken_everything"] = false,
    ["scroll_speed"] = 2,
    ["font_sizes"] = {
      ["menus"] = 10,
    },
    ["chat_tab_embed"] = {
      ["enabled"] = false,
      ["y_offset"] = 0,
      ["x_offset"] = 0,
      ["tab_name"] = "",
      ["single_window"] = false,
    },
    ["deadlog_events"] = 32,
    ["trash_auto_remove"] = true,
    ["close_shields"] = false,
    ["class_coords"] = {
      ["HUNTER"] = {
        0, -- [1]
        0.25, -- [2]
        0.25, -- [3]
        0.5, -- [4]
      },
      ["WARRIOR"] = {
        0, -- [1]
        0.25, -- [2]
        0, -- [3]
        0.25, -- [4]
      },
      ["SHAMAN"] = {
        0.25, -- [1]
        0.49609375, -- [2]
        0.25, -- [3]
        0.5, -- [4]
      },
      ["MAGE"] = {
        0.25, -- [1]
        0.49609375, -- [2]
        0, -- [3]
        0.25, -- [4]
      },
      ["PET"] = {
        0.25, -- [1]
        0.49609375, -- [2]
        0.75, -- [3]
        1, -- [4]
      },
      ["DRUID"] = {
        0.7421875, -- [1]
        0.98828125, -- [2]
        0, -- [3]
        0.25, -- [4]
      },
      ["MONK"] = {
        0.5, -- [1]
        0.73828125, -- [2]
        0.5, -- [3]
        0.75, -- [4]
      },
      ["DEATHKNIGHT"] = {
        0.25, -- [1]
        0.5, -- [2]
        0.5, -- [3]
        0.75, -- [4]
      },
      ["MONSTER"] = {
        0, -- [1]
        0.25, -- [2]
        0.75, -- [3]
        1, -- [4]
      },
      ["UNKNOW"] = {
        0.5, -- [1]
        0.75, -- [2]
        0.75, -- [3]
        1, -- [4]
      },
      ["PRIEST"] = {
        0.49609375, -- [1]
        0.7421875, -- [2]
        0.25, -- [3]
        0.5, -- [4]
      },
      ["ROGUE"] = {
        0.49609375, -- [1]
        0.7421875, -- [2]
        0, -- [3]
        0.25, -- [4]
      },
      ["Alliance"] = {
        0.49609375, -- [1]
        0.7421875, -- [2]
        0.75, -- [3]
        1, -- [4]
      },
      ["WARLOCK"] = {
        0.7421875, -- [1]
        0.98828125, -- [2]
        0.25, -- [3]
        0.5, -- [4]
      },
      ["DEMONHUNTER"] = {
        0.73828126, -- [1]
        1, -- [2]
        0.5, -- [3]
        0.75, -- [4]
      },
      ["Horde"] = {
        0.7421875, -- [1]
        0.98828125, -- [2]
        0.75, -- [3]
        1, -- [4]
      },
      ["PALADIN"] = {
        0, -- [1]
        0.25, -- [2]
        0.5, -- [3]
        0.75, -- [4]
      },
      ["UNGROUPPLAYER"] = {
        0.5, -- [1]
        0.75, -- [2]
        0.75, -- [3]
        1, -- [4]
      },
      ["ENEMY"] = {
        0, -- [1]
        0.25, -- [2]
        0.75, -- [3]
        1, -- [4]
      },
    },
    ["overall_clear_newboss"] = true,
    ["disable_alldisplays_window"] = false,
    ["overall_flag"] = 16,
    ["numerical_system_symbols"] = "auto",
    ["segments_amount_to_save"] = 18,
    ["standard_skin"] = false,
    ["clear_graphic"] = true,
    ["hotcorner_topleft"] = {
      ["hide"] = false,
    },
    ["animation_speed_triggertravel"] = 5,
    ["options_group_edit"] = true,
    ["broadcaster_enabled"] = false,
    ["minimap"] = {
      ["onclick_what_todo"] = 1,
      ["radius"] = 160,
      ["text_type"] = 1,
      ["minimapPos"] = 186.2772831387607,
      ["text_format"] = 3,
      ["hide"] = false,
    },
    ["instances_amount"] = 5,
    ["max_window_size"] = {
      ["height"] = 450,
      ["width"] = 480,
    },
    ["default_bg_color"] = 0.0941,
    ["only_pvp_frags"] = false,
    ["disable_stretch_button"] = false,
    ["deny_score_messages"] = false,
    ["segments_auto_erase"] = 1,
    ["class_colors"] = {
      ["HUNTER"] = {
        0.67, -- [1]
        0.83, -- [2]
        0.45, -- [3]
      },
      ["WARRIOR"] = {
        0.78, -- [1]
        0.61, -- [2]
        0.43, -- [3]
      },
      ["PALADIN"] = {
        0.96, -- [1]
        0.55, -- [2]
        0.73, -- [3]
      },
      ["MAGE"] = {
        0.41, -- [1]
        0.8, -- [2]
        0.94, -- [3]
      },
      ["ARENA_YELLOW"] = {
        1, -- [1]
        1, -- [2]
        0.25, -- [3]
      },
      ["UNGROUPPLAYER"] = {
        0.4, -- [1]
        0.4, -- [2]
        0.4, -- [3]
      },
      ["DRUID"] = {
        1, -- [1]
        0.49, -- [2]
        0.04, -- [3]
      },
      ["MONK"] = {
        0, -- [1]
        1, -- [2]
        0.59, -- [3]
      },
      ["DEATHKNIGHT"] = {
        0.77, -- [1]
        0.12, -- [2]
        0.23, -- [3]
      },
      ["WARLOCK"] = {
        0.58, -- [1]
        0.51, -- [2]
        0.79, -- [3]
      },
      ["UNKNOW"] = {
        0.2, -- [1]
        0.2, -- [2]
        0.2, -- [3]
      },
      ["PRIEST"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
      },
      ["ROGUE"] = {
        1, -- [1]
        0.96, -- [2]
        0.41, -- [3]
      },
      ["PET"] = {
        0.3, -- [1]
        0.4, -- [2]
        0.5, -- [3]
      },
      ["ENEMY"] = {
        0.94117, -- [1]
        0, -- [2]
        0.0196, -- [3]
        1, -- [4]
      },
      ["DEMONHUNTER"] = {
        0.64, -- [1]
        0.19, -- [2]
        0.79, -- [3]
      },
      ["version"] = 1,
      ["NEUTRAL"] = {
        1, -- [1]
        1, -- [2]
        0, -- [3]
      },
      ["SHAMAN"] = {
        0, -- [1]
        0.44, -- [2]
        0.87, -- [3]
      },
      ["ARENA_GREEN"] = {
        0.4, -- [1]
        1, -- [2]
        0.4, -- [3]
      },
    },
    ["segments_panic_mode"] = false,
    ["windows_fade_out"] = {
      "out", -- [1]
      0.2, -- [2]
    },
    ["window_clamp"] = {
      -8, -- [1]
      0, -- [2]
      21, -- [3]
      -14, -- [4]
    },
    ["row_fade_out"] = {
      "out", -- [1]
      0.2, -- [2]
    },
    ["overall_clear_logout"] = false,
    ["use_scroll"] = false,
    ["new_window_size"] = {
      ["height"] = 158,
      ["width"] = 310,
    },
    ["report_schema"] = 1,
    ["use_battleground_server_parser"] = false,
    ["total_abbreviation"] = 2,
    ["disable_reset_button"] = false,
    ["data_broker_text"] = "",
    ["segments_amount"] = 18,
    ["instances_no_libwindow"] = false,
    ["deadlog_limit"] = 16,
    ["instances_segments_locked"] = false,
  }

  --Apply the profile after it's been imported
    _detalhes:ApplyProfile(profileName, true, false)
end
