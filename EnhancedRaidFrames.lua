local  BUI = select(2, ...):unpack()

function BUI:ImportEnhancedRaidFrames()
  --The DB might not have been created for new ERF uses, create it if it doesn't exist
  IndicatorsDB["profiles"] = IndicatorsDB["profiles"] or {}

  local druidProfileName = "Bob's Druid ERF"
  --A copy paste of the "Gigabob - Kazzak" profile from the EnhancedRaidFrames WTF file (it's named RaidFrameIndicators)
  IndicatorsDB["profiles"][druidProfileName] = {
    ["showRaidIcons"] = false,
    ["mine9"] = true,
    ["mine2"] = true,
    ["mine3"] = true,
    ["mine1"] = true,
    ["auras8"] = "Cenarion Ward\nRejuvenation (Germination)",
    ["mine6"] = true,
    ["mine4"] = true,
    ["auras3"] = "Rejuvenation\n",
    ["auras6"] = "Regrowth",
    ["mine8"] = true,
    ["auras4"] = "Spring Blossoms",
    ["auras1"] = "Grove Tending",
    ["mine7"] = true,
    ["auras9"] = "Wild Growth",
    ["auras2"] = "Lifebloom",
    ["showBuffs"] = false,
  }
  local paladinProfileName = "Bob's Paladin ERF"
  --A copy paste of the "Bobra - Kazzak" profile from the EnhancedRaidFrames WTF file (it's named RaidFrameIndicators)
  IndicatorsDB["profiles"][paladinProfileName] = {
    ["auras1"] = "Eye of Corruption",
    ["iconSize2"] = 24,
    ["iconSize3"] = 24,
    ["auras3"] = "Grand Delusions",
    ["iconSize1"] = 24,
    ["showRaidIcons"] = false,
    ["stack1"] = true,
    ["auras2"] = "Grasping Tendrils",
  }

  --Add the EnhancedRaidFrames so we can access the DB
  local EnhancedRaidFrames = LibStub("AceAddon-3.0"):GetAddon("EnhancedRaidFrames")
  --Apply the profile after it's been imported
  EnhancedRaidFrames.db:SetProfile(druidProfileName)
end
