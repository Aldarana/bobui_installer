local  BUI = select(2, ...):unpack()

function BUI:ImportBigDebuffs()
  --The DB might not have been created for new ERF uses, create it if it doesn't exist
  BigDebuffs["profiles"] = BigDebuffs["profiles"] or {}

  local profileName = "Bob's BigDebuffs"
--This is basically just a straight copy paste from the BigDebuffs WTF file
  BigDebuffs["profiles"][profileName] = {
    ["unitFrames"] = {
      ["arena1"] = {
      },
      ["player"] = {
        ["enabled"] = false,
      },
    },
    ["raidFrames"] = {
      ["maxDebuffs"] = 6,
      ["cc"] = 40,
      ["interrupts"] = 40,
      ["anchor"] = "RIGHT",
      ["hideBliz"] = false,
      ["pve"] = 40,
      ["default"] = 40,
      ["increaseBuffs"] = true,
      ["dispellable"] = {
        ["cc"] = 70,
        ["roots"] = 60,
      },
    },
  }

  --Apply the profile after it's been imported
  BigDebuffs.db:SetProfile(profileName)
end
