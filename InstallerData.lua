local addon, _ = ...
local Version = GetAddOnMetadata(addon, "Version")
local  BUI = select(2, ...):unpack()

BUI.installerData = {
  [1] = function()
    InstallerFrame.Title:SetText('Title Text')
    InstallerFrame.PageTitle:SetText('Title Text')
    InstallerFrame.PageText1:SetText('Desc 1 Tet')
    InstallerFrame.PageText1:SetText('Desc 2 Tet')

    InstallerFrame.Button1:Show()
    InstallerFrame.Button1:SetText('Text 1')
    InstallerFrame.Button1:SetScript('OnClick', function()
      --Add in all the things that should happen when the button is clicked
      BUI:ImportWeakAuras()
      BUI:ImportBigWigs()
      BUI:ImportPlater()
      BUI:ImportDetails()
      BUI:ImportTellMeWhen()
      BUI:ImportBartender()
      BUI:ImportBlizzardSettings()
      BUI:ImportBigDebuffs()
      BUI:ImportEnhancedRaidFrames()
      BUI:ImportGottaGoFast()

      --save a variable so we know we've installed all the profiles
      BobUI.db.profile.install_version = Version

      --Plater dose not like it when you change profiles and requies a reload after so do it right before the reload
    	if IsAddOnLoaded("Plater") then
        --make sure this profile name matches the name from our Plater.lua file
    		Plater.db:SetProfile(BUI.platerProfileName)
      end

      ReloadUI()
    end)

    --Totally Optional Button2, simply remove the Show() line to disable
    InstallerFrame.Button2:Show()
    InstallerFrame.Button2:SetText('Text 2')
    InstallerFrame.Button2:SetScript('OnClick', function()

    end)
    end,
}
