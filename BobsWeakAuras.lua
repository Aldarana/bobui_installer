local  BUI = select(2, ...):unpack()

function BUI:ImportWeakAuras()
  --[[
  To add auras use the follwing format:
    WeakAurasSaved["displays"]["Aura Name"] = {In game export to lua table}
    The aura name needs to be manually added for each aura,
    it does not need to match the name of the aura being exported
    I recommend using a good text editor to manage the auras, Notepad++ is decent.

    If you want to add a group you needed to add each aura in the group seperatly
    *WARNING*!!: The name of a group and all it's children MUST match the names used in game

    *WARNING*!!:If you add an aura with custom code the in game export will add an extra escape character
    To fix this find "\\" and replace with "\" !! Do this before pasting into this file to avoid damaging other auras
    For very complex custom auras it's probably easier just to get them from the WTF
  ]]--

  WeakAurasSaved["displays"]["Dungeon - Targeted Spells"] = {
    ["arcLength"] = 360,
    ["controlledChildren"] = {
        [1] = "Targeted Spells 3",
        [2] = "Sound when targeted by a boss 3",
    },
    ["xOffset"] = 0,
    ["displayText"] = "%p",
    ["yOffset"] = 0,
    ["anchorPoint"] = "CENTER",
    ["borderColor"] = {
        [1] = 0,
        [2] = 0,
        [3] = 0,
        [4] = 1,
    },
    ["space"] = 1,
    ["url"] = "https://wago.io/BFADungeonTargetedSpells/61",
    ["actions"] = {
        ["start"] = {
        },
        ["init"] = {
        },
        ["finish"] = {
        },
    },
    ["backgroundInset"] = 0,
    ["selfPoint"] = "CENTER",
    ["rotation"] = 0,
    ["font"] = "Friz Quadrata TT",
    ["load"] = {
        ["use_class"] = false,
        ["class"] = {
            ["multi"] = {
            },
        },
        ["spec"] = {
            ["multi"] = {
            },
        },
        ["size"] = {
            ["multi"] = {
            },
        },
    },
    ["animate"] = false,
    ["scale"] = 1,
    ["regionType"] = "dynamicgroup",
    ["constantFactor"] = "RADIUS",
    ["borderOffset"] = 4,
    ["tocversion"] = 80205,
    ["borderInset"] = 1,
    ["fixedWidth"] = 200,
    ["outline"] = "OUTLINE",
    ["borderBackdrop"] = "Blizzard Tooltip",
    ["color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["useAnchorPerUnit"] = true,
    ["automaticWidth"] = "Auto",
    ["triggers"] = {
        [1] = {
            ["trigger"] = {
                ["type"] = "aura2",
                ["subeventSuffix"] = "_CAST_START",
                ["event"] = "Health",
                ["unit"] = "player",
                ["spellIds"] = {
                },
                ["names"] = {
                },
                ["subeventPrefix"] = "SPELL",
                ["debuffType"] = "HELPFUL",
            },
            ["untrigger"] = {
            },
        },
    },
    ["columnSpace"] = 1,
    ["internalVersion"] = 29,
    ["useLimit"] = true,
    ["align"] = "CENTER",
    ["version"] = 61,
    ["subRegions"] = {
    },
    ["backdropColor"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 0.5,
    },
    ["fontSize"] = 12,
    ["authorOptions"] = {
    },
    ["sort"] = "none",
    ["preferToUpdate"] = false,
    ["border"] = false,
    ["borderEdge"] = "1 Pixel",
    ["grow"] = "HORIZONTAL",
    ["borderSize"] = 2,
    ["anchorPerUnit"] = "UNITFRAME",
    ["groupIcon"] = 1033497,
    ["gridType"] = "RD",
    ["animation"] = {
        ["start"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["main"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["finish"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
    },
    ["customTextUpdate"] = "update",
    ["background"] = "None",
    ["radius"] = 200,
    ["semver"] = "2.5.6",
    ["wordWrap"] = "WordWrap",
    ["id"] = "Dungeon - Targeted Spells",
    ["stagger"] = 0,
    ["frameStrata"] = 6,
    ["anchorFrameType"] = "SCREEN",
    ["rowSpace"] = 1,
    ["limit"] = 3,
    ["config"] = {
    },
    ["justify"] = "LEFT",
    ["conditions"] = {
    },
    ["gridWidth"] = 5,
    ["uid"] = "zSrl30KxJ6i",
  }

  WeakAurasSaved["displays"]["Targeted Spells 3"] = {
    ["text2Point"] = "CENTER",
    ["text1FontSize"] = 12,
    ["cooldownTextEnabled"] = true,
    ["preferToUpdate"] = false,
    ["yOffset"] = 0,
    ["anchorPoint"] = "LEFT",
    ["url"] = "https://wago.io/BFADungeonTargetedSpells/61",
    ["icon"] = true,
    ["text1Enabled"] = true,
    ["keepAspectRatio"] = false,
    ["selfPoint"] = "CENTER",
    ["desaturate"] = false,
    ["text1Point"] = "BOTTOM",
    ["text2FontFlags"] = "OUTLINE",
    ["load"] = {
        ["ingroup"] = {
            ["multi"] = {
            },
        },
        ["use_never"] = false,
        ["talent"] = {
            ["multi"] = {
            },
        },
        ["class"] = {
            ["multi"] = {
            },
        },
        ["difficulty"] = {
            ["multi"] = {
            },
        },
        ["race"] = {
            ["multi"] = {
            },
        },
        ["faction"] = {
            ["multi"] = {
            },
        },
        ["pvptalent"] = {
            ["multi"] = {
            },
        },
        ["spec"] = {
            ["multi"] = {
            },
        },
        ["role"] = {
            ["multi"] = {
            },
        },
        ["talent2"] = {
            ["multi"] = {
            },
        },
        ["size"] = {
            ["single"] = "party",
            ["multi"] = {
                ["party"] = true,
            },
        },
    },
    ["text1FontFlags"] = "OUTLINE",
    ["regionType"] = "icon",
    ["text2FontSize"] = 24,
    ["cooldownTextDisabled"] = true,
    ["auto"] = true,
    ["tocversion"] = 80205,
    ["text2Enabled"] = false,
    ["config"] = {
        ["nameplate"] = {
            ["enable"] = 4,
        },
        ["glow"] = {
        },
        ["icon"] = {
            ["enable"] = true,
        },
    },
    ["color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["customText"] = "function()\n    if aura_env.state and aura_env.state.stacks and aura_env.state.stacks > 1 then\n        return aura_env.state.stacks\n    end\nend",
    ["cooldownSwipe"] = true,
    ["customTextUpdate"] = "event",
    ["cooldownEdge"] = false,
    ["internalVersion"] = 29,
    ["animation"] = {
        ["start"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["main"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["finish"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
    },
    ["stickyDuration"] = false,
    ["version"] = 61,
    ["subRegions"] = {
        [1] = {
            ["text_shadowXOffset"] = 0,
            ["text_text"] = "%c",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "AUTO",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_color"] = {
                [1] = 1,
                [2] = 1,
                [3] = 1,
                [4] = 1,
            },
            ["text_font"] = "2002",
            ["text_shadowYOffset"] = 0,
            ["text_wordWrap"] = "WordWrap",
            ["text_fontType"] = "OUTLINE",
            ["text_anchorPoint"] = "OUTER_BOTTOM",
            ["text_fontSize"] = 13,
            ["anchorXOffset"] = 0,
            ["text_visible"] = true,
        },
        [2] = {
            ["text_shadowXOffset"] = 0,
            ["text_text"] = "Nameplate Font Settings",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "CENTER",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_anchorXOffset"] = 0,
            ["text_color"] = {
                [1] = 1,
                [2] = 1,
                [3] = 1,
                [4] = 1,
            },
            ["text_font"] = "Friz Quadrata TT",
            ["text_anchorYOffset"] = 0,
            ["text_wordWrap"] = "WordWrap",
            ["text_fontType"] = "OUTLINE",
            ["text_anchorPoint"] = "CENTER",
            ["text_visible"] = false,
            ["text_fontSize"] = 20,
            ["anchorXOffset"] = 0,
            ["text_shadowYOffset"] = 0,
        },
    },
    ["height"] = 20,
    ["parent"] = "Dungeon - Targeted Spells",
    ["text1Containment"] = "OUTSIDE",
    ["text2Containment"] = "INSIDE",
    ["text1Color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["alpha"] = 1,
    ["uid"] = "FkqDfwsa2vz",
    ["anchorFrameFrame"] = "UIParent",
    ["text2Color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["authorOptions"] = {
        [1] = {
            ["subOptions"] = {
                [1] = {
                    ["type"] = "toggle",
                    ["key"] = "enable",
                    ["desc"] = "Show cast's icons on Unit Frames",
                    ["default"] = true,
                    ["useDesc"] = true,
                    ["name"] = "Show (default)",
                    ["width"] = 2,
                },
            },
            ["type"] = "group",
            ["width"] = 2,
            ["useCollapse"] = true,
            ["name"] = "Cast icons on Unit Frames",
            ["key"] = "icon",
            ["limitType"] = "none",
            ["groupType"] = "simple",
            ["collapse"] = false,
            ["size"] = 10,
        },
        [2] = {
            ["subOptions"] = {
                [1] = {
                    ["desc"] = "Show cast's target on nameplate",
                    ["type"] = "select",
                    ["default"] = 1,
                    ["name"] = "Show",
                    ["values"] = {
                        [1] = "Always show",
                        [2] = "Show by default (overide with \"actions > init\" list)",
                        [3] = "Disabled by default (overide with \"actions > init\" list)",
                        [4] = "Disabled",
                    },
                    ["useDesc"] = true,
                    ["key"] = "enable",
                    ["width"] = 2,
                },
                [2] = {
                    ["type"] = "description",
                    ["text"] = "Nameplate font, shadow, shadow color, size and offsets settings are now in the \"Display\" tab",
                    ["fontSize"] = "medium",
                    ["width"] = 2,
                },
            },
            ["type"] = "group",
            ["width"] = 2,
            ["useCollapse"] = true,
            ["name"] = "Target Name on Nameplates",
            ["key"] = "nameplate",
            ["limitType"] = "none",
            ["groupType"] = "simple",
            ["collapse"] = false,
            ["size"] = 10,
        },
        [3] = {
            ["subOptions"] = {
                [1] = {
                    ["type"] = "description",
                    ["fontSize"] = "medium",
                    ["text"] = "Set options for Glow on Unit Frames in the \"Display\" tab.\n\nClick the pencil icon for all options, but don't enable \"Show Glow Effect\"",
                    ["width"] = 2,
                },
            },
            ["type"] = "group",
            ["width"] = 2,
            ["useCollapse"] = true,
            ["name"] = "Glow on Unit Frames",
            ["key"] = "glow",
            ["limitType"] = "none",
            ["groupType"] = "simple",
            ["collapse"] = false,
            ["size"] = 10,
        },
        [4] = {
            ["type"] = "space",
            ["variableWidth"] = true,
            ["height"] = 10,
            ["width"] = 2,
            ["useHeight"] = true,
        },
        [5] = {
            ["type"] = "space",
            ["variableWidth"] = true,
            ["height"] = 10,
            ["width"] = 2,
            ["useHeight"] = true,
        },
    },
    ["text1Font"] = "Friz Quadrata TT",
    ["zoom"] = 0,
    ["text1"] = "%c",
    ["anchorFrameType"] = "SCREEN",
    ["xOffset"] = 0,
    ["triggers"] = {
        [1] = {
            ["trigger"] = {
                ["duration"] = "1",
                ["genericShowOn"] = "showOnActive",
                ["unit"] = "player",
                ["debuffType"] = "HELPFUL",
                ["type"] = "custom",
                ["subeventSuffix"] = "_CAST_START",
                ["custom_hide"] = "timed",
                ["buffShowOn"] = "showOnActive",
                ["event"] = "Health",
                ["custom"] = "function(allstates, event, sourceUnit)\n    if not aura_env.loaded then return false end\n    if event == \"ENCOUNTER_END\" then\n        -- reset aura state to avoid ghost icons between pulls\n        for _,cast in pairs(aura_env.allcasts) do\n            if cast.nameplate then\n                aura_env.removeFontFromNameplate(cast.nameplate)\n            end\n        end\n        aura_env.allcasts = {}\n        for _,state in pairs(allstates) do\n            state.show = false\n            state.changed = true\n        end\n        return true\n    end\n    if sourceUnit and UnitIsEnemy(sourceUnit, \"player\") then\n        local allcasts = aura_env.allcasts\n        local sourceGUID = UnitGUID(sourceUnit)\n        local iconChanged = false\n        local cast = allcasts[sourceGUID]\n        \n        if event == \"UNIT_SPELLCAST_START\"\n        or event == \"UNIT_SPELLCAST_DELAYED\" \n        or event == \"UNIT_SPELLCAST_CHANNEL_START\"\n        or event == \"UNIT_SPELLCAST_CHANNEL_UPDATE\"\n        or event == \"UNIT_TARGET\"\n        then\n            if not cast then\n                -- check if unit is casting\n                local castType\n                local name,_,texture,castStart,castEnd,_,_,notInterruptible,spellId = UnitCastingInfo(sourceUnit)\n                if name then\n                    castType = \"cast\"\n                else\n                    name,_,texture,castStart,castEnd,_,notInterruptible,spellId = UnitChannelInfo(sourceUnit)\n                    if name then\n                        castType = \"channel\"\n                    end\n                end\n                \n                if spellId and not aura_env.isBlackListed(spellId, sourceUnit) then\n                    local spellInList = aura_env.spells[spellId]\n                    -- find npc's target\n                    local targetUnit = sourceUnit..\"target\"\n                    if UnitExists(targetUnit) then\n                        for groupmember in WA_IterateGroupMembers() do\n                            if UnitIsUnit(targetUnit, groupmember) then\n                                local targetGUID = UnitGUID(groupmember)\n                                local targeted = targetGUID == WeakAuras.myGUID\n                                local showIcon = (spellInList and spellInList.icon) or (not spellInList and aura_env.config.icon.enable)\n                                local showGlow = spellInList and spellInList.glow\n                                local showNameplate\n                                if aura_env.config.nameplate.enable == 1 then\n                                    showNameplate = true\n                                elseif aura_env.config.nameplate.enable == 2 then\n                                    showNameplate = (spellInList and spellInList.nameplate) or (not spellInList)\n                                elseif aura_env.config.nameplate.enable == 3 then\n                                    showNameplate = spellInList and spellInList.nameplate\n                                elseif aura_env.config.nameplate.enable == 4 then\n                                    showNameplate = false\n                                end\n                                local playSound = targeted and spellInList and spellInList.sound\n                                local bigIcon = targeted and spellInList and spellInList.bigIcon\n                                allcasts[sourceGUID] = {\n                                    name = name,\n                                    icon = texture,\n                                    start = castStart/1000,\n                                    expirationTime = castEnd/1000,\n                                    spellId = spellId,\n                                    target = groupmember,\n                                    targetGUID = targetGUID,\n                                    targeted = targeted,\n                                    spellInList = spellInList,\n                                    notInterruptible = notInterruptible,\n                                    castType = castType,\n                                    showIcon = showIcon,\n                                    showGlow = showGlow,\n                                    bigIcon = bigIcon,\n                                    showNameplate = showNameplate,\n                                    nameplate = showNameplate and aura_env.addFontToNameplate(\n                                        sourceUnit,\n                                        groupmember\n                                    ),\n                                    playSound = playSound\n                                }\n                                iconChanged = true\n                                break\n                            end\n                        end\n                    end\n                end\n            else\n                if UnitExists(sourceUnit) then\n                    if event == \"UNIT_SPELLCAST_DELAYED\" \n                    or event == \"UNIT_SPELLCAST_CHANNEL_UPDATE\"\n                    or event == \"UNIT_SPELLCAST_CHANNEL_START\"\n                    then\n                        local castType\n                        local name,_,_,castStart,castEnd,_,_,notInterruptible,spellId = UnitCastingInfo(sourceUnit)\n                        if name then\n                            castType = \"cast\"\n                        else\n                            name,_,_,castStart,castEnd,_,notInterruptible,spellId = UnitChannelInfo(sourceUnit)\n                            if name then\n                                castType = \"channel\"\n                            end\n                        end\n                        if spellId then\n                            cast.notInterruptible = notInterruptible\n                            cast.castType = castType\n                            cast.start = castStart/1000\n                            cast.expirationTime = castEnd/1000\n                            cast.changed = true\n                            iconChanged = true\n                        end\n                    elseif event == \"UNIT_TARGET\" then\n                        local targetUnit = sourceUnit..\"target\"\n                        if UnitExists(targetUnit) then\n                            for groupmember in WA_IterateGroupMembers() do\n                                if UnitIsUnit(targetUnit, groupmember) then\n                                    if cast.nameplate then\n                                        aura_env.removeFontFromNameplate(cast.nameplate)\n                                    end\n                                    \n                                    local targetGUID = UnitGUID(groupmember)\n                                    local targeted = targetGUID == WeakAuras.myGUID\n                                    local spellId = cast.spellId\n                                    cast.changed = true\n                                    cast.target = groupmember\n                                    cast.targetGUID = targetGUID\n                                    cast.targeted = targeted\n                                    \n                                    if cast.showNameplate then\n                                        cast.nameplate = aura_env.addFontToNameplate(\n                                            sourceUnit,\n                                            groupmember\n                                        )\n                                    end\n                                    \n                                    iconChanged = true\n                                    break\n                                end\n                            end\n                        end\n                    end\n                end\n            end\n        elseif cast then\n            if event == \"UNIT_SPELLCAST_STOP\" --  or event == \"UNIT_SPELLCAST_SUCCEEDED\"\n            or event == \"UNIT_SPELLCAST_INTERRUPTED\"\n            or event == \"UNIT_SPELLCAST_FAILED\"\n            or event == \"UNIT_SPELLCAST_FAILED_QUIET\"\n            or event == \"UNIT_SPELLCAST_CHANNEL_STOP\"\n            then\n                if cast.nameplate then\n                    aura_env.removeFontFromNameplate(cast.nameplate)\n                end\n                allcasts[sourceGUID] = nil\n                iconChanged = true\n            elseif event == \"NAME_PLATE_UNIT_REMOVED\" then\n                if cast.nameplate then\n                    aura_env.removeFontFromNameplate(cast.nameplate)\n                end\n            elseif event == \"NAME_PLATE_UNIT_ADDED\" then\n                if cast.showNameplate then\n                    cast.nameplate = aura_env.addFontToNameplate(\n                        sourceUnit,\n                        cast.target\n                    )\n                end\n            end\n        end\n        \n        if iconChanged then\n            -- update allstates from allcasts\n            for sourceGUID, cast in pairs(allcasts) do\n                -- index for allstates is \"spellId_targetGUID\"\n                local index = (\"%s_%s\"):format(cast.spellId, cast.targetGUID)\n                \n                local state = allstates[index]\n                \n                if state and state.show then\n                    state.casts[sourceGUID] = true\n                    if cast.expirationTime > state.expirationTime then\n                        state.expirationTime = cast.expirationTime\n                        state.changed = true\n                    end\n                else\n                    allstates[index] = {\n                        show = true,\n                        name = cast.name,\n                        icon = cast.icon,\n                        changed = true,\n                        autoHide = true,\n                        progressType = \"timed\",\n                        duration = cast.expirationTime - cast.start,\n                        expirationTime = cast.expirationTime,\n                        spellId = cast.spellId,\n                        castType = cast.castType,\n                        notInterruptible = cast.notInterruptible,\n                        target = cast.target,\n                        unit = cast.showIcon and cast.target,\n                        targetGUID = cast.targetGUID,\n                        frame = WeakAuras.GetUnitFrame(cast.target),\n                        casts = {\n                            [sourceGUID] = true\n                        },\n                        targeted = cast.targeted,\n                        showGlow = cast.showGlow,\n                        playSound = cast.playSound,\n                        bigIcon = cast.bigIcon,\n                        showIcon = cast.showIcon,\n                    }\n                end\n            end\n            \n            -- count how much of the same cast is showing each icon\n            -- remove casts stopped from state.casts\n            -- remove state if state.casts is empty\n            for index, state in pairs(allstates) do \n                if state.show and state.showIcon then\n                    local countcasts = 0\n                    for sourceGUID,_ in pairs(state.casts) do\n                        local cast = allcasts[sourceGUID]\n                        if not cast\n                        or (cast and cast.targetGUID ~= state.targetGUID)\n                        then\n                            state.casts[sourceGUID] = nil\n                        else \n                            state.changed = true\n                            countcasts = countcasts + 1\n                        end\n                    end\n                    allstates[index].stacks = countcasts\n                    -- hide if no cast\n                    if countcasts == 0 then\n                        state.show = false \n                        state.changed = true\n                    end\n                end\n            end\n        end\n    end\n    \n    return true\nend",
                ["custom_type"] = "stateupdate",
                ["names"] = {
                },
                ["spellIds"] = {
                },
                ["subeventPrefix"] = "SPELL",
                ["check"] = "event",
                ["events"] = "NAME_PLATE_UNIT_ADDED NAME_PLATE_UNIT_REMOVED UNIT_SPELLCAST_CHANNEL_START UNIT_SPELLCAST_CHANNEL_STOP UNIT_SPELLCAST_CHANNEL_UPDATE UNIT_SPELLCAST_DELAYED UNIT_SPELLCAST_FAILED UNIT_SPELLCAST_FAILED_QUIET UNIT_SPELLCAST_INTERRUPTED UNIT_SPELLCAST_START UNIT_SPELLCAST_STOP UNIT_TARGET ENCOUNTER_END",
                ["unevent"] = "timed",
                ["customVariables"] = "{\n    targeted = {\n        display = \"Targeted\",\n        type = \"bool\"\n    },\n    playSound = {\n        display = \"Play Sound\",\n        type = \"bool\"\n    },\n    showGlow = {\n        display = \"Glow\",\n        type = \"bool\"\n    },\n    bigIcon = {\n        display = \"Big Icon\",\n        type = \"bool\"\n    },\n    spellId = {\n        display = \"Spell Id\",\n        type = \"number\"\n    },\n    notInterruptible = {\n        display = \"Not Interruptible\",\n        type = \"bool\"\n    },\n    castType = {\n        display = \"Cast Type\",\n        type = \"select\",\n        values = {\n            [\"cast\"] = \"Cast\",\n            [\"channel\"] = \"Channeling\"\n        }\n    }\n}",
            },
            ["untrigger"] = {
            },
        },
        ["disjunctive"] = "any",
        ["customTriggerLogic"] = "",
        ["activeTriggerMode"] = -10,
    },
    ["semver"] = "2.5.6",
    ["actions"] = {
        ["start"] = {
            ["do_glow"] = false,
            ["custom"] = "\n\n",
            ["glow_action"] = "show",
            ["do_custom"] = false,
            ["sound"] = " custom",
            ["do_sound"] = false,
        },
        ["init"] = {
            ["custom"] = "-- update 26/08/09 -- by Buds - https://wago.io/BFADungeonTargetedSpells\n\n-- Spell list\naura_env.spells = {\n    [259832] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = true }, -- Massive Glaive - Stormbound Conqueror (Warport Wastari, Zuldazar, for testing purpose only)\n    \n    -- Raid\n    [284405] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Sirens - Tormented Song (Stormwall Blockade)\n    \n    -- Affixes\n    [288693] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Tormented Soul - Grave Bolt (Reaping affix)\n    \n    -- Atal'Dazar\n    [253239] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Dazar'ai Juggernaut - Merciless Assault\n    [256846] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Dinomancer Kish'o - Deadeye Aim\n    [257407] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Rezan - Pursuit\n    \n    -- Freehold\n    [257739] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Blacktooth Scrapper - Blind Rage\n    [258338] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Captain Raoul - Blackout Barrel\n    [256979] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Captain Eudora - Powder Shot\n    \n    -- Kings'Rest\n    [266231] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Kula the Butcher - Severing Axe\n    [270507] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, --  Spectral Beastmaster - Poison Barrage\n    [265773] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- The Golden Serpent - Spit Gold\n    [270506] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Spectral Beastmaster - Deadeye Shot\n    \n    -- Shrine of the Storm\n    [264166] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Aqu'sirr - Undertow\n    [268214] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Runecarver Sorn - Carve Flesh\n    \n    -- Siege of Boralus\n    [257641] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Kul Tiran Marksman - Molten Slug\n    [272874] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Ashvane Commander - Trample\n    [272581] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Bilge Rat Tempest - Water Spray\n    [272528] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Ashvane Sniper - Shoot\n    [272542] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Ashvane Sniper - Ricochet\n    \n    -- Temple of Sethraliss\n    [268703] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Charged Dust Devil - Lightning Bolt\n    [272670] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Sandswept Marksman - Shoot\n    [267278] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Static-charged Dervish - Electrocute\n    [272820] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Spark Channeler - Shock\n    [274642] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Hoodoo Hexer - Lava Burst\n    [268061] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Plague Doctor - Chain Lightning\n    \n    -- The Motherlode!!\n    [268185] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Refreshment Vendor, Iced Spritzer\n    [258674] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Off-Duty Laborer - Throw Wrench\n    [276304] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Rowdy Reveler - Penny For Your Thoughts\n    [263628] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Mechanized Peacekeeper - Charged Claw\n    [263209] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Mine Rat - Throw Rock\n    [263202] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Venture Co. Earthshaper - Rock Lance\n    [262794] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Venture Co. Mastermind - Energy Lash\n    [260669] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Rixxa Fluxflame - Propellant Blast\n    \n    -- The Underrot\n    [265376] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Fanatical Headhunter - Barbed Spear\n    [265084] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Devout Blood Priest - Blood Bolt\n    [265625] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Befouled Spirit - Dark Omen\n    \n    -- Tol Dagor\n    [256039] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Overseer Korgus - Deadeye\n    [185857] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Ashvane Spotter - Shoot\n    \n    -- Waycrest Manor\n    [263891] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Heartsbane Vinetwister - Grasping Thorns\n    [264510] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Crazed Marksman - Shoot\n    [260699] = { icon = true, glow = true, nameplate = true, sound = false, bigIcon = false }, -- Coven Diviner - Soul Bolt\n    [260551] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Soulbound Goliath - Soul Thorns\n    [260741] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Heartsbane Triad - Jagged Nettles\n    [268202] = { icon = true, glow = true, nameplate = true, sound = true, bigIcon = false }, -- Gorak Tul - Death Lens\n}\n\n\n-- TODO move this in aura_env.config\naura_env.blacklist_npc = {\n    [120651] = true, -- explosive orb affix\n}\naura_env.blacklist_spell = {\n    \n}\n\n-- DO NOT EDIT BELOW --\n\naura_env.loaded = false\n\naura_env.position = {\"LEFT\",\"TOPLEFT\",\"TOP\",\"TOPRIGHT\",\"RIGHT\",\"BOTTOMRIGHT\",\"BOTTOM\",\"BOTTOMLEFT\",\"CENTER\"}\naura_env.direction = {\"LEFT\",\"RIGHT\",\"TOP\",\"BOTTOM\",\"HORIZONTAL\",\"VERTICAL\"}\n\naura_env.isBlackListed = function(spellId, unit)\n    local guid = UnitGUID(unit)\n    local npc_id= select(6,strsplit(\"-\",guid))\n    return aura_env.blacklist_spell[spellId] or aura_env.blacklist_npc[npc_id]\nend\n\naura_env.allcasts = {}\n\n\n-- Custom Glow\n\nlocal LCG = LibStub(\"LibCustomGlow-1.0\")\n\nlocal glowFunc\nif type(budsCleanFrames) ~= \"table\" then\n    budsCleanFrames = {}\n    local cleaner = function()\n        if not InCombatLockdown() then\n            for auraid, frames in pairs(budsCleanFrames) do\n                for frame, glowType in pairs(frames) do\n                    glowFunc(frame, false, auraid, glowType)\n                end\n            end\n        end\n    end\n    -- hide glow on opening WeakAurasOptions\n    hooksecurefunc(WeakAuras, \"OpenOptions\", cleaner)\nend\nif type(budsCleanFrames[aura_env.id]) ~= \"table\" then\n    budsCleanFrames[aura_env.id] = {} \nend\n\nlocal data = WeakAuras.GetData(aura_env.id)\nglowFunc = function(frame, show, id, glowType)\n    if frame then\n        id = id or aura_env.id\n        local glowType = data.glowType\n        if show then\n            budsCleanFrames[id][frame] = glowType\n            if glowType == \"ACShine\" then\n                LCG.AutoCastGlow_Start(\n                    frame,\n                    data.glowColor,\n                    data.glowParticules,\n                    data.glowFrequency,\n                    data.glowScale,\n                    data.glowXOffset,\n                    data.glowYOffset,\n                    id\n                )\n            elseif glowType == \"Pixel\" then\n                LCG.PixelGlow_Start(\n                    frame,\n                    data.glowColor,\n                    data.glowLines,\n                    data.glowFrequency,\n                    data.glowLength,\n                    data.glowThickness,\n                    data.glowXOffset,\n                    data.glowYOffset,\n                    data.glowBorder,\n                    id\n                )\n            elseif glowType == \"buttonOverlay\" then\n                LCG.ButtonGlow_Start(\n                    frame,\n                    data.glowColor,\n                    data.glowFrequency\n                )\n            end\n        else\n            budsCleanFrames[id][frame] = nil\n            if glowType == \"ACShine\" then\n                LCG.AutoCastGlow_Stop(frame, id)\n            elseif glowType == \"Pixel\" then\n                LCG.PixelGlow_Stop(frame, id)\n            elseif glowType == \"buttonOverlay\" then\n                LCG.ButtonGlow_Stop(frame)\n            end\n        end\n    end\nend\n\naura_env.Glow = glowFunc\n\n\n-- Nameplates Text\nlocal fonts = {}\nlocal subtext = WeakAuras.GetData(aura_env.id).subRegions[2]\nlocal SharedMedia = LibStub(\"LibSharedMedia-3.0\")\n\naura_env.addFontToNameplate = function(unit, target)\n    local nameplate = C_NamePlate.GetNamePlateForUnit(unit)\n    if nameplate then\n        local nameplateUnit = nameplate.namePlateUnitToken or nameplate.unitFrame and nameplate.unitFrame.unit\n        if nameplateUnit then\n            local font = fonts[nameplateUnit]\n            if not font then\n                fonts[nameplateUnit] = UIParent:CreateFontString(nil, \"OVERLAY\")\n                font = fonts[nameplateUnit]\n                local fontPath = SharedMedia:Fetch(\"font\", subtext.text_font)\n                font:SetFont(fontPath or STANDARD_TEXT_FONT, subtext.text_fontSize or 20, subtext.text_fontType or \"OUTLINE\")\n                font:SetShadowColor(unpack(subtext.text_shadowColor))\n                font:SetShadowOffset(subtext.text_shadowXOffset, subtext.text_shadowYOffset)\n            end\n            font:ClearAllPoints()\n            local selfPoint = subtext.text_selfPoint == \"AUTO\" and \"CENTER\" or subtext.text_selfPoint\n            local anchorPoint = subtext.text_anchorPoint:gsub(\"%OUTER_\", \"\"):gsub(\"%INNER_\", \"\")\n            font:SetPoint(selfPoint, nameplate, anchorPoint, subtext.text_anchorXOffset, subtext.text_anchorYOffset)\n            font:SetText(WA_ClassColorName(target))\n            font:Show()\n            return nameplateUnit\n        end\n    end\n    return true\nend\n\naura_env.removeFontFromNameplate = function(unit)\n    local font = fonts[unit]\n    if font then\n        font:Hide()\n    end\nend\n\n\naura_env.loaded = true",
            ["do_custom"] = true,
        },
        ["finish"] = {
            ["custom"] = "local env = aura_env\nif env.state.frame and env.state.showGlow then\n    env.Glow(env.state.frame, false)\nend\n\nif env.state.bigIcon then\n    WeakAuras.ScanEvents(\n        \"HIDE_BIGICON\",\n        env.state.spellId\n    )\nend\n\n-- fix \"ghosts\" casts\nfor sourceGUID in pairs(env.state.casts) do\n    local cast = env.allcasts[sourceGUID]\n    if cast then\n        if cast.nameplate then\n            env.removeFontFromNameplate(cast.nameplate)\n        end\n        if cast.lineToNameplate then\n            env.removeLineFromNameplate(cast.lineToNameplate)\n        end\n        env.allcasts[sourceGUID] = nil\n    end\nend",
            ["do_custom"] = true,
        },
    },
    ["id"] = "Targeted Spells 3",
    ["text2"] = "%p",
    ["frameStrata"] = 6,
    ["width"] = 20,
    ["text2Font"] = "Friz Quadrata TT",
    ["inverse"] = false,
    ["glowParticules"] = 4,
    ["conditions"] = {
        [1] = {
            ["check"] = {
                ["trigger"] = 1,
                ["variable"] = "playSound",
                ["value"] = 1,
                ["checks"] = {
                    [1] = {
                        ["trigger"] = 1,
                        ["variable"] = "targeted",
                        ["value"] = 1,
                    },
                    [2] = {
                        ["trigger"] = 1,
                        ["variable"] = "playSound",
                        ["value"] = 1,
                    },
                },
            },
            ["changes"] = {
                [1] = {
                    ["value"] = {
                        ["sound_kit_id"] = "12889",
                        ["sound_type"] = "Play",
                        ["sound"] = " KitID",
                        ["sound_channel"] = "Master",
                    },
                    ["property"] = "sound",
                },
            },
        },
        [2] = {
            ["check"] = {
                ["trigger"] = 1,
                ["variable"] = "showGlow",
                ["value"] = 1,
            },
            ["changes"] = {
                [1] = {
                    ["value"] = {
                        ["custom"] = "-- Glow frame\nif aura_env.state.frame then\n    aura_env.Glow(aura_env.state.frame, true)\nend",
                    },
                    ["property"] = "customcode",
                },
            },
        },
        [3] = {
            ["check"] = {
                ["trigger"] = 1,
                ["variable"] = "bigIcon",
                ["value"] = 1,
            },
            ["changes"] = {
                [1] = {
                    ["value"] = {
                        ["custom"] = "-- Send event for aura \"BigIcon when targeted\"\nWeakAuras.ScanEvents(\n    \"SHOW_BIGICON\",\n    aura_env.state.icon,\n    aura_env.state.spellId,\n    aura_env.state.duration\n)",
                    },
                    ["property"] = "customcode",
                },
            },
        },
    },
    ["cooldown"] = true,
  }

  WeakAurasSaved["displays"]["Sound when targeted by a boss 3"] = {
    ["glow"] = false,
    ["text1FontSize"] = 12,
    ["authorOptions"] = {
    },
    ["displayText"] = " ",
    ["yOffset"] = -40,
    ["anchorPoint"] = "TOP",
    ["url"] = "https://wago.io/BFADungeonTargetedSpells/61",
    ["icon"] = true,
    ["text2Font"] = "Friz Quadrata TT",
    ["keepAspectRatio"] = false,
    ["selfPoint"] = "TOP",
    ["desaturate"] = false,
    ["glowColor"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["text1Point"] = "BOTTOMRIGHT",
    ["text2FontFlags"] = "OUTLINE",
    ["load"] = {
        ["class"] = {
            ["multi"] = {
            },
        },
        ["talent2"] = {
            ["multi"] = {
            },
        },
        ["affixes"] = {
            ["multi"] = {
            },
        },
        ["talent"] = {
            ["multi"] = {
            },
        },
        ["use_role"] = false,
        ["spec"] = {
            ["multi"] = {
            },
        },
        ["ingroup"] = {
            ["multi"] = {
            },
        },
        ["use_encounter"] = true,
        ["difficulty"] = {
            ["multi"] = {
            },
        },
        ["race"] = {
            ["multi"] = {
            },
        },
        ["talent3"] = {
            ["multi"] = {
            },
        },
        ["faction"] = {
            ["multi"] = {
            },
        },
        ["role"] = {
            ["multi"] = {
                ["HEALER"] = true,
                ["DAMAGER"] = true,
            },
        },
        ["pvptalent"] = {
            ["multi"] = {
            },
        },
        ["use_never"] = false,
        ["size"] = {
            ["multi"] = {
            },
        },
    },
    ["glowType"] = "buttonOverlay",
    ["shadowXOffset"] = 1,
    ["text1FontFlags"] = "OUTLINE",
    ["regionType"] = "text",
    ["text2FontSize"] = 24,
    ["cooldownTextDisabled"] = false,
    ["auto"] = false,
    ["tocversion"] = 80205,
    ["text2Enabled"] = false,
    ["uid"] = "znq3Rbd4TXr",
    ["displayIcon"] = 134427,
    ["outline"] = "OUTLINE",
    ["color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["customText"] = "\n-- Do not remove this comment, it is part of this trigger: Sound when a boss target you",
    ["shadowYOffset"] = -1,
    ["cooldownSwipe"] = true,
    ["customTextUpdate"] = "update",
    ["automaticWidth"] = "Auto",
    ["triggers"] = {
        [1] = {
            ["trigger"] = {
                ["use_status"] = true,
                ["duration"] = "0.1",
                ["genericShowOn"] = "showOnActive",
                ["subeventPrefix"] = "SPELL",
                ["status"] = 2,
                ["debuffType"] = "HELPFUL",
                ["type"] = "custom",
                ["names"] = {
                },
                ["subeventSuffix"] = "_CAST_START",
                ["use_unit"] = true,
                ["event"] = "Threat Situation",
                ["threatUnit"] = "target",
                ["custom_type"] = "stateupdate",
                ["use_threatUnit"] = true,
                ["spellIds"] = {
                },
                ["custom"] = "function(states, event, unit)\n    if unit == \"boss1\"\n    or unit == \"boss2\"\n    or unit == \"boss3\"\n    or unit == \"boss4\"\n    or unit == \"boss5\"\n    then\n        if UnitIsUnit(\"player\", unit..\"target\") then\n            states[\"\"] = {\n                show = true,\n                changed = true,\n            }\n        else\n            local state = states[\"\"]\n            if state then\n                state.show = false\n                state.changed = true \n            end\n        end\n    end\n    return true\nend",
                ["events"] = "UNIT_TARGET:boss",
                ["check"] = "event",
                ["unit"] = "player",
                ["unevent"] = "auto",
                ["custom_hide"] = "custom",
            },
            ["untrigger"] = {
                ["threatUnit"] = "target",
                ["custom"] = "function(event, unit)\n    if aura_env.unit and aura_env.unit == unit then\n        return not UnitIsUnit(\"player\", unit..\"target\")\n    end    \nend",
            },
        },
        ["activeTriggerMode"] = 1,
    },
    ["internalVersion"] = 29,
    ["animation"] = {
        ["start"] = {
            ["type"] = "none",
            ["easeType"] = "none",
            ["preset"] = "spiral",
            ["easeStrength"] = 3,
            ["duration_type"] = "seconds",
        },
        ["main"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["finish"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
    },
    ["stickyDuration"] = false,
    ["preferToUpdate"] = false,
    ["version"] = 61,
    ["height"] = 102.63238525391,
    ["parent"] = "Dungeon - Targeted Spells",
    ["fontSize"] = 12,
    ["text2Containment"] = "INSIDE",
    ["text1Color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["text1Font"] = "Friz Quadrata TT",
    ["config"] = {
    },
    ["text2Color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["anchorFrameFrame"] = "UIParent",
    ["fixedWidth"] = 200,
    ["alpha"] = 1,
    ["anchorFrameParent"] = false,
    ["text2Point"] = "CENTER",
    ["zoom"] = 0,
    ["font"] = "Friz Quadrata TT",
    ["text1"] = " ",
    ["actions"] = {
        ["start"] = {
            ["message"] = "Saw Dingo is forever.",
            ["do_sound"] = true,
            ["message_type"] = "SAY",
            ["sound"] = "Interface\AddOns\WeakAuras\Media\Sounds\RobotBlip.ogg",
            ["do_message"] = false,
        },
        ["init"] = {
            ["custom"] = "\n-- Do not remove this comment, it is part of this trigger: Sound when a boss target you",
        },
        ["finish"] = {
        },
    },
    ["useglowColor"] = false,
    ["width"] = 100.44454956055,
    ["semver"] = "2.5.6",
    ["text1Enabled"] = true,
    ["id"] = "Sound when targeted by a boss 3",
    ["wordWrap"] = "WordWrap",
    ["frameStrata"] = 1,
    ["anchorFrameType"] = "SELECTFRAME",
    ["text2"] = "%p",
    ["text1Containment"] = "INSIDE",
    ["inverse"] = false,
    ["justify"] = "LEFT",
    ["shadowColor"] = {
        [1] = 0,
        [2] = 0,
        [3] = 0,
        [4] = 1,
    },
    ["conditions"] = {
    },
    ["cooldownEdge"] = false,
    ["xOffset"] = 0,
  }

  WeakAurasSaved["displays"]["Cooldowns on Raidframe"] = {
    ["authorOptions"] = {
    },
    ["preferToUpdate"] = false,
    ["customText"] = "\n\n",
    ["yOffset"] = 229.00018310547,
    ["anchorPoint"] = "CENTER",
    ["cooldownSwipe"] = true,
    ["customTextUpdate"] = "event",
    ["cooldownEdge"] = false,
    ["icon"] = true,
    ["triggers"] = {
        [1] = {
            ["trigger"] = {
                ["groupclone"] = true,
                ["duration"] = "1",
                ["genericShowOn"] = "showOnActive",
                ["subeventPrefix"] = "SPELL",
                ["debuffType"] = "HELPFUL",
                ["spellName"] = "Anti-Magic Shield",
                ["use_destFlags"] = true,
                ["use_cloneId"] = false,
                ["custom_hide"] = "timed",
                ["names"] = {
                    [1] = "Anti-Magic Shell",
                    [2] = "Rune Tap",
                    [3] = "Icebound Fortitude",
                    [4] = "Blur",
                    [5] = "Netherwalk",
                },
                ["type"] = "custom",
                ["destFlags"] = "InGroup",
                ["subeventSuffix"] = "_AURA_APPLIED",
                ["custom_type"] = "stateupdate",
                ["custom"] = "function(allstates, event,...)\n    if type(allstates) ~= \"table\" then\n        return\n    end\n    \n    local _,subevent = ...\n    \n    --Active Mitigation fix (there is no refresh event)\n    if subevent == \"SPELL_CAST_SUCCESS\" then\n        local spellId = select(12,...)\n        local destName = select(5,...)\n        if not destName then return end\n        destName = gsub(destName, \"%-[^|]+\", \"\") --strip realm name\n        if IsInGroup() and aura_env.activeMitigation[spellId] then\n            local unit            \n            for u in aura_env.GroupMembers() do\n                if UnitName(u) == destName then unit = u end\n            end\n            if not unit then return end\n            spellId = aura_env.activeMitigation[spellId]\n            for _, state in pairs(allstates) do\n                if state.spellId == spellId and state.name == destName then\n                    local spellName = GetSpellInfo(spellId)\n                    local _,icon,count,_,duration,expires,_,_,_,id = WA_GetUnitBuff(state.unit,spellName)                    \n                    state.show = true;\n                    state.changed = true;\n                    state.name = destName\n                    state.progressType = 'timed'\n                    state.autoHide = true\n                    state.changed = true\n                    state.spellId = spellId\n                    state.icon = icon\n                    state.duration = duration\n                    state.expirationTime = expires\n                    state.unit = state.unit\n                    return true\n                end\n            end\n            return true\n        end\n    end\n    \n    if subevent == \"SPELL_AURA_APPLIED\" then \n        local destName = select(9,...)\n        local destGUID = select(8,...)\n        destName = gsub(destName, \"%-[^|]+\", \"\") --strip realm name\n        local spellId = select(12,...)\n        if aura_env.trackedSpells[spellId] and IsInGroup() then\n            --touch of karma fix\n            if spellId == 122470 then\n                local sourceName = select(5,...)\n                sourceName =  gsub(sourceName, \"%-[^|]+\", \"\")\n                if sourceName~=destName then return end\n            end\n            \n            local unit            \n            for u in aura_env.GroupMembers() do\n                if UnitName(u) == destName then unit = u end\n            end\n            if not unit then return end\n            local spellName = GetSpellInfo(spellId)\n            local _,icon,count,_,duration,expires,_,_,_,id = WA_GetUnitBuff(unit,spellName) \n            \n            allstates[destGUID..spellId] = allstates[destGUID..spellId] or {}\n            local state = allstates[destGUID..spellId]\n            \n            state.ID = destGUID..spellId\n            state.show = true;\n            state.changed = true;\n            state.name = destName\n            state.progressType = 'timed'\n            state.autoHide = true\n            state.changed = true\n            state.spellId = spellId\n            state.icon = icon\n            state.duration = duration\n            state.expirationTime = expires\n            state.unit = unit\n            return true\n        end             \n    elseif subevent == \"SPELL_AURA_REFRESH\" then\n        local destName = select(9,...)\n        destName = gsub(destName, \"%-[^|]+\", \"\") --strip realm name\n        local spellId = select(12,...)\n        if aura_env.trackedSpells[spellId] and IsInGroup() then\n            for _, state in pairs(allstates) do\n                if state.spellId == spellId and state.name == destName then\n                    local spellName = GetSpellInfo(spellId)\n                    local _,icon,count,_,duration,expires,_,_,_,id = WA_GetUnitBuff(state.unit,spellName)                    \n                    state.show = true;\n                    state.changed = true;\n                    state.name = destName\n                    state.progressType = 'timed'\n                    state.autoHide = true\n                    state.changed = true\n                    state.spellId = spellId\n                    state.icon = icon\n                    state.duration = duration\n                    state.expirationTime = expires\n                end\n            end\n            return true\n        end        \n    elseif subevent == \"SPELL_AURA_REMOVED\" then\n        local destName = select(9,...)\n        destName = gsub(destName, \"%-[^|]+\", \"\") --strip realm name\n        local spellId = select(12,...)\n        if aura_env.trackedSpells[spellId] and IsInGroup() then\n            local unit\n            for u in aura_env.GroupMembers() do\n                if UnitName(u) == destName then unit = u end\n            end\n            if not unit then return end\n            for k,v in pairs(allstates) do\n                if spellId == v.spellId and unit == v.unit then\n                    v.show = false\n                    v.changed = true\n                end                \n            end\n            return true\n        end        \n    end\n    \n    if event==\"GROUP_ROSTER_UPDATE\" or event ==\"WA_CD_RAIDFRAMES_UPDATE\" then\n        aura_env.updateFrames(allstates)\n        return true\n    end\n    \nend",
                ["event"] = "Combat Log",
                ["events"] = "COMBAT_LOG_EVENT_UNFILTERED,GROUP_ROSTER_UPDATE WA_CD_RAIDFRAMES_UPDATE",
                ["use_spellId"] = false,
                ["use_spellName"] = false,
                ["spellIds"] = {
                },
                ["name"] = "Anti-Magic Shell",
                ["check"] = "event",
                ["unit"] = "group",
                ["unevent"] = "timed",
                ["use_specific_unit"] = false,
            },
            ["untrigger"] = {
            },
        },
        ["disjunctive"] = "any",
        ["customTriggerLogic"] = "\n\n\n",
        ["activeTriggerMode"] = 1,
    },
    ["internalVersion"] = 29,
    ["keepAspectRatio"] = false,
    ["animation"] = {
        ["start"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["main"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["finish"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
    },
    ["actions"] = {
        ["start"] = {
            ["message"] = "",
            ["custom"] = "WeakAuras.ScanEvents(\"WA_CD_RAIDFRAMES_UPDATE\")",
            ["do_sound"] = false,
            ["message_type"] = "COMBAT",
            ["do_message"] = false,
            ["do_custom"] = true,
        },
        ["init"] = {
            ["custom"] = "--CHANGE HERE--\naura_env.maxAuraCount = 3\naura_env.xOffset = 0\naura_env.yOffset = 0\naura_env.spacing = 5\naura_env.position = \"CENTER\" -- TOPLEFT,TOP,TOPRIGHT,RIGHT,BOTTOMRIGHT,BOTTOM,BOTTOMLEFT,LEFT,CENTER\naura_env.growdirection = \"RIGHT\" -- LEFT,TOP,RIGHT,BOTTOM,HORIZONTAL,VERTICAL (use last 2 with CENTER)\n---------------\n\n\naura_env.auraCount = {}\n\naura_env.activeMitigation = { --Spell - Buff\n    [203720] = 203819, --Demon Spikes\n    [53600]  = 132403, --SotR\n    [192081] = 192081, --Ironfur \n    [2565]   = 132404, --Shield Block    \n    [115308] = 215479, --Ironskin Brew\n}\naura_env.trackedSpells = {\n    --Death Knight\n    [194679] = true, --Rune Tap\n    [48707]  = true, --Anti-Magic Shell\n    [55233]  = true, --Vampiric Blood\n    [48792]  = true, --Icebound Fortitude\n    [81256]  = true, --Dancing Rune Weapon\n    [194844] = true, --Bonestorm\n    --Demon Hunter\n    [212800] = true, --Blur\n    [196555] = true, --Nether Walk\n    [187827] = true, --Metamorphosis (Tank)\n    [203819] = true, --Demon Spikes\n    --Warlock\n    [104773] = true, --Unending Resolve\n    [108416] = true, --Dark Pact\n    [132413] = true, --Shadow Bulwark\n    --Hunter\n    [186265] = true, --Aspect of the Turtle\n    [264735] = true, --Survival of the Fittest (Command Pet)\n    [281195] = true, --Survival of the Fittest (Lone Wolf)\n    --Rogue\n    [1966]   = true, --Feint\n    [31224]  = true, --Cloak of Shadows\n    [5277]   = true, --Evasion\n    [199754] = true, --Riposte\n    [45182]  = true, --Cheating Death\n    --Paladin\n    [498]    = true, --Divine Protection\n    [204018] = true, --Blessing of Spellwarding\n    [6940]   = true, --Blessing of Sacrifice\n    [1022]   = true, --Blessing of Protection\n    [642]    = true, --Divine Shield\n    [86659]  = true, --Guardian of the Ancient Kings\n    [31850]  = true, --Ardent Defender\n    [132403] = true, --SotR\n    [184662] = true, --Shield of Vengeance\n    [205191] = true, --Eye for an Eye\n    --Monk\n    [122470] = true, --Touch of Karma\n    [122783] = true, --Diffuse Magic\n    [122278] = true, --Dampen Harm\n    [243435] = true, --Fortifying Brew (Heal)\n    [120954] = true, --Fortifying Brew (Tank)\n    [115176] = true, --Zen Meditation\n    [215479] = true, --Ironskin Brew\n    [115295] = true, --Guard\n    [116849] = true, --Life Cocoon\n    --Mage\n    [45438]  = true, --Ice Block\n    --[235313] = true, --Blazing Barrier\n    --[235450] = true, --Prismatic Barrier\n    --[11426]  = true, --Ice Barrier\n    --Druid\n    [61336]  = true, --Survival Instincts\n    [22812]  = true, --Barkskin\n    [102342] = true, --Ironbark\n    [192081] = true, --Ironfur\n    [158792] = true, --Pulverize\n    [102558] = true, --Incarnation: Guardian of Ursoc\n    --Warrior\n    [197690] = true, --Defensive Stance\n    [118038] = true, --Die by the Sword\n    [184364] = true, --Enraged Regeneration\n    [871]    = true, --Shield Wall\n    [23920]  = true, --Spell Reflection\n    [132404] = true, --Shield Block\n    [97463]  = true, --Rallying Cry\n    [12975] = true,  --Last Stand\n    [190456] = true, --Ignore Pain\n    --Priest\n    [47788]  = true, --Guardian Spirit\n    [33206]  = true, --Pain Suppression\n    [47585]  = true, --Dispersion\n    [19236]  = true, --Desperate Prayer\n    --Shaman\n    [108271] = true, --Astral Shift\n}\n\n\n\n--https://wago.io/profile/asakawa\nfunction aura_env.GroupMembers(reversed, forceParty)\n    local unit  = (not forceParty and IsInRaid()) and 'raid' or 'party'\n    local numGroupMembers = (forceParty and GetNumSubgroupMembers()  or GetNumGroupMembers()) - (unit == \"party\" and 1 or 0)\n    local i = reversed and numGroupMembers or (unit == 'party' and 0 or 1)\n    return function()\n        local ret\n        if i == 0 and unit == 'party' then\n            ret = 'player'\n        elseif i <= numGroupMembers and i > 0 then\n            ret = unit .. i\n        end\n        i = i + (reversed and -1 or 1)\n        return ret\n    end\nend\n\n-- credit to rivers for writing the function\n-- Send this function a group/raid member's unitID or GUID and it will return their raid frame.\nfunction aura_env.GetFrame(target)\n    if not UnitExists(target) then\n        if type(target) == \"string\" and target:find(\"Player\") then\n            target = select(6,GetPlayerInfoByGUID(target))\n        else\n            return \n        end\n    end \n    --PitBull4\n    if IsAddOnLoaded(\"PitBull4\") then\n        if IsInGroup() and not IsInRaid() then\n            for _,frame in pairs(PitBull4.all_frames_list) do\n                if frame.classification == \"Party\" and frame.unit then\n                    return frame                \n                end\n            end\n        end\n    end\n    --ShadowUF\n    if IsAddOnLoaded(\"ShadowedUnitFrames\") then\n        local unittype = nil\n        local path = nil\n        if (IsInRaid() and ShadowUF.enabledUnits.raid)\n        or (not IsInRaid() and IsInGroup() and ShadowUF.db.profile.units.raid.showParty and ShadowUF.enabledUnits.raid) -- show party as raid\n        then\n            unittype = \"raid\"\n            path = ShadowUF.Units.headerFrames.raid\n        else \n            if IsInGroup() and not IsInRaid() and ShadowUF.enabledUnits.party then\n                unittype = \"party\"\n                path = ShadowUF.Units.headerFrames.party\n            end\n        end\n        \n        if unittype and path then\n            for _,frame in pairs(path) do\n                if type(frame) == \"table\" \n                and frame.unitType == unittype\n                and frame:IsVisible() \n                and frame.unit \n                and UnitIsUnit(frame.unit, target)\n                then\n                    return frame\n                end\n            end\n        end\n    end\n    -- CompactRaid\n    if IsAddOnLoaded(\"CompactRaid\") then\n        return CompactRaid:FindUnitFrame(target)\n    end\n    -- Healbot\n    if IsAddOnLoaded(\"HealBot\") then\n        for _,frame in pairs(HealBot_Unit_Button) do\n            if UnitIsUnit(frame.unit, target) then\n                return frame\n            end\n        end\n    end\n    --Vuhdo\n    if IsAddOnLoaded(\"VuhDo\") and VUHDO_CONFIG[\"SHOW_PANELS\"] then\n        for _, v in pairs(VUHDO_UNIT_BUTTONS) do\n            if v[1].raidid and UnitIsUnit(v[1].raidid, target) then\n                return v[1]\n            end \n        end\n    end\n    if IsAddOnLoaded(\"Grid\") then\n        for _, frame in pairs(Grid.modules.GridFrame.registeredFrames) do\n            if frame:IsVisible() then\n                if frame.unit and UnitIsUnit(frame.unit, target) then\n                    return frame\n                end\n            end\n        end\n    end\n    --Grid2\n    if IsAddOnLoaded(\"Grid2\") then\n        for _, frame in pairs(Grid2Frame.registeredFrames) do\n            if frame:IsVisible() then\n                if frame.unit and UnitIsUnit(frame.unit, target) then\n                    return frame\n                end\n            end\n        end\n    end\n    -- ElvUI\n    if ElvUF then\n        for _,frame in pairs(ElvUF.objects) do\n            if (frame.unitframeType == \"raid\" \n                or frame.unitframeType == \"party\" \n                or frame.unitframeType == \"raid40\"\n            )\n            and frame:IsVisible() and frame.unit and UnitIsUnit(frame.unit, target)\n            then\n                return frame.Health\n            end\n        end\n    end\n    -- bdGrid\n    if IsAddOnLoaded(\"bdGrid\") then\n        for _,frame in pairs(oUF_bdGridRaid) do\n            if UnitIsUnit(frame.unit, target) then\n                return frame\n            end\n        end\n    end\n    --Lastly, default frames\n    if IsInRaid() then \n        if CompactRaidFrameContainer.groupMode == \"flush\" then\n            return CompactRaidFrameContainer_GetUnitFrame(CompactRaidFrameContainer, target, \"raid\")\n        else\n            for i = 1,8 do\n                for j = 1,5 do\n                    local frame = _G[\"CompactRaidGroup\"..i..\"Member\"..j]\n                    if frame\n                    and frame:IsVisible()\n                    and frame.unit\n                    and UnitIsUnit(frame.unit, target)\n                    then\n                        return frame\n                    end\n                end\n            end\n        end\n    elseif IsInGroup() and GetCVarBool(\"useCompactPartyFrames\") then\n        if CompactRaidFrameContainer.groupMode == \"flush\" then\n            return CompactRaidFrameContainer_GetUnitFrame(CompactRaidFrameContainer, target, \"raid\")\n        else\n            for i = 1,5 do\n                local frame = _G[\"CompactPartyFrameMember\"..i]\n                if frame\n                and frame:IsVisible()\n                and frame.unit\n                and UnitIsUnit(frame.unit, target)\n                then\n                    return frame\n                end\n            end\n        end\n    else \n        -- Player Frame\n        local frame = PlayerFrame\n        if frame \n        and frame:IsVisible() \n        and frame.unit \n        and UnitIsUnit(frame.unit, target) then\n            return frame\n        end\n        -- Default party frames\n        for i = 1,4 do\n            local frame = _G[\"PartyMemberFrame\"..i]\n            if frame\n            and frame:IsVisible()\n            and frame.unit\n            and UnitIsUnit(frame.unit, target)\n            then\n                return frame\n            end\n        end\n    end\nend\n\nfunction aura_env.updateFrames(allstates)\n    table.wipe(aura_env.auraCount)\n    for _, v in pairs(allstates) do        \n        local unit            \n        for u in aura_env.GroupMembers() do\n            if UnitName(u) == v.name then unit = u end\n        end\n        if not unit then \n            v.show = false\n            v.changed = true\n        else\n            v.unit = unit\n            local region = WeakAuras.GetRegion(aura_env.id, v.ID)\n            local f = aura_env.GetFrame(v.unit)\n            if f and region and region:IsVisible() then\n                aura_env.auraCount[v.unit] = aura_env.auraCount[v.unit] or 0                    \n                local order = aura_env.auraCount[v.unit]\n                local xoffset, yoffset = 0, 0\n                local height,width = region:GetHeight()+aura_env.spacing, region:GetWidth()+aura_env.spacing\n                if aura_env.growdirection == \"TOP\" then\n                    yoffset = (order) * height\n                elseif aura_env.growdirection == \"BOTTOM\" then\n                    yoffset = - (order) * height\n                elseif aura_env.growdirection == \"RIGHT\" then\n                    xoffset = (order) * width\n                elseif aura_env.growdirection == \"LEFT\" then\n                    xoffset = - (order) * width\n                elseif aura_env.growdirection == \"HORIZONTAL\" then\n                    xoffset = (-((order) * width / 2)) + ((order - 1) * width)\n                elseif aura_env.growdirection == \"VERTICAL\" then\n                    xoffset = (-((order) * width / 2)) + ((order - 1) * width)\n                end                    \n                if aura_env.auraCount[v.unit]+1 > aura_env.maxAuraCount then\n                    xoffset = -3000\n                end\n                --region:ClearAllPoints()\n                --region:SetPoint(aura_env.position,f,aura_env.position,xoffset+aura_env.xOffset,yoffset+aura_env.yOffset)\n                region:SetAnchor(aura_env.position, f, aura_env.position) \n                region:SetOffset(xoffset+aura_env.xOffset, yoffset+aura_env.yOffset)\n                aura_env.auraCount[v.unit] = aura_env.auraCount[v.unit] + 1 \n            end            \n        end         \n    end    \nend",
            ["do_custom"] = true,
        },
        ["finish"] = {
            ["b"] = 1,
            ["message_type"] = "COMBAT",
            ["do_message"] = false,
            ["message"] = "",
            ["custom"] = "C_Timer.After(0.05,function() \n        WeakAuras.ScanEvents(\"WA_CD_RAIDFRAMES_UPDATE\") \nend)",
            ["g"] = 1,
            ["stop_sound"] = false,
            ["do_custom"] = true,
            ["r"] = 1,
        },
    },
    ["desaturate"] = false,
    ["alpha"] = 1,
    ["version"] = 22,
    ["subRegions"] = {
        [1] = {
            ["text_shadowXOffset"] = 0,
            ["text_text"] = "%c",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "AUTO",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_color"] = {
                [1] = 1,
                [2] = 1,
                [3] = 1,
                [4] = 1,
            },
            ["text_font"] = "Friz Quadrata TT",
            ["text_shadowYOffset"] = 0,
            ["text_wordWrap"] = "WordWrap",
            ["text_fontType"] = "OUTLINE",
            ["text_anchorPoint"] = "INNER_BOTTOMRIGHT",
            ["text_fontSize"] = 12,
            ["anchorXOffset"] = 0,
            ["text_visible"] = false,
        },
    },
    ["height"] = 20,
    ["url"] = "https://wago.io/By6zywh9-/22",
    ["selfPoint"] = "CENTER",
    ["load"] = {
        ["talent2"] = {
            ["multi"] = {
            },
        },
        ["use_never"] = false,
        ["talent"] = {
            ["multi"] = {
            },
        },
        ["spec"] = {
            ["multi"] = {
            },
        },
        ["class"] = {
            ["multi"] = {
            },
        },
        ["difficulty"] = {
            ["multi"] = {
            },
        },
        ["race"] = {
            ["multi"] = {
            },
        },
        ["ingroup"] = {
            ["multi"] = {
                ["group"] = true,
                ["raid"] = true,
            },
        },
        ["faction"] = {
            ["multi"] = {
            },
        },
        ["use_ingroup"] = false,
        ["role"] = {
            ["multi"] = {
            },
        },
        ["pvptalent"] = {
            ["multi"] = {
            },
        },
        ["size"] = {
            ["multi"] = {
            },
        },
    },
    ["stickyDuration"] = false,
    ["regionType"] = "icon",
    ["uid"] = "uNAfNURagDA",
    ["color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["width"] = 20,
    ["semver"] = "1.0.1",
    ["zoom"] = 0,
    ["auto"] = true,
    ["tocversion"] = 80205,
    ["id"] = "Cooldowns on Raidframe",
    ["cooldownTextDisabled"] = true,
    ["frameStrata"] = 1,
    ["anchorFrameType"] = "SCREEN",
    ["config"] = {
    },
    ["inverse"] = false,
    ["xOffset"] = -3000,
    ["conditions"] = {
    },
    ["cooldown"] = true,
  }

  --You probably don't want to touch this one
  WeakAurasSaved["displays"]["ZenTracker (ZT) Main"] = {
			["outline"] = "OUTLINE",
			["xOffset"] = 0,
			["displayText"] = " ",
			["yOffset"] = 0,
			["anchorPoint"] = "CENTER",
			["customTextUpdate"] = "update",
			["url"] = "https://wago.io/r14U746B7/66",
			["actions"] = {
				["start"] = {
					["do_message"] = false,
					["custom"] = "",
					["do_custom"] = false,
				},
				["finish"] = {
					["custom"] = "",
					["do_custom"] = false,
				},
				["init"] = {
					["custom"] = "--------------------------------------------------------------------------------\n-- CONFIGURATION\n--\n-- The configuration options have moved to the \"Author Options\" tab as of\n-- WeakAuras Version 2.10. \n--\n-- DO NOT EDIT THIS CODE!\n--------------------------------------------------------------------------------\nlocal ZT = aura_env\n\n-- Local versions of commonly used functions\nlocal ipairs = ipairs\nlocal pairs = pairs\nlocal print = print\nlocal select = select\nlocal tonumber = tonumber\n\nlocal IsInGroup = IsInGroup\nlocal IsInRaid = IsInRaid\nlocal UnitGUID = UnitGUID\n\n-- Turns on/off debugging messages\nlocal DEBUG_EVENT = { isEnabled = false, color = \"FF2281F4\" }\nlocal DEBUG_MESSAGE = { isEnabled = false, color = \"FF11D825\" }\nlocal DEBUG_TIMER = { isEnabled = false, color = \"FFF96D27\" }\nlocal DEBUG_TRACKING = { isEnabled = false, color = \"FFA53BF7\" }\n\n-- Turns on/off testing of combatlog-based tracking for the player\n-- (Note: This will disable sharing of player CD updates over addon messages)\nlocal TEST_CLEU = false\n\nlocal function prdebug(type, ...)\n    if type.isEnabled then\n        print(\"|c\"..type.color..\"[ZT-Debug]\", ...)\n    end\nend\n\nlocal function prerror(...)\n    print(\"|cFFFF0000[ZT-Error]\", ...)\nend\n\nlocal function Table_Create(values)\n    if not values then\n        return {}\n    elseif not values[1] then\n        return { values }\n    end\n    return values\nend\n\nlocal function DefaultTable_Create(genDefaultFunc)\n    local table = {}\n    local metatable = {}\n    metatable.__index = function(table, key)\n        local value = genDefaultFunc()\n        rawset(table, key, value)\n        return value\n    end\n\n    return setmetatable(table, metatable)\nend\n\nlocal function Map_FromTable(table)\n    local map = {}\n    for _,value in ipairs(table) do\n        map[value] = true\n    end\n    return map\nend\n\n-- TODOs\n--\n-- 1) Fix the registration to allow for spellIDs or types\n-- 2) Track front-end registration at the level of spellIDs\n-- 3) Change spell list to allow for multiple types per spell (introduce RAIDCD)\n\n--##############################################################################\n-- Class and Spec Information\n\nlocal DH = {ID=12, name=\"DEMONHUNTER\", Havoc=577, Veng=581}\nlocal DK = {ID=6, name=\"DEATHKNIGHT\", Blood=250, Frost=251, Unholy=252}\nlocal Druid = {ID=11, name=\"DRUID\", Balance=102, Feral=103, Guardian=104, Resto=105}\nlocal Hunter = {ID=3, name=\"HUNTER\", BM=253, MM=254, SV=255}\nlocal Mage = {ID=8, name=\"MAGE\", Arcane=62, Fire=63, Frost=64}\nlocal Monk = {ID=10, name=\"MONK\", BRM=268, WW=269, MW=270}\nlocal Paladin = {ID=2, name=\"PALADIN\", Holy=65, Prot=66, Ret=70}\nlocal Priest = {ID=5, name=\"PRIEST\", Disc=256, Holy=257, Shadow=258}\nlocal Rogue = {ID=4, name=\"ROGUE\", Sin=259, Outlaw=260, Sub=261}\nlocal Shaman = {ID=7, name=\"SHAMAN\", Ele=262, Enh=263, Resto=264}\nlocal Warlock = {ID=9, name=\"WARLOCK\", Affl=265, Demo=266, Destro=267}\nlocal Warrior = {ID=1, name=\"WARRIOR\", Arms=71, Fury=72, Prot=73}\n\nlocal AllClasses = {\n    [DH.name] = DH, [DK.name] = DK, [Druid.name] = Druid, [Hunter.name] = Hunter,\n    [Mage.name] = Mage, [Monk.name] = Monk, [Paladin.name] = Paladin,\n    [Priest.name] = Priest, [Rogue.name] = Rogue, [Shaman.name] = Shaman,\n    [Warlock.name] = Warlock, [Warrior.name] = Warrior\n}\n\n--##############################################################################\n-- Spell Cooldown Modifiers\n\nlocal function StaticMod(type, value)\n    return { type = \"Static\", [type] = value }\nend\n\nlocal function DynamicMod(handlers)\n    if not handlers[1] then\n        handlers = { handlers }\n    end\n    \n    return { type = \"Dynamic\", handlers = handlers }\nend\n\nlocal function EventDeltaMod(type, spellID, delta)\n    return DynamicMod({\n            type = type,\n            spellID = spellID,\n            handler = function(watchInfo)\n                watchInfo:updateCDDelta(delta)\n            end\n    })\nend\n\nlocal function CastDeltaMod(spellID, delta)\n    return EventDeltaMod(\"SPELL_CAST_SUCCESS\", spellID, delta)\nend\n\nlocal function EventRemainingMod(type, spellID, remaining)\n    return DynamicMod({\n            type = type,\n            spellID = spellID,\n            handler = function(watchInfo)\n                watchInfo:updateCDRemaining(remaining)\n            end\n    })\nend\n\nlocal function CastRemainingMod(spellID, remaining)\n    return EventRemainingMod(\"SPELL_CAST_SUCCESS\", spellID, remaining)\nend\n\n-- Shockwave: If 3+ targets hit then reduces by 15 seconds\nlocal modShockwave = DynamicMod({\n        {\n            type = \"SPELL_CAST_SUCCESS\", spellID = 46968,\n            handler = function(watchInfo)\n                watchInfo.numHits = 0\n            end\n        },\n        {\n            type = \"SPELL_AURA_APPLIED\", spellID = 132168,\n            handler = function(watchInfo)\n                watchInfo.numHits = watchInfo.numHits + 1\n                if watchInfo.numHits == 3 then\n                    watchInfo:updateCDDelta(-15)\n                end\n            end\n        }\n})\n\n-- Capacitor Totem: Each target hit reduces by 5 seconds (up to 4 targets hit)\nlocal modCapTotem = DynamicMod({\n        type = \"SPELL_SUMMON\", spellID = 192058,\n        handler = function(watchInfo)\n            watchInfo.numHits = 0\n            \n            if not watchInfo.totemHandler then\n                watchInfo.totemHandler = function(watchInfo)\n                    watchInfo.numHits = watchInfo.numHits + 1\n                    if watchInfo.numHits <= 4 then\n                        watchInfo:updateCDDelta(-5)\n                    end\n                end\n            end\n            \n            if watchInfo.totemGUID then\n                ZT.eventHandlers:remove(\"SPELL_AURA_APPLIED\", 118905, watchInfo.totemGUID, watchInfo.totemHandler)\n            end\n            \n            watchInfo.totemGUID = select(8, CombatLogGetCurrentEventInfo())\n            ZT.eventHandlers:add(\"SPELL_AURA_APPLIED\", 118905, watchInfo.totemGUID, watchInfo.totemHandler, watchInfo)\n        end\n})\n\n\n-- Guardian Spirit: If expires watchInfothout healing then reset to 60 seconds\nlocal modGuardianSpirit = DynamicMod({\n        {\n            type = \"SPELL_HEAL\", spellID = 48153,\n            handler = function(watchInfo)\n                watchInfo.spiritHeal = true\n            end\n        },\n        {\n            type = \"SPELL_AURA_REMOVED\", spellID = 47788,\n            handler = function(watchInfo)\n                if not watchInfo.spiritHeal then\n                    watchInfo:updateCDRemaining(60)\n                end\n                watchInfo.spiritHeal = false\n            end\n        }\n})\n\n-- Dispels: Go on cooldown only if a debuff is dispelled\nlocal function DispelMod(spellID)\n    return DynamicMod({\n            type = \"SPELL_DISPEL\",\n            spellID = spellID,\n            handler = function(watchInfo)\n                watchInfo:updateCDRemaining(8)\n            end\n    })\nend\n\n-- Resource Spending: For every spender, reduce cooldown by (coefficient * cost) seconds\n--   Note: By default, I try to use minimum cost values as to not over-estimate the cooldown reduction\nlocal specIDToSpenderInfo = {\n    [DK.Blood] = {\n        [49998]  = 40, -- Death Strike (Assumes -5 from Ossuary)\n        [61999]  = 30, -- Raise Ally\n        [206940] = 30, -- Mark of Blood\n    },\n    [Warrior.Arms] = {\n        [845]    = 20, -- Cleave\n        [163201] = 20, -- Execute (Ignores Sudden Death)\n        [1715]   = 10, -- Hamstring\n        [202168] = 10, -- Impending Victory\n        [12294]  = 30, -- Moral Strike\n        [772]    = 30, -- Rend\n        [1464]   = 20, -- Slam\n        [1680]   = 30, -- Whirlwind\n    },\n    [Warrior.Fury] = {\n        [202168] = 10, -- Impending Victory\n        [184367] = 75, -- Rampage (Assumes -10 from Carnage)\n        [12323]  = 10, -- Piercing Howl\n    },\n    [Warrior.Prot] = {\n        [190456] = 40, -- Ignore Pain (Ignores Vengeance)\n        [202168] = 10, -- Impending Victory\n        [6572]   = 30, -- Revenge (Ignores Vengeance)\n        [2565]   = 30, -- Shield Block\n    }\n}\n\nlocal function ResourceSpendingMods(specID, coefficient)\n    local handlers = {}\n    local spenderInfo = specIDToSpenderInfo[specID]\n    \n    for spellID,cost in pairs(spenderInfo) do\n        local delta = -(coefficient * cost)\n        \n        handlers[#handlers+1] = {\n            type = \"SPELL_CAST_SUCCESS\",\n            spellID = spellID,\n            handler = function(watchInfo)\n                watchInfo:updateCDDelta(delta)\n            end\n        }\n    end\n    \n    return DynamicMod(handlers)\nend\n\n--##############################################################################\n-- List of Tracked Spells\n\nZT.spellsVersion = 8\nZT.spells = {\n    -- Interrupts\n    {type=\"INTERRUPT\", spellID=183752, class=DH, baseCD=15}, -- Disrupt\n    {type=\"INTERRUPT\", spellID=47528, class=DK, baseCD=15}, -- Mind Freeze\n    {type=\"INTERRUPT\", spellID=91802, specs={DK.Unholy}, baseCD=30}, -- Shambling Rush\n    {type=\"INTERRUPT\", spellID=78675, specs={Druid.Balance}, baseCD=60}, -- Solar Beam\n    {type=\"INTERRUPT\", spellID=106839, specs={Druid.Feral, Druid.Guardian}, baseCD=15}, -- Skull Bash\n    {type=\"INTERRUPT\", spellID=147362, specs={Hunter.BM, Hunter.MM}, baseCD=24}, -- Counter Shot\n    {type=\"INTERRUPT\", spellID=187707, specs={Hunter.SV}, baseCD=15}, -- Muzzle\n    {type=\"INTERRUPT\", spellID=2139, class=Mage, baseCD=24}, -- Counter Spell\n    {type=\"INTERRUPT\", spellID=116705, specs={Monk.WW, Monk.BRM}, baseCD=15}, -- Spear Hand Strike\n    {type=\"INTERRUPT\", spellID=96231, specs={Paladin.Prot, Paladin.Ret}, baseCD=15}, -- Rebuke\n    {type=\"INTERRUPT\", spellID=15487, specs={Priest.Shadow}, baseCD=45, modTalents={[41]=StaticMod(\"sub\", 15)}}, -- Silence\n    {type=\"INTERRUPT\", spellID=1766, class=Rogue, baseCD=15}, -- Kick\n    {type=\"INTERRUPT\", spellID=57994, class=Shaman, baseCD=12}, -- Wind Shear\n    {type=\"INTERRUPT\", spellID=19647, class=Warlock, baseCD=24}, -- Spell Lock\n    {type=\"INTERRUPT\", spellID=6552, class=Warrior, baseCD=15}, -- Pummel\n    -- Hard Crowd Control (AOE)\n    {type=\"HARDCC\", spellID=179057, specs={DH.Havoc}, baseCD=60, modTalents={[61]=StaticMod(\"mul\", 0.666667)}}, -- Chaos Nova\n    {type=\"HARDCC\", spellID=119381, class=Monk, baseCD=60, modTalents={[41]=StaticMod(\"sub\", 10)}}, -- Leg Sweep\n    {type=\"HARDCC\", spellID=192058, class=Shaman, baseCD=60, modTalents={[33]=modCapTotem}}, -- Capacitor Totem\n    {type=\"HARDCC\", spellID=30283, class=Warlock, baseCD=60, modTalents={[51]=StaticMod(\"sub\", 15)}}, -- Shadowfury\n    {type=\"HARDCC\", spellID=46968, specs={Warrior.Prot}, baseCD=40, modTalents={[52]=modShockwave}}, -- Shockwave\n    {type=\"HARDCC\", spellID=255654, race=\"HighmountainTauren\", baseCD=120}, -- Bull Rush\n    {type=\"HARDCC\", spellID=20549, race=\"Tauren\", baseCD=90}, -- War Stomp\n    -- Soft Crowd Control (AOE)\n    {type=\"SOFTCC\", spellID=202138, specs={DH.Veng}, baseCD=90, reqTalents={53}}, -- Sigil of Chains\n    {type=\"SOFTCC\", spellID=207684, specs={DH.Veng}, baseCD=90, modTalents={[52]=StaticMod(\"mul\", 0.8)}}, -- Sigil of Misery\n    {type=\"SOFTCC\", spellID=202137, specs={DH.Veng}, baseCD=60, modTalents={[52]=StaticMod(\"mul\", 0.8)}}, -- Sigil of Silence\n    {type=\"SOFTCC\", spellID=108199, specs={DK.Blood}, baseCD=120, modTalents={[52]=StaticMod(\"sub\", 30)}}, -- Gorefiend's Grasp\n    {type=\"SOFTCC\", spellID=207167, specs={DK.Frost}, baseCD=60, reqTalents={33}}, -- Blinding Sleet\n    {type=\"SOFTCC\", spellID=132469, class=Druid, baseCD=30, reqTalents={43}}, -- Typhoon\n    {type=\"SOFTCC\", spellID=102359, class=Druid, baseCD=30, reqTalents={42}}, -- Mass Entanglement\n    {type=\"SOFTCC\", spellID=99, specs={Druid.Guardian}, baseCD=30}, -- Incapacitating Roar\n    {type=\"SOFTCC\", spellID=102793, specs={Druid.Guardian}, baseCD=60, reqTalents={22}}, -- Ursol's Vortex\n    {type=\"SOFTCC\", spellID=102793, specs={Druid.Resto}, baseCD=60}, -- Ursol's Vortex\n    {type=\"SOFTCC\", spellID=109248, class=Hunter, baseCD=30, reqTalents={53}}, -- Binding Shot\n    {type=\"SOFTCC\", spellID=122, class=Mage, baseCD=30, reqTalents={51,53}, mods=CastRemainingMod(235219,0), version=6}, -- Frost Nova\n    {type=\"SOFTCC\", spellID=122, class=Mage, baseCD=30, charges=2, reqTalents={52}, mods=CastRemainingMod(235219,0), version=6}, -- Frost Nova\n    {type=\"SOFTCC\", spellID=113724, class=Mage, baseCD=30, reqTalents={53}, version=6}, -- Ring of Frost\n    {type=\"SOFTCC\", spellID=31661, specs={Mage.Fire}, baseCD=20, version=2}, -- Dragon's Breath\n    {type=\"SOFTCC\", spellID=33395, specs={Mage.Frost}, baseCD=25, reqTalents={11,13}, version=6}, -- Freeze (Pet)\n    {type=\"SOFTCC\", spellID=116844, class=Monk, baseCD=45, reqTalents={43}}, -- Ring of Peace\n    {type=\"SOFTCC\", spellID=115750, class=Paladin, baseCD=90, reqTalents={33}, version=3}, -- Blinding Light\n    {type=\"SOFTCC\", spellID=8122, specs={Priest.Disc, Priest.Holy}, baseCD=60, modTalents={[41]=StaticMod(\"sub\", 30)}}, -- Psychic Scream\n    {type=\"SOFTCC\", spellID=204263, specs={Priest.Disc, Priest.Holy}, baseCD=45, reqTalents={43}}, -- Shining Force\n    {type=\"SOFTCC\", spellID=8122, specs={Priest.Shadow}, baseCD=60}, -- Psychic Scream\n    {type=\"SOFTCC\", spellID=51490, specs={Shaman.Ele}, baseCD=45}, -- Thunderstorm\n    -- Hard Crowd Control (Single Target)\n    {type=\"STHARDCC\", spellID=211881, specs={DH.Havoc}, baseCD=30, reqTalents={63}}, -- Fel Eruption\n    {type=\"STHARDCC\", spellID=221562, specs={DK.Blood}, baseCD=45}, -- Asphyxiate\n    {type=\"STHARDCC\", spellID=108194, specs={DK.FrostDK}, baseCD=45, reqTalents={32}}, -- Asphyxiate\n    {type=\"STHARDCC\", spellID=108194, specs={DK.Unholy}, baseCD=45, reqTalents={33}}, -- Asphyxiate\n    {type=\"STHARDCC\", spellID=5211, class=Druid, baseCD=50, reqTalents={41}}, -- Mighty Bash\n    {type=\"STHARDCC\", spellID=19577, specs={Hunter.BM, Hunter.Surv}, baseCD=60}, -- Intimidation\n    {type=\"STHARDCC\", spellID=853, specs={Paladin.Holy}, baseCD=60, modTalents={[31]=CastDeltaMod(275773, -10)}}, -- Hammer of Justice\n    {type=\"STHARDCC\", spellID=853, specs={Paladin.Prot}, baseCD=60, modTalents={[31]=CastDeltaMod(275779, -6)}}, -- Hammer of Justice\n    {type=\"STHARDCC\", spellID=853, specs={Paladin.Ret}, baseCD=60}, -- Hammer of Justice\n    {type=\"STHARDCC\", spellID=88625, specs={Priest.Holy}, baseCD=60, reqTalents={42}, mods=CastDeltaMod(585, -4), modTalents={[71]=CastDeltaMod(585, -1.333333)}}, -- Holy Word: Chastise\n    {type=\"STHARDCC\", spellID=64044, specs={Priest.Shadow}, baseCD=45, reqTalents={43}}, -- Psychic Horror\n    {type=\"STHARDCC\", spellID=6789, class=Warlock, baseCD=45, reqTalents={52}}, -- Mortal Coil\n    {type=\"STHARDCC\", spellID=107570, specs={Warrior.Arms,Warrior.Fury}, baseCD=30, reqTalents={23}}, -- Storm Bolt\n    {type=\"STHARDCC\", spellID=107570, specs={Warrior.Prot}, baseCD=30, reqTalents={53}}, -- Storm Bolt\n    -- Soft Crowd Control (Single Target)\n    {type=\"STSOFTCC\", spellID=217832, class=DH, baseCD=45}, -- Imprison\n    {type=\"STSOFTCC\", spellID=49576, specs={DK.Blood}, baseCD=15, version=2}, -- Death Grip\n    {type=\"STSOFTCC\", spellID=49576, specs={DK.Frost, DK.Unholy}, baseCD=25, version=2}, -- Death Grip\n    {type=\"STSOFTCC\", spellID=2094, specs={Rogue.Outlaw}, baseCD=120, modTalents={[52]=StaticMod(\"sub\", 30)}}, -- Blind\n    {type=\"STSOFTCC\", spellID=2094, specs={Rogue.Sin, Rogue.Sub}, baseCD=120}, -- Blind\n    {type=\"STSOFTCC\", spellID=115078, class=Monk, baseCD=45}, -- Paralysis\n    {type=\"STSOFTCC\", spellID=187650, class=Hunter, baseCD=30}, -- Freezing Trap\n    {type=\"STSOFTCC\", spellID=107079, race=\"Pandaren\", baseCD=120, version=4}, -- Quaking Palm\n    -- Dispel (Offensive)\n    {type=\"DISPEL\", spellID=278326, class=DH, baseCD=10, version=6}, -- Disrupt\n    {type=\"DISPEL\", spellID=2908, class=Druid, baseCD=10, version=6}, -- Soothe\n    {type=\"DISPEL\", spellID=32375, class=Priest, baseCD=45}, -- Mass Dispel\n    {type=\"DISPEL\", spellID=202719, race=\"BloodElf\", class=DH, baseCD=120}, -- Arcane Torrent\n    {type=\"DISPEL\", spellID=50613, race=\"BloodElf\", class=DK, baseCD=120}, -- Arcane Torrent\n    {type=\"DISPEL\", spellID=80483, race=\"BloodElf\", class=Hunter, baseCD=120}, -- Arcane Torrent\n    {type=\"DISPEL\", spellID=28730, race=\"BloodElf\", class=Mage, baseCD=120}, -- Arcane Torrent\n    {type=\"DISPEL\", spellID=129597, race=\"BloodElf\", class=Monk, baseCD=120}, -- Arcane Torrent\n    {type=\"DISPEL\", spellID=155145, race=\"BloodElf\", class=Paladin, baseCD=120}, -- Arcane Torrent\n    {type=\"DISPEL\", spellID=232633, race=\"BloodElf\", class=Priest, baseCD=120}, -- Arcane Torrent\n    {type=\"DISPEL\", spellID=25046, race=\"BloodElf\", class=Rogue, baseCD=120}, -- Arcane Torrent\n    {type=\"DISPEL\", spellID=28730, race=\"BloodElf\", class=Warlock, baseCD=120}, -- Arcane Torrent\n    {type=\"DISPEL\", spellID=69179, race=\"BloodElf\", class=Warrior, baseCD=120}, -- Arcane Torrent\n    -- Dispel (Defensive, Magic)\n    {type=\"DEFMDISPEL\", spellID=88423, specs={Druid.Resto}, baseCD=8, mods=DispelMod(88423), ignoreCast=true}, -- Nature's Cure\n    {type=\"DEFMDISPEL\", spellID=115450, specs={Monk.MW}, baseCD=8, mods=DispelMod(115450), ignoreCast=true}, -- Detox\n    {type=\"DEFMDISPEL\", spellID=4987, specs={Paladin.Holy}, baseCD=8, mods=DispelMod(4987), ignoreCast=true}, -- Cleanse\n    {type=\"DEFMDISPEL\", spellID=527, specs={Priest.Disc, Priest.Holy}, baseCD=8, mods=DispelMod(527), ignoreCast=true}, -- Purify\n    {type=\"DEFMDISPEL\", spellID=77130, specs={Shaman.Resto}, baseCD=8, mods=DispelMod(77130), ignoreCast=true}, -- Purify Spirit\n    -- Raid-Wide Defensives\n    {type=\"RAIDCD\", spellID=196718, specs={DH.Havoc}, baseCD=180}, -- Darkness\n    {type=\"RAIDCD\", spellID=31821, specs={Paladin.Holy}, baseCD=180}, -- Aura Mastery\n    {type=\"RAIDCD\", spellID=204150, specs={Paladin.Prot}, baseCD=180, reqTalents={63}, version=6}, -- Aegis of Light\n    {type=\"RAIDCD\", spellID=62618, specs={Priest.Disc}, baseCD=180, reqTalents={71,73}}, -- Power Word: Barrier\n    {type=\"RAIDCD\", spellID=207399, specs={Shaman.Resto}, baseCD=300, reqTalents={43}}, -- Ancestral Protection Totem\n    {type=\"RAIDCD\", spellID=98008, specs={Shaman.Resto}, baseCD=180}, -- Spirit Link Totem\n    {type=\"RAIDCD\", spellID=97462, class=Warrior, baseCD=180}, -- Rallying Cry\n    -- External Defensives (Single Target)\n    {type=\"EXTERNAL\", spellID=102342, specs={Druid.Resto}, baseCD=60, modTalents={[62]=StaticMod(\"sub\", 15)}}, -- Ironbark\n    {type=\"EXTERNAL\", spellID=116849, specs={Monk.MW}, baseCD=120}, -- Life Cocoon\n    {type=\"EXTERNAL\", spellID=6940, specs={Paladin.Holy, Paladin.Prot}, baseCD=120}, -- Blessing of Sacrifice\n    {type=\"EXTERNAL\", spellID=1022, specs={Paladin.Holy, Paladin.Ret}, baseCD=300}, -- Blessing of Protection\n    {type=\"EXTERNAL\", spellID=1022, specs={Paladin.Prot}, baseCD=300, reqTalents={41,42}}, -- Blessing of Protection\n    {type=\"EXTERNAL\", spellID=204018, specs={Paladin.Prot}, baseCD=180, reqTalents={43}}, -- Blessing of Spellwarding\n    {type=\"EXTERNAL\", spellID=33206, specs={Priest.Disc}, baseCD=180}, -- Pain Supression\n    {type=\"EXTERNAL\", spellID=47788, specs={Priest.Holy}, baseCD=180, modTalents={[32]=modGuardianSpirit}}, -- Guardian Spirit\n    -- Healing and Healing Buffs \n    {type=\"HEALING\", spellID=33891, specs={Druid.Resto}, baseCD=180, reqTalents={53}, ignoreCast=true, mods=EventRemainingMod(\"SPELL_AURA_APPLIED\",117679,180), version=6}, -- Incarnation: Tree of Life\n    {type=\"HEALING\", spellID=740, specs={Druid.Resto}, baseCD=180, modTalents={[61]=StaticMod(\"sub\", 60)}}, -- Tranquility\n    {type=\"HEALING\", spellID=198664, specs={Monk.MW}, baseCD=180, reqTalents={63}, version=6}, -- Invoke Chi-Ji, the Red Crane\n    {type=\"HEALING\", spellID=115310, specs={Monk.MW}, baseCD=180}, -- Revival\n    {type=\"HEALING\", spellID=31884, specs={Paladin.Holy}, baseCD=120, reqTalents={61,63}, version=7}, -- Avenging Wrath\n    {type=\"HEALING\", spellID=216331, specs={Paladin.Holy}, baseCD=120, reqTalents={62}}, -- Avenging Crusader\n    {type=\"HEALING\", spellID=105809, specs={Paladin.Holy}, baseCD=90, reqTalents={53}}, -- Holy Avenger\n    {type=\"HEALING\", spellID=633, specs={Paladin.Holy}, baseCD=600, modTalents={[21]=StaticMod(\"mul\", 0.7)}}, -- Lay on Hands\n    {type=\"HEALING\", spellID=633, specs={Paladin.Prot, Paladin.Ret}, baseCD=600, modTalents={[51]=StaticMod(\"mul\", 0.7)}}, -- Lay on Hands\n    {type=\"HEALING\", spellID=210191, specs={Paladin.Ret}, baseCD=60, charges=2, reqTalents={63}, version=6}, -- Word of Glory\n    {type=\"HEALING\", spellID=246287, specs={Priest.Disc}, baseCD=90, reqTalents={73}}, -- Evangelism\n    {type=\"HEALING\", spellID=47536, specs={Priest.Disc}, baseCD=90}, -- Rapture\n    {type=\"HEALING\", spellID=271466, specs={Priest.Disc}, baseCD=180, reqTalents={72}}, -- Luminous Barrier\n    {type=\"HEALING\", spellID=200183, specs={Priest.Holy}, baseCD=120, reqTalents={72}}, -- Apotheosis\n    {type=\"HEALING\", spellID=64843, specs={Priest.Holy}, baseCD=180}, -- Divine Hymn\n    {type=\"HEALING\", spellID=265202, specs={Priest.Holy}, baseCD=720, reqTalents={73}, mods={CastDeltaMod(34861,-30), CastDeltaMod(2050,-30)}}, -- Holy Word: Salvation\n    {type=\"HEALING\", spellID=15286, specs={Priest.Shadow}, baseCD=120, modTalents={[22]=StaticMod(\"sub\", 45)}}, -- Vampiric Embrace\n    {type=\"HEALING\", spellID=114052, specs={Shaman.Resto}, baseCD=180, reqTalents={73}}, -- Ascendance\n    {type=\"HEALING\", spellID=198838, specs={Shaman.Resto}, baseCD=60, reqTalents={42}}, -- Earthen Wall Totem\n    {type=\"HEALING\", spellID=108280, specs={Shaman.Resto}, baseCD=180}, -- Healing Tide Totem\n    -- Utility (Movement, Taunts, etc)\n    {type=\"UTILITY\", spellID=205636, specs={Druid.Balance}, baseCD=60, reqTalents={13}}, -- Force of Nature (Treants)\n    {type=\"UTILITY\", spellID=29166, specs={Druid.Balance, Druid.Resto}, baseCD=180}, -- Innervate\n    {type=\"UTILITY\", spellID=106898, specs={Druid.Feral}, baseCD=120, version=2}, -- Stampeding Roar\n    {type=\"UTILITY\", spellID=106898, specs={Druid.Guardian}, baseCD=60, version=2}, -- Stampeding Roar\n    {type=\"UTILITY\", spellID=116841, class=Monk, baseCD=30, reqTalents={23}, version=6}, -- Tiger's Lust\n    {type=\"UTILITY\", spellID=1044, class=Paladin, baseCD=25, version=6}, -- Blessing of Freedom\n    {type=\"UTILITY\", spellID=73325, class=Priest, baseCD=90}, -- Leap of Faith\n    {type=\"UTILITY\", spellID=64901, specs={Priest.Holy}, baseCD=300}, -- Symbol of Hope\n    {type=\"UTILITY\", spellID=114018, class=Rogue, baseCD=360}, -- Shroud of Concealment\n    {type=\"UTILITY\", spellID=198103, class=Shaman, baseCD=300, version=2}, -- Earth Elemental\n    {type=\"UTILITY\", spellID=8143, class=Shaman, baseCD=60, version=6}, -- Tremor Totem\n    {type=\"UTILITY\", spellID=192077, class=Shaman, baseCD=120, reqTalents={53}, version=2}, -- Wind Rush Totem\n    {type=\"UTILITY\", spellID=58984, race=\"NightElf\", baseCD=120, version=3}, -- Shadowmeld\n    -- Personal Defensives\n    {type=\"PERSONAL\", spellID=198589, specs={DH.Havoc}, baseCD=60, mods=EventRemainingMod(\"SPELL_AURA_APPLIED\", 212800, 60)}, -- Blur\n    {type=\"PERSONAL\", spellID=48792, class=DK, baseCD=180}, -- Icebound Fortitude\n    {type=\"PERSONAL\", spellID=48707, specs={DK.Frost, DK.Unholy}, baseCD=60}, -- Anti-Magic Shell\n    {type=\"PERSONAL\", spellID=48707, specs={DK.Blood}, baseCD=60, modTalents={[42]=StaticMod(\"sub\", 15)}}, -- Anti-Magic Shell\n    {type=\"PERSONAL\", spellID=48743, specs={DK.Frost, DK.Unholy}, baseCD=120, reqTalents={53}}, -- Death Pact\n    {type=\"PERSONAL\", spellID=22812, specs={Druid.Balance, Druid.Guardian, Druid.Resto}, baseCD=60}, -- Barkskin\n    {type=\"PERSONAL\", spellID=108238, specs={Druid.Balance, Druid.Feral, Druid.Resto}, baseCD=90, reqTalents={22}, version=6}, -- Renewal\n    {type=\"PERSONAL\", spellID=61336, specs={Druid.Feral,Druid.Guardian}, baseCD=180, charges=2, version=6}, -- Survival Instincts\n    {type=\"PERSONAL\", spellID=109304, class=Hunter, baseCD=120}, -- Exhilaration\n    {type=\"PERSONAL\", spellID=5384, class=Hunter, baseCD=30, version=6}, -- Feign Death\n    {type=\"PERSONAL\", spellID=235219, specs={Mage.Frost}, baseCD=300}, -- Cold Snap\n    {type=\"PERSONAL\", spellID=122278, class=Monk, baseCD=120, reqTalents={53}}, -- Dampen Harm\n    {type=\"PERSONAL\", spellID=243435, specs={Monk.MW}, baseCD=90}, -- Fortifying Brew\n    {type=\"PERSONAL\", spellID=122281, specs={Monk.BRM}, baseCD=30, charges=2, reqTalents={52}, version=6}, -- Healing Elixir\n    {type=\"PERSONAL\", spellID=122281, specs={Monk.MW}, baseCD=30, charges=2, reqTalents={51}, version=6}, -- Healing Elixir\n    {type=\"PERSONAL\", spellID=122783, specs={Monk.MW, Monk.WW}, baseCD=90, reqTalents={52}}, -- Diffuse Magic\n    {type=\"PERSONAL\", spellID=122470, specs={Monk.WW}, baseCD=90}, -- Touch of Karma\n    {type=\"PERSONAL\", spellID=498, specs={Paladin.Holy}, baseCD=60}, -- Divine Protection\n    {type=\"PERSONAL\", spellID=184662, specs={Paladin.Ret}, baseCD=120, modTalents={[51]=StaticMod(\"mul\", 0.7)}}, -- Shield of Vengeance\n    {type=\"PERSONAL\", spellID=205191, specs={Paladin.Ret}, baseCD=60, reqTalents={53}}, -- Eye for an Eye\n    {type=\"PERSONAL\", spellID=19236, specs={Priest.Disc, Priest.Holy}, baseCD=90}, -- Desperate Prayer\n    {type=\"PERSONAL\", spellID=47585, specs={Priest.Shadow}, baseCD=120, duration=6, modTalents={[23]=StaticMod(\"sub\", 30)}, version=8}, -- Dispersion\n    {type=\"PERSONAL\", spellID=199754, specs={Rogue.Outlaw}, baseCD=120, version=2}, -- Riposte\n    {type=\"PERSONAL\", spellID=5277, specs={Rogue.Sin, Rogue.Sub}, baseCD=120, version=2}, -- Evasion\n    {type=\"PERSONAL\", spellID=108271, class=Shaman, baseCD=90}, -- Astral Shift\n    {type=\"PERSONAL\", spellID=108416, class=Warlock, baseCD=60, reqTalents={33}, version=6}, -- Dark Pact\n    {type=\"PERSONAL\", spellID=104773, class=Warlock, baseCD=180}, -- Unending Resolve\n    {type=\"PERSONAL\", spellID=118038, specs={Warrior.Arms}, baseCD=180}, -- Die by the Sword\n    {type=\"PERSONAL\", spellID=184364, specs={Warrior.Fury}, baseCD=120}, -- Enraged Regeneration\n    -- Tank-Only Defensives\n    {type=\"TANK\", spellID=212084, specs={DH.Veng}, baseCD=60, reqTalents={63}, version=6}, -- Fel Devastation\n    {type=\"TANK\", spellID=204021, specs={DH.Veng}, baseCD=60}, -- Fiery Brand\n    {type=\"TANK\", spellID=187827, specs={DH.Veng}, baseCD=180}, -- Metamorphosis\n    {type=\"TANK\", spellID=206931, specs={DK.Blood}, baseCD=30, reqTalents={12}, version=6}, -- Blooddrinker\n    {type=\"TANK\", spellID=274156, specs={DK.Blood}, baseCD=45, reqTalents={23}, version=6}, -- Consumption\n    {type=\"TANK\", spellID=49028, specs={DK.Blood}, baseCD=120}, -- Dancing Rune Weapon\n    {type=\"TANK\", spellID=194679, specs={DK.Blood}, baseCD=25, charges=2, reqTalents={43}, version=6}, -- Rune Tap\n    {type=\"TANK\", spellID=194844, specs={DK.Blood}, baseCD=60, reqTalents={73}}, -- Bonestorm\n    {type=\"TANK\", spellID=55233, specs={DK.Blood}, baseCD=90, modTalents={[72]=ResourceSpendingMods(DK.Blood, 0.1)}}, -- Vampiric Blood\n    {type=\"TANK\", spellID=102558, specs={Druid.Guardian}, baseCD=180, reqTalents={53}, version=6}, -- Incarnation: Guardian of Ursoc\n    {type=\"TANK\", spellID=132578, specs={Monk.BRM}, baseCD=180, reqTalents={63}, version=4}, -- Invoke Niuzao\n    {type=\"TANK\", spellID=115203, specs={Monk.BRM}, baseCD=420}, -- Fortifying Brew\n    {type=\"TANK\", spellID=115176, specs={Monk.BRM}, baseCD=300}, -- Zen Meditation\n    {type=\"TANK\", spellID=31850, specs={Paladin.Prot}, baseCD=120, modTalents={[51]=StaticMod(\"mul\", 0.7)}}, -- Ardent Defender\n    {type=\"TANK\", spellID=86659, specs={Paladin.Prot}, baseCD=300, version=5}, -- Guardian of the Ancient Kings\n    {type=\"TANK\", spellID=12975, specs={Warrior.Prot}, baseCD=180, modTalents={[43]=StaticMod(\"sub\", 60), [71]=ResourceSpendingMods(Warrior.Prot, 0.1)}}, -- Last Stand\n    {type=\"TANK\", spellID=871, specs={Warrior.Prot}, baseCD=240, modTalents={[71]=ResourceSpendingMods(Warrior.Prot, 0.1)}}, -- Shield Wall\n    {type=\"TANK\", spellID=1160, specs={Warrior.Prot}, baseCD=45, modTalents={[71]=ResourceSpendingMods(Warrior.Prot, 0.1)}}, -- Demoralizing Shout\n    {type=\"TANK\", spellID=228920, specs={Warrior.Prot}, baseCD=60, reqTalents={73}, version=6}, -- Ravager\n    {type=\"TANK\", spellID=23920, specs={Warrior.Prot}, baseCD=25, version=6}, -- Spell Reflection\n    -- Immunities\n    {type=\"IMMUNITY\", spellID=196555, specs={DH.Havoc}, baseCD=120, reqTalents={43}}, -- Netherwalk\n    {type=\"IMMUNITY\", spellID=186265, class=Hunter, baseCD=180, modTalents={[51]=StaticMod(\"mul\", 0.8)}}, -- Aspect of the Turtle\n    {type=\"IMMUNITY\", spellID=45438, specs={Mage.Arcane,Mage.Fire}, baseCD=240}, -- Ice Block\n    {type=\"IMMUNITY\", spellID=45438, specs={Mage.Frost}, baseCD=240, mods=CastRemainingMod(235219, 0)}, -- Ice Block\n    {type=\"IMMUNITY\", spellID=642, specs={Paladin.Holy}, baseCD=300, modTalents={[21]=StaticMod(\"mul\", 0.7)}}, -- Divine Shield\n    {type=\"IMMUNITY\", spellID=642, specs={Paladin.Prot, Paladin.Ret}, baseCD=300, modTalents={[51]=StaticMod(\"mul\", 0.7)}}, -- Divine Shield\n    {type=\"IMMUNITY\", spellID=31224, class=Rogue, baseCD=120}, -- Cloak of Shadows\n    -- Damage and Damage Buffs\n    {type=\"DAMAGE\", spellID=191427, specs={DH.Havoc}, baseCD=240}, -- Metamorphosis\n    {type=\"DAMAGE\", spellID=258925, specs={DH.Havoc}, baseCD=60, reqTalents={33}}, -- Fel Barrage\n    {type=\"DAMAGE\", spellID=206491, specs={DH.Havoc}, baseCD=120, reqTalents={73}}, -- Nemesis\n    {type=\"DAMAGE\", spellID=47568, specs={DK.Frost}, baseCD=120, version=6}, -- Empower Rune Weapon\n    {type=\"DAMAGE\", spellID=279302, specs={DK.Frost}, baseCD=180, reqTalents={63}}, -- Frostwyrm's Fury\n    {type=\"DAMAGE\", spellID=152279, specs={DK.Frost}, baseCD=120, reqTalents={73}}, -- Breath of Sindragosaa\n    {type=\"DAMAGE\", spellID=275699, specs={DK.Unholy}, baseCD=90, modTalents={[71]={CastDeltaMod(47541,-1), CastDeltaMod(207317,-1)}}, version=6}, -- Apocalypse\n    {type=\"DAMAGE\", spellID=42650, specs={DK.Unholy}, baseCD=480, modTalents={[71]={CastDeltaMod(47541,-5), CastDeltaMod(207317,-5)}}}, -- Army of the Dead\n    {type=\"DAMAGE\", spellID=49206, specs={DK.Unholy}, baseCD=180, reqTalents={73}}, -- Summon Gargoyle\n    {type=\"DAMAGE\", spellID=207289, specs={DK.Unholy}, baseCD=75, reqTalents={72}}, -- Unholy Frenzy\n    {type=\"DAMAGE\", spellID=194223, specs={Druid.Balance}, baseCD=180, reqTalents={51,52}}, -- Celestial Alignment\n    {type=\"DAMAGE\", spellID=202770, specs={Druid.Balance}, baseCD=60, reqTalents={72}, version=6}, -- Fury of Elune\n    {type=\"DAMAGE\", spellID=102560, specs={Druid.Balance}, baseCD=180, reqTalents={53}}, -- Incarnation: Chosen of Elune\n    {type=\"DAMAGE\", spellID=106951, specs={Druid.Feral}, baseCD=180, version=3}, -- Berserk\n    {type=\"DAMAGE\", spellID=102543, specs={Druid.Feral}, baseCD=180, reqTalents={53}}, -- Incarnation: King of the Jungle\n    {type=\"DAMAGE\", spellID=19574, specs={Hunter.BM}, baseCD=90, mods=CastDeltaMod(217200,-12)}, -- Bestial Wrath\n    {type=\"DAMAGE\", spellID=193530, specs={Hunter.BM}, baseCD=120}, -- Aspect of the Wild\n    {type=\"DAMAGE\", spellID=201430, specs={Hunter.BM}, baseCD=180, reqTalents={63}}, -- Stampede\n    {type=\"DAMAGE\", spellID=288613, specs={Hunter.MM}, baseCD=120, version=3}, -- Trueshot\n    {type=\"DAMAGE\", spellID=266779, specs={Hunter.SV}, baseCD=120}, -- Coordinated Assault\n    {type=\"DAMAGE\", spellID=55342, class=Mage, baseCD=120, reqTalents={32}}, -- Mirror Image\n    {type=\"DAMAGE\", spellID=12042, specs={Mage.Arcane}, baseCD=90}, -- Arcane Power\n    {type=\"DAMAGE\", spellID=190319, specs={Mage.Fire}, baseCD=120}, -- Combustion\n    {type=\"DAMAGE\", spellID=12472, specs={Mage.Frost}, baseCD=180}, -- Icy Veins\n    {type=\"DAMAGE\", spellID=115080, specs={Monk.WW}, baseCD=120}, -- Touch of Death\n    {type=\"DAMAGE\", spellID=123904, specs={Monk.WW}, baseCD=180, reqTalents={63}}, -- Invoke Xuen, the White Tiger\n    {type=\"DAMAGE\", spellID=137639, specs={Monk.WW}, baseCD=90, charges=2, reqTalents={71, 72}, version=6}, -- Storm, Earth, and Fire\n    {type=\"DAMAGE\", spellID=152173, specs={Monk.WW}, baseCD=90, reqTalents={73}}, -- Serenity\n    {type=\"DAMAGE\", spellID=152262, specs={Paladin.Prot}, baseCD=45, reqTalents={73}, version=6}, -- Seraphim\n    {type=\"DAMAGE\", spellID=31884, specs={Paladin.Prot}, baseCD=120, version=6}, -- Avenging Wrath\n    {type=\"DAMAGE\", spellID=31884, specs={Paladin.Ret}, baseCD=120, reqTalents={71,73}}, -- Avenging Wrath\n    {type=\"DAMAGE\", spellID=231895, specs={Paladin.Ret}, baseCD=120, reqTalents={72}}, -- Crusade\n    {type=\"DAMAGE\", spellID=280711, specs={Priest.Shadow}, baseCD=60, reqTalents={72}}, -- Dark Ascension\n    {type=\"DAMAGE\", spellID=193223, specs={Priest.Shadow}, baseCD=180, reqTalents={73}}, -- Surrender to Madness\n    {type=\"DAMAGE\", spellID=13750, specs={Rogue.Outlaw}, baseCD=180}, -- Adrenaline Rush\n    {type=\"DAMAGE\", spellID=51690, specs={Rogue.Outlaw}, baseCD=120, reqTalents={73}}, -- Killing Spree\n    {type=\"DAMAGE\", spellID=79140, specs={Rogue.Sin}, baseCD=120}, -- Vendetta\n    {type=\"DAMAGE\", spellID=121471, specs={Rogue.Sub}, baseCD=180}, -- Shadow Blades\n    {type=\"DAMAGE\", spellID=114050, specs={Shaman.Ele}, baseCD=180, reqTalents={73}}, -- Ascendance\n    {type=\"DAMAGE\", spellID=192249, specs={Shaman.Ele}, baseCD=150, reqTalents={42}, version=3}, -- Storm Elemental\n    {type=\"DAMAGE\", spellID=191634, specs={Shaman.Ele}, baseCD=60, reqTalents={72}, version=3}, -- Stormkeeper\n    {type=\"DAMAGE\", spellID=114051, specs={Shaman.Enh}, baseCD=180, reqTalents={73}}, -- Ascendance\n    {type=\"DAMAGE\", spellID=51533, specs={Shaman.Enh}, baseCD=180, modTalents={[71]=StaticMod(\"sub\", 30)}, version=6}, -- Feral Spirit\n    {type=\"DAMAGE\", spellID=205180, specs={Warlock.Affl}, baseCD=180}, -- Summon Darkglare\n    {type=\"DAMAGE\", spellID=113860, specs={Warlock.Affl}, baseCD=120, reqTalents={73}}, -- Dark Soul: Misery\n    {type=\"DAMAGE\", spellID=265187, specs={Warlock.Demo}, baseCD=90}, -- Summon Demonic Tyrant\n    {type=\"DAMAGE\", spellID=267217, specs={Warlock.Demo}, baseCD=180, reqTalents={73}}, -- Nether Portal\n    {type=\"DAMAGE\", spellID=113858, specs={Warlock.Destro}, baseCD=120, reqTalents={73}}, -- Dark Soul: Instability\n    {type=\"DAMAGE\", spellID=1122, specs={Warlock.Destro}, baseCD=180}, -- Summon Infernal\n    {type=\"DAMAGE\", spellID=227847, specs={Warrior.Arms}, baseCD=90, modTalents={[71]=ResourceSpendingMods(Warrior.Arms, 0.05)}}, -- Bladestorm\n    {type=\"DAMAGE\", spellID=107574, specs={Warrior.Arms}, baseCD=120, reqTalents={62}}, -- Avatar\n    {type=\"DAMAGE\", spellID=1719, specs={Warrior.Fury}, baseCD=90, modTalents={[72]=ResourceSpendingMods(Warrior.Fury, 0.05)}}, -- Recklessness\n    {type=\"DAMAGE\", spellID=46924, specs={Warrior.Fury}, baseCD=60, reqTalents={63}}, -- Bladestorm\n    {type=\"DAMAGE\", spellID=107574, specs={Warrior.Prot}, baseCD=120, modTalents={[71]=ResourceSpendingMods(Warrior.Prot, 0.1)}, version=6}, -- Avatar\n}\n\nZT.linkedSpellIDs = {\n    [19647]  = {119910, 132409, 115781}, -- Spell Lock\n    [132469] = {61391}, -- Typhoon\n    [191427] = {200166}, -- Metamorphosis\n    [106898] = {77761, 77764}, -- Stampeding Roar\n    [86659] = {212641}, -- Guardian of the Ancient Kings (+Glyph)\n}\n\nZT.separateLinkedSpellIDs = {\n    [86659] = {212641}, -- Guardian of the Ancient Kings (+Glyph)\n}\n\n--##############################################################################\n-- Handling custom spells specified by the user in the configuration\n\nlocal spellConfigFuncHeader = \"return function(DK,DH,Druid,Hunter,Mage,Monk,Paladin,Priest,Rogue,Shaman,Warlock,Warrior,StaticMod,DynamicMod,EventDeltaMod,CastDeltaMod,EventRemainingMod,CastRemainingMod,DispelMod)\"\n\nlocal function trim(s) -- From PiL2 20.4\n    return (s:gsub(\"^%s*(.-)%s*$\", \"%1\"))\nend\n\nlocal function addCustomSpell(spellConfig, i)\n    if not spellConfig or type(spellConfig) ~= \"table\" then\n        prerror(\"Custom Spell\", i, \"is not represented as a valid table\")\n        return\n    end\n    \n    if type(spellConfig.type) ~= \"string\" then\n        prerror(\"Custom Spell\", i, \"does not have a valid 'type' entry\")\n        return\n    end\n    \n    if type(spellConfig.spellID) ~= \"number\" then\n        prerror(\"Custom Spell\", i, \"does not have a valid 'spellID' entry\")\n        return\n    end\n    \n    if type(spellConfig.baseCD) ~= \"number\" then\n        prerror(\"Custom Spell\", i, \"does not have a valid 'baseCD' entry\")\n        return\n    end\n    \n    spellConfig.version = 10000\n    spellConfig.isCustom = true\n    \n    ZT.spells[#ZT.spells + 1] = spellConfig\nend\n\nfor i = 1,16 do\n    local spellConfig = ZT.config[\"custom\"..i]\n    if spellConfig then\n        spellConfig = trim(spellConfig)\n    \n        if spellConfig ~= \"\" then\n            local spellConfigFuncStr = spellConfigFuncHeader..\" return \"..spellConfig..\" end\"\n            local spellConfigFunc = WeakAuras.LoadFunction(spellConfigFuncStr, \"ZenTracker Custom Spell \"..i)\n            if spellConfigFunc then\n                local spellConfig = spellConfigFunc(DK,DH,Druid,Hunter,Mage,Monk,Paladin,Priest,Rogue,Shaman,Warlock,Warrior,\n                    StaticMod,DynamicMod,EventDeltaMod,CastDeltaMod,EventRemainingMod,CastRemainingMod,DispelMod)\n                addCustomSpell(spellConfig, i)\n            end\n        end\n    end\nend\n\n--##############################################################################\n-- Compiling the complete indexed tables of spells\n\nZT.spellsByRace = DefaultTable_Create(function() return DefaultTable_Create(function() return {} end) end)\nZT.spellsByClass = DefaultTable_Create(function() return DefaultTable_Create(function() return {} end) end)\nZT.spellsBySpec = DefaultTable_Create(function() return DefaultTable_Create(function() return {} end) end)\nZT.spellsByType = DefaultTable_Create(function() return {} end)\nZT.spellsByID = DefaultTable_Create(function() return {} end)\n\nfor _,spellInfo in ipairs(ZT.spells) do\n    -- Making the structuring for spell info more uniform\n    spellInfo.version = spellInfo.version or 1\n    spellInfo.specs = spellInfo.specs and Map_FromTable(spellInfo.specs)\n    spellInfo.mods = Table_Create(spellInfo.mods)\n    if spellInfo.modTalents then\n        for talent,mods in pairs(spellInfo.modTalents) do\n            spellInfo.modTalents[talent] = Table_Create(mods)\n        end\n    end\n\n    spellInfo.isRegistered = false\n    spellInfo.frontends = {}\n\n    -- Indexing for faster lookups\n    local spells\n    if spellInfo.race then\n        if spellInfo.class then\n            spells = ZT.spellsByRace[spellInfo.race][spellInfo.class]\n            spells[#spells + 1] = spellInfo\n        else\n            for _,class in pairs(AllClasses) do\n                spells = ZT.spellsByRace[spellInfo.race][class]\n                spells[#spells + 1] = spellInfo\n            end\n        end\n    elseif spellInfo.class then\n        if spellInfo.reqTalents then\n            for _,talent in ipairs(spellInfo.reqTalents) do\n                spells = ZT.spellsByClass[spellInfo.class][talent]\n                spells[#spells + 1] = spellInfo\n            end\n        else\n            if spellInfo.modTalents then\n                for talent,_ in pairs(spellInfo.modTalents) do\n                    spells = ZT.spellsByClass[spellInfo.class][talent]\n                    spells[#spells + 1] = spellInfo\n                end\n            end\n            spells = ZT.spellsByClass[spellInfo.class][\"Base\"]\n            spells[#spells + 1] = spellInfo\n        end\n    elseif spellInfo.specs then\n        for specID,_ in pairs(spellInfo.specs) do\n            if spellInfo.reqTalents then\n                for _,talent in ipairs(spellInfo.reqTalents) do\n                    spells = ZT.spellsBySpec[specID][talent]\n                    spells[#spells + 1] = spellInfo\n                end\n            else\n                if spellInfo.modTalents then\n                    for talent,_ in pairs(spellInfo.modTalents) do\n                        spells = ZT.spellsBySpec[specID][talent]\n                        spells[#spells + 1] = spellInfo\n                    end\n                end\n                spells = ZT.spellsBySpec[specID][\"Base\"]\n                spells[#spells + 1] = spellInfo\n            end\n        end\n    else\n        spells = ZT.spellsByClass[\"None\"]\n        spells[#spells + 1] = spellInfo\n    end\n\n    spells = ZT.spellsByType[spellInfo.type]\n    spells[#spells + 1] = spellInfo\n\n    spells = ZT.spellsByID[spellInfo.spellID]\n    spells[#spells + 1] = spellInfo\nend\n\n\n--##############################################################################\n-- Handling combatlog and WeakAura events by invoking specified callbacks\n\nZT.eventHandlers = { handlers = {} }\n\nfunction ZT.eventHandlers:add(type, spellID, sourceGUID, func, data)\n    local types = self.handlers[spellID]\n    if not types then\n        types = {}\n        self.handlers[spellID] = types\n    end\n    \n    local sources = types[type]\n    if not sources then\n        sources = {}\n        types[type] = sources\n    end\n    \n    local handlers = sources[sourceGUID]\n    if not handlers then\n        handlers = {}\n        sources[sourceGUID] = handlers\n    end\n    \n    handlers[func] = data\nend\n\nfunction ZT.eventHandlers:remove(type, spellID, sourceGUID, func)\n    local types = self.handlers[spellID]\n    if types then\n        local sources = types[type]\n        if sources then\n            local handlers = sources[sourceGUID]\n            if handlers then\n                handlers[func] = nil\n            end\n        end\n    end\nend\n\nfunction ZT.eventHandlers:removeAll(sourceGUID)\n    for _,spells in pairs(self.eventHandlers) do\n        for _,sources in pairs(spells) do\n            for GUID,handlers in pairs(sources) do\n                if GUID == sourceGUID then\n                    wipe(handlers)\n                end\n            end\n        end\n    end\nend\n\nlocal function fixSourceGUID(sourceGUID) -- Based on https://wago.io/p/Nnogga\n    local type = strsplit(\"-\",sourceGUID)\n    if type == \"Pet\" then\n        for unit in WA_IterateGroupMembers() do\n            if UnitGUID(unit..\"pet\") == sourceGUID then\n                sourceGUID = UnitGUID(unit)\n                break\n            end\n        end\n    end\n    \n    return sourceGUID\nend\n\nfunction ZT.eventHandlers:handle(type, spellID, sourceGUID)\n    local types = self.handlers[spellID]\n    if not types then\n        return\n    end\n    \n    local sources = types[type]\n    if not sources then\n        return\n    end\n    \n    local handlers = sources[sourceGUID]\n    if not handlers then\n        sourceGUID = fixSourceGUID(sourceGUID)\n        handlers = sources[sourceGUID]\n        if not handlers then\n            return\n        end\n    end\n    \n    for func,data in pairs(handlers) do\n        func(data, spellID)\n    end\nend\n\n--##############################################################################\n-- Managing timer callbacks in a way that allows for updates/removals\n\nZT.timers = { heap={}, callbackTimes={} }\n\nfunction ZT.timers:fixHeapUpwards(index)\n    local heap = self.heap\n    local timer = heap[index]\n\n    local parentIndex, parentTimer\n    while index > 1 do\n        parentIndex = floor(index / 2)\n        parentTimer = heap[parentIndex]\n        if timer.time >= parentTimer.time then\n            break\n        end\n\n        parentTimer.index = index\n        heap[index] = parentTimer\n        index = parentIndex\n    end\n\n    if timer.index ~= index then\n        timer.index = index\n        heap[index] = timer\n    end\nend\n\nfunction ZT.timers:fixHeapDownwards(index)\n    local heap = self.heap\n    local timer = heap[index]\n\n    local childIndex, minChildTimer, leftChildTimer, rightChildTimer\n    while true do\n        childIndex = 2 * index\n\n        leftChildTimer = heap[childIndex]\n        if leftChildTimer then\n            rightChildTimer = heap[childIndex + 1]\n            if rightChildTimer and (rightChildTimer.time < leftChildTimer.time) then\n                minChildTimer = rightChildTimer\n            else\n                minChildTimer = leftChildTimer\n            end\n        else\n            break\n        end\n\n        if timer.time <= minChildTimer.time then\n            break\n        end\n\n        childIndex = minChildTimer.index\n        minChildTimer.index = index\n        heap[index] = minChildTimer\n        index = childIndex\n    end\n\n    if timer.index ~= index then\n        timer.index = index\n        heap[index] = timer\n    end\nend\n\nfunction ZT.timers:setupCallback()\n    local minTimer = self.heap[1]\n    if minTimer then\n        local timeNow = GetTime()\n        local remaining = minTimer.time - timeNow\n        if remaining <= 0 then\n            self:handle()\n        elseif not self.callbackTimes[minTimer.time] then\n            for time,_ in pairs(self.callbackTimes) do\n                if time < timeNow then\n                    self.callbackTimes[time] = nil\n                end\n            end\n            self.callbackTimes[minTimer.time] = true\n\n            -- Note: This 0.001 avoids early callbacks that I ran into\n            remaining = remaining + 0.001\n            prdebug(DEBUG_TIMER, \"Setting callback for handling timers after\", remaining, \"seconds\")\n            C_Timer.After(remaining, function() self:handle() end)\n        end\n    end\nend\n\nfunction ZT.timers:handle()\n    local timeNow = GetTime()\n    local heap = self.heap \n    local minTimer = heap[1]\n\n    prdebug(DEBUG_TIMER, \"Handling timers at time\", timeNow, \"( Min @\", minTimer and minTimer.time or \"NONE\", \")\")\n    while minTimer and minTimer.time <= timeNow do\n        local heapSize = #heap\n        if heapSize > 1 then\n            heap[1] = heap[heapSize]\n            heap[1].index = 1\n            heap[heapSize] = nil\n            self:fixHeapDownwards(1)\n        else\n            heap[1] = nil\n        end\n\n        minTimer.index = -1\n        minTimer.callback()\n\n        minTimer = heap[1]\n    end\n\n    self:setupCallback()\nend\n\nfunction ZT.timers:add(time, callback)\n    local heap = self.heap \n\n    local index = #heap + 1\n    local timer = {time=time, callback=callback, index=index}\n    heap[index] = timer\n\n    self:fixHeapUpwards(index)\n    self:setupCallback()\n\n    return timer\nend\n\nfunction ZT.timers:cancel(timer)\n    local index = timer.index\n    if index == -1 then\n        return\n    end\n\n    timer.index = -1\n\n    local heap = self.heap\n    local heapSize = #heap\n    if heapSize ~= index then\n        heap[index] = heap[heapSize]\n        heap[index].index = index\n        heap[heapSize] = nil\n        self:fixHeapDownwards(index)\n        self:setupCallback()\n    else\n        heap[index] = nil\n    end\nend\n\nfunction ZT.timers:update(timer, time)\n    local heap = self.heap \n\n    local fixHeapFunc = (time <= timer.time) and self.fixHeapUpwards or self.fixHeapDownwards\n    timer.time = time\n\n    fixHeapFunc(self, timer.index)\n    self:setupCallback()\nend\n\n--##############################################################################\n-- Managing the set of <spell, member> pairs that are being watched\n\nlocal WatchInfo = { nextID = 1 }\nlocal WatchInfoMT = { __index = WatchInfo }\n\nZT.watching = {}\n\nfunction WatchInfo:create(member, specInfo, spellInfo, isHidden)\n    local watchInfo = {\n        watchID = self.nextID,\n        member = member,\n        spellInfo = spellInfo,\n        duration = member:calcSpellCD(spellInfo, specInfo),\n        expiration = GetTime(),\n        charges = spellInfo.charges,\n        isHidden = isHidden,\n        isLazy = spellInfo.isLazy,\n        ignoreSharing = false,\n    }\n    self.nextID = self.nextID + 1\n\n    watchInfo = setmetatable(watchInfo, WatchInfoMT)\n    return watchInfo\nend\n\nfunction WatchInfo:sendAddEvent()\n    if not self.isLazy and not self.isHidden then\n        local spellInfo = self.spellInfo\n        prdebug(DEBUG_EVENT, \"Sending ZT_ADD\", spellInfo.type, self.watchID, self.member.name, spellInfo.spellID, self.duration, self.charges)\n        WeakAuras.ScanEvents(\"ZT_ADD\", spellInfo.type, self.watchID, self.member, spellInfo.spellID, self.duration, self.charges)\n        \n        if self.expiration > GetTime() then\n            self:sendTriggerEvent()\n        end\n    end\nend\n\nfunction WatchInfo:sendTriggerEvent()\n    if self.isLazy then\n        self.isLazy = false\n        self:sendAddEvent()\n    end\n\n    if not self.isHidden then\n        prdebug(DEBUG_EVENT, \"Sending ZT_TRIGGER\", self.spellInfo.type, self.watchID, self.duration, self.expiration, self.charges)\n        WeakAuras.ScanEvents(\"ZT_TRIGGER\", self.spellInfo.type, self.watchID, self.duration, self.expiration, self.charges)\n    end\nend\n\nfunction WatchInfo:sendRemoveEvent()\n    if not self.isLazy and not self.isHidden then\n        prdebug(DEBUG_EVENT, \"Sending ZT_REMOVE\", self.spellInfo.type, self.watchID)\n        WeakAuras.ScanEvents(\"ZT_REMOVE\", self.spellInfo.type, self.watchID)\n    end\nend\n\nfunction WatchInfo:hide()\n    if not self.isHidden then\n        self:sendRemoveEvent()\n        self.isHidden = true\n    end\nend\n\nfunction WatchInfo:unhide(suppressAddEvent)\n    if self.isHidden then\n        self.isHidden = false\n        if not suppressAddEvent then\n            self:sendAddEvent()\n        end\n    end\nend\n\nfunction WatchInfo:toggleHidden(toggle, suppressAddEvent)\n    if toggle then\n        self:hide()\n    else\n        self:unhide(suppressAddEvent)\n    end\nend\n\nfunction WatchInfo:handleReadyTimer()\n    if self.charges then\n        self.charges = self.charges + 1\n\n        -- If we are not at max charges, update expiration and start a ready timer\n        if self.charges < self.spellInfo.charges then\n            self.expiration = self.expiration + self.duration\n            prdebug(DEBUG_TIMER, \"Adding ready timer of\", self.expiration, \"for spellID\", self.spellInfo.spellID)\n            self.readyTimer = ZT.timers:add(self.expiration, function() self:handleReadyTimer() end)\n        else\n            self.readyTimer = nil\n        end\n    else\n        self.readyTimer = nil\n    end\n\n    self:sendTriggerEvent()\nend\n\nfunction WatchInfo:updateReadyTimer() -- Returns true if a timer was set, false if handled immediately\n    if self.expiration > GetTime() then\n        if self.readyTimer then\n            prdebug(DEBUG_TIMER, \"Updating ready timer from\", self.readyTimer.time, \"to\", self.expiration, \"for spellID\", self.spellInfo.spellID)\n            ZT.timers:update(self.readyTimer, self.expiration)\n        else\n            prdebug(DEBUG_TIMER, \"Adding ready timer of\", self.expiration, \"for spellID\", self.spellInfo.spellID)\n            self.readyTimer = ZT.timers:add(self.expiration, function() self:handleReadyTimer() end)\n        end\n\n        return true\n    else\n        if self.readyTimer then\n            prdebug(DEBUG_TIMER, \"Canceling ready timer for spellID\", self.spellInfo.spellID)\n            ZT.timers:cancel(self.readyTimer)\n            self.readyTimer = nil\n        end\n\n        self:handleReadyTimer(self.expiration)\n        return false\n    end\nend\n\nfunction WatchInfo:startCD()\n    if self.charges then\n        if self.charges == 0 or self.charges == self.spellInfo.charges then\n            self.expiration = GetTime() + self.duration\n            self:updateReadyTimer()\n        end\n\n        if self.charges > 0 then\n            self.charges = self.charges - 1\n        end\n    else\n        self.expiration = GetTime() + self.duration\n        self:updateReadyTimer()\n    end\n\n    self:sendTriggerEvent()\nend\n\nfunction WatchInfo:updateCDDelta(delta)\n    self.expiration = self.expiration + delta\n\n    local time = GetTime()\n    local remaining = self.expiration - time\n\n    if self.charges and remaining <= 0 then\n        local chargesGained = 1 - floor(remaining / self.duration)\n        self.charges = max(self.charges + chargesGained, self.spellInfo.charges)\n        if self.charges == self.spellInfo.charges then\n            self.expiration = time\n        else\n            self.expiration = self.expiration + (chargesGained * self.duration)\n        end\n    end\n\n    if self:updateReadyTimer() then\n        self:sendTriggerEvent()\n    end\nend\n\nfunction WatchInfo:updateCDRemaining(remaining)\n    -- Note: This assumes that when remaining is 0 and the spell uses charges then it gains a charge\n    if self.charges and remaining == 0 then\n        if self.charges < self.spellInfo.charges then\n            self.charges = self.charges + 1\n        end\n\n        -- Below maximum charges the expiration time doesn't change\n        if self.charges < self.spellInfo.charges then\n            self:sendTriggerEvent() \n        else\n            self.expiration = GetTime()\n            self:updateReadyTimer()\n        end\n    else\n        self.expiration = GetTime() + remaining\n        if self:updateReadyTimer() then\n            self:sendTriggerEvent() \n        end\n    end\nend\n\nfunction WatchInfo:updatePlayerCharges()\n    charges = GetSpellCharges(self.spellInfo.spellID)\n    if charges then\n        self.charges = charges\n    end\nend\n\nfunction WatchInfo:updatePlayerCD(spellID, ignoreIfReady)\n    local startTime, duration, enabled\n    if self.charges then\n        local charges, maxCharges\n        charges, maxCharges, startTime, duration = GetSpellCharges(spellID)\n        if charges == maxCharges then\n            startTime = 0\n        end\n        enabled = 1\n        self.charges = charges\n    else\n        startTime, duration, enabled = GetSpellCooldown(spellID)\n    end\n\n    if enabled ~= 0 then\n        local ignoreRateLimit\n        if startTime ~= 0 then\n            ignoreRateLimit = (self.expiration < GetTime())\n            self.duration = duration\n            self.expiration = startTime + duration\n        else\n            ignoreRateLimit = true\n            self.expiration = GetTime()\n        end\n        \n        if (not ignoreIfReady) or (startTime ~= 0) then\n            ZT:sendCDUpdate(self, ignoreRateLimit)\n            self:sendTriggerEvent()\n        end\n    end\nend\n\nfunction ZT:togglePlayerHandlers(watchInfo, enable)\n    local spellID = watchInfo.spellInfo.spellID\n    local toggleHandlerFunc = enable and self.eventHandlers.add or self.eventHandlers.remove\n    \n    if enable then\n        WeakAuras.WatchSpellCooldown(spellID)\n    end\n    toggleHandlerFunc(self.eventHandlers, \"SPELL_COOLDOWN_CHANGED\", spellID, 0, watchInfo.updatePlayerCD, watchInfo)\n    \n    local links = self.separateLinkedSpellIDs[spellID]\n    if links then\n        for _,linkedSpellID in ipairs(links) do\n            if enable then\n                WeakAuras.WatchSpellCooldown(linkedSpellID)\n            end\n            toggleHandlerFunc(self.eventHandlers, \"SPELL_COOLDOWN_CHANGED\", linkedSpellID, 0, watchInfo.updatePlayerCD, watchInfo)\n        end\n    end\nend\n\nfunction ZT:toggleCombatLogHandlers(watchInfo, enable, specInfo)\n    local spellInfo = watchInfo.spellInfo\n    local spellID = spellInfo.spellID\n    local member = watchInfo.member\n    local toggleHandlerFunc = enable and self.eventHandlers.add or self.eventHandlers.remove\n    \n    if not spellInfo.ignoreCast then\n        toggleHandlerFunc(self.eventHandlers, \"SPELL_CAST_SUCCESS\", spellID, member.GUID, watchInfo.startCD, watchInfo)\n        \n        local links = self.linkedSpellIDs[spellID]\n        if links then\n            for _,linkedSpellID in ipairs(links) do\n                toggleHandlerFunc(self.eventHandlers, \"SPELL_CAST_SUCCESS\", linkedSpellID, member.GUID, watchInfo.startCD, watchInfo)\n            end\n        end\n    end\n    \n    for _,modifier in pairs(spellInfo.mods) do\n        if modifier.type == \"Dynamic\" then\n            for _,handlerInfo in ipairs(modifier.handlers) do\n                toggleHandlerFunc(self.eventHandlers, handlerInfo.type, handlerInfo.spellID, member.GUID, handlerInfo.handler, watchInfo)\n            end\n        end\n    end\n    \n    if spellInfo.modTalents then\n        for talent, modifiers in pairs(spellInfo.modTalents) do\n            if specInfo.talentsMap[talent] then\n                for _, modifier in pairs(modifiers) do\n                    if modifier.type == \"Dynamic\" then\n                        for _,handlerInfo in ipairs(modifier.handlers) do\n                            toggleHandlerFunc(self.eventHandlers, handlerInfo.type, handlerInfo.spellID, member.GUID, handlerInfo.handler, watchInfo)\n                        end\n                    end\n                end\n            end\n        end\n    end\nend\n\nfunction ZT:watch(spellInfo, member, specInfo)\n    -- Only handle registered spells (or those for the player)\n    if not spellInfo.isRegistered and not member.isPlayer then\n        return\n    end\n    \n    local spellID = spellInfo.spellID\n    local spells = self.watching[spellID]\n    if not spells then\n        spells = {}\n        self.watching[spellID] = spells\n    end\n\n    specInfo = specInfo or member.specInfo\n    local isHidden = (member.isPlayer and not spellInfo.isRegistered) or member.isHidden\n    \n    local watchInfo = spells[member.GUID]\n    local isNew = (watchInfo == nil)\n    if not watchInfo then\n        watchInfo = WatchInfo:create(member, specInfo, spellInfo, isHidden)\n        spells[member.GUID] = watchInfo\n        member.watching[spellID] = watchInfo\n    else\n        watchInfo.spellInfo = spellInfo\n        watchInfo.charges = spellInfo.charges\n        watchInfo.duration = member:calcSpellCD(spellInfo, specInfo)\n        watchInfo:toggleHidden(isHidden, true) -- We will send the ZT_ADD event later\n    end\n\n    if member.isPlayer then\n        watchInfo:updatePlayerCharges()\n        watchInfo:sendAddEvent()\n\n        watchInfo:updatePlayerCD(spellID, true)\n        \n        local links = self.separateLinkedSpellIDs[spellID]\n        if links then\n            for _,linkedSpellID in ipairs(links) do\n                watchInfo:updatePlayerCD(linkedSpellID, true)\n            end\n        end\n    else\n        watchInfo:sendAddEvent()\n    end \n\n    if member.isPlayer and not TEST_CLEU then\n        if isNew then\n            self:togglePlayerHandlers(watchInfo, true)\n        end\n    elseif member.tracking == \"CombatLog\" or (member.tracking == \"Sharing\" and member.spellsVersion < spellInfo.version) then\n        watchInfo.ignoreSharing = true\n        if not isNew then\n            self:toggleCombatLogHandlers(watchInfo, false, member.specInfo)\n        end\n        self:toggleCombatLogHandlers(watchInfo, true, specInfo)\n    else\n        watchInfo.ignoreSharing = false\n    end\nend\n\nfunction ZT:unwatch(spellInfo, member)\n    -- Only handle registered spells (or those for the player)\n    if not spellInfo.isRegistered and not member.isPlayer then\n        return\n    end\n\n    local spellID = spellInfo.spellID\n    local sources = self.watching[spellID]\n    if not sources then\n        return\n    end\n    \n    local watchInfo = sources[member.GUID]\n    if not watchInfo then\n        return\n    end\n\n    -- Ignoring unwatch requests if the spellInfo doesn't match (yet spellID does)\n    -- (Note: This serves to ease updating after spec/talent changes)\n    if watchInfo.spellInfo ~= spellInfo then\n        return\n    end\n\n    if member.isPlayer and not TEST_CLEU then\n        -- If called due to front-end unregistration, only hide it to allow continued sharing of updates\n        -- Otherwise, called due to a spec/talent change, so actually unwatch it\n        if not spellInfo.isRegistered then\n            watchInfo:hide()\n            return\n        end\n\n        self:togglePlayerHandlers(watchInfo, false)\n    elseif member.tracking == \"CombatLog\"  or (member.tracking == \"Sharing\" and member.spellsVersion < spellInfo.version) then\n        self:toggleCombatLogHandlers(watchInfo, false, member.specInfo)\n    end\n\n    if watchInfo.readyTimer then\n        self.timers:cancel(watchInfo.readyTimer)\n    end\n    \n    sources[member.GUID] = nil\n    member.watching[spellID] = nil\n    \n    watchInfo:sendRemoveEvent()\nend\n\n--##############################################################################\n-- Tracking types registered by front-end WAs\n\nfunction ZT:registerSpells(frontendID, spells)\n    for _,spellInfo in ipairs(spells) do\n        local frontends = spellInfo.frontends\n        if next(frontends, nil) ~= nil then\n            -- Some front-end already registered for this spell, so just send ADD events\n            local watched = self.watching[spellInfo.spellID]\n            if watched then\n                for _,watchInfo in pairs(watched) do\n                    if watchInfo.spellInfo == spellInfo then\n                        watchInfo:sendAddEvent()\n                    end\n                end\n            end\n        else\n            -- No front-end was registered for this spell, so watch as needed\n            spellInfo.isRegistered = true\n            for _,member in pairs(self.members) do\n                if member:knowsSpell(spellInfo) and not member.isIgnored then\n                    self:watch(spellInfo, member)\n                end\n            end\n        end\n\n        frontends[frontendID] = true\n    end\nend\n\nfunction ZT:unregisterSpells(frontendID, spells)\n    for _,spellInfo in ipairs(spells) do\n        local frontends = spellInfo.frontends\n        frontends[frontendID] = nil\n    \n        if next(frontends, nil) == nil then\n            local watched = self.watching[spellInfo.spellID]\n            if watched then\n                for _,watchInfo in pairs(watched) do\n                    if watchInfo.spellInfo == spellInfo then\n                        self:unwatch(spellInfo, watchInfo.member)\n                    end\n                end\n            end\n            spellInfo.isRegistered = false\n        end\n    end\nend\n\nfunction ZT:toggleFrontEndRegistration(frontendID, info, toggle)\n    local infoType = type(info)\n    local registerFunc = toggle and self.registerSpells or self.unregisterSpells\n\n    if infoType == \"string\" then -- Registration info is a type\n        prdebug(DEBUG_EVENT, \"Received\", toggle and \"ZT_REGISTER\" or \"ZT_UNREGISTER\", \"from\", frontendID, \"for type\", info)\n        registerFunc(self, frontendID, self.spellsByType[info])\n    elseif infoType == \"number\" then -- Registration info is a spellID\n        prdebug(DEBUG_EVENT, \"Received\", toggle and \"ZT_REGISTER\" or \"ZT_UNREGISTER\", \"from\", frontendID, \"for spellID\", info)\n        registerFunc(self, frontendID, self.spellsByID[info])\n    elseif infoType == \"table\" then -- Registration info is a table of types or spellIDs\n        infoType = type(info[1])\n\n        if infoType == \"string\" then\n            prdebug(DEBUG_EVENT, \"Received\", toggle and \"ZT_REGISTER\" or \"ZT_UNREGISTER\", \"from\", frontendID, \"for multiple types\")\n            for _,type in ipairs(info) do\n                registerFunc(self, frontendID, self.spellsByType[type])\n            end\n        elseif infoType == \"number\" then\n            prdebug(DEBUG_EVENT, \"Received\", toggle and \"ZT_REGISTER\" or \"ZT_UNREGISTER\", \"from\", frontendID, \"for multiple spells\")\n            for _,spellID in ipairs(info) do\n                registerFunc(self, frontendID, self.spellsByID[spellID])\n            end\n        end\n    end\nend\n\nfunction ZT:registerFrontEnd(frontendID, info)\n    self:toggleFrontEndRegistration(frontendID, info, true)\nend\n\nfunction ZT:unregisterFrontEnd(frontendID, info)\n    self:toggleFrontEndRegistration(frontendID, info, false)\nend\n\n\n--##############################################################################\n-- Managing member information (e.g., spec, talents) for all group members\n\nlocal Member = { }\nlocal MemberMT = { __index = Member }\n\nZT.members = {}\nZT.inEncounter = false\n\nlocal membersToIgnore = {}\nif ZT.config[\"ignoreList\"] then\n    local ignoreListStr = trim(ZT.config[\"ignoreList\"])\n    if ignoreListStr ~= \"\" then\n        ignoreListStr = \"return \"..ignoreListStr\n        local ignoreList = WeakAuras.LoadFunction(ignoreListStr, \"ZenTracker Ignore List\")\n        if ignoreList and (type(ignoreList) == \"table\") then\n            for i,name in ipairs(ignoreList) do\n                if type(name) == \"string\" then\n                    membersToIgnore[strlower(name)] = true\n                else\n                    prerror(\"Ignore list entry\", i, \"is not a string. Skipping...\")\n                end\n            end\n        else\n            prerror(\"Ignore list is not in the form of a table. For example: {\\\"Zenlia\\\", \\\"Cistara\\\"}\")\n        end\n    end\nend\n\nfunction Member:create(memberInfo)\n    local member = memberInfo\n    member.watching = {}\n    member.tracking = member.tracking and member.tracking or \"CombatLog\"\n    member.isPlayer = (member.GUID == UnitGUID(\"player\"))\n    member.isHidden = false\n    member.isReady = false\n\n    return setmetatable(member, MemberMT)\nend\n\nfunction Member:gatherInfo()\n    local _,className,_,race,_,name = GetPlayerInfoByGUID(self.GUID)\n    self.name = name and gsub(name, \"%-[^|]+\", \"\") or nil\n    self.class = className and AllClasses[className] or nil\n    self.classID = className and AllClasses[className].ID or nil\n    self.classColor = className and RAID_CLASS_COLORS[className] or nil\n    self.race = race\n    \n    if (self.tracking == \"Sharing\") and self.name then\n        prdebug(DEBUG_TRACKING, self.name, \"is using ZenTracker with spellsVersion\", self.spellsVersion)\n    end\n\n    if self.name and membersToIgnore[strlower(self.name)] then\n        self.isIgnored = true\n        return false\n    end\n\n    self.isReady = (self.name ~= nil) and (self.classID ~= nil) and (self.race ~= nil)\n    return self.isReady\nend\n\nfunction Member:knowsSpell(spellInfo, specInfo)\n    specInfo = specInfo or self.specInfo\n    \n    if spellInfo.race and spellInfo.race ~= self.race then\n        return false\n    end\n    if spellInfo.class and spellInfo.class.ID ~= self.classID then\n        return false\n    end\n    if spellInfo.specs and (not specInfo.specID or not spellInfo.specs[specInfo.specID]) then\n        return false\n    end\n    \n    if not spellInfo.reqTalents then\n        return true\n    end\n    for _,t in ipairs(spellInfo.reqTalents) do\n        if specInfo.talentsMap[t] then\n            return true\n        end\n    end\n\n    return false\nend\n\nfunction Member:calcSpellCD(spellInfo, specInfo)\n    specInfo = specInfo or self.specInfo\n    \n    local cooldown = spellInfo.baseCD\n    if spellInfo.modTalents then\n        for talent,modifiers in pairs(spellInfo.modTalents) do\n            if specInfo.talentsMap[talent] then\n                for _,modifier in ipairs(modifiers) do\n                    if modifier.type == \"Static\" then\n                        if modifier.sub then\n                            cooldown = cooldown - modifier.sub\n                        elseif modifier.mul then\n                            cooldown = cooldown * modifier.mul\n                        end\n                    end\n                end\n            end\n        end\n    end\n    \n    return cooldown\nend\n\nfunction Member:hide()\n    if not self.isHidden and not self.isPlayer then\n        self.isHidden = true\n        for _,watchInfo in pairs(self.watching) do\n            watchInfo:hide()\n        end\n    end\nend\n\nfunction Member:unhide()\n    if self.isHidden and not self.isPlayer then\n        self.isHidden = false\n        for _,watchInfo in pairs(self.watching) do\n            watchInfo:unhide()\n        end\n    end\nend\n\nfunction ZT:addOrUpdateMember(memberInfo)\n    local specInfo = memberInfo.specInfo\n\n    local member = self.members[memberInfo.GUID]\n    if not member then\n        member = Member:create(memberInfo)\n        self.members[member.GUID] = member\n    end\n\n    if member.isIgnored then\n        return\n    end\n\n    -- Update if the member is now ready, or if they swapped specs/talents\n    local needsUpdate = not member.isReady and member:gatherInfo()\n    local needsSpecUpdate = specInfo.specID and (specInfo.specID ~= member.specInfo.specID)\n    local needsTalentUpdate = specInfo.talents and (specInfo.talents ~= member.specInfo.talents)\n\n    if member.isReady and (needsUpdate or needsSpecUpdate or needsTalentUpdate) then\n        -- This handshake comes before any cooldown updates for newly watched spells\n        if member.isPlayer then\n            self:sendHandshake(specInfo)\n        end\n\n        -- If we are in an encounter, hide the member if they are outside the player's instance\n        -- (Note: Previously did this on member creation, which seemed to introduce false positives)\n        if needsUpdate and self.inEncounter and not member.isPlayer then\n            local _,_,_,instanceID = UnitPosition(\"player\")\n            local _,_,_,mInstanceID = UnitPosition(self.inspectLib:GuidToUnit(member.GUID))\n            if instanceID ~= mInstanceID then\n                member:hide()\n            end\n        end\n\n        -- Generic Spells (i.e., no class/race/spec)\n        -- Note: These are set once and never change\n        if needsUpdate then\n            for _,spellInfo in ipairs(self.spellsByClass[\"None\"]) do\n                self:watch(spellInfo, member, specInfo)\n            end\n        end\n\n        -- Class Spells (Base) + Race Spells\n        -- Note: These are set once and never change\n        if needsUpdate then\n            for _,spellInfo in ipairs(self.spellsByRace[member.race][member.class]) do\n                self:watch(spellInfo, member, specInfo)\n            end\n\n            for _,spellInfo in ipairs(self.spellsByClass[member.class][\"Base\"]) do\n                self:watch(spellInfo, member, specInfo)\n            end\n        end\n\n        -- Class Spells (Talented)\n        if needsUpdate or needsTalentUpdate then\n            local classSpells = self.spellsByClass[member.class]\n\n            for talent,_ in pairs(specInfo.talentsMap) do\n                for _,spellInfo in ipairs(classSpells[talent]) do\n                    self:watch(spellInfo, member, specInfo)\n                end\n            end\n\n            if needsTalentUpdate then\n                for talent,_ in pairs(member.specInfo.talentsMap) do\n                    if not specInfo.talentsMap[talent] then\n                        for _,spellInfo in ipairs(classSpells[talent]) do\n                            if not member:knowsSpell(spellInfo, specInfo) then\n                                self:unwatch(spellInfo, member)\n                            else\n                                self:watch(spellInfo, member, specInfo)\n                            end\n                        end\n                    end\n                end\n            end\n        end\n\n        -- Specialization Spells (Base/Talented)\n        if (needsUpdate or needsSpecUpdate or needsTalentUpdate) and specInfo.specID then\n            local specSpells = self.spellsBySpec[specInfo.specID]\n\n            if needsUpdate or needsSpecUpdate then\n                for _,spellInfo in ipairs(specSpells[\"Base\"]) do\n                    self:watch(spellInfo, member, specInfo)\n                end\n            end\n            for talent,_ in pairs(specInfo.talentsMap) do\n                for _,spellInfo in ipairs(specSpells[talent]) do\n                    self:watch(spellInfo, member, specInfo)\n                end\n            end\n\n            if (needsSpecUpdate or needsTalentUpdate) and member.specInfo.specID then\n                specSpells = self.spellsBySpec[member.specInfo.specID]\n\n                if needsSpecUpdate then\n                    for _,spellInfo in ipairs(specSpells[\"Base\"]) do\n                        if not member:knowsSpell(spellInfo, specInfo) then\n                            self:unwatch(spellInfo, member)\n                        else\n                            self:watch(spellInfo, member, specInfo)\n                        end\n                    end\n                end\n\n                for talent,_ in pairs(member.specInfo.talentsMap) do\n                    if not specInfo.talentsMap[talent] then\n                        for _,spellInfo in ipairs(specSpells[talent]) do\n                            if not member:knowsSpell(spellInfo, specInfo) then\n                                self:unwatch(spellInfo, member)\n                            else\n                                self:watch(spellInfo, member, specInfo)\n                            end\n                        end\n                    end\n                end\n            end\n        end\n        \n        member.specInfo = specInfo\n    end\n \n    -- If tracking changed from \"CombatLog\" to \"Sharing\", remove unnecessary event handlers and send a handshake/updates\n    if (member.tracking == \"CombatLog\") and (memberInfo.tracking == \"Sharing\") then\n        member.tracking = \"Sharing\"\n        member.spellsVersion = memberInfo.spellsVersion\n        \n        if member.name then\n            prdebug(DEBUG_TRACKING, member.name, \"is using ZenTracker with spell list version\", member.spellsVersion)\n        end\n        \n        for _,watchInfo in pairs(member.watching) do\n            if watchInfo.spellInfo.version <= member.spellsVersion then\n                watchInfo.ignoreSharing = false\n                self:toggleCombatLogHandlers(watchInfo, false, member.specInfo)\n            end\n        end\n        \n        self:sendHandshake()\n        local time = GetTime()\n        for _,watchInfo in pairs(self.members[UnitGUID(\"player\")].watching) do\n            if watchInfo.expiration > time then\n                self:sendCDUpdate(watchInfo)\n            end\n        end\n    end\nend\n\n--##############################################################################\n-- Handling raid and M+ encounters\n\nfunction ZT:resetEncounterCDs()\n    for _,member in pairs(self.members) do\n        local resetMemberCDs = not member.isPlayer and member.tracking ~= \"Sharing\"\n\n        for _,watchInfo in pairs(member.watching) do\n            if resetMemberCDs and watchInfo.duration >= 180 then\n                watchInfo.charges = watchInfo.spellInfo.charges\n                watchInfo:updateCDRemaining(0)\n            end\n\n            -- If spell uses lazy tracking and it was triggered, reset lazy tracking at this point\n            if watchInfo.spellInfo.isLazy and not watchInfo.isLazy then\n                watchInfo:sendRemoveEvent()\n                watchInfo.isLazy = true\n            end\n        end\n    end\nend\n\nfunction ZT:startEncounter(event)\n    self.inEncounter = true\n\n    local _,_,_,instanceID = UnitPosition(\"player\")\n    for _,member in pairs(self.members) do\n        local _,_,_,mInstanceID = UnitPosition(self.inspectLib:GuidToUnit(member.GUID))\n        if mInstanceID ~= instanceID then\n            member:hide()\n        else\n            member:unhide() -- Note: Shouldn't be hidden, but just in case...\n        end\n    end\n    \n    if event == \"CHALLENGE_MODE_START\" then\n        self:resetEncounterCDs()\n    end\nend\n\nfunction ZT:endEncounter(event)\n    if self.inEncounter then\n        self.inEncounter = false\n        for _,member in pairs(self.members) do\n            member:unhide()\n        end\n    end\n    \n    if event == \"ENCOUNTER_END\" then\n        self:resetEncounterCDs()\n    end\nend\n\n--##############################################################################\n-- Handling the exchange of addon messages with other ZT clients\n--\n-- Message Format = <Protocol Version (%d)>:<Message Type (%s)>:<Member GUID (%s)>...\n--   Type = \"H\" (Handshake)\n--     ...:<Spec ID (%d)>:<Talents (%s)>:<IsInitial? (%d) [2]>:<Spells Version (%d) [2]>\n--   Type = \"U\" (CD Update)\n--     ...:<Spell ID (%d)>:<Duration (%f)>:<Remaining (%f)>:<#Charges (%d) [3]>\n\nZT.protocolVersion = 3\n\nZT.timeBetweenHandshakes = 5 --seconds\nZT.timeOfNextHandshake = 0\nZT.handshakeTimer = nil\n\nZT.timeBetweenCDUpdates = 5 --seconds (per spellID)\nZT.timeOfNextCDUpdate = {}\nZT.updateTimers = {}\n\nlocal function sendMessage(message)\n    prdebug(DEBUG_MESSAGE, \"Sending message '\"..message..\"'\")\n\n    if not IsInGroup() and not IsInRaid() then\n        return\n    end\n\n    local channel = IsInGroup(2) and \"INSTANCE_CHAT\" or \"RAID\"\n    C_ChatInfo.SendAddonMessage(\"ZenTracker\", message, channel)\nend\n\nZT.hasSentHandshake = false\nfunction ZT:sendHandshake(specInfo)\n    local time = GetTime()\n    if time < self.timeOfNextHandshake then\n        if not self.handshakeTimer then\n            self.handshakeTimer = self.timers:add(self.timeOfNextHandshake, function() self:sendHandshake() end)\n        end\n        return\n    end\n    \n    local GUID = UnitGUID(\"player\")\n    if not self.members[GUID] then\n        return -- This may happen when rejoining a group after login, so ignore this attempt to send a handshake\n    end\n\n    specInfo = specInfo or self.members[GUID].specInfo\n    local specID = specInfo.specID or 0\n    local talents = specInfo.talents or \"\"\n    local isInitial = self.hasSentHandshake and 0 or 1\n    local message = string.format(\"%d:H:%s:%d:%s:%d:%d\", self.protocolVersion, GUID, specID, talents, isInitial, self.spellsVersion)\n    sendMessage(message)\n    \n    self.hasSentHandshake = true\n    self.timeOfNextHandshake = time + self.timeBetweenHandshakes\n    if self.handshakeTimer then\n        self.timers:cancel(self.handshakeTimer)\n        self.handshakeTimer = nil\n    end\nend\n\nfunction ZT:sendCDUpdate(watchInfo, ignoreRateLimit)\n    local spellID = watchInfo.spellInfo.spellID\n    local time = GetTime()\n\n    local timer = self.updateTimers[spellID]\n    if ignoreRateLimit then\n        if timer then\n            self.timers:cancel(timer)\n            self.updateTimers[spellID] = nil\n        end\n    elseif timer then\n        return\n    else\n        local timeOfNextCDUpdate = self.timeOfNextCDUpdate[spellID]\n        if timeOfNextCDUpdate and (time < timeOfNextCDUpdate) then\n            self.updateTimers[spellID] = self.timers:add(timeOfNextCDUpdate, function() self:sendCDUpdate(watchInfo, true) end)\n            return\n        end\n    end\n    \n    local GUID = watchInfo.member.GUID\n    local duration = watchInfo.duration\n    local remaining = watchInfo.expiration - time\n    if remaining < 0 then\n        remaining = 0\n    end\n    local charges = watchInfo.charges and tostring(watchInfo.charges) or \"-\"\n    local message = string.format(\"%d:U:%s:%d:%0.2f:%0.2f:%s\", self.protocolVersion, GUID, spellID, duration, remaining, charges)\n    sendMessage(message)\n    \n    self.timeOfNextCDUpdate[spellID] = time + self.timeBetweenCDUpdates\nend\n\nfunction ZT:handleHandshake(mGUID, specID, talents, isInitial, spellsVersion)\n    specID = tonumber(specID)\n    if specID == 0 then\n        specID = nil\n    end\n    \n    local talentsMap = {}\n    if talents ~= \"\" then\n        for index in talents:gmatch(\"%d%d\") do\n            index = tonumber(index)\n            talentsMap[index] = true\n        end\n    else\n        talents = nil\n    end\n    \n    -- Protocol V2: Assume false if not present\n    if isInitial == \"1\" then\n        isInitial = true\n    else\n        isInitial = false\n    end\n    \n    -- Protocol V2: Assume spellsVersion is 1 if not present\n    if spellsVersion then\n        spellsVersion = tonumber(spellsVersion)\n        if not spellsVersion then\n            spellsVersion = 1\n        end\n    else\n        spellsVersion = 1\n    end\n    \n    local memberInfo = {\n        GUID = mGUID,\n        specInfo = {\n            specID = specID,\n            talents = talents,\n            talentsMap = talentsMap,\n        },\n        tracking = \"Sharing\",\n        spellsVersion = spellsVersion,\n    }\n    \n    self:addOrUpdateMember(memberInfo)\n    if isInitial then\n        self:sendHandshake()\n    end\nend\n\nfunction ZT:handleCDUpdate(mGUID, spellID, duration, remaining, charges)\n    local member = self.members[mGUID]\n    if not member or not member.isReady then\n        return\n    end\n    \n    spellID = tonumber(spellID)\n    duration = tonumber(duration)\n    remaining = tonumber(remaining)\n    if not spellID or not duration or not remaining then\n        return\n    end\n    \n    local sources = self.watching[spellID]\n    if sources then\n        local watchInfo = sources[member.GUID]\n        if not watchInfo or watchInfo.ignoreSharing then\n            return\n        end\n       \n        -- Protocol V3: Ignore charges if not present\n        -- (Note that this shouldn't happen because of spell list version handling)\n        if charges then\n            charges = tonumber(charges)\n            if charges then\n                watchInfo.charges = charges\n            end\n        end\n\n        watchInfo.duration = duration\n        watchInfo.expiration = GetTime() + remaining\n        watchInfo:sendTriggerEvent()\n    end\nend\n\nfunction ZT:handleMessage(message)\n    local protocolVersion, type, mGUID, arg1, arg2, arg3, arg4, arg5 = strsplit(\":\", message)\n    \n    -- Ignore any messages sent by the player\n    if mGUID == UnitGUID(\"player\") then\n        return\n    end\n    \n    prdebug(DEBUG_MESSAGE, \"Received message '\"..message..\"'\")\n    \n    if type == \"H\" then     -- Handshake\n        self:handleHandshake(mGUID, arg1, arg2, arg3, arg4, arg5)\n    elseif type == \"U\" then -- CD Update\n        self:handleCDUpdate(mGUID, arg1, arg2, arg3, arg4, arg5)\n    else\n        return\n    end\nend\n\nif not C_ChatInfo.RegisterAddonMessagePrefix(\"ZenTracker\") then\n    prerror(\"Could not register addon message prefix. Defaulting to local-only cooldown tracking.\")\nend\n\n--##############################################################################\n-- Callback functions for libGroupInspecT for updating/removing members\n\nZT.delayedUpdates = {}\n\nfunction ZT:libInspectUpdate(event, GUID, unit, info)\n    local specID = info.global_spec_id\n    if specID == 0 then\n        specID = nil\n    end\n    \n    local talents = nil\n    local talentsMap = {}\n    if info.talents then\n        for _,talentInfo in pairs(info.talents) do\n            local index = (talentInfo.tier * 10) + talentInfo.column\n            if not talents then\n                talents = \"\"..index\n            else\n                talents = talents..\",\"..index\n            end\n            \n            talentsMap[index] = true\n        end\n    end\n    \n    local memberInfo = {\n        GUID = GUID,\n        specInfo = {\n            specID = specID,\n            talents = talents,\n            talentsMap = talentsMap,\n        },\n    }\n    \n    if not self.delayedUpdates then\n        self:addOrUpdateMember(memberInfo)\n    else\n        self.delayedUpdates[#self.delayedUpdates + 1] = memberInfo\n    end\nend\n\nfunction ZT:libInspectRemove(event, GUID)\n    local member = self.members[GUID]\n    if not member then\n        return\n    end\n    \n    for _,watchInfo in pairs(member.watching) do\n        self:unwatch(watchInfo.spellInfo, member)\n    end\n    self.members[GUID] = nil\nend\n\nfunction ZT:handleDelayedUpdates()\n    if self.delayedUpdates then\n        for _,memberInfo in ipairs(self.delayedUpdates) do\n            self:addOrUpdateMember(memberInfo)\n        end\n        self.delayedUpdates = nil\n    end\nend\n\nZT.inspectLib = LibStub:GetLibrary(\"LibGroupInSpecT-1.1\", true)\n\nif ZT.inspectLib then\n    WeakAurasSaved[\"ZenTracker_AuraEnv\"] = nil -- Flushing out past garbage :)\n\n    local prevZT = _G[\"ZenTracker_AuraEnv\"]\n    if prevZT then\n        ZT.inspectLib.UnregisterAllCallbacks(prevZT)\n        if prevZT.timers then\n            prevZT.timers.heap = {}\n        end\n    end\n    _G[\"ZenTracker_AuraEnv\"] = ZT\n\n    -- If prevZT exists, we know it wasn't a login or reload. If it doesn't exist,\n    -- it still might not be a login or reload if the user is installing ZenTracker\n    -- for the first time. IsLoginFinished() takes care of the second case.\n    if prevZT or WeakAuras.IsLoginFinished() then\n        ZT.delayedUpdates = nil\n    end\n    \n    ZT.inspectLib.RegisterCallback(ZT, \"GroupInSpecT_Update\", \"libInspectUpdate\")\n    ZT.inspectLib.RegisterCallback(ZT, \"GroupInSpecT_Remove\", \"libInspectRemove\")\n\n    for unit in WA_IterateGroupMembers() do\n        local GUID = UnitGUID(unit)\n        if GUID then\n            local info = ZT.inspectLib:GetCachedInfo(GUID)\n            if info then\n                ZT:libInspectUpdate(\"Init\", GUID, unit, info)\n            else\n                ZT.inspectLib:Rescan(GUID)\n            end\n        end\n    end\nelse\n    prerror(\"LibGroupInSpecT-1.1 not found\")\nend\n",
					["do_custom"] = true,
				},
			},
			["triggers"] = {
				{
					["trigger"] = {
						["type"] = "custom",
						["custom_type"] = "event",
						["debuffType"] = "HELPFUL",
						["genericShowOn"] = "showOnActive",
						["subeventPrefix"] = "SPELL",
						["unit"] = "player",
						["subeventSuffix"] = "_CAST_START",
						["spellIds"] = {
						},
						["names"] = {
						},
						["events"] = "COMBAT_LOG_EVENT_UNFILTERED",
						["custom"] = "function(event,...)\n    if event == \"COMBAT_LOG_EVENT_UNFILTERED\" then\n        local _, eventType, _, sourceGUID, _, _, _, destGUID, _, _, _, spellID = ...\n        aura_env.eventHandlers:handle(eventType, spellID, sourceGUID)\n    end\nend\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n",
						["event"] = "Health",
						["custom_hide"] = "timed",
					},
					["untrigger"] = {
					},
				}, -- [1]
				{
					["trigger"] = {
						["subeventPrefix"] = "SPELL",
						["type"] = "custom",
						["events"] = "SPELL_COOLDOWN_CHANGED, SPELL_COOLDOWN_READY",
						["custom_type"] = "event",
						["subeventSuffix"] = "_CAST_START",
						["custom"] = "function (event, id)\n    aura_env.eventHandlers:handle(event, id, 0)\nend",
						["event"] = "Health",
						["custom_hide"] = "timed",
					},
					["untrigger"] = {
					},
				}, -- [2]
				{
					["trigger"] = {
						["type"] = "custom",
						["unevent"] = "auto",
						["event"] = "Chat Message",
						["subeventPrefix"] = "SPELL",
						["events"] = "ENCOUNTER_START,ENCOUNTER_END, CHALLENGE_MODE_START,CHALLENGE_MODE_COMPLETED,PLAYER_ENTERING_WORLD",
						["custom_type"] = "event",
						["subeventSuffix"] = "_CAST_START",
						["custom"] = "function(event)\n    if event == \"ENCOUNTER_START\" or event == \"ENCOUNTER_END\" then\n        local _,instanceType = IsInInstance()\n        if instanceType ~= \"raid\" then\n            return\n        end\n    end\n    \n    if event == \"ENCOUNTER_START\" or event == \"CHALLENGE_MODE_START\" then\n        aura_env:startEncounter(event)\n    elseif event == \"ENCOUNTER_END\" or event == \"CHALLENGE_MODE_COMPLETED\" or event == \"PLAYER_ENTERING_WORLD\" then\n        aura_env:endEncounter(event)\n    end\nend",
						["custom_hide"] = "timed",
					},
					["untrigger"] = {
					},
				}, -- [3]
				{
					["trigger"] = {
						["type"] = "custom",
						["custom_type"] = "event",
						["use_absorbMode"] = true,
						["event"] = "Chat Message",
						["subeventPrefix"] = "SPELL",
						["unit"] = "player",
						["custom"] = "function (event, type, frontendID)\n    if event == \"ZT_REGISTER\" then\n        aura_env:registerFrontEnd(frontendID, type)\n    elseif event == \"ZT_UNREGISTER\" then\n        aura_env:unregisterFrontEnd(frontendID, type)\n    end\nend",
						["subeventSuffix"] = "_CAST_START",
						["use_unit"] = true,
						["events"] = "ZT_REGISTER, ZT_UNREGISTER",
						["unevent"] = "auto",
						["custom_hide"] = "timed",
					},
					["untrigger"] = {
					},
				}, -- [4]
				{
					["trigger"] = {
						["custom_hide"] = "timed",
						["type"] = "custom",
						["events"] = "CHAT_MSG_ADDON",
						["subeventSuffix"] = "_CAST_START",
						["custom_type"] = "event",
						["custom"] = "function(event,prefix,message,type,sender)\n    if prefix == \"ZenTracker\" then\n        aura_env:handleMessage(message)\n    end\nend",
						["event"] = "Health",
						["subeventPrefix"] = "SPELL",
					},
					["untrigger"] = {
					},
				}, -- [5]
				{
					["trigger"] = {
						["subeventPrefix"] = "SPELL",
						["type"] = "custom",
						["events"] = "GROUP_JOINED",
						["custom_type"] = "event",
						["subeventSuffix"] = "_CAST_START",
						["custom"] = "function()\n    aura_env:sendHandshake()\nend",
						["event"] = "Health",
						["custom_hide"] = "timed",
					},
					["untrigger"] = {
					},
				}, -- [6]
				{
					["trigger"] = {
						["type"] = "custom",
						["subeventSuffix"] = "_CAST_START",
						["event"] = "Chat Message",
						["subeventPrefix"] = "SPELL",
						["events"] = "SPELLS_CHANGED",
						["custom_type"] = "event",
						["check"] = "event",
						["custom"] = "function()\n    aura_env:handleDelayedUpdates()\nend",
						["unevent"] = "timed",
						["custom_hide"] = "timed",
					},
					["untrigger"] = {
					},
				}, -- [7]
				["disjunctive"] = "any",
				["activeTriggerMode"] = -10,
			},
			["internalVersion"] = 26,
			["selfPoint"] = "BOTTOM",
			["font"] = "Friz Quadrata TT",
			["version"] = 66,
			["height"] = 12.000001907349,
			["load"] = {
				["talent2"] = {
					["multi"] = {
					},
				},
				["use_never"] = false,
				["talent"] = {
					["multi"] = {
					},
				},
				["class"] = {
					["multi"] = {
					},
				},
				["ingroup"] = {
					["single"] = "group",
					["multi"] = {
						["group"] = true,
					},
				},
				["difficulty"] = {
					["multi"] = {
					},
				},
				["race"] = {
					["multi"] = {
					},
				},
				["faction"] = {
					["multi"] = {
					},
				},
				["pvptalent"] = {
					["multi"] = {
					},
				},
				["affixes"] = {
					["multi"] = {
					},
				},
				["role"] = {
					["multi"] = {
					},
				},
				["spec"] = {
					["multi"] = {
					},
				},
				["size"] = {
					["multi"] = {
					},
				},
			},
			["fontSize"] = 12,
			["regionType"] = "text",
			["_table"] = "d",
			["width"] = 6.9999642372131,
			["color"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
			},
			["automaticWidth"] = "Auto",
			["justify"] = "LEFT",
			["tocversion"] = 80200,
			["id"] = "ZenTracker (ZT) Main",
			["conditions"] = {
			},
			["frameStrata"] = 1,
			["anchorFrameType"] = "SCREEN",
			["config"] = {
				["custom2"] = "{type=\"UTILITY\",spellID=260783,baseCD=120,class=Warrior}",
				["custom8"] = "",
				["custom15"] = "",
				["custom5"] = "",
				["custom6"] = "",
				["custom4"] = "",
				["custom1"] = "{type=\"UTILITY\",spellID=18499,baseCD=60,class=Warrior}",
				["custom7"] = "",
				["custom14"] = "",
				["custom13"] = "",
				["custom12"] = "",
				["ignoreList"] = "{\"Admintoxiceu\",\"Obstoto\",\"Obsspoon\"}",
				["custom11"] = "",
				["custom9"] = "",
				["custom3"] = "",
				["custom10"] = "",
			},
			["uid"] = "ejYTBRnd5n5",
			["animation"] = {
				["start"] = {
					["duration_type"] = "seconds",
					["type"] = "none",
				},
				["main"] = {
					["duration_type"] = "seconds",
					["type"] = "none",
				},
				["finish"] = {
					["duration_type"] = "seconds",
					["type"] = "none",
				},
			},
			["semver"] = "1.1.11",
			["fixedWidth"] = 200,
			["authorOptions"] = {
				{
					["fontSize"] = "large",
					["type"] = "description",
					["text"] = "Custom Spell List",
					["width"] = 2,
				}, -- [1]
				{
					["fontSize"] = "medium",
					["type"] = "description",
					["text"] = "You may add custom entries to the spell list here. The format consists of a Lua table with mandatory entries of type, spellID, baseCD and optional entries of class, specs, race, mods, modTalents, reqTalents. You can look at how such values are defined in the Actions tab 'OnInit' code.",
					["width"] = 2,
				}, -- [2]
				{
					["fontSize"] = "medium",
					["type"] = "description",
					["text"] = "Example: {type=\"INTERRUPT\", spellID=183752, class=DH, baseCD=15}",
					["width"] = 2,
				}, -- [3]
				{
					["fontSize"] = "medium",
					["type"] = "description",
					["text"] = " ",
					["width"] = 2,
				}, -- [4]
				{
					["type"] = "input",
					["key"] = "custom1",
					["useLength"] = false,
					["name"] = "Spell 1",
					["length"] = 10,
					["default"] = "",
					["width"] = 2,
				}, -- [5]
				{
					["type"] = "input",
					["key"] = "custom2",
					["useLength"] = false,
					["name"] = "Spell 2",
					["length"] = 10,
					["default"] = "",
					["width"] = 2,
				}, -- [6]
				{
					["type"] = "input",
					["key"] = "custom3",
					["useLength"] = false,
					["name"] = "Spell 3",
					["length"] = 10,
					["default"] = "",
					["width"] = 2,
				}, -- [7]
				{
					["type"] = "input",
					["key"] = "custom4",
					["useLength"] = false,
					["name"] = "Spell 4",
					["length"] = 10,
					["default"] = "",
					["width"] = 2,
				}, -- [8]
				{
					["type"] = "input",
					["key"] = "custom5",
					["useLength"] = false,
					["name"] = "Spell 5",
					["length"] = 10,
					["default"] = "",
					["width"] = 2,
				}, -- [9]
				{
					["type"] = "input",
					["key"] = "custom6",
					["useLength"] = false,
					["name"] = "Spell 6",
					["length"] = 10,
					["default"] = "",
					["width"] = 2,
				}, -- [10]
				{
					["type"] = "input",
					["key"] = "custom7",
					["useLength"] = false,
					["name"] = "Spell 7",
					["length"] = 10,
					["default"] = "",
					["width"] = 2,
				}, -- [11]
				{
					["type"] = "input",
					["key"] = "custom8",
					["useLength"] = false,
					["name"] = "Spell 8",
					["length"] = 10,
					["default"] = "",
					["width"] = 2,
				}, -- [12]
				{
					["type"] = "input",
					["key"] = "custom9",
					["useLength"] = false,
					["name"] = "Spell 9",
					["length"] = 10,
					["default"] = "",
					["width"] = 2,
				}, -- [13]
				{
					["type"] = "input",
					["key"] = "custom10",
					["useLength"] = false,
					["name"] = "Spell 10",
					["length"] = 10,
					["default"] = "",
					["width"] = 2,
				}, -- [14]
				{
					["type"] = "input",
					["key"] = "custom11",
					["useLength"] = false,
					["name"] = "Spell 11",
					["length"] = 10,
					["default"] = "",
					["width"] = 2,
				}, -- [15]
				{
					["type"] = "input",
					["key"] = "custom12",
					["useLength"] = false,
					["name"] = "Spell 12",
					["length"] = 10,
					["default"] = "",
					["width"] = 2,
				}, -- [16]
				{
					["type"] = "input",
					["key"] = "custom13",
					["useLength"] = false,
					["name"] = "Spell 13",
					["length"] = 10,
					["default"] = "",
					["width"] = 2,
				}, -- [17]
				{
					["type"] = "input",
					["key"] = "custom14",
					["useLength"] = false,
					["name"] = "Spell 14",
					["length"] = 10,
					["default"] = "",
					["width"] = 2,
				}, -- [18]
				{
					["type"] = "input",
					["key"] = "custom15",
					["useLength"] = false,
					["name"] = "Spell 15",
					["length"] = 10,
					["default"] = "",
					["width"] = 2,
				}, -- [19]
				{
					["fontSize"] = "medium",
					["type"] = "description",
					["text"] = " ",
					["width"] = 2,
				}, -- [20]
				{
					["fontSize"] = "medium",
					["type"] = "description",
					["text"] = " ",
					["width"] = 2,
				}, -- [21]
				{
					["fontSize"] = "large",
					["type"] = "description",
					["text"] = "Ignore List",
					["width"] = 2,
				}, -- [22]
				{
					["fontSize"] = "medium",
					["type"] = "description",
					["text"] = "You can provide a list of player names here whose cooldowns should be ignored.",
					["width"] = 2,
				}, -- [23]
				{
					["fontSize"] = "medium",
					["type"] = "description",
					["text"] = "Example: {\"Zenlia\", \"Cistara\"}",
					["width"] = 2,
				}, -- [24]
				{
					["fontSize"] = "medium",
					["type"] = "description",
					["text"] = " ",
					["width"] = 2,
				}, -- [25]
				{
					["type"] = "input",
					["key"] = "ignoreList",
					["useLength"] = false,
					["name"] = "",
					["length"] = 10,
					["default"] = "",
					["width"] = 2,
				}, -- [26]
				{
					["fontSize"] = "medium",
					["type"] = "description",
					["text"] = " ",
					["width"] = 2,
				}, -- [27]
				{
					["fontSize"] = "medium",
					["type"] = "description",
					["text"] = " ",
					["width"] = 2,
				}, -- [28]
			},
			["wordWrap"] = "WordWrap",
	}

  WeakAurasSaved["displays"]["Entangling roots"] = {
    ["cooldownTextEnabled"] = true,
    ["preferToUpdate"] = false,
    ["yOffset"] = 0,
    ["anchorPoint"] = "CENTER",
    ["cooldownSwipe"] = true,
    ["customTextUpdate"] = "update",
    ["cooldownEdge"] = false,
    ["actions"] = {
        ["start"] = {
        },
        ["init"] = {
        },
        ["finish"] = {
        },
    },
    ["triggers"] = {
        [1] = {
            ["trigger"] = {
                ["spellId"] = 2637,
                ["useGroup_count"] = false,
                ["matchesShowOn"] = "showOnActive",
                ["names"] = {
                    [1] = "Blind",
                },
                ["unitExists"] = false,
                ["use_tooltip"] = false,
                ["debuffType"] = "HARMFUL",
                ["showClones"] = true,
                ["type"] = "aura2",
                ["use_debuffClass"] = false,
                ["subeventSuffix"] = "_CAST_START",
                ["event"] = "Health",
                ["subeventPrefix"] = "SPELL",
                ["unit"] = "multi",
                ["useName"] = true,
                ["name"] = "Hibernate",
                ["use_specific_unit"] = false,
                ["spellIds"] = {
                    [1] = 2094,
                },
                ["combineMatches"] = "showLowest",
                ["auranames"] = {
                    [1] = "201589",
                },
                ["buffShowOn"] = "showOnActive",
            },
            ["untrigger"] = {
            },
        },
        ["activeTriggerMode"] = -10,
    },
    ["internalVersion"] = 29,
    ["selfPoint"] = "CENTER",
    ["semver"] = "1.0.0",
    ["animation"] = {
        ["start"] = {
            ["type"] = "preset",
            ["easeType"] = "none",
            ["preset"] = "fade",
            ["easeStrength"] = 3,
            ["duration_type"] = "seconds",
        },
        ["main"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["finish"] = {
            ["type"] = "preset",
            ["easeType"] = "none",
            ["preset"] = "fade",
            ["easeStrength"] = 3,
            ["duration_type"] = "seconds",
        },
    },
    ["stickyDuration"] = false,
    ["progressPrecision"] = 0,
    ["version"] = 10,
    ["subRegions"] = {
        [1] = {
            ["text_shadowXOffset"] = 0,
            ["text_text"] = "%unitName",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "AUTO",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_color"] = {
                [1] = 1,
                [2] = 1,
                [3] = 1,
                [4] = 1,
            },
            ["text_font"] = "FrancoisOne",
            ["text_shadowYOffset"] = 0,
            ["text_wordWrap"] = "WordWrap",
            ["text_visible"] = false,
            ["text_anchorPoint"] = "OUTER_BOTTOM",
            ["text_fontSize"] = 12,
            ["anchorXOffset"] = 0,
            ["text_fontType"] = "OUTLINE",
        },
        [2] = {
            ["text_shadowXOffset"] = 0,
            ["text_text"] = "%p",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "AUTO",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_color"] = {
                [1] = 1,
                [2] = 1,
                [3] = 1,
                [4] = 1,
            },
            ["text_font"] = "FrancoisOne",
            ["text_shadowYOffset"] = 0,
            ["text_wordWrap"] = "WordWrap",
            ["text_visible"] = true,
            ["text_anchorPoint"] = "CENTER",
            ["text_fontSize"] = 20,
            ["anchorXOffset"] = 0,
            ["text_fontType"] = "OUTLINE",
        },
    },
    ["height"] = 44,
    ["icon"] = true,
    ["conditions"] = {
    },
    ["url"] = "https://wago.io/Sy0JESg9Q/10",
    ["load"] = {
        ["use_class"] = true,
        ["use_petbattle"] = false,
        ["ingroup"] = {
            ["single"] = "group",
            ["multi"] = {
                ["group"] = true,
            },
        },
        ["use_vehicleUi"] = false,
        ["use_vehicle"] = false,
        ["class"] = {
            ["single"] = "DRUID",
            ["multi"] = {
                ["DRUID"] = true,
            },
        },
        ["spec"] = {
            ["single"] = 1,
            ["multi"] = {
                [1] = true,
            },
        },
        ["size"] = {
            ["single"] = "arena",
            ["multi"] = {
                ["scenario"] = true,
                ["ten"] = true,
                ["twentyfive"] = true,
                ["fortyman"] = true,
                ["party"] = true,
                ["flexible"] = true,
                ["arena"] = true,
                ["twenty"] = true,
                ["pvp"] = true,
            },
        },
    },
    ["config"] = {
    },
    ["keepAspectRatio"] = true,
    ["regionType"] = "icon",
    ["anchorFrameType"] = "SCREEN",
    ["frameStrata"] = 1,
    ["authorOptions"] = {
    },
    ["desaturate"] = false,
    ["zoom"] = 0.3,
    ["width"] = 44,
    ["cooldownTextDisabled"] = false,
    ["auto"] = false,
    ["tocversion"] = 80205,
    ["id"] = "Entangling roots",
    ["color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["alpha"] = 1,
    ["uid"] = "gNDWCw1oD6g",
    ["inverse"] = false,
    ["xOffset"] = 50,
    ["displayIcon"] = "136100",
    ["cooldown"] = false,
  }

  WeakAurasSaved["displays"]["Hibernate"] = {
    ["authorOptions"] = {
    },
    ["preferToUpdate"] = false,
    ["yOffset"] = 0,
    ["anchorPoint"] = "CENTER",
    ["cooldownSwipe"] = true,
    ["customTextUpdate"] = "update",
    ["cooldownEdge"] = false,
    ["icon"] = true,
    ["internalVersion"] = 29,
    ["keepAspectRatio"] = true,
    ["selfPoint"] = "CENTER",
    ["auto"] = false,
    ["stickyDuration"] = false,
    ["progressPrecision"] = 0,
    ["actions"] = {
        ["start"] = {
        },
        ["finish"] = {
        },
        ["init"] = {
        },
    },
    ["version"] = 10,
    ["subRegions"] = {
        [1] = {
            ["text_shadowXOffset"] = 0,
            ["text_text"] = "%unitName",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "AUTO",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_color"] = {
                [1] = 1,
                [2] = 1,
                [3] = 1,
                [4] = 1,
            },
            ["text_font"] = "FrancoisOne",
            ["text_shadowYOffset"] = 0,
            ["text_wordWrap"] = "WordWrap",
            ["text_visible"] = false,
            ["text_anchorPoint"] = "OUTER_BOTTOM",
            ["text_fontSize"] = 12,
            ["anchorXOffset"] = 0,
            ["text_fontType"] = "OUTLINE",
        },
        [2] = {
            ["text_shadowXOffset"] = 0,
            ["text_text"] = "%p",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "AUTO",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_color"] = {
                [1] = 1,
                [2] = 1,
                [3] = 1,
                [4] = 1,
            },
            ["text_font"] = "FrancoisOne",
            ["text_shadowYOffset"] = 0,
            ["text_wordWrap"] = "WordWrap",
            ["text_visible"] = true,
            ["text_anchorPoint"] = "CENTER",
            ["text_fontSize"] = 20,
            ["anchorXOffset"] = 0,
            ["text_fontType"] = "OUTLINE",
        },
    },
    ["height"] = 44,
    ["triggers"] = {
        [1] = {
            ["trigger"] = {
                ["spellId"] = 2637,
                ["auranames"] = {
                    [1] = "2637",
                },
                ["matchesShowOn"] = "showOnActive",
                ["names"] = {
                    [1] = "Blind",
                },
                ["unitExists"] = false,
                ["use_tooltip"] = false,
                ["buffShowOn"] = "showOnActive",
                ["showClones"] = true,
                ["useName"] = true,
                ["use_debuffClass"] = false,
                ["subeventSuffix"] = "_CAST_START",
                ["event"] = "Health",
                ["debuffType"] = "HARMFUL",
                ["useGroup_count"] = false,
                ["type"] = "aura2",
                ["name"] = "Hibernate",
                ["spellIds"] = {
                    [1] = 2094,
                },
                ["use_specific_unit"] = false,
                ["combineMatches"] = "showLowest",
                ["subeventPrefix"] = "SPELL",
                ["unit"] = "multi",
            },
            ["untrigger"] = {
            },
        },
        ["activeTriggerMode"] = -10,
    },
    ["load"] = {
        ["use_class"] = true,
        ["ingroup"] = {
            ["single"] = "group",
            ["multi"] = {
                ["group"] = true,
            },
        },
        ["use_petbattle"] = false,
        ["use_vehicleUi"] = false,
        ["use_vehicle"] = false,
        ["class"] = {
            ["single"] = "DRUID",
            ["multi"] = {
                ["DRUID"] = true,
            },
        },
        ["spec"] = {
            ["single"] = 1,
            ["multi"] = {
                [1] = true,
            },
        },
        ["size"] = {
            ["single"] = "arena",
            ["multi"] = {
                ["scenario"] = true,
                ["ten"] = true,
                ["twentyfive"] = true,
                ["fortyman"] = true,
                ["flexible"] = true,
                ["party"] = true,
                ["arena"] = true,
                ["twenty"] = true,
                ["pvp"] = true,
            },
        },
    },
    ["displayIcon"] = 136090,
    ["url"] = "https://wago.io/Sy0JESg9Q/10",
    ["animation"] = {
        ["start"] = {
            ["type"] = "preset",
            ["easeType"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["preset"] = "fade",
        },
        ["main"] = {
            ["duration_type"] = "seconds",
            ["type"] = "none",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["finish"] = {
            ["type"] = "preset",
            ["easeType"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["preset"] = "fade",
        },
    },
    ["uid"] = "PDSRavTB1iM",
    ["desaturate"] = false,
    ["regionType"] = "icon",
    ["alpha"] = 1,
    ["cooldownTextDisabled"] = false,
    ["width"] = 44,
    ["zoom"] = 0.3,
    ["semver"] = "1.0.0",
    ["tocversion"] = 80205,
    ["id"] = "Hibernate",
    ["xOffset"] = -50,
    ["frameStrata"] = 1,
    ["anchorFrameType"] = "SCREEN",
    ["color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["config"] = {
    },
    ["inverse"] = false,
    ["cooldownTextEnabled"] = true,
    ["conditions"] = {
    },
    ["cooldown"] = false,
  }

  WeakAurasSaved["displays"]["well of existence_bar"] = {
    ["sparkWidth"] = 1,
    ["glow"] = false,
    ["text1FontSize"] = 29,
    ["xOffset"] = 452.0009765625,
    ["adjustedMax"] = 245980,
    ["yOffset"] = -158.99969482422,
    ["anchorPoint"] = "CENTER",
    ["sparkRotation"] = 0,
    ["url"] = "https://wago.io/5nROlLjHj/2",
    ["backgroundColor"] = {
        [1] = 0.2156862745098,
        [2] = 0.23529411764706,
        [3] = 0.22352941176471,
        [4] = 1,
    },
    ["icon_color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["text1Enabled"] = true,
    ["keepAspectRatio"] = false,
    ["selfPoint"] = "CENTER",
    ["barColor"] = {
        [1] = 0.27058823529412,
        [2] = 0.62352941176471,
        [3] = 0.27843137254902,
        [4] = 1,
    },
    ["text1Containment"] = "OUTSIDE",
    ["glowColor"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["text1Point"] = "BOTTOM",
    ["sparkOffsetY"] = 0,
    ["text2FontFlags"] = "OUTLINE",
    ["load"] = {
        ["spec"] = {
            ["multi"] = {
            },
        },
        ["class"] = {
            ["multi"] = {
            },
        },
        ["size"] = {
            ["multi"] = {
            },
        },
    },
    ["glowType"] = "buttonOverlay",
    ["text1FontFlags"] = "OUTLINE",
    ["regionType"] = "aurabar",
    ["text2FontSize"] = 24,
    ["texture"] = "Grid2 Flat",
    ["zoom"] = 0,
    ["auto"] = true,
    ["tocversion"] = 80205,
    ["alpha"] = 1,
    ["uid"] = "eGggrV8RcrX",
    ["borderBackdrop"] = "Blizzard Tooltip",
    ["color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["customText"] = "\n\nfunction()\n    local p = aura_env.p * 100.0;\n    return AbbreviateLargeNumbers(aura_env.value or 0), string.format(\"%.0f%%\", p);\nend\n\n\n",
    ["cooldownSwipe"] = true,
    ["customTextUpdate"] = "event",
    ["cooldownEdge"] = false,
    ["preferToUpdate"] = false,
    ["triggers"] = {
        [1] = {
            ["trigger"] = {
                ["tooltipValueNumber"] = 1,
                ["unit"] = "player",
                ["customIcon"] = "function()\n    return aura_env.icon\nend\n\n\n\n\n",
                ["debuffType"] = "HELPFUL",
                ["type"] = "custom",
                ["names"] = {
                },
                ["custom_type"] = "status",
                ["events"] = "UNIT_AURA",
                ["fetchTooltip"] = true,
                ["event"] = "Health",
                ["useExactSpellId"] = true,
                ["customDuration"] = "function()\n    \n    return aura_env.p, 1.0, true\nend\n\n\n",
                ["customName"] = "\n\n",
                ["spellIds"] = {
                },
                ["subeventSuffix"] = "_CAST_START",
                ["check"] = "event",
                ["custom"] = "function()\n    \n    aura_env.buff_id = 296138;\n    aura_env.max = UnitHealthMax(\"player\");\n    \n    local name, icon, _, _, _, _, _, _, _, _, _, _, _, _, _, value = WA_GetUnitBuff(\"player\", aura_env.buff_id, \"HELPFUL|PLAYER\")\n    \n    aura_env.icon = icon\n    aura_env.value = value or 0\n    aura_env.p = aura_env.value / aura_env.max;\n    \n    return name;\nend",
                ["auraspellids"] = {
                    [1] = "296138",
                },
                ["subeventPrefix"] = "SPELL",
            },
            ["untrigger"] = {
            },
        },
        ["activeTriggerMode"] = -10,
    },
    ["internalVersion"] = 29,
    ["animation"] = {
        ["start"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["main"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["finish"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
    },
    ["backdropInFront"] = false,
    ["stickyDuration"] = false,
    ["text2Enabled"] = false,
    ["sparkColor"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["version"] = 2,
    ["subRegions"] = {
        [1] = {
            ["type"] = "aurabar_bar",
        },
        [2] = {
            ["text_shadowXOffset"] = 1,
            ["text_text"] = "%c2",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "AUTO",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_color"] = {
                [1] = 1,
                [2] = 1,
                [3] = 1,
                [4] = 1,
            },
            ["text_font"] = "Cabin",
            ["text_shadowYOffset"] = -1,
            ["text_wordWrap"] = "WordWrap",
            ["text_visible"] = true,
            ["text_anchorPoint"] = "INNER_RIGHT",
            ["text_fontSize"] = 20,
            ["anchorXOffset"] = 0,
            ["text_fontType"] = "None",
        },
        [3] = {
            ["text_shadowXOffset"] = 1,
            ["text_text"] = "%c1",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "AUTO",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_color"] = {
                [1] = 1,
                [2] = 1,
                [3] = 1,
                [4] = 1,
            },
            ["text_font"] = "Cabin",
            ["text_shadowYOffset"] = -1,
            ["text_wordWrap"] = "WordWrap",
            ["text_visible"] = true,
            ["text_anchorPoint"] = "INNER_LEFT",
            ["text_fontSize"] = 20,
            ["anchorXOffset"] = 0,
            ["text_fontType"] = "None",
        },
        [4] = {
            ["text_shadowXOffset"] = 1,
            ["text_text"] = "%s",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "AUTO",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_color"] = {
                [1] = 1,
                [2] = 1,
                [3] = 1,
                [4] = 1,
            },
            ["text_font"] = "Friz Quadrata TT",
            ["text_shadowYOffset"] = -1,
            ["text_wordWrap"] = "WordWrap",
            ["text_visible"] = false,
            ["text_anchorPoint"] = "ICON_CENTER",
            ["text_fontSize"] = 12,
            ["anchorXOffset"] = 0,
            ["text_fontType"] = "None",
        },
        [5] = {
            ["border_offset"] = 5,
            ["border_anchor"] = "bar",
            ["type"] = "subborder",
            ["border_color"] = {
                [1] = 1,
                [2] = 1,
                [3] = 1,
                [4] = 0.5,
            },
            ["border_visible"] = false,
            ["border_edge"] = "None",
            ["border_size"] = 16,
        },
    },
    ["height"] = 32,
    ["spark"] = true,
    ["actions"] = {
        ["start"] = {
        },
        ["init"] = {
        },
        ["finish"] = {
        },
    },
    ["sparkBlendMode"] = "ADD",
    ["useAdjustededMax"] = false,
    ["useAdjustededMin"] = false,
    ["text2Containment"] = "INSIDE",
    ["useglowColor"] = false,
    ["text1Font"] = "2002",
    ["backdropColor"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 0.5,
    },
    ["text2"] = "%p",
    ["text2Color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["anchorFrameType"] = "SCREEN",
    ["borderInFront"] = true,
    ["sparkOffsetX"] = 0,
    ["icon_side"] = "LEFT",
    ["text2Point"] = "CENTER",
    ["sparkHidden"] = "NEVER",
    ["sparkHeight"] = 32,
    ["text1Color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["sparkRotationMode"] = "AUTO",
    ["authorOptions"] = {
    },
    ["semver"] = "1.0.1",
    ["text2Font"] = "Friz Quadrata TT",
    ["id"] = "well of existence_bar",
    ["text1"] = "%c",
    ["frameStrata"] = 1,
    ["width"] = 200,
    ["cooldownTextDisabled"] = false,
    ["icon"] = true,
    ["inverse"] = false,
    ["desaturate"] = false,
    ["orientation"] = "HORIZONTAL",
    ["conditions"] = {
        [1] = {
            ["check"] = {
                ["trigger"] = 1,
                ["op"] = ">",
                ["variable"] = "value",
                ["value"] = "0.5",
            },
            ["changes"] = {
                [1] = {
                    ["value"] = {
                        [1] = 0.84313725490196,
                        [2] = 0.69019607843137,
                        [3] = 0.2156862745098,
                        [4] = 1,
                    },
                    ["property"] = "barColor",
                },
                [2] = {
                },
            },
        },
        [2] = {
            ["check"] = {
                ["trigger"] = 1,
                ["op"] = ">",
                ["variable"] = "value",
                ["value"] = "0.9",
            },
            ["changes"] = {
                [1] = {
                    ["value"] = {
                        [1] = 0.71764705882353,
                        [2] = 0.26274509803922,
                        [3] = 0.25490196078431,
                        [4] = 1,
                    },
                    ["property"] = "barColor",
                },
            },
        },
    },
    ["config"] = {
    },
    ["sparkTexture"] = "Interface\AddOns\WeakAuras\Media\Textures\Square_FullWhite",
  }

  WeakAurasSaved["displays"]["SoF"] = {
    ["xOffset"] = -35,
    ["preferToUpdate"] = false,
    ["yOffset"] = 0,
    ["anchorPoint"] = "CENTER",
    ["url"] = "https://wago.io/Hk5yS8yeG/1",
    ["actions"] = {
        ["start"] = {
        },
        ["init"] = {
        },
        ["finish"] = {
        },
    },
    ["triggers"] = {
        [1] = {
            ["trigger"] = {
                ["useGroup_count"] = false,
                ["useName"] = true,
                ["use_debuffClass"] = false,
                ["subeventSuffix"] = "_CAST_START",
                ["auranames"] = {
                    [1] = "Soul of the Forest",
                },
                ["matchesShowOn"] = "showOnActive",
                ["event"] = "Health",
                ["subeventPrefix"] = "SPELL",
                ["type"] = "aura2",
                ["use_tooltip"] = false,
                ["spellIds"] = {
                },
                ["buffShowOn"] = "showOnActive",
                ["names"] = {
                    [1] = "Soul of the Forest",
                },
                ["combineMatches"] = "showLowest",
                ["unit"] = "player",
                ["debuffType"] = "HELPFUL",
            },
            ["untrigger"] = {
            },
        },
        ["activeTriggerMode"] = 1,
    },
    ["internalVersion"] = 29,
    ["selfPoint"] = "CENTER",
    ["desaturate"] = false,
    ["discrete_rotation"] = 0,
    ["version"] = 1,
    ["height"] = 20,
    ["rotate"] = true,
    ["load"] = {
        ["affixes"] = {
            ["multi"] = {
            },
        },
        ["ingroup"] = {
            ["multi"] = {
            },
        },
        ["class_and_spec"] = {
            ["multi"] = {
            },
        },
        ["talent"] = {
            ["single"] = 13,
            ["multi"] = {
            },
        },
        ["spec"] = {
            ["multi"] = {
            },
        },
        ["class"] = {
            ["single"] = "DRUID",
            ["multi"] = {
            },
        },
        ["pvptalent"] = {
            ["multi"] = {
            },
        },
        ["use_talent"] = true,
        ["use_class"] = true,
        ["role"] = {
            ["multi"] = {
            },
        },
        ["talent3"] = {
            ["multi"] = {
            },
        },
        ["faction"] = {
            ["multi"] = {
            },
        },
        ["talent2"] = {
            ["multi"] = {
            },
        },
        ["difficulty"] = {
            ["multi"] = {
            },
        },
        ["race"] = {
            ["multi"] = {
            },
        },
        ["size"] = {
            ["multi"] = {
            },
        },
    },
    ["mirror"] = false,
    ["uid"] = ")nVBRXO8BdS",
    ["regionType"] = "texture",
    ["blendMode"] = "BLEND",
    ["authorOptions"] = {
    },
    ["anchorFrameType"] = "SCREEN",
    ["texture"] = "Interface\\\\AddOns\\\\WeakAuras\\\\Media\\\\Textures\\\\Circle_White",
    ["color"] = {
        [1] = 0,
        [2] = 1,
        [3] = 0.22352941176471,
        [4] = 0.75,
    },
    ["semver"] = "1.0.0",
    ["id"] = "SoF",
    ["frameStrata"] = 1,
    ["width"] = 20,
    ["rotation"] = 0,
    ["config"] = {
    },
    ["conditions"] = {
    },
    ["alpha"] = 1,
    ["animation"] = {
        ["start"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["main"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["finish"] = {
            ["type"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
    },
  }

  WeakAurasSaved["displays"]["Dungeons: M+ Battle Res"] = {
      ["outline"] = "OUTLINE",
      ["authorOptions"] = {
      },
      ["displayText"] = "%c",
      ["customText"] = "function()\n    local _, elapsedTime = GetWorldElapsedTime(1);\n    local _, deathTimeLost = C_ChallengeMode.GetDeathCount();\n    \n    local timeToRes = 600 - mod(elapsedTime - deathTimeLost + 10, 600);\n    local charges, _, started, duration = GetSpellCharges(20484);\n    local formattedTime = (\"%d:%02d\"):format(floor(timeToRes / 60), mod(timeToRes, 60))\n    \n    if charges == nil then \n        charges = 0;\n    end\n    \n    local color = \"|cFFFFFFFF\";\n    if charges < 1 then\n        color = \"|cFFFF0000\";\n    elseif charges > 1 then\n        color = \"|cFF00FF00\";\n    end\n    \n    return \"|cFFAAAAAACR:|r \" .. color .. charges .. \"|r |cFFAAAAAA/|r \" .. formattedTime\nend",
      ["yOffset"] = -364.99966430664,
      ["anchorPoint"] = "CENTER",
      ["customTextUpdate"] = "update",
      ["url"] = "https://wago.io/eclrF8Azq/6",
      ["actions"] = {
          ["start"] = {
          },
          ["init"] = {
              ["do_custom"] = false,
          },
          ["finish"] = {
          },
      },
      ["triggers"] = {
          [1] = {
              ["trigger"] = {
                  ["genericShowOn"] = "showOnActive",
                  ["names"] = {
                  },
                  ["customTexture"] = "\n\n",
                  ["custom_hide"] = "custom",
                  ["type"] = "custom",
                  ["customIcon"] = "\n\n",
                  ["subeventSuffix"] = "_CAST_START",
                  ["events"] = "CHALLENGE_MODE_START,CHALLENGE_MODE_COMPLETED,CHALLENGE_MODE_RESET,PLAYER_ENTERING_WORLD",
                  ["custom"] = "function(...)    \n    local _, _, difficulty, _, _, _, _, _ = GetInstanceInfo();\n    local _, timeCM = GetWorldElapsedTime(1);\n    \n    return difficulty == 8 and timeCM > 0\nend",
                  ["event"] = "Health",
                  ["customStacks"] = "\n\n",
                  ["customDuration"] = "\n\n",
                  ["customName"] = "\n\n",
                  ["spellIds"] = {
                  },
                  ["subeventPrefix"] = "SPELL",
                  ["check"] = "update",
                  ["unit"] = "player",
                  ["custom_type"] = "status",
                  ["debuffType"] = "HELPFUL",
              },
              ["untrigger"] = {
                  ["custom"] = "function(event)\n    if event == \"CHALLENGE_MODE_RESET\" or event == \"CHALLENGE_MODE_COMPLETED\" then \n        return true\n    end \nend",
              },
          },
          ["disjunctive"] = "any",
          ["customTriggerLogic"] = "function(t)\n  return t[1]\nend",
          ["activeTriggerMode"] = -10,
      },
      ["internalVersion"] = 29,
      ["wordWrap"] = "WordWrap",
      ["font"] = "PT Sans Narrow",
      ["version"] = 6,
      ["height"] = 20.000017166138,
      ["load"] = {
          ["ingroup"] = {
              ["multi"] = {
              },
          },
          ["use_never"] = false,
          ["talent"] = {
              ["multi"] = {
              },
          },
          ["class"] = {
              ["multi"] = {
              },
          },
          ["spec"] = {
              ["multi"] = {
              },
          },
          ["difficulty"] = {
              ["single"] = "challenge",
              ["multi"] = {
              },
          },
          ["race"] = {
              ["multi"] = {
              },
          },
          ["talent2"] = {
              ["multi"] = {
              },
          },
          ["faction"] = {
              ["multi"] = {
              },
          },
          ["role"] = {
              ["multi"] = {
              },
          },
          ["use_difficulty"] = true,
          ["pvptalent"] = {
              ["multi"] = {
              },
          },
          ["size"] = {
              ["single"] = "party",
              ["multi"] = {
                  ["party"] = true,
                  ["fortyman"] = true,
                  ["ten"] = true,
                  ["twentyfive"] = true,
                  ["twenty"] = true,
                  ["flexible"] = true,
              },
          },
      },
      ["fontSize"] = 20,
      ["shadowXOffset"] = 1,
      ["regionType"] = "text",
      ["semver"] = "1.1.4",
      ["selfPoint"] = "LEFT",
      ["fixedWidth"] = 200,
      ["animation"] = {
          ["start"] = {
              ["colorR"] = 1,
              ["duration"] = "0.2",
              ["alphaType"] = "straight",
              ["colorB"] = 1,
              ["colorG"] = 1,
              ["alphaFunc"] = "function(progress, start, delta)\n      return start + (progress * delta)\n    end\n  ",
              ["use_translate"] = false,
              ["use_alpha"] = true,
              ["type"] = "none",
              ["easeType"] = "none",
              ["scaley"] = 1,
              ["alpha"] = 0.2,
              ["y"] = 0,
              ["x"] = 0,
              ["colorA"] = 1,
              ["scalex"] = 1,
              ["easeStrength"] = 3,
              ["rotate"] = 0,
              ["duration_type"] = "seconds",
          },
          ["main"] = {
              ["type"] = "none",
              ["duration_type"] = "seconds",
              ["easeStrength"] = 3,
              ["easeType"] = "none",
          },
          ["finish"] = {
              ["colorR"] = 1,
              ["duration_type"] = "seconds",
              ["alphaType"] = "straight",
              ["colorA"] = 1,
              ["colorG"] = 1,
              ["alphaFunc"] = "function(progress, start, delta)\n      return start + (progress * delta)\n    end\n  ",
              ["use_alpha"] = true,
              ["type"] = "none",
              ["easeType"] = "none",
              ["scaley"] = 1,
              ["alpha"] = 0.2,
              ["y"] = 0,
              ["x"] = 0,
              ["colorB"] = 1,
              ["scalex"] = 1,
              ["easeStrength"] = 3,
              ["rotate"] = 0,
              ["duration"] = "0.2",
          },
      },
      ["uid"] = "YlptELEbW(L",
      ["justify"] = "LEFT",
      ["tocversion"] = 80205,
      ["id"] = "Dungeons: M+ Battle Res",
      ["xOffset"] = 366.99975585938,
      ["frameStrata"] = 1,
      ["width"] = 129.99964904785,
      ["color"] = {
          [1] = 1,
          [2] = 1,
          [3] = 1,
          [4] = 1,
      },
      ["config"] = {
      },
      ["anchorFrameType"] = "SCREEN",
      ["shadowYOffset"] = -1,
      ["shadowColor"] = {
          [1] = 0,
          [2] = 0,
          [3] = 0,
          [4] = 1,
      },
      ["conditions"] = {
      },
      ["automaticWidth"] = "Auto",
      ["preferToUpdate"] = false,
  }

  WeakAurasSaved["displays"]["Tank Activating Nyalotha Spire"] = {
      ["color"] = {
          [1] = 1,
          [2] = 1,
          [3] = 1,
          [4] = 1,
      },
      ["preferToUpdate"] = false,
      ["customText"] = "",
      ["yOffset"] = 250,
      ["anchorPoint"] = "CENTER",
      ["cooldownSwipe"] = true,
      ["url"] = "https://wago.io/TankEnteringNyalotha/3",
      ["icon"] = true,
      ["triggers"] = {
          [1] = {
              ["trigger"] = {
                  ["use_castType"] = false,
                  ["spellId"] = 313352,
                  ["use_absorbMode"] = true,
                  ["use_spell"] = true,
                  ["unit"] = "group",
                  ["debuffType"] = "HELPFUL",
                  ["use_unit"] = true,
                  ["type"] = "custom",
                  ["use_clone"] = true,
                  ["unevent"] = "timed",
                  ["duration"] = "1",
                  ["subeventSuffix"] = "_CAST_START",
                  ["event"] = "Chat Message",
                  ["spell"] = "Activating",
                  ["subeventPrefix"] = "SPELL",
                  ["use_spellId"] = false,
                  ["events"] = "UNIT_SPELLCAST_START",
                  ["custom_type"] = "stateupdate",
                  ["check"] = "event",
                  ["spellIds"] = {
                  },
                  ["names"] = {
                  },
                  ["custom"] = "function(s, event, unit, _, spellID)\n    if event == \"UNIT_SPELLCAST_START\" and spellID == 313352 and UnitGroupRolesAssigned(unit) == \"TANK\" then --313352 activating\n        local name, _, icon, startTimeMS, endTimeMS = UnitCastingInfo(unit)\n        local duration = (endTimeMS - startTimeMS)/1000\n        s[unit] = {\n            show = true,\n            changed = true,\n            unit = unit,\n            progressType = \"timed\",\n            expirationTime = endTimeMS/1000,\n            duration = duration,\n            autoHide = true,\n            spellName = name,\n            name = WA_ClassColorName(unit),\n            icon = icon,\n        }\n        if aura_env.config.tankSay and UnitIsUnit(unit,\"player\") then\n            SendChatMessage(aura_env.config.enteringText, \"SAY\")\n        end        \n        return true\n    elseif event == \"UNIT_SPELLCAST_STOP\" and s[unit] then\n        s[unit].show = false\n        s[unit].changed = true\n        return true\n    end\nend",
              },
              ["untrigger"] = {
                  ["unit"] = "group",
              },
          },
          ["disjunctive"] = "any",
          ["customTriggerLogic"] = "\n\n\n\n",
          ["activeTriggerMode"] = -10,
      },
      ["internalVersion"] = 29,
      ["keepAspectRatio"] = false,
      ["selfPoint"] = "CENTER",
      ["desaturate"] = false,
      ["version"] = 3,
      ["subRegions"] = {
          [1] = {
              ["text_shadowXOffset"] = 0,
              ["text_text"] = "%n",
              ["text_shadowColor"] = {
                  [1] = 0,
                  [2] = 0,
                  [3] = 0,
                  [4] = 1,
              },
              ["text_selfPoint"] = "AUTO",
              ["text_automaticWidth"] = "Auto",
              ["text_fixedWidth"] = 64,
              ["anchorYOffset"] = 0,
              ["text_justify"] = "CENTER",
              ["rotateText"] = "NONE",
              ["type"] = "subtext",
              ["text_color"] = {
                  [1] = 1,
                  [2] = 1,
                  [3] = 1,
                  [4] = 1,
              },
              ["text_font"] = "ArchivoNarrow-Bold",
              ["text_shadowYOffset"] = 0,
              ["text_wordWrap"] = "WordWrap",
              ["text_visible"] = true,
              ["text_anchorPoint"] = "OUTER_RIGHT",
              ["text_fontSize"] = 30,
              ["anchorXOffset"] = 0,
              ["text_fontType"] = "OUTLINE",
          },
          [2] = {
              ["glowFrequency"] = 0.25,
              ["type"] = "subglow",
              ["useGlowColor"] = true,
              ["glowScale"] = 1,
              ["glowThickness"] = 1,
              ["glowYOffset"] = 0,
              ["glowColor"] = {
                  [1] = 1,
                  [2] = 0,
                  [3] = 0.57647058823529,
                  [4] = 1,
              },
              ["glowXOffset"] = 0,
              ["glowType"] = "buttonOverlay",
              ["glowLength"] = 10,
              ["glow"] = true,
              ["glowLines"] = 8,
              ["glowBorder"] = false,
          },
      },
      ["height"] = 64,
      ["load"] = {
          ["difficulty"] = {
          },
          ["use_size"] = true,
          ["affixes"] = {
          },
          ["class"] = {
              ["multi"] = {
              },
          },
          ["spec"] = {
              ["multi"] = {
              },
          },
          ["ingroup"] = {
              ["single"] = "group",
              ["multi"] = {
                  ["group"] = true,
                  ["raid"] = true,
              },
          },
          ["size"] = {
              ["single"] = "party",
              ["multi"] = {
              },
          },
      },
      ["actions"] = {
          ["start"] = {
              ["custom"] = "\n\n",
              ["do_custom"] = false,
          },
          ["finish"] = {
          },
          ["init"] = {
          },
      },
      ["regionType"] = "icon",
      ["authorOptions"] = {
          [1] = {
              ["type"] = "toggle",
              ["key"] = "tankSay",
              ["default"] = true,
              ["name"] = "Say Message",
              ["useDesc"] = true,
              ["width"] = 1,
              ["desc"] = "Tank will trigger a Say message in chat when activating.",
          },
          [2] = {
              ["type"] = "input",
              ["useDesc"] = true,
              ["width"] = 1,
              ["default"] = "{rt3}Tank Activating{rt3}",
              ["key"] = "enteringText",
              ["name"] = "Entering Text",
              ["multiline"] = false,
              ["length"] = 10,
              ["desc"] = "The text that a tank says when entering a rift.",
              ["useLength"] = false,
          },
      },
      ["anchorFrameType"] = "SCREEN",
      ["alpha"] = 1,
      ["zoom"] = 0,
      ["config"] = {
          ["enteringText"] = "{rt3}Tank Activating{rt3}",
          ["tankSay"] = true,
      },
      ["conditions"] = {
      },
      ["cooldownTextDisabled"] = false,
      ["semver"] = "1.0.2",
      ["tocversion"] = 80300,
      ["id"] = "Tank Activating Nyalotha Spire",
      ["auto"] = false,
      ["frameStrata"] = 1,
      ["width"] = 64,
      ["xOffset"] = 0,
      ["uid"] = "XaEDFr5Ovto",
      ["inverse"] = false,
      ["animation"] = {
          ["start"] = {
              ["duration_type"] = "seconds",
              ["type"] = "none",
              ["easeStrength"] = 3,
              ["easeType"] = "none",
          },
          ["main"] = {
              ["duration_type"] = "seconds",
              ["type"] = "none",
              ["easeStrength"] = 3,
              ["easeType"] = "none",
          },
          ["finish"] = {
              ["duration_type"] = "seconds",
              ["type"] = "none",
              ["easeStrength"] = 3,
              ["easeType"] = "none",
          },
      },
      ["displayIcon"] = "442737",
      ["cooldown"] = true,
      ["cooldownEdge"] = false,
  }

  WeakAurasSaved["displays"]["PvP Buffs (Domzae)"] = {
    ["grow"] = "LEFT",
    ["controlledChildren"] = {
        [1] = "AoE Defensives",
        [2] = "Maledict Outgoing",
        [3] = "Innervate",
        [4] = "Cast Immune",
        [5] = "Grounding Totem",
        [6] = "Defensive CDs",
    },
    ["borderBackdrop"] = "Blizzard Tooltip",
    ["xOffset"] = -118.51831054688,
    ["preferToUpdate"] = false,
    ["yOffset"] = -7.58544921875,
    ["anchorPoint"] = "CENTER",
    ["borderColor"] = {
        [1] = 0,
        [2] = 0,
        [3] = 0,
        [4] = 1,
    },
    ["space"] = 2,
    ["actions"] = {
        ["start"] = {
        },
        ["finish"] = {
        },
        ["init"] = {
        },
    },
    ["triggers"] = {
        [1] = {
            ["trigger"] = {
                ["debuffType"] = "HELPFUL",
                ["type"] = "aura2",
                ["spellIds"] = {
                },
                ["subeventSuffix"] = "_CAST_START",
                ["names"] = {
                },
                ["subeventPrefix"] = "SPELL",
                ["event"] = "Health",
                ["unit"] = "player",
            },
            ["untrigger"] = {
            },
        },
    },
    ["columnSpace"] = 1,
    ["internalVersion"] = 29,
    ["animation"] = {
        ["start"] = {
            ["duration_type"] = "seconds",
            ["type"] = "none",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["main"] = {
            ["duration_type"] = "seconds",
            ["type"] = "none",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["finish"] = {
            ["duration_type"] = "seconds",
            ["type"] = "none",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
    },
    ["align"] = "CENTER",
    ["rotation"] = 0,
    ["version"] = 3,
    ["load"] = {
        ["use_class"] = false,
        ["class"] = {
            ["multi"] = {
            },
        },
        ["spec"] = {
            ["multi"] = {
            },
        },
        ["size"] = {
            ["multi"] = {
            },
        },
    },
    ["gridWidth"] = 5,
    ["backdropColor"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 0.5,
    },
    ["url"] = "https://wago.io/be_Cf2OeY/3",
    ["animate"] = false,
    ["arcLength"] = 360,
    ["scale"] = 1,
    ["borderInset"] = 0,
    ["border"] = false,
    ["borderEdge"] = "Square Full White",
    ["regionType"] = "dynamicgroup",
    ["borderSize"] = 2,
    ["sort"] = "none",
    ["useLimit"] = false,
    ["gridType"] = "RD",
    ["constantFactor"] = "RADIUS",
    ["rowSpace"] = 1,
    ["borderOffset"] = 16,
    ["semver"] = "1.0.2",
    ["limit"] = 5,
    ["id"] = "PvP Buffs (Domzae)",
    ["uid"] = "v8Z8oAPnSdJ",
    ["frameStrata"] = 1,
    ["anchorFrameType"] = "SCREEN",
    ["config"] = {
    },
    ["authorOptions"] = {
    },
    ["stagger"] = 0,
    ["conditions"] = {
    },
    ["radius"] = 200,
    ["selfPoint"] = "RIGHT",
  }

  WeakAurasSaved["displays"]["AoE Defensives"] = {
    ["authorOptions"] = {
    },
    ["preferToUpdate"] = false,
    ["yOffset"] = 0,
    ["anchorPoint"] = "CENTER",
    ["cooldownSwipe"] = true,
    ["customTextUpdate"] = "update",
    ["url"] = "https://wago.io/be_Cf2OeY/3",
    ["icon"] = true,
    ["triggers"] = {
        [1] = {
            ["trigger"] = {
                ["auranames"] = {
                    [1] = "Earthen Wall",
                    [2] = "Darkness",
                    [3] = "Anti-Magic Zone",
                    [4] = "Holy Word: Concentration",
                    [5] = "Power Word: Barrier",
                    [6] = "Spirit Link Totem",
                    [7] = "Ancestral Protection",
                },
                ["matchesShowOn"] = "showOnActive",
                ["subeventPrefix"] = "SPELL",
                ["use_tooltip"] = false,
                ["debuffType"] = "HELPFUL",
                ["showClones"] = false,
                ["type"] = "aura2",
                ["use_debuffClass"] = false,
                ["subeventSuffix"] = "_CAST_START",
                ["event"] = "Health",
                ["unit"] = "player",
                ["spellIds"] = {
                },
                ["useName"] = true,
                ["useGroup_count"] = false,
                ["combineMatches"] = "showLowest",
                ["names"] = {
                    [1] = "Earthen Wall",
                    [2] = "Darkness",
                },
                ["buffShowOn"] = "showOnActive",
            },
            ["untrigger"] = {
            },
        },
        ["disjunctive"] = "all",
        ["activeTriggerMode"] = -10,
    },
    ["internalVersion"] = 29,
    ["keepAspectRatio"] = false,
    ["animation"] = {
        ["start"] = {
            ["duration_type"] = "seconds",
            ["type"] = "none",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["main"] = {
            ["type"] = "preset",
            ["easeType"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["preset"] = "pulse",
        },
        ["finish"] = {
            ["duration_type"] = "seconds",
            ["type"] = "none",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
    },
    ["stickyDuration"] = false,
    ["version"] = 3,
    ["subRegions"] = {
        [1] = {
            ["text_shadowXOffset"] = 0,
            ["text_text"] = "%s",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "AUTO",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_color"] = {
                [1] = 1,
                [2] = 1,
                [3] = 1,
                [4] = 1,
            },
            ["text_font"] = "Friz Quadrata TT",
            ["text_shadowYOffset"] = 0,
            ["text_wordWrap"] = "WordWrap",
            ["text_fontType"] = "OUTLINE",
            ["text_anchorPoint"] = "INNER_BOTTOMRIGHT",
            ["text_fontSize"] = 12,
            ["anchorXOffset"] = 0,
            ["text_visible"] = true,
        },
        [2] = {
            ["text_shadowXOffset"] = 0,
            ["text_text"] = "STAY THE FUCK HERE",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "AUTO",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_color"] = {
                [1] = 0.91372549019608,
                [2] = 1,
                [3] = 0.33333333333333,
                [4] = 1,
            },
            ["text_font"] = "Friz Quadrata TT",
            ["text_shadowYOffset"] = 0,
            ["text_wordWrap"] = "WordWrap",
            ["text_fontType"] = "OUTLINE",
            ["text_anchorPoint"] = "OUTER_BOTTOM",
            ["text_fontSize"] = 23,
            ["anchorXOffset"] = 0,
            ["text_visible"] = true,
        },
        [3] = {
            ["glowFrequency"] = 0.25,
            ["type"] = "subglow",
            ["useGlowColor"] = true,
            ["glowType"] = "ACShine",
            ["glowLength"] = 10,
            ["glowScale"] = 1,
            ["glowYOffset"] = 0,
            ["glowColor"] = {
                [1] = 0.36862745098039,
                [2] = 0.94509803921569,
                [3] = 1,
                [4] = 1,
            },
            ["glowThickness"] = 1,
            ["glowXOffset"] = 0,
            ["glow"] = true,
            ["glowLines"] = 8,
            ["glowBorder"] = false,
        },
    },
    ["height"] = 45,
    ["load"] = {
        ["talent2"] = {
            ["multi"] = {
            },
        },
        ["use_never"] = false,
        ["talent"] = {
            ["multi"] = {
            },
        },
        ["class"] = {
            ["multi"] = {
            },
        },
        ["difficulty"] = {
            ["multi"] = {
            },
        },
        ["race"] = {
            ["multi"] = {
            },
        },
        ["pvptalent"] = {
            ["multi"] = {
            },
        },
        ["faction"] = {
            ["multi"] = {
            },
        },
        ["ingroup"] = {
            ["multi"] = {
            },
        },
        ["role"] = {
            ["multi"] = {
            },
        },
        ["spec"] = {
            ["multi"] = {
            },
        },
        ["size"] = {
            ["multi"] = {
                ["pvp"] = true,
                ["arena"] = true,
                ["party"] = true,
                ["none"] = true,
            },
        },
    },
    ["parent"] = "PvP Buffs (Domzae)",
    ["cooldownEdge"] = false,
    ["regionType"] = "icon",
    ["xOffset"] = 0,
    ["color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["desaturate"] = false,
    ["actions"] = {
        ["start"] = {
            ["sound"] = "Interface\\\\AddOns\\\\WeakAuras\\\\Media\\\\Sounds\\\\Stack.ogg",
            ["do_sound"] = false,
        },
        ["finish"] = {
        },
        ["init"] = {
        },
    },
    ["cooldownTextDisabled"] = false,
    ["zoom"] = 0,
    ["semver"] = "1.0.2",
    ["selfPoint"] = "CENTER",
    ["id"] = "AoE Defensives",
    ["config"] = {
    },
    ["alpha"] = 1,
    ["width"] = 45,
    ["anchorFrameType"] = "SCREEN",
    ["uid"] = "eED6YeB5DTw",
    ["inverse"] = false,
    ["auto"] = true,
    ["conditions"] = {
    },
    ["cooldown"] = true,
    ["frameStrata"] = 1,
  }

  WeakAurasSaved["displays"]["Maledict Outgoing"] = {
    ["sparkWidth"] = 10,
    ["stacksSize"] = 12,
    ["authorOptions"] = {
    },
    ["stacksFlags"] = "None",
    ["yOffset"] = 0,
    ["anchorPoint"] = "CENTER",
    ["borderColor"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 0.5,
    },
    ["url"] = "https://wago.io/be_Cf2OeY/3",
    ["backgroundColor"] = {
        [1] = 0,
        [2] = 0,
        [3] = 0,
        [4] = 0.5,
    },
    ["icon_color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["keepAspectRatio"] = false,
    ["wordWrap"] = "WordWrap",
    ["barColor"] = {
        [1] = 1,
        [2] = 0,
        [3] = 0,
        [4] = 1,
    },
    ["desaturate"] = false,
    ["font"] = "Friz Quadrata TT",
    ["sparkOffsetY"] = 0,
    ["load"] = {
        ["use_size"] = false,
        ["use_never"] = false,
        ["spec"] = {
            ["multi"] = {
            },
        },
        ["class"] = {
            ["multi"] = {
            },
        },
        ["size"] = {
            ["multi"] = {
                ["pvp"] = true,
                ["arena"] = true,
                ["party"] = true,
                ["none"] = true,
            },
        },
    },
    ["timerColor"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["useAdjustededMin"] = false,
    ["regionType"] = "icon",
    ["stacks"] = true,
    ["texture"] = "Blizzard",
    ["textFont"] = "Friz Quadrata TT",
    ["borderOffset"] = 5,
    ["spark"] = false,
    ["timerFont"] = "Friz Quadrata TT",
    ["alpha"] = 1,
    ["uid"] = "LCYGGbdDv8h",
    ["preferToUpdate"] = false,
    ["fixedWidth"] = 200,
    ["textFlags"] = "None",
    ["textColor"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["outline"] = "OUTLINE",
    ["xOffset"] = 0,
    ["borderBackdrop"] = "Blizzard Tooltip",
    ["displayText"] = "MALEDICT INCOMMING",
    ["color"] = {
        [1] = 0.63137254901961,
        [2] = 1,
        [3] = 0.63921568627451,
        [4] = 1,
    },
    ["parent"] = "PvP Buffs (Domzae)",
    ["customText"] = "function(event,a,b,c,d)  \n    sourceName = a    \nend",
    ["height"] = 45,
    ["sparkRotation"] = 0,
    ["rotateText"] = "NONE",
    ["cooldownSwipe"] = true,
    ["icon"] = true,
    ["sparkRotationMode"] = "AUTO",
    ["cooldownEdge"] = true,
    ["displayTextLeft"] = "%n",
    ["triggers"] = {
        [1] = {
            ["trigger"] = {
                ["spellId"] = "",
                ["duration"] = "1.5",
                ["unit"] = "player",
                ["debuffType"] = "HELPFUL",
                ["use_spellId"] = false,
                ["type"] = "custom",
                ["names"] = {
                },
                ["unevent"] = "timed",
                ["subeventSuffix"] = "_CAST_SUCCESS",
                ["subeventPrefix"] = "SPELL",
                ["event"] = "Combat Log",
                ["spellIds"] = {
                },
                ["custom_type"] = "event",
                ["customName"] = "function()\nreturn name\nend",
                ["custom"] = "function(event,a,b,c,d)  \n    if c==286348 then\n        if UnitIsFriend(a,\"player\") then\n            name = UnitName(a)\n            return true\n        end\n        \n    end\n    \n    \nend\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n",
                ["custom_hide"] = "timed",
                ["check"] = "event",
                ["use_absorbMode"] = true,
                ["use_unit"] = true,
                ["events"] = "UNIT_SPELLCAST_SUCCEEDED",
            },
            ["untrigger"] = {
            },
        },
        ["disjunctive"] = "custom",
        ["customTriggerLogic"] = "function(trigger)\n    return trigger[1];\nend\n\n\n",
        ["activeTriggerMode"] = 1,
    },
    ["displayIcon"] = 1028995,
    ["internalVersion"] = 29,
    ["selfPoint"] = "CENTER",
    ["animation"] = {
        ["start"] = {
            ["type"] = "none",
            ["easeType"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["preset"] = "slidetop",
        },
        ["main"] = {
            ["duration_type"] = "seconds",
            ["type"] = "none",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["finish"] = {
            ["type"] = "none",
            ["easeType"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["preset"] = "fade",
        },
    },
    ["backdropInFront"] = false,
    ["text"] = true,
    ["timerSize"] = 12,
    ["stickyDuration"] = false,
    ["anchorFrameType"] = "SCREEN",
    ["actions"] = {
        ["start"] = {
        },
        ["finish"] = {
        },
        ["init"] = {
        },
    },
    ["version"] = 3,
    ["subRegions"] = {
        [1] = {
            ["text_shadowXOffset"] = 0,
            ["text_text"] = "%n >>",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "AUTO",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_color"] = {
                [1] = 0.50980392156863,
                [2] = 1,
                [3] = 0.52156862745098,
                [4] = 1,
            },
            ["text_font"] = "Friz Quadrata TT",
            ["text_shadowYOffset"] = 0,
            ["text_wordWrap"] = "WordWrap",
            ["text_fontType"] = "OUTLINE",
            ["text_anchorPoint"] = "OUTER_BOTTOM",
            ["text_fontSize"] = 14,
            ["anchorXOffset"] = 0,
            ["text_visible"] = true,
        },
        [2] = {
            ["glowFrequency"] = 0.25,
            ["type"] = "subglow",
            ["useGlowColor"] = true,
            ["glowType"] = "Pixel",
            ["glowLength"] = 10,
            ["glowScale"] = 1,
            ["glowYOffset"] = 0,
            ["glowColor"] = {
                [1] = 0.050980392156863,
                [2] = 1,
                [3] = 0.29019607843137,
                [4] = 1,
            },
            ["glowThickness"] = 1,
            ["glowXOffset"] = 0,
            ["glow"] = true,
            ["glowLines"] = 8,
            ["glowBorder"] = false,
        },
    },
    ["timer"] = true,
    ["timerFlags"] = "None",
    ["config"] = {
    },
    ["sparkBlendMode"] = "ADD",
    ["useAdjustededMax"] = false,
    ["fontSize"] = 34,
    ["cooldownTextDisabled"] = false,
    ["customTextUpdate"] = "event",
    ["textSize"] = 12,
    ["sparkTexture"] = "Interface\CastingBar\UI-CastingBar-Spark",
    ["border"] = false,
    ["borderEdge"] = "None",
    ["auto"] = true,
    ["borderInFront"] = true,
    ["sparkColor"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["icon_side"] = "RIGHT",
    ["borderInset"] = 11,
    ["zoom"] = 0,
    ["sparkHeight"] = 30,
    ["justify"] = "LEFT",
    ["sparkOffsetX"] = 0,
    ["stacksColor"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["semver"] = "1.0.2",
    ["stacksFont"] = "Friz Quadrata TT",
    ["sparkHidden"] = "NEVER",
    ["backdropColor"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 0.5,
    },
    ["frameStrata"] = 1,
    ["width"] = 45,
    ["id"] = "Maledict Outgoing",
    ["automaticWidth"] = "Auto",
    ["inverse"] = false,
    ["borderSize"] = 16,
    ["orientation"] = "HORIZONTAL",
    ["conditions"] = {
    },
    ["cooldown"] = true,
    ["displayTextRight"] = "%p",
  }

  WeakAurasSaved["displays"]["Innervate"] = {
    ["authorOptions"] = {
    },
    ["preferToUpdate"] = false,
    ["yOffset"] = 0,
    ["anchorPoint"] = "CENTER",
    ["cooldownSwipe"] = true,
    ["customTextUpdate"] = "update",
    ["url"] = "https://wago.io/be_Cf2OeY/3",
    ["actions"] = {
        ["start"] = {
        },
        ["finish"] = {
        },
        ["init"] = {
        },
    },
    ["triggers"] = {
        [1] = {
            ["trigger"] = {
                ["buffShowOn"] = "showOnActive",
                ["useName"] = true,
                ["use_debuffClass"] = false,
                ["useGroup_count"] = false,
                ["type"] = "aura2",
                ["matchesShowOn"] = "showOnActive",
                ["event"] = "Health",
                ["subeventPrefix"] = "SPELL",
                ["subeventSuffix"] = "_CAST_START",
                ["use_tooltip"] = false,
                ["spellIds"] = {
                },
                ["auranames"] = {
                    [1] = "Innervate",
                },
                ["names"] = {
                    [1] = "Innervate",
                },
                ["combineMatches"] = "showLowest",
                ["unit"] = "player",
                ["debuffType"] = "HELPFUL",
            },
            ["untrigger"] = {
            },
        },
        ["disjunctive"] = "all",
        ["activeTriggerMode"] = -10,
    },
    ["internalVersion"] = 29,
    ["keepAspectRatio"] = false,
    ["selfPoint"] = "CENTER",
    ["stickyDuration"] = false,
    ["version"] = 3,
    ["subRegions"] = {
        [1] = {
            ["text_shadowXOffset"] = 0,
            ["text_text"] = "%s",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "AUTO",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_color"] = {
                [1] = 1,
                [2] = 1,
                [3] = 1,
                [4] = 1,
            },
            ["text_font"] = "Friz Quadrata TT",
            ["text_shadowYOffset"] = 0,
            ["text_wordWrap"] = "WordWrap",
            ["text_fontType"] = "OUTLINE",
            ["text_anchorPoint"] = "INNER_BOTTOMRIGHT",
            ["text_fontSize"] = 12,
            ["anchorXOffset"] = 0,
            ["text_visible"] = false,
        },
        [2] = {
            ["glowFrequency"] = 0.25,
            ["type"] = "subglow",
            ["useGlowColor"] = true,
            ["glowType"] = "ACShine",
            ["glowLength"] = 10,
            ["glowScale"] = 1,
            ["glowYOffset"] = 0,
            ["glowColor"] = {
                [1] = 0,
                [2] = 0.52156862745098,
                [3] = 1,
                [4] = 1,
            },
            ["glowThickness"] = 1,
            ["glowXOffset"] = 0,
            ["glow"] = true,
            ["glowLines"] = 8,
            ["glowBorder"] = false,
        },
    },
    ["height"] = 45,
    ["load"] = {
        ["ingroup"] = {
            ["multi"] = {
            },
        },
        ["talent"] = {
            ["multi"] = {
            },
        },
        ["class"] = {
            ["single"] = "PRIEST",
            ["multi"] = {
                ["PRIEST"] = true,
            },
        },
        ["difficulty"] = {
            ["multi"] = {
            },
        },
        ["role"] = {
            ["multi"] = {
            },
        },
        ["talent2"] = {
            ["multi"] = {
            },
        },
        ["faction"] = {
            ["multi"] = {
            },
        },
        ["pvptalent"] = {
            ["multi"] = {
            },
        },
        ["race"] = {
            ["multi"] = {
            },
        },
        ["spec"] = {
            ["single"] = 1,
            ["multi"] = {
                [1] = true,
                [2] = true,
            },
        },
        ["size"] = {
            ["multi"] = {
                ["pvp"] = true,
                ["arena"] = true,
                ["party"] = true,
                ["none"] = true,
            },
        },
    },
    ["cooldownEdge"] = false,
    ["regionType"] = "icon",
    ["desaturate"] = false,
    ["auto"] = true,
    ["parent"] = "PvP Buffs (Domzae)",
    ["xOffset"] = 0,
    ["icon"] = true,
    ["cooldownTextDisabled"] = false,
    ["zoom"] = 0,
    ["semver"] = "1.0.2",
    ["animation"] = {
        ["start"] = {
            ["duration_type"] = "seconds",
            ["type"] = "none",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["main"] = {
            ["type"] = "preset",
            ["easeType"] = "none",
            ["duration_type"] = "seconds",
            ["easeStrength"] = 3,
            ["preset"] = "pulse",
        },
        ["finish"] = {
            ["duration_type"] = "seconds",
            ["type"] = "none",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
    },
    ["id"] = "Innervate",
    ["config"] = {
    },
    ["alpha"] = 1,
    ["width"] = 45,
    ["color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["uid"] = "wmapAR38lcR",
    ["inverse"] = false,
    ["frameStrata"] = 1,
    ["conditions"] = {
    },
    ["cooldown"] = true,
    ["anchorFrameType"] = "SCREEN",
  }

  WeakAurasSaved["displays"]["Cast Immune"] = {
    ["color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["preferToUpdate"] = false,
    ["yOffset"] = 0,
    ["anchorPoint"] = "CENTER",
    ["cooldownSwipe"] = true,
    ["customTextUpdate"] = "update",
    ["cooldownEdge"] = false,
    ["icon"] = true,
    ["triggers"] = {
        [1] = {
            ["trigger"] = {
                ["auranames"] = {
                    [1] = "Ancestral Guidance",
                    [2] = "Divine Favor",
                },
                ["matchesShowOn"] = "showOnActive",
                ["unit"] = "player",
                ["use_tooltip"] = false,
                ["debuffType"] = "HELPFUL",
                ["showClones"] = true,
                ["type"] = "aura2",
                ["use_debuffClass"] = false,
                ["subeventSuffix"] = "_CAST_START",
                ["event"] = "Health",
                ["useGroup_count"] = false,
                ["spellIds"] = {
                },
                ["useName"] = true,
                ["names"] = {
                    [1] = "Innervate",
                },
                ["combineMatches"] = "showLowest",
                ["buffShowOn"] = "showOnActive",
                ["subeventPrefix"] = "SPELL",
            },
            ["untrigger"] = {
            },
        },
        ["disjunctive"] = "all",
        ["activeTriggerMode"] = -10,
    },
    ["internalVersion"] = 29,
    ["keepAspectRatio"] = false,
    ["animation"] = {
        ["start"] = {
            ["duration_type"] = "seconds",
            ["type"] = "none",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["main"] = {
            ["type"] = "preset",
            ["easeType"] = "none",
            ["preset"] = "pulse",
            ["easeStrength"] = 3,
            ["duration_type"] = "seconds",
        },
        ["finish"] = {
            ["duration_type"] = "seconds",
            ["type"] = "none",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
    },
    ["stickyDuration"] = false,
    ["version"] = 3,
    ["subRegions"] = {
        [1] = {
            ["text_shadowXOffset"] = 0,
            ["text_text"] = "%s",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "AUTO",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_color"] = {
                [1] = 1,
                [2] = 1,
                [3] = 1,
                [4] = 1,
            },
            ["text_font"] = "Friz Quadrata TT",
            ["text_shadowYOffset"] = 0,
            ["text_wordWrap"] = "WordWrap",
            ["text_fontType"] = "OUTLINE",
            ["text_anchorPoint"] = "OUTER_BOTTOM",
            ["text_fontSize"] = 12,
            ["anchorXOffset"] = 0,
            ["text_visible"] = false,
        },
        [2] = {
            ["glowFrequency"] = 0.25,
            ["type"] = "subglow",
            ["useGlowColor"] = true,
            ["glowType"] = "ACShine",
            ["glowLength"] = 10,
            ["glowScale"] = 1,
            ["glowYOffset"] = 0,
            ["glowColor"] = {
                [1] = 0,
                [2] = 1,
                [3] = 0.93725490196078,
                [4] = 1,
            },
            ["glowThickness"] = 1,
            ["glowXOffset"] = 0,
            ["glow"] = true,
            ["glowLines"] = 8,
            ["glowBorder"] = false,
        },
    },
    ["height"] = 45,
    ["load"] = {
        ["talent2"] = {
            ["multi"] = {
            },
        },
        ["talent"] = {
            ["multi"] = {
            },
        },
        ["class"] = {
            ["single"] = "PRIEST",
            ["multi"] = {
                ["PRIEST"] = true,
            },
        },
        ["difficulty"] = {
            ["multi"] = {
            },
        },
        ["role"] = {
            ["multi"] = {
            },
        },
        ["pvptalent"] = {
            ["multi"] = {
            },
        },
        ["faction"] = {
            ["multi"] = {
            },
        },
        ["ingroup"] = {
            ["multi"] = {
            },
        },
        ["race"] = {
            ["multi"] = {
            },
        },
        ["spec"] = {
            ["single"] = 1,
            ["multi"] = {
                [1] = true,
                [2] = true,
            },
        },
        ["size"] = {
            ["multi"] = {
                ["pvp"] = true,
                ["arena"] = true,
                ["party"] = true,
                ["none"] = true,
            },
        },
    },
    ["parent"] = "PvP Buffs (Domzae)",
    ["actions"] = {
        ["start"] = {
        },
        ["finish"] = {
        },
        ["init"] = {
        },
    },
    ["regionType"] = "icon",
    ["url"] = "https://wago.io/be_Cf2OeY/3",
    ["frameStrata"] = 1,
    ["desaturate"] = false,
    ["cooldownTextDisabled"] = false,
    ["xOffset"] = 0,
    ["selfPoint"] = "CENTER",
    ["zoom"] = 0,
    ["semver"] = "1.0.2",
    ["anchorFrameType"] = "SCREEN",
    ["id"] = "Cast Immune",
    ["uid"] = "gDtU8CXvKER",
    ["alpha"] = 1,
    ["width"] = 45,
    ["conditions"] = {
    },
    ["config"] = {
    },
    ["inverse"] = true,
    ["authorOptions"] = {
    },
    ["displayIcon"] = 136039,
    ["cooldown"] = true,
    ["auto"] = true,
  }

  WeakAurasSaved["displays"]["Grounding Totem"] = {
    ["authorOptions"] = {
    },
    ["preferToUpdate"] = false,
    ["yOffset"] = 0,
    ["anchorPoint"] = "CENTER",
    ["cooldownSwipe"] = true,
    ["customTextUpdate"] = "update",
    ["cooldownEdge"] = false,
    ["actions"] = {
        ["start"] = {
            ["do_message"] = false,
        },
        ["finish"] = {
        },
        ["init"] = {
            ["do_custom"] = false,
        },
    },
    ["triggers"] = {
        [1] = {
            ["trigger"] = {
                ["type"] = "aura2",
                ["useName"] = true,
                ["use_debuffClass"] = false,
                ["auranames"] = {
                    [1] = "Grounding Totem Effect",
                },
                ["useGroup_count"] = false,
                ["matchesShowOn"] = "showOnActive",
                ["event"] = "Health",
                ["unit"] = "player",
                ["subeventSuffix"] = "_CAST_START",
                ["use_tooltip"] = false,
                ["spellIds"] = {
                },
                ["buffShowOn"] = "showOnActive",
                ["names"] = {
                    [1] = "Innervate",
                },
                ["combineMatches"] = "showLowest",
                ["subeventPrefix"] = "SPELL",
                ["debuffType"] = "HELPFUL",
            },
            ["untrigger"] = {
            },
        },
        [2] = {
            ["trigger"] = {
                ["type"] = "custom",
                ["unevent"] = "timed",
                ["custom"] = "function(event,a,b,c,d)  \n    if c==204336 then\n        if UnitIsFriend(a,\"player\") then\n            name = UnitName(a)\n            return true\n        end\n        \n    end\n    \n    \nend",
                ["duration"] = "3",
                ["event"] = "Combat Log",
                ["unit"] = "player",
                ["subeventSuffix"] = "_CAST_SUCCESS",
                ["subeventPrefix"] = "SPELL",
                ["spellIds"] = {
                },
                ["events"] = "UNIT_SPELLCAST_SUCCEEDED",
                ["names"] = {
                },
                ["custom_type"] = "event",
                ["debuffType"] = "HELPFUL",
                ["custom_hide"] = "timed",
            },
            ["untrigger"] = {
            },
        },
        ["disjunctive"] = "all",
        ["activeTriggerMode"] = 2,
    },
    ["internalVersion"] = 29,
    ["keepAspectRatio"] = false,
    ["selfPoint"] = "CENTER",
    ["desaturate"] = false,
    ["version"] = 3,
    ["subRegions"] = {
        [1] = {
            ["text_shadowXOffset"] = 0,
            ["text_text"] = "Free Cast!",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "AUTO",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_color"] = {
                [1] = 1,
                [2] = 1,
                [3] = 1,
                [4] = 1,
            },
            ["text_font"] = "Friz Quadrata TT",
            ["text_shadowYOffset"] = 0,
            ["text_wordWrap"] = "WordWrap",
            ["text_fontType"] = "OUTLINE",
            ["text_anchorPoint"] = "OUTER_BOTTOM",
            ["text_fontSize"] = 12,
            ["anchorXOffset"] = 0,
            ["text_visible"] = false,
        },
        [2] = {
            ["glowFrequency"] = 0.25,
            ["type"] = "subglow",
            ["useGlowColor"] = true,
            ["glowType"] = "ACShine",
            ["glowLength"] = 10,
            ["glowScale"] = 1,
            ["glowYOffset"] = 0,
            ["glowColor"] = {
                [1] = 0,
                [2] = 1,
                [3] = 0.93725490196078,
                [4] = 1,
            },
            ["glowThickness"] = 1,
            ["glowXOffset"] = 0,
            ["glow"] = true,
            ["glowLines"] = 8,
            ["glowBorder"] = false,
        },
    },
    ["height"] = 45,
    ["load"] = {
        ["use_size"] = false,
        ["talent"] = {
            ["multi"] = {
            },
        },
        ["class"] = {
            ["single"] = "PRIEST",
            ["multi"] = {
                ["PRIEST"] = true,
            },
        },
        ["talent2"] = {
            ["multi"] = {
            },
        },
        ["difficulty"] = {
            ["multi"] = {
            },
        },
        ["role"] = {
            ["multi"] = {
            },
        },
        ["ingroup"] = {
            ["multi"] = {
            },
        },
        ["faction"] = {
            ["multi"] = {
            },
        },
        ["pvptalent"] = {
            ["multi"] = {
            },
        },
        ["race"] = {
            ["multi"] = {
            },
        },
        ["spec"] = {
            ["single"] = 1,
            ["multi"] = {
                [1] = true,
                [2] = true,
            },
        },
        ["size"] = {
            ["multi"] = {
                ["pvp"] = true,
                ["arena"] = true,
                ["party"] = true,
                ["none"] = true,
            },
        },
    },
    ["parent"] = "PvP Buffs (Domzae)",
    ["url"] = "https://wago.io/be_Cf2OeY/3",
    ["cooldownTextDisabled"] = false,
    ["conditions"] = {
    },
    ["regionType"] = "icon",
    ["frameStrata"] = 1,
    ["xOffset"] = 0,
    ["config"] = {
    },
    ["icon"] = true,
    ["color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["zoom"] = 0,
    ["semver"] = "1.0.2",
    ["animation"] = {
        ["start"] = {
            ["duration_type"] = "seconds",
            ["type"] = "none",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["main"] = {
            ["type"] = "preset",
            ["easeType"] = "none",
            ["preset"] = "pulse",
            ["easeStrength"] = 3,
            ["duration_type"] = "seconds",
        },
        ["finish"] = {
            ["duration_type"] = "seconds",
            ["type"] = "none",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
    },
    ["id"] = "Grounding Totem",
    ["anchorFrameType"] = "SCREEN",
    ["alpha"] = 1,
    ["width"] = 45,
    ["stickyDuration"] = false,
    ["uid"] = "BOh7)wlL5nu",
    ["inverse"] = true,
    ["auto"] = true,
    ["displayIcon"] = 136039,
    ["cooldown"] = true,
  }

  WeakAurasSaved["displays"]["Defensive CDs"] = {
    ["color"] = {
        [1] = 1,
        [2] = 1,
        [3] = 1,
        [4] = 1,
    },
    ["preferToUpdate"] = false,
    ["yOffset"] = 0,
    ["anchorPoint"] = "CENTER",
    ["cooldownSwipe"] = true,
    ["customTextUpdate"] = "update",
    ["url"] = "https://wago.io/be_Cf2OeY/3",
    ["actions"] = {
        ["start"] = {
        },
        ["finish"] = {
        },
        ["init"] = {
        },
    },
    ["triggers"] = {
        [1] = {
            ["trigger"] = {
                ["useGroup_count"] = false,
                ["matchesShowOn"] = "showOnActive",
                ["names"] = {
                    [1] = "Innervate",
                },
                ["use_tooltip"] = false,
                ["match_count"] = "1",
                ["debuffType"] = "HELPFUL",
                ["showClones"] = true,
                ["type"] = "aura2",
                ["use_debuffClass"] = false,
                ["subeventSuffix"] = "_CAST_START",
                ["subeventPrefix"] = "SPELL",
                ["match_countOperator"] = "==",
                ["event"] = "Health",
                ["spellIds"] = {
                },
                ["useName"] = true,
                ["auranames"] = {
                    [1] = "Blessing of Sacrifice",
                    [2] = "Pain Suppression",
                    [3] = "Ironbark",
                    [4] = "Life Cocoon",
                    [5] = "Hand of Protection",
                    [6] = "Guardian Spirit",
                    [7] = "Roar of Sacrifice",
                    [8] = "War Banner",
                    [9] = "Gladiator's Emblem",
                    [10] = "Commanding Shout",
                },
                ["combineMatches"] = "showLowest",
                ["unit"] = "player",
                ["buffShowOn"] = "showOnActive",
            },
            ["untrigger"] = {
            },
        },
        ["disjunctive"] = "all",
        ["activeTriggerMode"] = -10,
    },
    ["internalVersion"] = 29,
    ["keepAspectRatio"] = false,
    ["selfPoint"] = "CENTER",
    ["stickyDuration"] = false,
    ["version"] = 3,
    ["subRegions"] = {
        [1] = {
            ["text_shadowXOffset"] = 0,
            ["text_text"] = "%s",
            ["text_shadowColor"] = {
                [1] = 0,
                [2] = 0,
                [3] = 0,
                [4] = 1,
            },
            ["text_selfPoint"] = "AUTO",
            ["text_automaticWidth"] = "Auto",
            ["text_fixedWidth"] = 64,
            ["anchorYOffset"] = 0,
            ["text_justify"] = "CENTER",
            ["rotateText"] = "NONE",
            ["type"] = "subtext",
            ["text_color"] = {
                [1] = 1,
                [2] = 1,
                [3] = 1,
                [4] = 1,
            },
            ["text_font"] = "Friz Quadrata TT",
            ["text_shadowYOffset"] = 0,
            ["text_wordWrap"] = "WordWrap",
            ["text_fontType"] = "OUTLINE",
            ["text_anchorPoint"] = "INNER_BOTTOMRIGHT",
            ["text_fontSize"] = 12,
            ["anchorXOffset"] = 0,
            ["text_visible"] = true,
        },
    },
    ["height"] = 45,
    ["load"] = {
        ["ingroup"] = {
            ["multi"] = {
            },
        },
        ["talent"] = {
            ["multi"] = {
            },
        },
        ["class"] = {
            ["single"] = "PRIEST",
            ["multi"] = {
                ["PRIEST"] = true,
            },
        },
        ["difficulty"] = {
            ["multi"] = {
            },
        },
        ["role"] = {
            ["multi"] = {
            },
        },
        ["talent2"] = {
            ["multi"] = {
            },
        },
        ["faction"] = {
            ["multi"] = {
            },
        },
        ["pvptalent"] = {
            ["multi"] = {
            },
        },
        ["race"] = {
            ["multi"] = {
            },
        },
        ["spec"] = {
            ["single"] = 1,
            ["multi"] = {
                [1] = true,
                [2] = true,
            },
        },
        ["size"] = {
            ["multi"] = {
                ["pvp"] = true,
                ["arena"] = true,
                ["party"] = true,
                ["none"] = true,
            },
        },
    },
    ["parent"] = "PvP Buffs (Domzae)",
    ["icon"] = true,
    ["regionType"] = "icon",
    ["cooldownEdge"] = false,
    ["frameStrata"] = 1,
    ["desaturate"] = false,
    ["animation"] = {
        ["start"] = {
            ["duration_type"] = "seconds",
            ["type"] = "none",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
        ["main"] = {
            ["type"] = "preset",
            ["easeType"] = "none",
            ["preset"] = "pulse",
            ["easeStrength"] = 3,
            ["duration_type"] = "seconds",
        },
        ["finish"] = {
            ["duration_type"] = "seconds",
            ["type"] = "none",
            ["easeStrength"] = 3,
            ["easeType"] = "none",
        },
    },
    ["cooldownTextDisabled"] = false,
    ["zoom"] = 0,
    ["semver"] = "1.0.2",
    ["anchorFrameType"] = "SCREEN",
    ["id"] = "Defensive CDs",
    ["uid"] = "lq0oRohpdMT",
    ["alpha"] = 1,
    ["width"] = 45,
    ["auto"] = true,
    ["config"] = {
    },
    ["inverse"] = false,
    ["authorOptions"] = {
    },
    ["conditions"] = {
    },
    ["cooldown"] = true,
    ["xOffset"] = 0,
  }

  WeakAurasSaved["displays"]["Quaking"] = {
      ["sparkWidth"] = 3,
      ["borderBackdrop"] = "None",
      ["color"] = {
      },
      ["preferToUpdate"] = false,
      ["zoom"] = 0.3,
      ["yOffset"] = -280,
      ["anchorPoint"] = "CENTER",
      ["useAdjustededMax"] = false,
      ["sparkRotation"] = 0,
      ["customTextUpdate"] = "update",
      ["url"] = "https://wago.io/B1H06AUNM/2",
      ["backgroundColor"] = {
          [1] = 0,
          [2] = 0,
          [3] = 0,
          [4] = 0.5,
      },
      ["triggers"] = {
          [1] = {
              ["trigger"] = {
                  ["itemName"] = 0,
                  ["spellId"] = "240447",
                  ["use_absorbMode"] = true,
                  ["genericShowOn"] = "showOnActive",
                  ["names"] = {
                  },
                  ["destUnit"] = "player",
                  ["unit"] = "player",
                  ["use_genericShowOn"] = true,
                  ["subeventPrefix"] = "SPELL",
                  ["duration"] = "60",
                  ["debuffType"] = "HELPFUL",
                  ["spellName"] = 0,
                  ["type"] = "event",
                  ["spellIds"] = {
                  },
                  ["subeventSuffix"] = "_AURA_APPLIED",
                  ["use_showOn"] = true,
                  ["use_spellName"] = false,
                  ["use_itemName"] = true,
                  ["event"] = "Combat Log",
                  ["realSpellName"] = 0,
                  ["use_spellId"] = true,
                  ["name"] = "Quake",
                  ["auraType"] = "DEBUFF",
                  ["unevent"] = "timed",
                  ["use_destUnit"] = true,
                  ["use_unit"] = true,
                  ["use_auraType"] = true,
              },
              ["untrigger"] = {
              },
          },
          ["disjunctive"] = "all",
          ["activeTriggerMode"] = -10,
      },
      ["icon_color"] = {
          [1] = 1,
          [2] = 1,
          [3] = 1,
          [4] = 1,
      },
      ["internalVersion"] = 29,
      ["authorOptions"] = {
      },
      ["selfPoint"] = "CENTER",
      ["backdropInFront"] = false,
      ["conditions"] = {
      },
      ["barColor"] = {
          [1] = 0.49019607843137,
          [2] = 0.26666666666667,
          [3] = 0.16470588235294,
          [4] = 1,
      },
      ["stickyDuration"] = false,
      ["progressPrecision"] = 0,
      ["actions"] = {
          ["start"] = {
              ["sound"] = "Interface\Addons\WeakAuras\PowerAurasMedia\Sounds\sonar.ogg",
              ["do_sound"] = false,
          },
          ["finish"] = {
          },
          ["init"] = {
              ["custom"] = "local bar = WeakAuras.regions[aura_env.id].region.bar\nlocal sparkTexture = bar.spark:GetTexture()\nlocal sparkBlendMode = bar.spark:GetBlendMode()\nlocal sparkWidth = bar.spark:GetWidth()\nlocal sparkHeight = bar.spark:GetHeight()\n\n-- create sparks\nif bar.spark20 then bar.spark20:Hide() end\nlocal spark20 = bar:CreateTexture(nil, \"ARTWORK\")\nspark20:SetDrawLayer(\"ARTWORK\", 4)\nspark20:SetTexture(sparkTexture)\nspark20:SetBlendMode(sparkBlendMode)\nspark20:SetWidth(sparkWidth)\nspark20:SetHeight(sparkHeight)\nbar.spark20 = spark20\n\nif bar.spark40 then bar.spark40:Hide() end\nlocal spark40 = bar:CreateTexture(nil, \"ARTWORK\")\nspark40:SetDrawLayer(\"ARTWORK\", 4)\nspark40:SetTexture(sparkTexture)\nspark40:SetBlendMode(sparkBlendMode)\nspark40:SetWidth(sparkWidth)\nspark40:SetHeight(sparkHeight)\nbar.spark40 = spark40\n\nif bar.spark60 then bar.spark60:Hide() end\nlocal spark60 = bar:CreateTexture(nil, \"ARTWORK\")\nspark60:SetDrawLayer(\"ARTWORK\", 4)\nspark60:SetTexture(sparkTexture)\nspark60:SetBlendMode(sparkBlendMode)\nspark60:SetWidth(sparkWidth)\nspark60:SetHeight(sparkHeight)\nbar.spark60 = spark60\n\n\nlocal barMax = 60\nlocal sizePerUnit = bar:GetWidth() / barMax\nlocal offset\n-- set spark positions\noffset = sizePerUnit * 20\nbar.spark20:SetPoint(\"LEFT\", bar, \"RIGHT\", offset * -1, offset * 0)\nbar.spark20:Show()\n\noffset = sizePerUnit * 40\nbar.spark40:SetPoint(\"LEFT\", bar, \"RIGHT\", offset * -1, offset * 0)\nbar.spark40:Show()\n\noffset = sizePerUnit * 60\nbar.spark60:SetPoint(\"LEFT\", bar, \"RIGHT\", offset * -1, offset * 0)\nbar.spark60:Show()",
              ["do_custom"] = true,
          },
      },
      ["sparkOffsetY"] = 0,
      ["subRegions"] = {
          [1] = {
              ["type"] = "aurabar_bar",
          },
          [2] = {
              ["text_shadowXOffset"] = 1,
              ["text_text"] = "%p",
              ["text_shadowColor"] = {
                  [1] = 0,
                  [2] = 0,
                  [3] = 0,
                  [4] = 1,
              },
              ["text_selfPoint"] = "AUTO",
              ["text_automaticWidth"] = "Auto",
              ["text_fixedWidth"] = 64,
              ["anchorYOffset"] = 0,
              ["text_justify"] = "CENTER",
              ["rotateText"] = "NONE",
              ["type"] = "subtext",
              ["text_color"] = {
                  [1] = 1,
                  [2] = 1,
                  [3] = 1,
                  [4] = 1,
              },
              ["text_font"] = "Expressway",
              ["text_shadowYOffset"] = -1,
              ["text_wordWrap"] = "WordWrap",
              ["text_visible"] = true,
              ["text_anchorPoint"] = "INNER_RIGHT",
              ["text_fontSize"] = 28,
              ["anchorXOffset"] = 0,
              ["text_fontType"] = "None",
          },
          [3] = {
              ["text_shadowXOffset"] = 1,
              ["text_text"] = "%n",
              ["text_shadowColor"] = {
                  [1] = 0,
                  [2] = 0,
                  [3] = 0,
                  [4] = 1,
              },
              ["text_selfPoint"] = "AUTO",
              ["text_automaticWidth"] = "Auto",
              ["text_fixedWidth"] = 64,
              ["anchorYOffset"] = 0,
              ["text_justify"] = "CENTER",
              ["rotateText"] = "NONE",
              ["type"] = "subtext",
              ["text_color"] = {
                  [1] = 1,
                  [2] = 1,
                  [3] = 1,
                  [4] = 1,
              },
              ["text_font"] = "Friz Quadrata TT",
              ["text_shadowYOffset"] = -1,
              ["text_wordWrap"] = "WordWrap",
              ["text_visible"] = false,
              ["text_anchorPoint"] = "INNER_LEFT",
              ["text_fontSize"] = 12,
              ["anchorXOffset"] = 0,
              ["text_fontType"] = "None",
          },
          [4] = {
              ["text_shadowXOffset"] = 1,
              ["text_text"] = "%s",
              ["text_shadowColor"] = {
                  [1] = 0,
                  [2] = 0,
                  [3] = 0,
                  [4] = 1,
              },
              ["text_selfPoint"] = "AUTO",
              ["text_automaticWidth"] = "Auto",
              ["text_fixedWidth"] = 64,
              ["anchorYOffset"] = 0,
              ["text_justify"] = "CENTER",
              ["rotateText"] = "NONE",
              ["type"] = "subtext",
              ["text_color"] = {
                  [1] = 1,
                  [2] = 1,
                  [3] = 1,
                  [4] = 1,
              },
              ["text_font"] = "Friz Quadrata TT",
              ["text_shadowYOffset"] = -1,
              ["text_wordWrap"] = "WordWrap",
              ["text_visible"] = false,
              ["text_anchorPoint"] = "ICON_CENTER",
              ["text_fontSize"] = 12,
              ["anchorXOffset"] = 0,
              ["text_fontType"] = "None",
          },
          [5] = {
              ["type"] = "subborder",
              ["border_anchor"] = "bar",
              ["border_offset"] = 2,
              ["border_color"] = {
                  [1] = 0,
                  [2] = 0,
                  [3] = 0,
                  [4] = 1,
              },
              ["border_visible"] = true,
              ["border_edge"] = "1 Pixel",
              ["border_size"] = 2,
          },
      },
      ["height"] = 21,
      ["config"] = {
      },
      ["load"] = {
          ["class"] = {
              ["multi"] = {
              },
          },
          ["use_size"] = true,
          ["affixes"] = {
              ["multi"] = {
              },
          },
          ["talent"] = {
              ["multi"] = {
              },
          },
          ["use_never"] = false,
          ["spec"] = {
              ["multi"] = {
              },
          },
          ["talent2"] = {
              ["multi"] = {
              },
          },
          ["race"] = {
              ["multi"] = {
              },
          },
          ["difficulty"] = {
              ["single"] = "challenge",
              ["multi"] = {
                  ["challenge"] = true,
              },
          },
          ["role"] = {
              ["multi"] = {
              },
          },
          ["talent3"] = {
              ["multi"] = {
              },
          },
          ["faction"] = {
              ["multi"] = {
              },
          },
          ["pvptalent"] = {
              ["multi"] = {
              },
          },
          ["use_difficulty"] = true,
          ["ingroup"] = {
              ["multi"] = {
              },
          },
          ["size"] = {
              ["single"] = "party",
              ["multi"] = {
                  ["party"] = true,
              },
          },
      },
      ["sparkBlendMode"] = "ADD",
      ["backdropColor"] = {
          [1] = 1,
          [2] = 1,
          [3] = 1,
          [4] = 0,
      },
      ["desaturate"] = false,
      ["uid"] = "NWr7QMr1z81",
      ["fontFlags"] = "OUTLINE",
      ["anchorFrameType"] = "SCREEN",
      ["alpha"] = 1,
      ["smoothProgress"] = false,
      ["useAdjustededMin"] = false,
      ["regionType"] = "aurabar",
      ["borderInFront"] = true,
      ["version"] = 2,
      ["icon_side"] = "LEFT",
      ["id"] = "Quaking",
      ["sparkHeight"] = 30,
      ["texture"] = "oRA3",
      ["auto"] = false,
      ["sparkTexture"] = "Interface\AddOns\WeakAuras\Media\Textures\Square_White",
      ["spark"] = false,
      ["tocversion"] = 80205,
      ["sparkHidden"] = "NEVER",
      ["animation"] = {
          ["start"] = {
              ["type"] = "none",
              ["duration_type"] = "seconds",
              ["easeStrength"] = 3,
              ["easeType"] = "none",
          },
          ["main"] = {
              ["type"] = "none",
              ["duration_type"] = "seconds",
              ["easeStrength"] = 3,
              ["easeType"] = "none",
          },
          ["finish"] = {
              ["type"] = "none",
              ["duration_type"] = "seconds",
              ["easeStrength"] = 3,
              ["easeType"] = "none",
          },
      },
      ["frameStrata"] = 1,
      ["width"] = 280,
      ["sparkOffsetX"] = 0,
      ["sparkColor"] = {
          [1] = 1,
          [2] = 1,
          [3] = 1,
          [4] = 1,
      },
      ["inverse"] = false,
      ["xOffset"] = -320,
      ["orientation"] = "HORIZONTAL",
      ["displayIcon"] = 136025,
      ["sparkRotationMode"] = "AUTO",
      ["icon"] = true,
  }
end
