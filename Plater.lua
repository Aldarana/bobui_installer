local  BUI = select(2, ...):unpack()

function BUI:ImportPlater()

  --Save this profile name under the BUI namespace so that it can be accssed when we need to apply the profile
  BUI.platerProfileName = "Bob's Plater"

  --This is basically just a straight copy paste from the Plater WTF file
  PlaterDB["profiles"][BUI.platerProfileName] = {
    ["script_data"] = {
      {
        ["ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --settings (you may need /reload if some configs isn't applied immediately)    \n    --change the nameplate color to this if allowed\n    envTable.CanChangeNameplateColor = false --change to true to change the color\n    envTable.NameplateColor = \"pink\"\n    envTable.NameplateSizeOffset = 6 --increase the nameplate height by this value\n    envTable.GlowAlpha = 0.5 --amount of alpha in the outside glow effect\n    \n    --create a glow effect around the nameplate\n    envTable.glowEffect = envTable.glowEffect or Plater.CreateNameplateGlow (unitFrame.healthBar, envTable.NameplateColor)\n    envTable.glowEffect:SetOffset (-27, 25, 9, -11)\n    --envTable.glowEffect:Show() --envTable.glowEffect:Hide() --\n    \n    --set the glow effect alpha\n    envTable.glowEffect:SetAlpha (envTable.GlowAlpha)\n    \nend\n\n--[=[\nUsing spellIDs for multi-language support\n\n135029 - A Knot of Snakes (Temple of Sethraliss)\n135388 - A Knot of Snakes (Temple of Sethraliss)\n134612 - Grasping Tentacles (Shrine of the Storm)\n133361 - Wasting Servant (Waycrest Manor)\n136330 - Soul Thorns (Waycrest Manor)\n130896 - Blackout Barrel (Freehold)\n129758 - Irontide Grenadier (Freehold)\n131009 - Spirit of Gold (Atal`Dazar)\n--]=]",
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.glowEffect:Hide()\n    \n    --restore the nameplate size\n    local nameplateHeight = Plater.db.profile.plate_config.enemynpc.health_incombat [2]\n    unitFrame.healthBar:SetHeight (nameplateHeight)    \n    \nend\n\n\n",
        ["Temp_OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.glowEffect:Show()\n    \n    --increase the nameplate size\n    local nameplateHeight = Plater.db.profile.plate_config.enemynpc.health_incombat [2]\n    unitFrame.healthBar:SetHeight (nameplateHeight + envTable.NameplateSizeOffset)\n    \nend\n\n\n",
        ["ScriptType"] = 3,
        ["UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --check if can change the nameplate color\n    if (envTable.CanChangeNameplateColor) then\n        Plater.SetNameplateColor (unitFrame, envTable.NameplateColor)\n    end\n    \nend\n\n\n\n\n",
        ["Time"] = 1586789807,
        ["Temp_ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --settings (you may need /reload if some configs isn't applied immediately)    \n    --change the nameplate color to this if allowed\n    envTable.CanChangeNameplateColor = false --change to true to change the color\n    envTable.NameplateColor = \"pink\"\n    envTable.NameplateSizeOffset = 6 --increase the nameplate height by this value\n    envTable.GlowAlpha = 0.5 --amount of alpha in the outside glow effect\n    \n    --create a glow effect around the nameplate\n    envTable.glowEffect = envTable.glowEffect or Plater.CreateNameplateGlow (unitFrame.healthBar, envTable.NameplateColor)\n    envTable.glowEffect:SetOffset (-27, 25, 9, -11)\n    --envTable.glowEffect:Show() --envTable.glowEffect:Hide() --\n    \n    --set the glow effect alpha\n    envTable.glowEffect:SetAlpha (envTable.GlowAlpha)\n    \nend\n\n--[=[\nUsing spellIDs for multi-language support\n\n135029 - A Knot of Snakes (Temple of Sethraliss)\n135388 - A Knot of Snakes (Temple of Sethraliss)\n134612 - Grasping Tentacles (Shrine of the Storm)\n133361 - Wasting Servant (Waycrest Manor)\n136330 - Soul Thorns (Waycrest Manor)\n130896 - Blackout Barrel (Freehold)\n129758 - Irontide Grenadier (Freehold)\n131009 - Spirit of Gold (Atal`Dazar)\n--]=]",
        ["NpcNames"] = {
          "135029", -- [1]
          "134388", -- [2]
          "134612", -- [3]
          "133361", -- [4]
          "136330", -- [5]
          "130896", -- [6]
          "129758", -- [7]
          "Healing Tide Totem", -- [8]
          "131009", -- [9]
          "Spirit Drain Totem", -- [10]
        },
        ["Enabled"] = true,
        ["Revision"] = 157,
        ["Author"] = "Izimode-Azralon",
        ["Desc"] = "Highlight a nameplate of an important Add. Add the unit name or NpcID into the trigger box to add more.",
        ["Temp_UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --check if can change the nameplate color\n    if (envTable.CanChangeNameplateColor) then\n        Plater.SetNameplateColor (unitFrame, envTable.NameplateColor)\n    end\n    \nend\n\n\n\n\n",
        ["Prio"] = 99,
        ["Name"] = "Unit - Important [Plater]",
        ["PlaterCore"] = 1,
        ["Temp_OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.glowEffect:Hide()\n    \n    --restore the nameplate size\n    local nameplateHeight = Plater.db.profile.plate_config.enemynpc.health_incombat [2]\n    unitFrame.healthBar:SetHeight (nameplateHeight)    \n    \nend\n\n\n",
        ["Icon"] = 135996,
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.glowEffect:Show()\n    \n    --increase the nameplate size\n    local nameplateHeight = Plater.db.profile.plate_config.enemynpc.health_incombat [2]\n    unitFrame.healthBar:SetHeight (nameplateHeight + envTable.NameplateSizeOffset)\n    \nend\n\n\n",
        ["SpellIds"] = {
        },
      }, -- [1]
      {
        ["Enabled"] = true,
        ["Revision"] = 399,
        ["Desc"] = "Add the buff name in the trigger box.",
        ["Icon"] = "Interface\\AddOns\\Plater\\images\\icon_aura",
        ["Author"] = "Tercioo-Sylvanas",
        ["ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --creates a glow around the icon\n    envTable.buffIconGlow = envTable.buffIconGlow or Plater.CreateIconGlow (self)\n    \nend",
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.buffIconGlow:Show()\n    \nend",
        ["SpellIds"] = {
          275826, -- [1]
          272888, -- [2]
          272659, -- [3]
          267901, -- [4]
          267830, -- [5]
          265393, -- [6]
        },
        ["UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    \n    \n    \nend",
        ["Time"] = 1539013601,
        ["PlaterCore"] = 1,
        ["Name"] = "Aura - Buff Alert [Plater]",
        ["ScriptType"] = 1,
        ["NpcNames"] = {
        },
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.buffIconGlow:Hide()\n    \nend",
      }, -- [2]
      {
        ["Enabled"] = true,
        ["Revision"] = 391,
        ["Desc"] = "Highlight a very important cast applying several effects into the Cast Bar. Add spell in the Add Trigger field.",
        ["Icon"] = "Interface\\AddOns\\Plater\\images\\cast_bar",
        ["Author"] = "Bombad�o-Azralon",
        ["ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --settings (you may need /reload if some configs isn't applied immediately)\n    local CONFIG_BACKGROUND_FLASH_DURATION = 0.8 --0.8\n    local CONFIG_BORDER_GLOW_ALPHA = 0.3 --0.3\n    local CONFIG_SHAKE_DURATION = 0.2 --0.2\n    local CONFIG_SHAKE_AMPLITUDE = 5 --5\n    \n    --create a glow effect in the border of the cast bar\n    envTable.glowEffect = envTable.glowEffect or Plater.CreateNameplateGlow (self)\n    envTable.glowEffect:SetOffset (-32, 30, 7, -9)\n    envTable.glowEffect:SetAlpha (CONFIG_BORDER_GLOW_ALPHA)\n    --envTable.glowEffect:Show() --envTable.glowEffect:Hide() \n    \n    --create a texture to use for a flash behind the cast bar\n    local backGroundFlashTexture = Plater:CreateImage (self, [[Interface\\ACHIEVEMENTFRAME\\UI-Achievement-Alert-Glow]], self:GetWidth()+40, self:GetHeight()+20, \"background\", {0, 400/512, 0, 170/256})\n    backGroundFlashTexture:SetBlendMode (\"ADD\")\n    backGroundFlashTexture:SetPoint (\"center\", self, \"center\")\n    backGroundFlashTexture:Hide()\n    \n    --create the animation hub to hold the flash animation sequence\n    envTable.BackgroundFlash = envTable.BackgroundFlash or Plater:CreateAnimationHub (backGroundFlashTexture, \n        function()\n            backGroundFlashTexture:Show()\n        end,\n        function()\n            backGroundFlashTexture:Hide()\n        end\n    )\n    \n    --create the flash animation sequence\n    local fadeIn = Plater:CreateAnimation (envTable.BackgroundFlash, \"ALPHA\", 1, CONFIG_BACKGROUND_FLASH_DURATION/2, 0, 1)\n    local fadeOut = Plater:CreateAnimation (envTable.BackgroundFlash, \"ALPHA\", 2, CONFIG_BACKGROUND_FLASH_DURATION/2, 1, 0)    \n    --envTable.BackgroundFlash:Play() --envTable.BackgroundFlash:Stop()\n    \n    --create a camera shake for the nameplate\n    envTable.FrameShake = Plater:CreateFrameShake (unitFrame, CONFIG_SHAKE_DURATION, CONFIG_SHAKE_AMPLITUDE, 35, false, false, 0, 1, 0.05, 0.1, Plater.GetPoints (unitFrame))    \n    \n    \n    --update the config for the flash here so it wont need a /reload\n    fadeIn:SetDuration (CONFIG_BACKGROUND_FLASH_DURATION/2)\n    fadeOut:SetDuration (CONFIG_BACKGROUND_FLASH_DURATION/2)    \n    \n    --update the config for the skake here so it wont need a /reload\n    envTable.FrameShake.OriginalAmplitude = CONFIG_SHAKE_AMPLITUDE\n    envTable.FrameShake.OriginalDuration = CONFIG_SHAKE_DURATION  \n    \nend",
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.glowEffect:Show()\n    \n    envTable.BackgroundFlash:Play()\n    \n    Plater.FlashNameplateBorder (unitFrame, 0.05)   \n    Plater.FlashNameplateBody (unitFrame, \"\", 0.075)\n    \n    unitFrame:PlayFrameShake (envTable.FrameShake)\n    \nend\n\n\n",
        ["SpellIds"] = {
          257785, -- [1]
          267237, -- [2]
          266951, -- [3]
          267273, -- [4]
          267433, -- [5]
          263066, -- [6]
          255577, -- [7]
          255371, -- [8]
        },
        ["UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    \nend\n\n\n",
        ["Time"] = 1561923707,
        ["PlaterCore"] = 1,
        ["Name"] = "Cast - Very Important [Plater]",
        ["ScriptType"] = 2,
        ["NpcNames"] = {
        },
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.glowEffect:Hide()\n    \n    envTable.BackgroundFlash:Stop()\n    \n    unitFrame:StopFrameShake (envTable.FrameShake)    \n    \nend\n\n\n",
      }, -- [3]
      {
        ["ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --settings\n    envTable.NameplateSizeOffset = 3\n    envTable.GlowAlpha = .45\n    envTable.ShowArrow = true\n    envTable.ArrowAlpha = .45    \n    envTable.HealthBarColor = \"orange\"\n    \n    --custom frames\n    envTable.glowEffect = envTable.glowEffect or Plater.CreateNameplateGlow (unitFrame.healthBar)\n    --envTable.glowEffect:Show() --envTable.glowEffect:Hide() \n    envTable.glowEffect:SetOffset (-27, 25, 6, -8)\n    \n    --creates the spark to show the cast progress inside the health bar\n    envTable.overlaySpark = envTable.overlaySpark or Plater:CreateImage (unitFrame.healthBar)\n    envTable.overlaySpark:SetBlendMode (\"ADD\")\n    envTable.overlaySpark.width = 32\n    envTable.overlaySpark.height = 36\n    envTable.overlaySpark.alpha = .9\n    envTable.overlaySpark.texture = [[Interface\\CastingBar\\UI-CastingBar-Spark]]\n    \n    envTable.topArrow = envTable.topArrow or Plater:CreateImage (unitFrame.healthBar)\n    envTable.topArrow:SetBlendMode (\"ADD\")\n    envTable.topArrow.width = 8\n    envTable.topArrow.height = 8\n    envTable.topArrow.alpha = envTable.ArrowAlpha\n    envTable.topArrow.texture = [[Interface\\BUTTONS\\Arrow-Down-Up]]\n    \n    --scale animation\n    envTable.smallScaleAnimation = envTable.smallScaleAnimation or Plater:CreateAnimationHub (unitFrame.healthBar)\n    Plater:CreateAnimation (envTable.smallScaleAnimation, \"SCALE\", 1, 0.075, 1, 1, 1.08, 1.08)\n    Plater:CreateAnimation (envTable.smallScaleAnimation, \"SCALE\", 2, 0.075, 1, 1, 0.95, 0.95)    \n    --envTable.smallScaleAnimation:Play() --envTable.smallScaleAnimation:Stop()\n    \nend\n\n\n\n\n\n\n\n",
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    envTable.glowEffect:Hide()\n    \n    envTable.overlaySpark:Hide()\n    envTable.topArrow:Hide()\n    \n    Plater.RefreshNameplateColor (unitFrame)\n    \n    envTable.smallScaleAnimation:Stop()\n    \n    --increase the nameplate size\n    local nameplateHeight = Plater.db.profile.plate_config.enemynpc.health_incombat [2]\n    unitFrame.healthBar:SetHeight (nameplateHeight)\nend\n\n\n",
        ["Temp_OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.glowEffect:Show()\n    envTable.overlaySpark:Show()\n    \n    if (envTable.ShowArrow) then\n        envTable.topArrow:Show()\n    end\n    \n    Plater.FlashNameplateBorder (unitFrame, 0.05)   \n    Plater.FlashNameplateBody (unitFrame, \"\", 0.075)\n    \n    envTable.smallScaleAnimation:Play()\n    \n    --increase the nameplate size\n    local nameplateHeight = Plater.db.profile.plate_config.enemynpc.health_incombat [2]\n    unitFrame.healthBar:SetHeight (nameplateHeight + envTable.NameplateSizeOffset)\n    \n    envTable.overlaySpark.height = nameplateHeight + 32\n    \n    envTable.glowEffect.Texture:SetAlpha (envTable.GlowAlpha)\n    \n    \nend\n\n\n\n\n\n\n",
        ["ScriptType"] = 2,
        ["UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --update the percent\n    envTable.overlaySpark:SetPoint (\"left\", unitFrame.healthBar:GetWidth() * (envTable._CastPercent / 100)-16, 0)\n    \n    envTable.topArrow:SetPoint (\"bottomleft\", unitFrame.healthBar, \"topleft\", unitFrame.healthBar:GetWidth() * (envTable._CastPercent / 100) - 4, 2 )\n    \n    --forces the script to update on a 60Hz base\n    self.ThrottleUpdate = 0.016\n    \n    --update the health bar color coloring from yellow to red\n    --Plater.SetNameplateColor (unitFrame, max (envTable._CastPercent/100, .66), abs (envTable._CastPercent/100 - 1), 0, 1)\n    \n    Plater.SetNameplateColor (unitFrame, envTable.HealthBarColor)\n    envTable.glowEffect.Texture:SetAlpha (envTable.GlowAlpha)\n    \nend\n\n\n",
        ["Time"] = 1585401434,
        ["Temp_ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --settings\n    envTable.NameplateSizeOffset = 3\n    envTable.GlowAlpha = .45\n    envTable.ShowArrow = true\n    envTable.ArrowAlpha = .45    \n    envTable.HealthBarColor = \"orange\"\n    \n    --custom frames\n    envTable.glowEffect = envTable.glowEffect or Plater.CreateNameplateGlow (unitFrame.healthBar)\n    --envTable.glowEffect:Show() --envTable.glowEffect:Hide() \n    envTable.glowEffect:SetOffset (-27, 25, 6, -8)\n    \n    --creates the spark to show the cast progress inside the health bar\n    envTable.overlaySpark = envTable.overlaySpark or Plater:CreateImage (unitFrame.healthBar)\n    envTable.overlaySpark:SetBlendMode (\"ADD\")\n    envTable.overlaySpark.width = 32\n    envTable.overlaySpark.height = 36\n    envTable.overlaySpark.alpha = .9\n    envTable.overlaySpark.texture = [[Interface\\CastingBar\\UI-CastingBar-Spark]]\n    \n    envTable.topArrow = envTable.topArrow or Plater:CreateImage (unitFrame.healthBar)\n    envTable.topArrow:SetBlendMode (\"ADD\")\n    envTable.topArrow.width = 8\n    envTable.topArrow.height = 8\n    envTable.topArrow.alpha = envTable.ArrowAlpha\n    envTable.topArrow.texture = [[Interface\\BUTTONS\\Arrow-Down-Up]]\n    \n    --scale animation\n    envTable.smallScaleAnimation = envTable.smallScaleAnimation or Plater:CreateAnimationHub (unitFrame.healthBar)\n    Plater:CreateAnimation (envTable.smallScaleAnimation, \"SCALE\", 1, 0.075, 1, 1, 1.08, 1.08)\n    Plater:CreateAnimation (envTable.smallScaleAnimation, \"SCALE\", 2, 0.075, 1, 1, 0.95, 0.95)    \n    --envTable.smallScaleAnimation:Play() --envTable.smallScaleAnimation:Stop()\n    \nend\n\n\n\n\n\n\n\n",
        ["Icon"] = 2175503,
        ["Enabled"] = true,
        ["Revision"] = 326,
        ["Author"] = "Bombad�o-Azralon",
        ["Desc"] = "Apply several animations when the explosion orb cast starts on a Mythic Dungeon with Explosion Affix",
        ["SpellIds"] = {
          240446, -- [1]
          273577, -- [2]
        },
        ["Temp_UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --update the percent\n    envTable.overlaySpark:SetPoint (\"left\", unitFrame.healthBar:GetWidth() * (envTable._CastPercent / 100)-16, 0)\n    \n    envTable.topArrow:SetPoint (\"bottomleft\", unitFrame.healthBar, \"topleft\", unitFrame.healthBar:GetWidth() * (envTable._CastPercent / 100) - 4, 2 )\n    \n    --forces the script to update on a 60Hz base\n    self.ThrottleUpdate = 0.016\n    \n    --update the health bar color coloring from yellow to red\n    --Plater.SetNameplateColor (unitFrame, max (envTable._CastPercent/100, .66), abs (envTable._CastPercent/100 - 1), 0, 1)\n    \n    Plater.SetNameplateColor (unitFrame, envTable.HealthBarColor)\n    envTable.glowEffect.Texture:SetAlpha (envTable.GlowAlpha)\n    \nend\n\n\n",
        ["Name"] = "Explosion Affix M+ [Plater]",
        ["PlaterCore"] = 1,
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.glowEffect:Show()\n    envTable.overlaySpark:Show()\n    \n    if (envTable.ShowArrow) then\n        envTable.topArrow:Show()\n    end\n    \n    Plater.FlashNameplateBorder (unitFrame, 0.05)   \n    Plater.FlashNameplateBody (unitFrame, \"\", 0.075)\n    \n    envTable.smallScaleAnimation:Play()\n    \n    --increase the nameplate size\n    local nameplateHeight = Plater.db.profile.plate_config.enemynpc.health_incombat [2]\n    unitFrame.healthBar:SetHeight (nameplateHeight + envTable.NameplateSizeOffset)\n    \n    envTable.overlaySpark.height = nameplateHeight + 32\n    \n    envTable.glowEffect.Texture:SetAlpha (envTable.GlowAlpha)\n    \n    \nend\n\n\n\n\n\n\n",
        ["NpcNames"] = {
        },
        ["Temp_OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    envTable.glowEffect:Hide()\n    \n    envTable.overlaySpark:Hide()\n    envTable.topArrow:Hide()\n    \n    Plater.RefreshNameplateColor (unitFrame)\n    \n    envTable.smallScaleAnimation:Stop()\n    \n    --increase the nameplate size\n    local nameplateHeight = Plater.db.profile.plate_config.enemynpc.health_incombat [2]\n    unitFrame.healthBar:SetHeight (nameplateHeight)\nend\n\n\n",
        ["Prio"] = 99,
      }, -- [4]
      {
        ["Enabled"] = true,
        ["Revision"] = 232,
        ["Desc"] = "Add the debuff name in the trigger box.",
        ["Icon"] = "Interface\\AddOns\\Plater\\images\\icon_aura",
        ["Author"] = "Tercioo-Sylvanas",
        ["ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --creates a glow around the icon\n    envTable.debuffIconGlow = envTable.debuffIconGlow or Plater.CreateIconGlow (self)\n    \nend\n\n\n",
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.debuffIconGlow:Show()\n    \nend\n\n\n",
        ["SpellIds"] = {
        },
        ["UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    \nend\n\n\n",
        ["Time"] = 1538429739,
        ["PlaterCore"] = 1,
        ["Name"] = "Aura - Debuff Alert [Plater]",
        ["ScriptType"] = 1,
        ["NpcNames"] = {
        },
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.debuffIconGlow:Hide()\n    \nend\n\n\n",
      }, -- [5]
      {
        ["Enabled"] = true,
        ["Revision"] = 574,
        ["Desc"] = "Flash, Bounce and Red Color the CastBar border when when an important cast is happening. Add spell in the Add Trigger field.",
        ["Icon"] = "Interface\\AddOns\\Plater\\images\\cast_bar",
        ["Author"] = "Tercioo-Sylvanas",
        ["ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --https://www.wowhead.com/spell=253583/fiery-enchant\n    \n    --settings (you may need /reload if some configs isn't applied immediately)\n    \n    --castbar color (when can be interrupted)\n    envTable.CastbarColor = \"darkorange\"\n    --flash duration\n    local CONFIG_BACKGROUND_FLASH_DURATION = 0.4\n    --add this value to the cast bar height\n    envTable.CastBarHeightAdd = 5\n    \n    \n    \n    --create a fast flash above the cast bar\n    envTable.FullBarFlash = envTable.FullBarFlash or Plater.CreateFlash (self, 0.05, 1, \"white\")\n    \n    --create a camera shake for the nameplate\n    envTable.FrameShake = Plater:CreateFrameShake (unitFrame, 0.2, 5, 35, false, false, 0, 1, 0.05, 0.1, Plater.GetPoints (unitFrame))\n    \n    --create a texture to use for a flash behind the cast bar\n    local backGroundFlashTexture = Plater:CreateImage (self, [[Interface\\ACHIEVEMENTFRAME\\UI-Achievement-Alert-Glow]], self:GetWidth()+60, self:GetHeight()+50, \"background\", {0, 400/512, 0, 170/256})\n    backGroundFlashTexture:SetBlendMode (\"ADD\")\n    backGroundFlashTexture:SetPoint (\"center\", self, \"center\")\n    backGroundFlashTexture:Hide()\n    \n    --create the animation hub to hold the flash animation sequence\n    envTable.BackgroundFlash = envTable.BackgroundFlash or Plater:CreateAnimationHub (backGroundFlashTexture, \n        function()\n            backGroundFlashTexture:Show()\n        end,\n        function()\n            backGroundFlashTexture:Hide()\n        end\n    )\n    \n    --create the flash animation sequence\n    local fadeIn = Plater:CreateAnimation (envTable.BackgroundFlash, \"ALPHA\", 1, CONFIG_BACKGROUND_FLASH_DURATION/2, 0, .75)\n    local fadeOut = Plater:CreateAnimation (envTable.BackgroundFlash, \"ALPHA\", 2, CONFIG_BACKGROUND_FLASH_DURATION/2, 1, 0)    \n    --envTable.BackgroundFlash:Play() --envTable.BackgroundFlash:Stop()        \n    \nend\n\n\n",
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --don't execute on battlegrounds and arenas\n    if (Plater.ZoneInstanceType == \"arena\" or Plater.ZoneInstanceType == \"pvp\") then\n        return\n    end\n    \n    --play flash animations\n    envTable.FullBarFlash:Play()\n    \n    --restoring the default size (not required since it already restore in the hide script)\n    if (envTable.OriginalHeight) then\n        self:SetHeight (envTable.OriginalHeight)\n    end\n    \n    --increase the cast bar size\n    local height = self:GetHeight()\n    envTable.OriginalHeight = height\n    \n    self:SetHeight (height + envTable.CastBarHeightAdd)\n    \n    Plater.SetCastBarBorderColor (self, 1, .2, .2, 0.4)\n    \n    unitFrame:PlayFrameShake (envTable.FrameShake)\n    \n    --set the color of the cast bar to dark orange (only if can be interrupted)\n    --Plater auto set this color to default when a new cast starts, no need to reset this value at OnHide.    \n    if (envTable._CanInterrupt) then\n        self:SetStatusBarColor (Plater:ParseColors (envTable.CastbarColor))\n    end\n    \n    envTable.BackgroundFlash:Play()\n    \nend\n\n\n\n\n\n\n\n\n",
        ["SpellIds"] = {
          258153, -- [1]
          258313, -- [2]
          257069, -- [3]
          274569, -- [4]
          278020, -- [5]
          261635, -- [6]
          272700, -- [7]
          280404, -- [8]
          268030, -- [9]
          265368, -- [10]
          263891, -- [11]
          264520, -- [12]
          265407, -- [13]
          278567, -- [14]
          278602, -- [15]
          258128, -- [16]
          257791, -- [17]
          258938, -- [18]
          265089, -- [19]
          272183, -- [20]
          256060, -- [21]
          257397, -- [22]
          257899, -- [23]
          269972, -- [24]
          270901, -- [25]
          270492, -- [26]
          268129, -- [27]
          268709, -- [28]
          263215, -- [29]
          268797, -- [30]
          262540, -- [31]
          262554, -- [32]
          253517, -- [33]
          255041, -- [34]
          252781, -- [35]
          250368, -- [36]
          258777, -- [37]
        },
        ["UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    \nend\n\n\n",
        ["Time"] = 1561924439,
        ["PlaterCore"] = 1,
        ["Name"] = "Cast - Big Alert [Plater]",
        ["ScriptType"] = 2,
        ["NpcNames"] = {
        },
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --don't execute on battlegrounds and arenas\n    if (Plater.ZoneInstanceType == \"arena\" or Plater.ZoneInstanceType == \"pvp\") then\n        return\n    end    \n    \n    --restore the cast bar to its original height\n    if (envTable.OriginalHeight) then\n        self:SetHeight (envTable.OriginalHeight)\n        envTable.OriginalHeight = nil\n    end\n    \n    --stop the camera shake\n    unitFrame:StopFrameShake (envTable.FrameShake)\n    \n    envTable.FullBarFlash:Stop()\n    envTable.BackgroundFlash:Stop()\n    \nend\n\n\n\n\n\n",
      }, -- [6]
      {
        ["Enabled"] = true,
        ["Revision"] = 376,
        ["Desc"] = "Flashes the Cast Bar when a spell in the trigger list is Cast. Add spell in the Add Trigger field.",
        ["Icon"] = "Interface\\AddOns\\Plater\\images\\cast_bar",
        ["Author"] = "Tercioo-Sylvanas",
        ["ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --settings (you may need /reload if some configs isn't applied immediately)\n    \n    --flash duration\n    local CONFIG_FLASH_DURATION = 0.6\n    \n    --manually create a new texture for the flash animation\n    if (not envTable.SmallFlashTexture) then\n        envTable.SmallFlashTexture = envTable.SmallFlashTexture or Plater:CreateImage (unitFrame.castBar)\n        envTable.SmallFlashTexture:SetColorTexture (1, 1, 1)\n        envTable.SmallFlashTexture:SetAllPoints()\n    end\n    \n    --manually create a flash animation using the framework\n    if (not envTable.SmallFlashAnimationHub) then \n        \n        local onPlay = function()\n            envTable.SmallFlashTexture:Show()\n        end\n        \n        local onFinished = function()\n            envTable.SmallFlashTexture:Hide()\n        end\n        \n        local animationHub = Plater:CreateAnimationHub (envTable.SmallFlashTexture, onPlay, onFinished)\n        Plater:CreateAnimation (animationHub, \"Alpha\", 1, CONFIG_FLASH_DURATION/2, 0, .6)\n        Plater:CreateAnimation (animationHub, \"Alpha\", 2, CONFIG_FLASH_DURATION/2, 1, 0)\n        \n        envTable.SmallFlashAnimationHub = animationHub\n    end\n    \n    \n    \nend\n\n\n\n\n\n\n\n",
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.SmallFlashAnimationHub:Play()\n    \nend\n\n\n",
        ["SpellIds"] = {
          275192, -- [1]
          265912, -- [2]
          274438, -- [3]
          268317, -- [4]
          268375, -- [5]
          276767, -- [6]
          264105, -- [7]
          265876, -- [8]
          270464, -- [9]
          266106, -- [10]
          272180, -- [11]
          278961, -- [12]
          278755, -- [13]
          265468, -- [14]
          256405, -- [15]
          256897, -- [16]
          264101, -- [17]
          280604, -- [18]
          268702, -- [19]
          281621, -- [20]
          262515, -- [21]
          255824, -- [22]
          253583, -- [23]
          250096, -- [24]
        },
        ["UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    \n    \nend\n\n\n",
        ["Time"] = 1539201768,
        ["PlaterCore"] = 1,
        ["Name"] = "Cast - Small Alert [Plater]",
        ["ScriptType"] = 2,
        ["NpcNames"] = {
        },
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.SmallFlashAnimationHub:Stop()\n    \nend\n\n\n",
      }, -- [7]
      {
        ["Enabled"] = true,
        ["Revision"] = 106,
        ["Desc"] = "When an aura makes the unit invulnarable and you don't want to attack it. Add spell in the Add Trigger field.",
        ["Icon"] = "Interface\\AddOns\\Plater\\images\\icon_invalid",
        ["Author"] = "Izimode-Azralon",
        ["ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --color to set the nameplate\n    envTable.NameplateColor = \"gray\"\n    \nend\n\n\n",
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    \nend\n\n\n",
        ["SpellIds"] = {
          261265, -- [1]
          261266, -- [2]
          271590, -- [3]
        },
        ["UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --only change the nameplate color in combat\n    if (InCombatLockdown()) then\n        Plater.SetNameplateColor (unitFrame, envTable.NameplateColor)\n    end\n    \nend\n\n\n\n\n\n\n",
        ["Time"] = 1538256464,
        ["PlaterCore"] = 1,
        ["Name"] = "Aura - Invalidate Unit [Plater]",
        ["ScriptType"] = 1,
        ["NpcNames"] = {
        },
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    \nend\n\n\n",
      }, -- [8]
      {
        ["Enabled"] = true,
        ["Revision"] = 59,
        ["Desc"] = "Add a unitID or unit name in 'Add Trigger' entry. See the constructor script for options.",
        ["Icon"] = 135024,
        ["Author"] = "Izimode-Azralon",
        ["ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --settings:\n    do\n        \n        --change the nameplate color to this color\n        --can use color names: \"red\", \"yellow\"\n        --can use color hex: \"#FF0000\", \"#FFFF00\"\n        --con use color table: {1, 0, 0}, {1, 1, 0}\n        \n        envTable.Color = \"green\"\n        \n        --if true, it'll replace the health info with the unit name\n        envTable.ReplaceHealthWithName = false\n        \n        --use flash when the unit is shown in the screen\n        envTable.FlashNameplate = true\n        \n    end\n    \n    --private:\n    do\n        --create a flash for when the unit if shown\n        envTable.smallFlash = envTable.smallFlash or Plater.CreateFlash (unitFrame.healthBar, 0.15, 1, envTable.Color)\n        \n    end\n    \nend\n\n--[=[\n\nNpc IDS:\n\n141851: Spawn of G'Huun on Mythic Dungeons\n\n\n--]=]\n\n\n\n\n",
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --check if can flash the nameplate\n    if (envTable.FlashNameplate) then\n        envTable.smallFlash:Play()\n    end\n    \nend\n\n\n\n\n\n\n\n\n",
        ["SpellIds"] = {
        },
        ["UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --adjust the nameplate color\n    Plater.SetNameplateColor (unitFrame, envTable.Color)\n    \n    --check if can replace the health amount with the unit name\n    if (envTable.ReplaceHealthWithName) then\n        \n        local healthPercent = format (\"%.1f\", unitFrame.healthBar.CurrentHealth / unitFrame.healthBar.CurrentHealthMax *100)\n        \n        unitFrame.healthBar.lifePercent:SetText (unitFrame.namePlateUnitName .. \"  (\" .. healthPercent  .. \"%)\")\n        \n    end\n    \nend\n\n\n",
        ["Time"] = 1543253273,
        ["PlaterCore"] = 1,
        ["Name"] = "Color Change [Plater]",
        ["ScriptType"] = 3,
        ["NpcNames"] = {
          "141851", -- [1]
        },
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --make plater refresh the nameplate color\n    Plater.RefreshNameplateColor (unitFrame)\n    \n        envTable.smallFlash:Stop()\n    \nend\n\n\n",
      }, -- [9]
      {
        ["Enabled"] = true,
        ["Revision"] = 157,
        ["Desc"] = "Blink, change the number and nameplate color. Add the debuffs int he trigger box. Set settings on constructor script.",
        ["Icon"] = "Interface\\AddOns\\Plater\\images\\icon_aura_blink",
        ["Author"] = "Izimode-Azralon",
        ["ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --settings (require a /reload after editing any setting)\n    do\n        --blink and glow\n        envTable.BlinkEnabled = true --set to 'false' to disable blinks\n        envTable.GlowEnabled = true --set to 'false' to disable glows\n        envTable.ChangeNameplateColor = true; --set to 'true' to enable nameplate color change\n        envTable.TimeLeftToBlink = 4.5; --in seconds, affects the blink effect only\n        envTable.BlinkSpeed = 1.0; --time to complete a blink loop\n        envTable.BlinkColor = \"white\"; --color of the blink\n        envTable.BlinkMaxAlpha = 0.50; --max transparency in the animation loop (1.0 is full opaque)\n        envTable.NameplateColor = \"darkred\"; --nameplate color if ChangeNameplateColor is true\n        \n        --text color\n        envTable.TimerColorEnabled = true --set to 'false' to disable changes in the color of the time left text\n        envTable.TimeLeftWarning = 8.0; --in seconds, affects the color of the text\n        envTable.TimeLeftCritical = 3.0; --in seconds, affects the color of the text    \n        envTable.TextColor_Warning = \"yellow\"; --color when the time left entered in a warning zone\n        envTable.TextColor_Critical = \"red\"; --color when the time left is critical\n        \n        --list of spellIDs to ignore\n        envTable.IgnoredSpellID = {\n            [12] = true, --use a simple comma here\n            [13] = true,\n        }\n    end\n    \n    \n    --private\n    do\n        envTable.blinkTexture = Plater:CreateImage (self, \"\", 1, 1, \"overlay\")\n        envTable.blinkTexture:SetPoint ('center', 0, 0)\n        envTable.blinkTexture:Hide()\n        \n        local onPlay = function()\n            envTable.blinkTexture:Show() \n            envTable.blinkTexture.color = envTable.BlinkColor\n        end\n        local onStop = function()\n            envTable.blinkTexture:Hide()  \n        end\n        envTable.blinkAnimation = Plater:CreateAnimationHub (envTable.blinkTexture, onPlay, onStop)\n        Plater:CreateAnimation (envTable.blinkAnimation, \"ALPHA\", 1, envTable.BlinkSpeed / 2, 0, envTable.BlinkMaxAlpha)\n        Plater:CreateAnimation (envTable.blinkAnimation, \"ALPHA\", 2, envTable.BlinkSpeed / 2, envTable.BlinkMaxAlpha, 0)\n        \n        envTable.glowEffect = envTable.glowEffect or Plater.CreateIconGlow (self)\n        --envTable.glowEffect:Show() --envTable.glowEffect:Hide()\n        \n    end\n    \nend\n\n\n\n\n",
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.blinkTexture:SetSize (self:GetSize())\n    \nend\n\n\n",
        ["SpellIds"] = {
        },
        ["UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    local timeLeft = envTable._RemainingTime\n    \n    --check if the spellID isn't being ignored\n    if (envTable.IgnoredSpellID [envTable._SpellID]) then\n        return\n    end\n    \n    --check the time left and start or stop the blink animation and also check if the time left is > zero\n    if ((envTable.BlinkEnabled or envTable.GlowEnabled) and timeLeft > 0) then\n        if (timeLeft < envTable.TimeLeftToBlink) then\n            --blink effect\n            if (envTable.BlinkEnabled) then\n                if (not envTable.blinkAnimation:IsPlaying()) then\n                    envTable.blinkAnimation:Play()\n                end\n            end\n            --glow effect\n            if (envTable.GlowEnabled) then\n                envTable.glowEffect:Show()\n            end\n            --nameplate color\n            if (envTable.ChangeNameplateColor) then\n                Plater.SetNameplateColor (unitFrame, envTable.NameplateColor)\n            end\n        else\n            --blink effect\n            if (envTable.blinkAnimation:IsPlaying()) then\n                envTable.blinkAnimation:Stop()\n            end\n            --glow effect\n            if (envTable.GlowEnabled and envTable.glowEffect:IsShown()) then\n                envTable.glowEffect:Hide()\n            end\n        end\n    end\n    \n    --timer color\n    if (envTable.TimerColorEnabled and timeLeft > 0) then\n        if (timeLeft < envTable.TimeLeftCritical) then\n            Plater:SetFontColor (self.Cooldown.Timer, envTable.TextColor_Critical)\n        elseif (timeLeft < envTable.TimeLeftWarning) then\n            Plater:SetFontColor (self.Cooldown.Timer, envTable.TextColor_Warning)        \n        else\n            Plater:SetFontColor (self.Cooldown.Timer, Plater.db.profile.aura_timer_text_color)\n        end\n    end\n    \nend",
        ["Time"] = 1547991413,
        ["PlaterCore"] = 1,
        ["Name"] = "Aura - Blink by Time Left [Plater]",
        ["ScriptType"] = 1,
        ["NpcNames"] = {
        },
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    envTable.blinkAnimation:Stop()\n    envTable.blinkTexture:Hide()\n    envTable.blinkAnimation:Stop()\n    envTable.glowEffect:Stop()\n    Plater:SetFontColor (self.Cooldown.Timer, Plater.db.profile.aura_timer_text_color)\nend\n\n\n",
      }, -- [10]
      {
        ["Enabled"] = false,
        ["Revision"] = 45,
        ["Desc"] = "Add a border to an aura icon. Add the aura into the Add Trigger entry. You can customize the icon color at the constructor script.",
        ["Icon"] = 133006,
        ["Author"] = "Izimode-Azralon",
        ["ConstructorCode"] = "--gray lines are comments and doesn't affect the code\n\n--1) add the aura you want by typing its name or spellID into the \"Add Trigger\" and click the \"Add\" button.\n--2) the border will use the default color set below, to a custom color type aura name and the color you want in the BorderColorByAura table.\n\nfunction (self, unitId, unitFrame, envTable)\n    \n    --default color if the aura name isn't found in the Color By Aura table below\n    envTable.DefaultBorderColor = \"orange\"\n    \n    --transparency, affect all borders\n    envTable.BorderAlpha = 1.0\n    \n    --add the aura name and the color, \n    envTable.BorderColorByAura = {\n        \n        --examples:\n        --[\"Aura Name\"] = \"yellow\", --using regular aura name | using the name of the color\n        --[\"aura name\"] = \"#FFFF00\", --using lower case in the aura name |using html #hex for the color\n        --[54214] = {1, 1, 0}, --using the spellID instead of the name | using rgb table (0 to 1) for the color\n        --color table uses zero to one values: 255 = 1.0, 127 = 0.5, orange color = {1, 0.7, 0}\n        \n        --add your custom border colors below:\n        \n        [\"Aura Name\"] = {1, .5, 0}, --example to copy/paste\n        \n    }\n    \n    \nend\n\n\n\n\n",
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --get the aura name in lower case\n    local auraLowerName = string.lower (envTable._SpellName)\n    \n    --attempt to get a custom color added by the user in the constructor script\n    local hasCustomBorderColor = envTable.BorderColorByAura [auraLowerName] or envTable.BorderColorByAura [envTable._SpellName] or envTable.BorderColorByAura [envTable._SpellID]\n    \n    --save the custom color\n    envTable.CustomBorderColor = hasCustomBorderColor\n    \nend\n\n\n",
        ["SpellIds"] = {
        },
        ["UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --get the custom color added by the user or the default color\n    local color = envTable.CustomBorderColor or envTable.DefaultBorderColor\n    --parse the color since it can be a color name, hex or color table\n    local r, g, b = DetailsFramework:ParseColors (color)\n    \n    --set the border color\n    self:SetBackdropBorderColor (r, g, b, envTable.BorderAlpha)\n    \nend\n\n\n\n\n",
        ["Time"] = 1543680853,
        ["PlaterCore"] = 1,
        ["Name"] = "Aura - Border Color [Plater]",
        ["ScriptType"] = 1,
        ["NpcNames"] = {
        },
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --reset the border color\n    self:SetBackdropBorderColor (0, 0, 0, 0)\n    \nend\n\n\n",
      }, -- [11]
      {
        ["Enabled"] = true,
        ["Revision"] = 131,
        ["Desc"] = "Show the energy amount above the nameplate",
        ["Icon"] = 136048,
        ["Author"] = "Celian-Sylvanas",
        ["ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    envTable.EnergyAmount = Plater:CreateLabel (unitFrame, \"\", 16, \"silver\");\n    envTable.EnergyAmount:SetPoint (\"bottom\", unitFrame, \"top\", 0, 18);\nend\n\n--[=[\n\n\n--]=]",
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    envTable.EnergyAmount:Show()\nend\n\n\n",
        ["SpellIds"] = {
        },
        ["UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    envTable.EnergyAmount.text = \"\" .. UnitPower (unitId);\nend\n\n\n",
        ["Time"] = 1539015649,
        ["PlaterCore"] = 1,
        ["Name"] = "UnitPower [Plater]",
        ["ScriptType"] = 3,
        ["NpcNames"] = {
          "Guardian of Yogg-Saron", -- [1]
        },
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    envTable.EnergyAmount:Hide()\nend\n\n\n",
      }, -- [12]
      {
        ["Enabled"] = true,
        ["Revision"] = 171,
        ["Desc"] = "Does an animation for casts that affect the frontal area of the enemy. Add spell in the Add Trigger field.",
        ["Icon"] = "Interface\\AddOns\\Plater\\images\\cast_bar",
        ["Author"] = "Izimode-Azralon",
        ["ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.movingArrow = envTable.movingArrow or Plater:CreateImage (self, [[Interface\\PETBATTLES\\PetBattle-StatIcons]], 16, self:GetHeight(), \"background\", {0, 15/32, 18/32, 30/32})\n    \n    envTable.movingArrow:SetAlpha (0.275)\n    --envTable.movingArrow:SetDesaturated (true)\n    \n    envTable.movingAnimation = envTable.movingAnimation or Plater:CreateAnimationHub (envTable.movingArrow, \n        function() \n            envTable.movingArrow:Show() \n            envTable.movingArrow:SetPoint(\"left\", 0, 0)\n        end, \n        function() envTable.movingArrow:Hide() end)\n    \n    envTable.movingAnimation:SetLooping (\"REPEAT\")\n    \n    local animation = Plater:CreateAnimation (envTable.movingAnimation, \"translation\", 1, 0.2, self:GetWidth()-16, 0)\n    \n    \n    \nend\n\n\n",
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    envTable.movingAnimation:Play()\nend\n\n\n",
        ["SpellIds"] = {
          255952, -- [1]
          257426, -- [2]
          274400, -- [3]
          272609, -- [4]
          269843, -- [5]
          269029, -- [6]
          272827, -- [7]
          269266, -- [8]
          263912, -- [9]
          264923, -- [10]
          258864, -- [11]
          256955, -- [12]
          265540, -- [13]
          260793, -- [14]
          270003, -- [15]
          270507, -- [16]
          257337, -- [17]
          268415, -- [18]
          275907, -- [19]
          268865, -- [20]
          260669, -- [21]
          260280, -- [22]
          253239, -- [23]
          265541, -- [24]
          250258, -- [25]
        },
        ["UpdateCode"] = "		function (self, unitId, unitFrame, envTable)\n			\n		end\n	",
        ["Time"] = 1539201849,
        ["PlaterCore"] = 1,
        ["Name"] = "Cast - Frontal Cone [Plater]",
        ["ScriptType"] = 2,
        ["NpcNames"] = {
        },
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    envTable.movingAnimation:Stop()\nend\n\n\n",
      }, -- [13]
      {
        ["Enabled"] = true,
        ["Revision"] = 190,
        ["Desc"] = "Show above the nameplate who is the player fixated",
        ["Icon"] = 1029718,
        ["Author"] = "Celian-Sylvanas",
        ["ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.FixateTarget = Plater:CreateLabel (unitFrame);\n    envTable.FixateTarget:SetPoint (\"bottom\", unitFrame.BuffFrame, \"top\", 0, 10);    \n    \n    envTable.FixateIcon = Plater:CreateImage (unitFrame, 236188, 16, 16, \"overlay\");\n    envTable.FixateIcon:SetPoint (\"bottom\", envTable.FixateTarget, \"top\", 0, 4);    \n    \nend\n\n\n\n\n\n\n\n\n",
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    envTable.FixateTarget:Show();\n    envTable.FixateIcon:Show();\n    \nend\n\n\n",
        ["SpellIds"] = {
          272584, -- [1]
          244653, -- [2]
        },
        ["UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    local targetName = UnitName (unitId .. \"target\");\n    if (targetName) then\n        local _, class = UnitClass (unitId .. \"target\");\n        targetName = Plater.SetTextColorByClass (unitId .. \"target\", targetName);\n        envTable.FixateTarget.text = targetName;\n    end    \nend\n\n\n",
        ["Time"] = 1539187387,
        ["PlaterCore"] = 1,
        ["Name"] = "Fixate [Plater]",
        ["ScriptType"] = 1,
        ["NpcNames"] = {
        },
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    envTable.FixateTarget:Hide()\n    envTable.FixateIcon:Hide()\nend\n\n\n",
      }, -- [14]
      {
        ["Enabled"] = true,
        ["Revision"] = 194,
        ["Desc"] = "When an enemy places a debuff and starts to chase you. This script changes the nameplate color and place your name above the nameplate as well.",
        ["Icon"] = 841383,
        ["Author"] = "Tecno-Azralon",
        ["ConstructorCode"] = "--todo: add npc ids for multilanguage support\n\nfunction (self, unitId, unitFrame, envTable)\n    \n    --settings\n    envTable.TextAboveNameplate = \"** On You **\"\n    envTable.NameplateColor = \"green\"\n    \n    --label to show the text above the nameplate\n    envTable.FixateTarget = Plater:CreateLabel (unitFrame);\n    envTable.FixateTarget:SetPoint (\"bottom\", unitFrame.healthBar, \"top\", 0, 30);\n    \n    --the spell casted by the npc in the trigger list needs to be in the list below as well\n    local spellList = {\n        [268074] = \"Dark Purpose\", --G'huun Mythic Add\n        [260954] = \"Iron Gaze\", --Sergeant Bainbridge - Siege of Boralus\n        [257739] = \"Blind Rage\", --Blacktooth Scrapper - Freehold\n        [257314] = \"Black Powder Bomb\", --Irontide Grenadier - Freehold\n        [266107] = \"Thirst For Blood\", --Feral Bloodswarmer - The Underrot\n        [257582] = \"Raging Gaze\", --Earthrager - The MOTHERLODE!!\n        [262377] = \"Seek and Destroy\", --Crawler Mine - The MOTHERLODE!!\n        [257407] = \"Pursuit\", --Rezan - Atal'Dazar\n        --[] = \"\" --       \n        \n    }\n    \n    --build the list with localized spell names\n    envTable.FixateDebuffs = {}\n    for spellID, enUSSpellName in pairs (spellList) do\n        local localizedSpellName = GetSpellInfo (spellID)\n        envTable.FixateDebuffs [localizedSpellName or enUSSpellName] = true\n    end\n    \n    --debug - smuggled crawg\n    envTable.FixateDebuffs [\"Jagged Maw\"] = true\n    \nend\n\n--[=[\nNpcIDs:\n136461: Spawn of G'huun (mythic uldir G'huun)\n\n--]=]\n\n\n\n\n",
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    \nend\n\n\n",
        ["SpellIds"] = {
          "spawn of g'huun", -- [1]
          "smuggled crawg", -- [2]
          "sergeant bainbridge", -- [3]
          "blacktooth scrapper", -- [4]
          "irontide grenadier", -- [5]
          "feral bloodswarmer", -- [6]
          "earthrager", -- [7]
          "crawler mine", -- [8]
          "rezan", -- [9]
        },
        ["UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    --swap this to true when it is fixated\n    local isFixated = false\n    \n    --check the debuffs the player has and see if any of these debuffs has been placed by this unit\n    for debuffId = 1, 40 do\n        local name, texture, count, debuffType, duration, expirationTime, caster = UnitDebuff (\"player\", debuffId)\n        \n        --cancel the loop if there's no more debuffs on the player\n        if (not name) then \n            break \n        end\n        \n        --check if the owner of the debuff is this unit\n        if (envTable.FixateDebuffs [name] and caster and UnitIsUnit (caster, unitId)) then\n            --the debuff the player has, has been placed by this unit, set the name above the unit name\n            envTable.FixateTarget:SetText (envTable.TextAboveNameplate)\n            envTable.FixateTarget:Show()\n            Plater.SetNameplateColor (unitFrame,  envTable.NameplateColor)\n            isFixated = true\n            \n            if (not envTable.IsFixated) then\n                envTable.IsFixated = true\n                Plater.FlashNameplateBody (unitFrame, \"fixate\", .2)\n            end\n        end\n        \n    end\n    \n    --check if the nameplate color is changed but isn't fixated any more\n    if (not isFixated and envTable.IsFixated) then\n        --refresh the nameplate color\n        Plater.RefreshNameplateColor (unitFrame)\n        --reset the text\n        envTable.FixateTarget:SetText (\"\")\n        \n        envTable.IsFixated = false\n    end\n    \nend\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n",
        ["Time"] = 1543250950,
        ["PlaterCore"] = 1,
        ["Name"] = "Fixate On You [Plater]",
        ["ScriptType"] = 3,
        ["NpcNames"] = {
          "smuggled crawg", -- [1]
          "sergeant bainbridge", -- [2]
          "blacktooth scrapper", -- [3]
          "irontide grenadier", -- [4]
          "feral bloodswarmer", -- [5]
          "earthrager", -- [6]
          "crawler mine", -- [7]
          "rezan", -- [8]
          "136461", -- [9]
        },
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    envTable.FixateTarget:SetText (\"\")\n    envTable.FixateTarget:Hide()\n    \n    envTable.IsFixated = false\n    \n    Plater.RefreshNameplateColor (unitFrame)\nend\n\n\n",
      }, -- [15]
      {
        ["ConstructorCode"] = "function (self, unitId, unitFrame, envTable, scriptTable)\n    --insert code here\n    \nend\n\n\n",
        ["OnHideCode"] = "		function (self, unitId, unitFrame, envTable, scriptTable)\n			--insert code here\n			\n		end\n	",
        ["Temp_OnShowCode"] = "		function (self, unitId, unitFrame, envTable, scriptTable)\n			--insert code here\n			\n		end\n	",
        ["ScriptType"] = 1,
        ["UpdateCode"] = "		function (self, unitId, unitFrame, envTable, scriptTable)\n			--insert code here\n			\n		end\n	",
        ["Time"] = 1585401441,
        ["Temp_ConstructorCode"] = "		function (self, unitId, unitFrame, envTable, scriptTable)\n			--insert code here\n			\n		end\n	",
        ["NpcNames"] = {
        },
        ["Enabled"] = true,
        ["Revision"] = 2,
        ["Author"] = "Bob-Mythic Dungeon Heroes - EU",
        ["Initialization"] = "		function (scriptTable)\n			--insert code here\n			\n		end\n	",
        ["Desc"] = "",
        ["SpellIds"] = {
        },
        ["Prio"] = 99,
        ["Name"] = "New Script",
        ["PlaterCore"] = 1,
        ["Temp_OnHideCode"] = "		function (self, unitId, unitFrame, envTable, scriptTable)\n			--insert code here\n			\n		end\n	",
        ["OnShowCode"] = "		function (self, unitId, unitFrame, envTable, scriptTable)\n			--insert code here\n			\n		end\n	",
        ["Temp_UpdateCode"] = "		function (self, unitId, unitFrame, envTable, scriptTable)\n			--insert code here\n			\n		end\n	",
        ["Temp_Initialization"] = "		function (scriptTable)\n			--insert code here\n			\n		end\n	",
      }, -- [16]
      {
        ["ConstructorCode"] = "function (self, unitId, unitFrame, envTable, scriptTable)\n    \n    --How to use: Enter NPCs that should be resized into \"Add Trigger\" (ID or Name), then change settings below\n    --If you want different Width/Height for different enemies, duplicate the Script.\n    \n    envTable.UnscaleTargeted = true;  --true = if a trigger unit is targeted custom Width/Height  won't be used, false = will be used\n    envTable.Width = 100;  --set Width of the specific Nameplates\n    envTable.Height = 10; --set Height of the specific Nameplates\nend\n\n\n-- Default scaled units:\n-- ID: 135971 = Faithless Conscript (ToS Orb Room Trash)\n-- ID: 130582 = Despondent Scallywag (Tol Dagor Neutrals)\n-- ID: 130521 = Freehold Deckhand (Freehold Neutrals)\n-- ID: 130522 = Freehold Shipmate (Freehold Neutrals)\n-- ID: 136347 = Tidesage Initiate (SotS mini-Trash)\n-- ID: 134423 = Abyss Dweller (SotS Trash)\n-- ID: 140038 = Abyssal Eel (SotS Eels before Last Boss)\n-- ID: 128435 = Toxic Saurid (Atal'Dazar annoying Raptors)\n-- ID: 137989 = Embalming Fluid (KR Oozes)\n-- ID: 157255 = Aqir Drone (NYA Hivemind Mini Bugs)\n\n\n\n\n\n\n\n\n\n\n\n",
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable, scriptTable)\n    --insert code here\n    \n    \n    \nend\n\n\n",
        ["Temp_OnShowCode"] = "function (self, unitId, unitFrame, envTable, scriptTable)\n    --insert code here\n    \n    if (envTable.UnscaleTargeted) and (unitFrame.namePlateIsTarget) then\n        \n    else\n        Plater.SetNameplateSize (unitFrame, envTable.Width, envTable.Height)\n        \n    end\nend\n\n\n\n\n",
        ["ScriptType"] = 3,
        ["Temp_Initialization"] = "		function (scriptTable)\n			--insert code here\n			\n		end\n	",
        ["Time"] = 1587568312,
        ["Url"] = "https://wago.io/o6sSQSCUI/1",
        ["Icon"] = 134164,
        ["Enabled"] = true,
        ["Revision"] = 170,
        ["NpcNames"] = {
          "135971", -- [1]
          "130582", -- [2]
          "130521", -- [3]
          "130522", -- [4]
          "136347", -- [5]
          "134423", -- [6]
          "140038", -- [7]
          "128435", -- [8]
          "137989", -- [9]
          "157255", -- [10]
          "Scrapbone Grunter", -- [11]
          "Animated Droplet", -- [12]
          "Tidesage Initiate", -- [13]
          "Dredged Sailor", -- [14]
          "Abyss Dweller", -- [15]
        },
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable, scriptTable)\n    --insert code here\n    \n    if (envTable.UnscaleTargeted) and (unitFrame.namePlateIsTarget) then\n        \n    else\n        Plater.SetNameplateSize (unitFrame, envTable.Width, envTable.Height)\n        \n    end\nend\n\n\n\n\n",
        ["Author"] = "Caeleran-Draenor",
        ["Initialization"] = "		function (scriptTable)\n			--insert code here\n			\n		end\n	",
        ["Desc"] = "",
        ["SpellIds"] = {
        },
        ["Temp_UpdateCode"] = "function (self, unitId, unitFrame, envTable, scriptTable)\n    --insert code here\n    \n    if (envTable.UnscaleTargeted) and (unitFrame.namePlateIsTarget) then\n        \n    else\n        Plater.SetNameplateSize (unitFrame, envTable.Width, envTable.Height)\n        \n    end\nend\n\n\n",
        ["Name"] = "SpecificNameplateResize",
        ["PlaterCore"] = 1,
        ["Temp_ConstructorCode"] = "function (self, unitId, unitFrame, envTable, scriptTable)\n    \n    --How to use: Enter NPCs that should be resized into \"Add Trigger\" (ID or Name), then change settings below\n    --If you want different Width/Height for different enemies, duplicate the Script.\n    \n    envTable.UnscaleTargeted = true;  --true = if a trigger unit is targeted custom Width/Height  won't be used, false = will be used\n    envTable.Width = 100;  --set Width of the specific Nameplates\n    envTable.Height = 10; --set Height of the specific Nameplates\nend\n\n\n-- Default scaled units:\n-- ID: 135971 = Faithless Conscript (ToS Orb Room Trash)\n-- ID: 130582 = Despondent Scallywag (Tol Dagor Neutrals)\n-- ID: 130521 = Freehold Deckhand (Freehold Neutrals)\n-- ID: 130522 = Freehold Shipmate (Freehold Neutrals)\n-- ID: 136347 = Tidesage Initiate (SotS mini-Trash)\n-- ID: 134423 = Abyss Dweller (SotS Trash)\n-- ID: 140038 = Abyssal Eel (SotS Eels before Last Boss)\n-- ID: 128435 = Toxic Saurid (Atal'Dazar annoying Raptors)\n-- ID: 137989 = Embalming Fluid (KR Oozes)\n-- ID: 157255 = Aqir Drone (NYA Hivemind Mini Bugs)\n\n\n\n\n\n\n\n\n\n\n\n",
        ["Prio"] = 99,
        ["Temp_OnHideCode"] = "function (self, unitId, unitFrame, envTable, scriptTable)\n    --insert code here\n    \n    \n    \nend\n\n\n",
        ["UpdateCode"] = "function (self, unitId, unitFrame, envTable, scriptTable)\n    --insert code here\n    \n    if (envTable.UnscaleTargeted) and (unitFrame.namePlateIsTarget) then\n        \n    else\n        Plater.SetNameplateSize (unitFrame, envTable.Width, envTable.Height)\n        \n    end\nend\n\n\n",
      }, -- [17]
      {
        ["ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.ShowTargeted = true; -- true or false; determines if the nameplate shoudl be shown if you have the unit targeted\n    \nend",
        ["OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    \nend",
        ["Temp_OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    if (envTable.ShowTargeted) and (unitFrame.namePlateIsTarget) then\n        Plater.ShowHealthBar (unitFrame)\n        Plater.EnableHighlight (unitFrame)\n    else\n        Plater.HideHealthBar (unitFrame)\n        Plater.DisableHighlight (unitFrame)\n        unitFrame.castBar:Hide ()\n    end    \n    \n    \nend",
        ["ScriptType"] = 3,
        ["UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    if (envTable.ShowTargeted) and (unitFrame.namePlateIsTarget) then\n        Plater.ShowHealthBar (unitFrame)\n        Plater.EnableHighlight (unitFrame)\n    else\n        Plater.HideHealthBar (unitFrame)\n        Plater.DisableHighlight (unitFrame)\n        unitFrame.castBar:Hide ()\n    end    \n    \n    \nend",
        ["Time"] = 1587568315,
        ["Url"] = "https://wago.io/eDUdpOkg2/6",
        ["Icon"] = 644389,
        ["Enabled"] = true,
        ["Revision"] = 322,
        ["Author"] = "Evosparks-Area 52",
        ["NpcNames"] = {
        },
        ["Desc"] = "Hide's nameplates based on the unit's name or npcID. Add the Unit Name or npcID as a trigger.",
        ["SpellIds"] = {
        },
        ["Temp_UpdateCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    if (envTable.ShowTargeted) and (unitFrame.namePlateIsTarget) then\n        Plater.ShowHealthBar (unitFrame)\n        Plater.EnableHighlight (unitFrame)\n    else\n        Plater.HideHealthBar (unitFrame)\n        Plater.DisableHighlight (unitFrame)\n        unitFrame.castBar:Hide ()\n    end    \n    \n    \nend",
        ["Name"] = "Hide Nameplate by UnitID",
        ["PlaterCore"] = 1,
        ["OnShowCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    if (envTable.ShowTargeted) and (unitFrame.namePlateIsTarget) then\n        Plater.ShowHealthBar (unitFrame)\n        Plater.EnableHighlight (unitFrame)\n    else\n        Plater.HideHealthBar (unitFrame)\n        Plater.DisableHighlight (unitFrame)\n        unitFrame.castBar:Hide ()\n    end    \n    \n    \nend",
        ["Temp_ConstructorCode"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.ShowTargeted = true; -- true or false; determines if the nameplate shoudl be shown if you have the unit targeted\n    \nend",
        ["Prio"] = 99,
        ["Temp_OnHideCode"] = "function (self, unitId, unitFrame, envTable)\n    \nend",
      }, -- [18]
    },
    ["plate_config"] = {
      ["friendlyplayer"] = {
        ["only_damaged"] = false,
        ["actorname_use_class_color"] = true,
      },
      ["enemynpc"] = {
        ["actorname_text_spacing"] = 9,
        ["spellname_text_anchor"] = {
          ["x"] = 3,
          ["side"] = 10,
        },
        ["actorname_text_color"] = {
          nil, -- [1]
          0.9490196078431372, -- [2]
          0.9372549019607843, -- [3]
        },
        ["percent_show_health"] = false,
        ["percent_text_show_decimals"] = false,
        ["actorname_text_size"] = 10,
        ["actorname_text_anchor"] = {
          ["side"] = 8,
        },
        ["level_text_enabled"] = false,
      },
      ["enemyplayer"] = {
        ["big_actorname_text_size"] = 10,
        ["level_text_size"] = 8,
        ["big_actortitle_text_size"] = 10,
        ["spellname_text_outline"] = "OUTLINE",
        ["all_names"] = true,
        ["actorname_text_spacing"] = 9,
        ["quest_color_enemy"] = {
          1, -- [1]
          0.369, -- [2]
          0, -- [3]
        },
        ["cast_incombat"] = {
          nil, -- [1]
          14, -- [2]
        },
        ["spellname_text_anchor"] = {
          ["x"] = 3,
          ["side"] = 10,
        },
        ["quest_color_neutral"] = {
          1, -- [1]
          0.65, -- [2]
          0, -- [3]
        },
        ["actorname_text_anchor"] = {
          ["side"] = 8,
        },
        ["spellpercent_text_size"] = 11,
        ["quest_enabled"] = true,
        ["actorname_text_color"] = {
          nil, -- [1]
          0.9490196078431372, -- [2]
          0.9372549019607843, -- [3]
        },
        ["percent_text_show_decimals"] = false,
        ["spellname_text_size"] = 12,
        ["actorname_text_size"] = 10,
        ["percent_show_health"] = false,
        ["level_text_enabled"] = false,
      },
    },
    ["use_ui_parent"] = true,
    ["indicator_elite"] = false,
    ["npc_cache"] = {
      [163678] = {
        "Clotted Corruption", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [134139] = {
        "Shrine Templar", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [151658] = {
        "Strider Tonk", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [152809] = {
        "Alx'kov the Infested", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [69947] = {
        "Monkey", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [134012] = {
        "Taskmaster Askari", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [136186] = {
        "Tidesage Spiritualist", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [151659] = {
        "Rocket Tonk", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [157158] = {
        "Cultist Slavedriver", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [150253] = {
        "Weaponized Crawler", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [158565] = {
        "Naros", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [138489] = {
        "Shadow of Zul", -- [1]
        "Kings' Rest", -- [2]
      },
      [152939] = {
        "Boundless Corruption", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [161251] = {
        "Cultist Sycophant", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [150254] = {
        "Scraphound", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [144244] = {
        "The Platinum Pummeler", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [129602] = {
        "Irontide Enforcer", -- [1]
        "Freehold", -- [2]
      },
      [161124] = {
        "Urg'roth, Breaker of Heroes", -- [1]
        "Tol Dagor", -- [2]
      },
      [131585] = {
        "Enthralled Guard", -- [1]
        "Waycrest Manor", -- [2]
      },
      [153196] = {
        "Scrapbone Grunter", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [158567] = {
        "Tormented Kor'kron Annihilator", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [158056] = {
        "Rat", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [131586] = {
        "Banquet Steward", -- [1]
        "Waycrest Manor", -- [2]
      },
      [144246] = {
        "K.U.-J.0.", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [156650] = {
        "Dark Manifestation", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [131587] = {
        "Bewitched Captain", -- [1]
        "Waycrest Manor", -- [2]
      },
      [161510] = {
        "Mindrend Tentacle", -- [1]
        "Tol Dagor", -- [2]
      },
      [156523] = {
        "Maut", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [77942] = {
        "Primal Storm Elemental", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [138493] = {
        "Minion of Zul", -- [1]
        "Kings' Rest", -- [2]
      },
      [135552] = {
        "Deathtouched Slaver", -- [1]
        "Waycrest Manor", -- [2]
      },
      [123210] = {
        "Seabreeze Gull", -- [1]
        "Freehold", -- [2]
      },
      [162534] = {
        "Anubisath Sentinel", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [144248] = {
        "Head Machinist Sparkflux", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [135169] = {
        "Spirit Drain Totem", -- [1]
        "The Underrot", -- [2]
      },
      [161895] = {
        "Thing From Beyond", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [133379] = {
        "Adderis", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [158315] = {
        "Eye of Chaos", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [144249] = {
        "Omega Buster", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [156653] = {
        "Coagulated Horror", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [127111] = {
        "Irontide Oarsman", -- [1]
        "Freehold", -- [2]
      },
      [130436] = {
        "Off-Duty Laborer", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [162664] = {
        "Aqir Swarmer", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [141565] = {
        "Kul Tiran Footman", -- [1]
        "Siege of Boralus", -- [2]
      },
      [156143] = {
        "Voidcrazed Hulk", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [131847] = {
        "Waycrest Reveler", -- [1]
        "Waycrest Manor", -- [2]
      },
      [129989] = {
        "Irontide Powdershot", -- [1]
        "Siege of Boralus", -- [2]
      },
      [138369] = {
        "Footbomb Hooligan", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [137474] = {
        "King Timalji", -- [1]
        "Kings' Rest", -- [2]
      },
      [130437] = {
        "Mine Rat", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [128455] = {
        "T'lonja", -- [1]
        "Atal'Dazar", -- [2]
      },
      [134150] = {
        "Runecarver Sorn", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [156145] = {
        "Burrowing Appendage", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [131849] = {
        "Crazed Marksman", -- [1]
        "Waycrest Manor", -- [2]
      },
      [163690] = {
        "Shath'Yar Scribe", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [133384] = {
        "Merektha", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [156146] = {
        "Voidbound Shieldbearer", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [131850] = {
        "Maddened Survivalist", -- [1]
        "Waycrest Manor", -- [2]
      },
      [134024] = {
        "Devouring Maggot", -- [1]
        "Waycrest Manor", -- [2]
      },
      [127497] = {
        "Ashvane Warden", -- [1]
        "Tol Dagor", -- [2]
      },
      [137989] = {
        "Embalming Fluid", -- [1]
        "Kings' Rest", -- [2]
      },
      [135048] = {
        "Gorestained Piglet", -- [1]
        "Waycrest Manor", -- [2]
      },
      [137478] = {
        "Queen Wasi", -- [1]
        "Kings' Rest", -- [2]
      },
      [132491] = {
        "Kul Tiran Marksman", -- [1]
        "Siege of Boralus", -- [2]
      },
      [135049] = {
        "Dreadwing Raven", -- [1]
        "Waycrest Manor", -- [2]
      },
      [139269] = {
        "Gloom Horror", -- [1]
        "Waycrest Manor", -- [2]
      },
      [157811] = {
        "Lilliam Sparkspindle", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [128649] = {
        "Sergeant Bainbridge", -- [1]
        "Siege of Boralus", -- [2]
      },
      [129928] = {
        "Irontide Powdershot", -- [1]
        "Siege of Boralus", -- [2]
      },
      [128969] = {
        "Ashvane Commander", -- [1]
        "Siege of Boralus", -- [2]
      },
      [138247] = {
        "Irontide Marauder", -- [1]
        "Siege of Boralus", -- [2]
      },
      [157812] = {
        "Billibub Cogspinner", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [135562] = {
        "Venomous Ophidian", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [159219] = {
        "Umbral Seer", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [156406] = {
        "Voidbound Honor Guard", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [150396] = {
        "Aerial Unit R-21/X", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [154744] = {
        "Toxic Monstrosity", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [128650] = {
        "Chopper Redhook", -- [1]
        "Siege of Boralus", -- [2]
      },
      [162417] = {
        "Anubisath Sentinel", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [150397] = {
        "King Mechagon", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [134157] = {
        "Shadow-Borne Warrior", -- [1]
        "Kings' Rest", -- [2]
      },
      [152699] = {
        "Voidbound Berserker", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [150142] = {
        "Scrapbone Trashtosser", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [136076] = {
        "Agitated Nimbus", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [134158] = {
        "Shadow-Borne Champion", -- [1]
        "Kings' Rest", -- [2]
      },
      [128651] = {
        "Hadal Darkfathom", -- [1]
        "Siege of Boralus", -- [2]
      },
      [161140] = {
        "Bwemba", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [129802] = {
        "Earthrager", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [150143] = {
        "Scrapbone Grinder", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [137484] = {
        "King A'akul", -- [1]
        "Kings' Rest", -- [2]
      },
      [129227] = {
        "Azerokk", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [158328] = {
        "Il'gynoth", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [131858] = {
        "Thornguard", -- [1]
        "Waycrest Manor", -- [2]
      },
      [129547] = {
        "Blacktooth Knuckleduster", -- [1]
        "Freehold", -- [2]
      },
      [156794] = {
        "SI:7 Light-Hunter", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [137485] = {
        "Bloodsworn Agent", -- [1]
        "Kings' Rest", -- [2]
      },
      [159224] = {
        "Gryth'ax the Executioner", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [156795] = {
        "SI:7 Informant", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [137486] = {
        "Queen Patlaa", -- [1]
        "Kings' Rest", -- [2]
      },
      [137614] = {
        "Demolishing Terror", -- [1]
        "Siege of Boralus", -- [2]
      },
      [150146] = {
        "Scrapbone Shaman", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [137103] = {
        "Blood Visage", -- [1]
        "The Underrot", -- [2]
      },
      [138254] = {
        "Irontide Powdershot", -- [1]
        "Siege of Boralus", -- [2]
      },
      [130635] = {
        "Stonefury", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [137487] = {
        "Skeletal Hunting Raptor", -- [1]
        "Kings' Rest", -- [2]
      },
      [160249] = {
        "Spike Tentacle", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [154367] = {
        "Octagon", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [129996] = {
        "Irontide Cleaver", -- [1]
        "Siege of Boralus", -- [2]
      },
      [138255] = {
        "Ashvane Spotter", -- [1]
        "Siege of Boralus", -- [2]
      },
      [122963] = {
        "Rezan", -- [1]
        "Atal'Dazar", -- [2]
      },
      [127119] = {
        "Freehold Deckhand", -- [1]
        "Freehold", -- [2]
      },
      [150276] = {
        "Heavy Scrapbot", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [158588] = {
        "Gamon", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [127503] = {
        "Overseer Korgus", -- [1]
        "Tol Dagor", -- [2]
      },
      [135699] = {
        "Ashvane Jailer", -- [1]
        "Tol Dagor", -- [2]
      },
      [131863] = {
        "Raal the Gluttonous", -- [1]
        "Waycrest Manor", -- [2]
      },
      [136083] = {
        "Forgotten Denizen", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [151812] = {
        "Detect-o-Bot", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [157439] = {
        "Fury of N'Zoth", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [129550] = {
        "Bilge Rat Padfoot", -- [1]
        "Freehold", -- [2]
      },
      [156161] = {
        "Inquisitor Gnshal", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [152325] = {
        "Rebecca Laughlin", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [122965] = {
        "Vol'kaal", -- [1]
        "Atal'Dazar", -- [2]
      },
      [134423] = {
        "Abyss Dweller", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [129231] = {
        "Rixxa Fluxflame", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [33510] = {
        "Animul", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [152326] = {
        "Kyra Boucher", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [136214] = {
        "Windspeaker Heldis", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [157825] = {
        "Crazed Tormenter", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [136470] = {
        "Refreshment Vendor", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [157442] = {
        "Gaze of Madness", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [134041] = {
        "Infected Peasant", -- [1]
        "Waycrest Manor", -- [2]
      },
      [139284] = {
        "Plague Doctor", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [129232] = {
        "Mogul Razdunk", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [150154] = {
        "Saurolisk Bonenipper", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [129552] = {
        "Monzumi", -- [1]
        "Atal'Dazar", -- [2]
      },
      [154758] = {
        "Toxic Monstrosity", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [162303] = {
        "Aqir Swarmkeeper", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [161408] = {
        "Malicious Growth", -- [1]
        "Tol Dagor", -- [2]
      },
      [136984] = {
        "Reban", -- [1]
        "Kings' Rest", -- [2]
      },
      [157700] = {
        "Agustus Moulaine", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [135322] = {
        "The Golden Serpent", -- [1]
        "Kings' Rest", -- [2]
      },
      [135706] = {
        "Bilge Rat Looter", -- [1]
        "Tol Dagor", -- [2]
      },
      [127315] = {
        "Reanimation Totem", -- [1]
        "Atal'Dazar", -- [2]
      },
      [152330] = {
        "Jelinek Sharpshear", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [132126] = {
        "Gilded Priestess", -- [1]
        "Atal'Dazar", -- [2]
      },
      [137625] = {
        "Demolishing Terror", -- [1]
        "Siege of Boralus", -- [2]
      },
      [139799] = {
        "Ironhull Apprentice", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [130896] = {
        "Blackout Barrel", -- [1]
        "Freehold", -- [2]
      },
      [152331] = {
        "Captain Lancy Revshon", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [163712] = {
        "Dying Voidspawn", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [134173] = {
        "Animated Droplet", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [136347] = {
        "Tidesage Initiate", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [127124] = {
        "Freehold Barhand", -- [1]
        "Freehold", -- [2]
      },
      [137626] = {
        "Demolishing Terror", -- [1]
        "Siege of Boralus", -- [2]
      },
      [139800] = {
        "Galecaller Apprentice", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [157447] = {
        "Fanatical Cultist", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [134174] = {
        "Shadow-Borne Witch Doctor", -- [1]
        "Kings' Rest", -- [2]
      },
      [137627] = {
        "Constricting Terror", -- [1]
        "Siege of Boralus", -- [2]
      },
      [134686] = {
        "Mature Krolusk", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [150159] = {
        "King Gobbamak", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [122969] = {
        "Zanchuli Witch-Doctor", -- [1]
        "Atal'Dazar", -- [2]
      },
      [150160] = {
        "Scrapbone Bully", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [127381] = {
        "Silt Crab", -- [1]
        "Tol Dagor", -- [2]
      },
      [152718] = {
        "Alleria Windrunner", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [161286] = {
        "Dark Ritualist", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [157450] = {
        "Spellbound Ritualist", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [122970] = {
        "Shadowblade Stalker", -- [1]
        "Atal'Dazar", -- [2]
      },
      [131492] = {
        "Devout Blood Priest", -- [1]
        "The Underrot", -- [2]
      },
      [136735] = {
        "Ashvane Marine", -- [1]
        "Tol Dagor", -- [2]
      },
      [157451] = {
        "Iron-Willed Enforcer", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [135329] = {
        "Matron Bryndle", -- [1]
        "Waycrest Manor", -- [2]
      },
      [161416] = {
        "Aqir Shadowcrafter", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [157452] = {
        "Nightmare Antigen", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [150547] = {
        "Scrapbone Grunter", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [136353] = {
        "Colossal Tentacle", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [158092] = {
        "Fallen Heartpiercer", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [134691] = {
        "Static-charged Dervish", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [150292] = {
        "Mechagon Cavalry", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [152722] = {
        "Fallen Voidspeaker", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [150165] = {
        "Slime Elemental", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [150293] = {
        "Mechagon Prowler", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [135204] = {
        "Spectral Hex Priest", -- [1]
        "Kings' Rest", -- [2]
      },
      [122972] = {
        "Dazar'ai Augur", -- [1]
        "Atal'Dazar", -- [2]
      },
      [136483] = {
        "Ashvane Deckhand", -- [1]
        "Siege of Boralus", -- [2]
      },
      [129366] = {
        "Bilge Rat Buccaneer", -- [1]
        "Siege of Boralus", -- [2]
      },
      [158478] = {
        "Corruption Tumor", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [136100] = {
        "Sunken Denizen", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [139425] = {
        "Crazed Incubator", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [150295] = {
        "Tank Buster MK1", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [122973] = {
        "Dazar'ai Confessor", -- [1]
        "Atal'Dazar", -- [2]
      },
      [161293] = {
        "Neglected Guild Bank", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [135846] = {
        "Sand-Crusted Striker", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [134056] = {
        "Aqu'sirr", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [130582] = {
        "Despondent Scallywag", -- [1]
        "Tol Dagor", -- [2]
      },
      [156818] = {
        "Wrathion", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [129879] = {
        "Irontide Cleaver", -- [1]
        "Siege of Boralus", -- [2]
      },
      [135975] = {
        "Off-Duty Laborer", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [159632] = {
        "Cultist Shadowblade", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [144160] = {
        "Chopper Redhook", -- [1]
        "Siege of Boralus", -- [2]
      },
      [134058] = {
        "Galecaller Faye", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [156820] = {
        "Dod", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [137511] = {
        "Bilge Rat Cutthroat", -- [1]
        "Siege of Boralus", -- [2]
      },
      [152089] = {
        "Thrall", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [156949] = {
        "Armsmaster Terenson", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [129369] = {
        "Irontide Raider", -- [1]
        "Siege of Boralus", -- [2]
      },
      [157461] = {
        "Mycelial Cyst", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [134060] = {
        "Lord Stormsong", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [151579] = {
        "Shield Generator", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [134828] = {
        "Aqualing", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [139304] = {
        "Solid Snake", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [152987] = {
        "Faceless Willbreaker", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [134701] = {
        "Blood Effigy", -- [1]
        "The Underrot", -- [2]
      },
      [129370] = {
        "Irontide Waveshaper", -- [1]
        "Siege of Boralus", -- [2]
      },
      [151325] = {
        "Alarm-o-Bot", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [130521] = {
        "Freehold Deckhand", -- [1]
        "Freehold", -- [2]
      },
      [153755] = {
        "Naeno Megacrash", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [152988] = {
        "Faceless Shadowcaller", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [153244] = {
        "Oblivion Elemental", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [133935] = {
        "Animated Guardian", -- [1]
        "Kings' Rest", -- [2]
      },
      [134063] = {
        "Brother Ironhull", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [135470] = {
        "Aka'ali the Conqueror", -- [1]
        "Kings' Rest", -- [2]
      },
      [161173] = {
        "Abyssal Watcher", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [139946] = {
        "Heart Guardian", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [130522] = {
        "Freehold Shipmate", -- [1]
        "Freehold", -- [2]
      },
      [159767] = {
        "Sanguimar", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [72339] = {
        "Large Illusionary Flamecaller", -- [1]
        "Proving Grounds", -- [2]
      },
      [137517] = {
        "Ashvane Destroyer", -- [1]
        "Siege of Boralus", -- [2]
      },
      [132530] = {
        "Kul Tiran Vanguard", -- [1]
        "Siege of Boralus", -- [2]
      },
      [26125] = {
        "Rootleaper", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [160663] = {
        "Essence of Nightmare", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [159768] = {
        "Deresh of the Nothingness", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [135472] = {
        "Zanazal the Wise", -- [1]
        "Kings' Rest", -- [2]
      },
      [153119] = {
        "Lesser Void Elemental", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [129372] = {
        "Blacktar Bomber", -- [1]
        "Siege of Boralus", -- [2]
      },
      [24207] = {
        "Army of the Dead", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [144296] = {
        "Spider Tank", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [132532] = {
        "Kul Tiran Marksman", -- [1]
        "Siege of Boralus", -- [2]
      },
      [139949] = {
        "Plague Doctor", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [159514] = {
        "Blood of Ny'alotha", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [130012] = {
        "Irontide Ravager", -- [1]
        "Freehold", -- [2]
      },
      [153760] = {
        "Enthralled Footman", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [135474] = {
        "Thistle Acolyte", -- [1]
        "Waycrest Manor", -- [2]
      },
      [129373] = {
        "Dockhound Packmaster", -- [1]
        "Siege of Boralus", -- [2]
      },
      [144170] = {
        "Ashvane Sniper", -- [1]
        "Siege of Boralus", -- [2]
      },
      [144298] = {
        "Defense Bot Mk III", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [137521] = {
        "Irontide Powdershot", -- [1]
        "Siege of Boralus", -- [2]
      },
      [133685] = {
        "Befouled Spirit", -- [1]
        "The Underrot", -- [2]
      },
      [156575] = {
        "Dark Inquisitor Xanesh", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [133430] = {
        "Venture Co. Mastermind", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [129374] = {
        "Scrimshaw Enforcer", -- [1]
        "Siege of Boralus", -- [2]
      },
      [144300] = {
        "Mechagon Citizen", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [130653] = {
        "Wanton Sapper", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [129758] = {
        "Irontide Grenadier", -- [1]
        "Freehold", -- [2]
      },
      [162331] = {
        "Corrupted Neuron", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [158367] = {
        "Basher Tentacle", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [133943] = {
        "Minion of Zul", -- [1]
        "Kings' Rest", -- [2]
      },
      [156577] = {
        "Therum Deepforge", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [133432] = {
        "Venture Co. Alchemist", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [161437] = {
        "Explosive Scarab", -- [1]
        "Tol Dagor", -- [2]
      },
      [133944] = {
        "Aspix", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [162716] = {
        "Spellbound Ritualist", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [72343] = {
        "Large Illusionary Hive-Singer", -- [1]
        "Proving Grounds", -- [2]
      },
      [157602] = {
        "Drest'agath", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [160416] = {
        "Angry Ale Barrel Spirit", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [157475] = {
        "Synthesis Growth", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [157603] = {
        "Void Globule", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [130655] = {
        "Bobby Howlis", -- [1]
        "Tol Dagor", -- [2]
      },
      [161312] = {
        "Crushing Tendril", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [158371] = {
        "Zardeth of the Black Claw", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [157604] = {
        "Crawling Corruption", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [154663] = {
        "Gnome-Eating Droplet", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [156837] = {
        "Valeera Sanguinar", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [89288] = {
        "Toxic", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [130400] = {
        "Irontide Crusher", -- [1]
        "Freehold", -- [2]
      },
      [157605] = {
        "Burrowing Appendage", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [136250] = {
        "Hoodoo Hexer", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [152874] = {
        "Vez'okk the Lightless", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [153130] = {
        "Greater Void Elemental", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [122984] = {
        "Dazar'ai Colossus", -- [1]
        "Atal'Dazar", -- [2]
      },
      [150190] = {
        "HK-8 Aerial Oppression Unit", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [161571] = {
        "Anubisath Sentinel", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [157607] = {
        "Faceless Shadowcaller", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [72346] = {
        "Large Illusionary Aqualyte", -- [1]
        "Proving Grounds", -- [2]
      },
      [158375] = {
        "Corruptor Tentacle", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [163746] = {
        "Walkie Shockie X1", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [158376] = {
        "Psychus", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [157609] = {
        "K'thir Mindcarver", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [135231] = {
        "Spectral Brute", -- [1]
        "Kings' Rest", -- [2]
      },
      [137405] = {
        "Gripping Terror", -- [1]
        "Siege of Boralus", -- [2]
      },
      [129699] = {
        "Ludwig Von Tortollan", -- [1]
        "Freehold", -- [2]
      },
      [157354] = {
        "Vexiona", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [157610] = {
        "K'thir Dominator", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [122986] = {
        "Wild Skyscreamer", -- [1]
        "Atal'Dazar", -- [2]
      },
      [134338] = {
        "Tidesage Enforcer", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [150195] = {
        "Gnome-Eating Slime", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [157612] = {
        "Eye of Drest'agath", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [135234] = {
        "Diseased Mastiff", -- [1]
        "Waycrest Manor", -- [2]
      },
      [160937] = {
        "Night Terror", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [159275] = {
        "Portal Keeper", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [130404] = {
        "Vermin Trapper", -- [1]
        "Freehold", -- [2]
      },
      [157613] = {
        "Maw of Drest'agath", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [135235] = {
        "Spectral Beastmaster", -- [1]
        "Kings' Rest", -- [2]
      },
      [155951] = {
        "Ruffer", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [131527] = {
        "Lord Waycrest", -- [1]
        "Waycrest Manor", -- [2]
      },
      [157486] = {
        "Horrific Hemorrhage", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [151476] = {
        "Blastatron X-80", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [155952] = {
        "Suffer", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [136643] = {
        "Azerite Extractor", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [157231] = {
        "Shad'har the Insatiable", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [159405] = {
        "Aqir Scarab", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [128551] = {
        "Irontide Mastiff", -- [1]
        "Freehold", -- [2]
      },
      [130661] = {
        "Venture Co. Earthshaper", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [155953] = {
        "C'Thuffer", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [131785] = {
        "Buzzing Drone", -- [1]
        "Tol Dagor", -- [2]
      },
      [136005] = {
        "Rowdy Reveler", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [135366] = {
        "Blacktooth Arsonist", -- [1]
        "Tol Dagor", -- [2]
      },
      [131402] = {
        "Underrot Tick", -- [1]
        "The Underrot", -- [2]
      },
      [134599] = {
        "Imbued Stormcaller", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [153141] = {
        "Endless Hunger Totem", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [137029] = {
        "Ordnance Specialist", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [26016] = {
        "Garbo", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [135239] = {
        "Spectral Witch Doctor", -- [1]
        "Kings' Rest", -- [2]
      },
      [150712] = {
        "Trixie Tazer", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [161198] = {
        "Warpweaver Dushar", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [151096] = {
        "Chonkadog", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [153526] = {
        "Aqir Swarmer", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [135240] = {
        "Soul Essence", -- [1]
        "Waycrest Manor", -- [2]
      },
      [136391] = {
        "Heart Guardian", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [153527] = {
        "Aqir Swarmleader", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [136264] = {
        "Half-Finished Mummy", -- [1]
        "Kings' Rest", -- [2]
      },
      [129640] = {
        "Snarling Dockhound", -- [1]
        "Siege of Boralus", -- [2]
      },
      [156980] = {
        "Essence of Void", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [134602] = {
        "Shrouded Fang", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [133835] = {
        "Feral Bloodswarmer", -- [1]
        "The Underrot", -- [2]
      },
      [133963] = {
        "Test Subject", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [157620] = {
        "Prophet Skitra", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [127019] = {
        "Training Dummy", -- [1]
        "Freehold", -- [2]
      },
      [148797] = {
        "Magus of the Dead", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [157365] = {
        "Crackling Stalker", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [153401] = {
        "K'thir Dominator", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [416] = {
        "Chouri", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [50051] = {
        "YEET", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [157238] = {
        "Prophet Skitra", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [157366] = {
        "Void Hunter", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [136139] = {
        "Mechanized Peacekeeper", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [153531] = {
        "Aqir Bonecrusher", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [158774] = {
        "Broken Citizen", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [156089] = {
        "Aqir Venomweaver", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [134990] = {
        "Charged Dust Devil", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [153532] = {
        "Aqir Mindhunter", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [160182] = {
        "Void Initiate", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [131666] = {
        "Coven Thornshaper", -- [1]
        "Waycrest Manor", -- [2]
      },
      [134991] = {
        "Sandfury Stonefist", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [160183] = {
        "Void Fanatic", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [161334] = {
        "Gnashing Terror", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [159416] = {
        "Spiked Tentacle", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [138061] = {
        "Venture Co. Longshoreman", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [130027] = {
        "Ashvane Marine", -- [1]
        "Tol Dagor", -- [2]
      },
      [158136] = {
        "Inquisitor Darkspeak", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [150251] = {
        "Pistonhead Mechanic", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [144071] = {
        "Irontide Waveshaper", -- [1]
        "Siege of Boralus", -- [2]
      },
      [138002] = {
        "Scrimshaw Gutter", -- [1]
        "Siege of Boralus", -- [2]
      },
      [161335] = {
        "Void Horror", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [159417] = {
        "Demented Knife-Twister", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [134993] = {
        "Mchimba the Embalmer", -- [1]
        "Kings' Rest", -- [2]
      },
      [164189] = {
        "Horrific Figment", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [128435] = {
        "Toxic Saurid", -- [1]
        "Atal'Dazar", -- [2]
      },
      [129600] = {
        "Bilge Rat Brinescale", -- [1]
        "Freehold", -- [2]
      },
      [158690] = {
        "Cultist Tormenter", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [133972] = {
        "Heavy Cannon", -- [1]
        "Tol Dagor", -- [2]
      },
      [135761] = {
        "Thundering Totem", -- [1]
        "Kings' Rest", -- [2]
      },
      [157813] = {
        "Sprite Jumpsprocket", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [138063] = {
        "Posh Vacationer", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [130028] = {
        "Ashvane Priest", -- [1]
        "Tol Dagor", -- [2]
      },
      [159266] = {
        "Portal Master", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [158437] = {
        "Fallen Taskmaster", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [139422] = {
        "Scaled Krolusk Tamer", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [158140] = {
        "Frenzied Rat", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [131670] = {
        "Heartsbane Vinetwister", -- [1]
        "Waycrest Manor", -- [2]
      },
      [61146] = {
        "Black Ox Statue", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [138064] = {
        "Posh Vacationer", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [129517] = {
        "Reanimated Raptor", -- [1]
        "Atal'Dazar", -- [2]
      },
      [152703] = {
        "Walkie Shockie X1", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [140038] = {
        "Abyssal Eel", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [134739] = {
        "Purification Construct", -- [1]
        "Kings' Rest", -- [2]
      },
      [134612] = {
        "Grasping Tentacles", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [122971] = {
        "Dazar'ai Juggernaut", -- [1]
        "Atal'Dazar", -- [2]
      },
      [126832] = {
        "Skycap'n Kragg", -- [1]
        "Freehold", -- [2]
      },
      [150249] = {
        "Pistonhead Scrapper", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [160699] = {
        "Angry Mailemental", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [158781] = {
        "Shredded Psyche", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [133663] = {
        "Fanatical Headhunter", -- [1]
        "The Underrot", -- [2]
      },
      [131667] = {
        "Soulbound Goliath", -- [1]
        "Waycrest Manor", -- [2]
      },
      [144311] = {
        "Orb Guardian", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [135764] = {
        "Explosive Totem", -- [1]
        "Kings' Rest", -- [2]
      },
      [89] = {
        "Infernal", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [138066] = {
        "Posh Vacationer", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [132056] = {
        "Venture Co. Skyscorcher", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [129599] = {
        "Cutwater Knife Juggler", -- [1]
        "Freehold", -- [2]
      },
      [133836] = {
        "Reanimated Guardian", -- [1]
        "The Underrot", -- [2]
      },
      [160061] = {
        "Crawling Corruption", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [131545] = {
        "Lady Waycrest", -- [1]
        "Waycrest Manor", -- [2]
      },
      [135765] = {
        "Torrent Totem", -- [1]
        "Kings' Rest", -- [2]
      },
      [151613] = {
        "Anti-Personnel Squirrel", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [122968] = {
        "Yazma", -- [1]
        "Atal'Dazar", -- [2]
      },
      [42717] = {
        "Bob", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [135254] = {
        "Irontide Raider", -- [1]
        "Tol Dagor", -- [2]
      },
      [135052] = {
        "Blight Toad", -- [1]
        "Waycrest Manor", -- [2]
      },
      [135365] = {
        "Matron Alma", -- [1]
        "Waycrest Manor", -- [2]
      },
      [157449] = {
        "Sinister Soulcarver", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [153377] = {
        "Goop", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [137940] = {
        "Safety Shark", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [138068] = {
        "Posh Vacationer", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [153541] = {
        "Slavemaster Ul'rok", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [134232] = {
        "Hired Assassin", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [156866] = {
        "Ra-den", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [126642] = {
        "Sandyback Crab", -- [1]
        "Siege of Boralus", -- [2]
      },
      [134616] = {
        "Krolusk Pup", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [156641] = {
        "Enthralled Weaponsmith", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [130024] = {
        "Soggy Shiprat", -- [1]
        "Freehold", -- [2]
      },
      [141495] = {
        "Kul Tiran Footman", -- [1]
        "Siege of Boralus", -- [2]
      },
      [162719] = {
        "Void Ascendant", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [155656] = {
        "Misha", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [157614] = {
        "Tentacle of Drest'agath", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [162717] = {
        "Sinister Soulcarver", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [134617] = {
        "Krolusk Hatchling", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [162718] = {
        "Iron-Willed Enforcer", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [159425] = {
        "Occult Shadowmender", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [144303] = {
        "G.U.A.R.D.", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [160704] = {
        "Letter Encrusted Void Globule", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [135971] = {
        "Faithless Conscript", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [135167] = {
        "Spectral Berserker", -- [1]
        "Kings' Rest", -- [2]
      },
      [159764] = {
        "Jesh'ra", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [152009] = {
        "Malfunctioning Scrapbot", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [131677] = {
        "Heartsbane Runeweaver", -- [1]
        "Waycrest Manor", -- [2]
      },
      [134144] = {
        "Living Current", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [136330] = {
        "Soul Thorns", -- [1]
        "Waycrest Manor", -- [2]
      },
      [134389] = {
        "Venomous Ophidian", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [135258] = {
        "Irontide Marauder", -- [1]
        "Siege of Boralus", -- [2]
      },
      [130435] = {
        "Addled Thug", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [133436] = {
        "Venture Co. Skyscorcher", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [136665] = {
        "Ashvane Spotter", -- [1]
        "Tol Dagor", -- [2]
      },
      [157253] = {
        "Ka'zir", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [133852] = {
        "Living Rot", -- [1]
        "The Underrot", -- [2]
      },
      [128434] = {
        "Feasting Skyscreamer", -- [1]
        "Atal'Dazar", -- [2]
      },
      [150696] = {
        "Ray", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [137969] = {
        "Interment Construct", -- [1]
        "Kings' Rest", -- [2]
      },
      [134364] = {
        "Faithless Tender", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [152396] = {
        "Guardian of Azeroth", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [110340] = {
        "Doggo", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [157254] = {
        "Tek'ris", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [144169] = {
        "Ashvane Commander", -- [1]
        "Siege of Boralus", -- [2]
      },
      [160904] = {
        "Image of Absolution", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [51052] = {
        "Andybrew", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [153065] = {
        "Voidbound Ravager", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [33776] = {
        "Spirit Beast", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [162432] = {
        "Awakened Terror", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [144295] = {
        "Mechagon Mechanic", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [157255] = {
        "Aqir Drone", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [150222] = {
        "Gunker", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [139097] = {
        "Sandswept Marksman", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [162306] = {
        "Aqir Drone", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [150169] = {
        "Toxic Lurker", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [129601] = {
        "Cutwater Harpooner", -- [1]
        "Freehold", -- [2]
      },
      [126983] = {
        "Harlan Sweete", -- [1]
        "Freehold", -- [2]
      },
      [153022] = {
        "Snang", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [158279] = {
        "Haywire Clockwork Rocket Bot", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [161218] = {
        "Aqir Crusher", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [159578] = {
        "Exposed Synapse", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [128967] = {
        "Ashvane Sniper", -- [1]
        "Siege of Boralus", -- [2]
      },
      [134417] = {
        "Deepsea Ritualist", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [136688] = {
        "Fanatical Driller", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [136541] = {
        "Bile Oozeling", -- [1]
        "Waycrest Manor", -- [2]
      },
      [134514] = {
        "Abyssal Cultist", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [159303] = {
        "Monstrous Behemoth", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [163708] = {
        "Umbral Gatekeeper", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [135007] = {
        "Orb Guardian", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [127477] = {
        "Saltwater Snapper", -- [1]
        "Tol Dagor", -- [2]
      },
      [135263] = {
        "Ashvane Spotter", -- [1]
        "Siege of Boralus", -- [2]
      },
      [133345] = {
        "Feckless Assistant", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [136249] = {
        "Guardian Elemental", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [135475] = {
        "Kula the Butcher", -- [1]
        "Kings' Rest", -- [2]
      },
      [159633] = {
        "Cultist Executioner", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [135903] = {
        "Manifestation of the Deep", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [157256] = {
        "Aqir Darter", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [144294] = {
        "Mechagon Tinkerer", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [128652] = {
        "Viq'Goth", -- [1]
        "Siege of Boralus", -- [2]
      },
      [130026] = {
        "Bilge Rat Seaspeaker", -- [1]
        "Tol Dagor", -- [2]
      },
      [135759] = {
        "Earthwall Totem", -- [1]
        "Kings' Rest", -- [2]
      },
      [157349] = {
        "Void Boar", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [159305] = {
        "Maddened Conscript", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [131812] = {
        "Heartsbane Soulcharmer", -- [1]
        "Waycrest Manor", -- [2]
      },
      [126928] = {
        "Irontide Corsair", -- [1]
        "Freehold", -- [2]
      },
      [136160] = {
        "King Dazar", -- [1]
        "Kings' Rest", -- [2]
      },
      [130011] = {
        "Irontide Buccaneer", -- [1]
        "Freehold", -- [2]
      },
      [162305] = {
        "Aqir Heartpiercer", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [133463] = {
        "Venture Co. War Machine", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [129553] = {
        "Dinomancer Kish'o", -- [1]
        "Atal'Dazar", -- [2]
      },
      [131685] = {
        "Runic Disciple", -- [1]
        "Waycrest Manor", -- [2]
      },
      [158411] = {
        "Unstable Servant", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [161217] = {
        "Aqir Skitterer", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [129548] = {
        "Blacktooth Brute", -- [1]
        "Freehold", -- [2]
      },
      [162828] = {
        "Corrosive Digester", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [158327] = {
        "Crackling Shard", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [130025] = {
        "Irontide Thug", -- [1]
        "Tol Dagor", -- [2]
      },
      [135989] = {
        "Shieldbearer of Zul", -- [1]
        "Atal'Dazar", -- [2]
      },
      [158284] = {
        "Craggle Wobbletop", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [141566] = {
        "Scrimshaw Gutter", -- [1]
        "Siege of Boralus", -- [2]
      },
      [140447] = {
        "Demolishing Terror", -- [1]
        "Siege of Boralus", -- [2]
      },
      [127479] = {
        "The Sand Queen", -- [1]
        "Tol Dagor", -- [2]
      },
      [131112] = {
        "Cutwater Striker", -- [1]
        "Tol Dagor", -- [2]
      },
      [138464] = {
        "Ashvane Deckhand", -- [1]
        "Siege of Boralus", -- [2]
      },
      [158285] = {
        "Tinkered Shieldbot", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [158157] = {
        "Overlord Mathias Shaw", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [127799] = {
        "Dazar'ai Honor Guard", -- [1]
        "Atal'Dazar", -- [2]
      },
      [134069] = {
        "Vol'zith the Whisperer", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [138281] = {
        "Faceless Corruptor", -- [1]
        "The Underrot", -- [2]
      },
      [131383] = {
        "Sporecaller Zancha", -- [1]
        "The Underrot", -- [2]
      },
      [131825] = {
        "Sister Briar", -- [1]
        "Waycrest Manor", -- [2]
      },
      [138465] = {
        "Ashvane Cannoneer", -- [1]
        "Siege of Boralus", -- [2]
      },
      [133990] = {
        "Scrimshaw Gutter", -- [1]
        "Siege of Boralus", -- [2]
      },
      [134629] = {
        "Scaled Krolusk Rider", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [159309] = {
        "Leeching Parasite", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [127480] = {
        "Stinging Parasite", -- [1]
        "Tol Dagor", -- [2]
      },
      [130485] = {
        "Mechanized Peacekeeper", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [129526] = {
        "Bilge Rat Swabby", -- [1]
        "Freehold", -- [2]
      },
      [138338] = {
        "Reanimated Guardian", -- [1]
        "The Underrot", -- [2]
      },
      [158286] = {
        "Reprogrammed Warbot", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [135192] = {
        "Honored Raptor", -- [1]
        "Kings' Rest", -- [2]
      },
      [155090] = {
        "Anodized Coilbearer", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [144293] = {
        "Waste Processing Unit", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [131817] = {
        "Cragmaw the Infested", -- [1]
        "The Underrot", -- [2]
      },
      [130909] = {
        "Fetid Maggot", -- [1]
        "The Underrot", -- [2]
      },
      [126969] = {
        "Trothak", -- [1]
        "Freehold", -- [2]
      },
      [162508] = {
        "Anubisath Sentinel", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [157904] = {
        "Aqir Scarab", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [136549] = {
        "Ashvane Cannoneer", -- [1]
        "Siege of Boralus", -- [2]
      },
      [161229] = {
        "Aqir Venomweaver", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [132713] = {
        "Mogul Razdunk", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [131818] = {
        "Marked Sister", -- [1]
        "Waycrest Manor", -- [2]
      },
      [131824] = {
        "Sister Solena", -- [1]
        "Waycrest Manor", -- [2]
      },
      [162764] = {
        "Twisted Appendage", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [129527] = {
        "Bilge Rat Buccaneer", -- [1]
        "Freehold", -- [2]
      },
      [38453] = {
        "bsyaseen", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [160405] = {
        "Angry Treant Chair Spirit", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [158146] = {
        "Fallen Riftwalker", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [159312] = {
        "Living Blood", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [136934] = {
        "Weapons Tester", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [129371] = {
        "Riptide Shredder", -- [1]
        "Siege of Boralus", -- [2]
      },
      [141282] = {
        "Kul Tiran Footman", -- [1]
        "Siege of Boralus", -- [2]
      },
      [136295] = {
        "Sunken Denizen", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [153942] = {
        "Annihilator Lak'hal", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [131436] = {
        "Chosen Blood Matron", -- [1]
        "The Underrot", -- [2]
      },
      [133482] = {
        "Crawler Mine", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [137830] = {
        "Pallid Gorger", -- [1]
        "Waycrest Manor", -- [2]
      },
      [137473] = {
        "Guard Captain Atu", -- [1]
        "Kings' Rest", -- [2]
      },
      [141283] = {
        "Kul Tiran Halberd", -- [1]
        "Siege of Boralus", -- [2]
      },
      [127482] = {
        "Sewer Vicejaw", -- [1]
        "Tol Dagor", -- [2]
      },
      [158035] = {
        "Magister Umbric", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [156884] = {
        "Essence of Vita", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [153943] = {
        "Decimator Shiq'voth", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [155094] = {
        "Mechagon Trooper", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [157268] = {
        "Crawling Corruption", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [131821] = {
        "Faceless Maiden", -- [1]
        "Waycrest Manor", -- [2]
      },
      [139110] = {
        "Spark Channeler", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [141284] = {
        "Kul Tiran Wavetender", -- [1]
        "Siege of Boralus", -- [2]
      },
      [136297] = {
        "Forgotten Denizen", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [159510] = {
        "Eye of the Depths", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [134418] = {
        "Drowned Depthbringer", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [134600] = {
        "Sandswept Marksman", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [161745] = {
        "Hepthys", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [134251] = {
        "Seneschal M'bara", -- [1]
        "Kings' Rest", -- [2]
      },
      [130488] = {
        "Mech Jockey", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [141285] = {
        "Kul Tiran Marksman", -- [1]
        "Siege of Boralus", -- [2]
      },
      [129529] = {
        "Blacktooth Scrapper", -- [1]
        "Freehold", -- [2]
      },
      [159308] = {
        "Zealous Adherent", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [122967] = {
        "Priestess Alun'za", -- [1]
        "Atal'Dazar", -- [2]
      },
      [142587] = {
        "Devouring Maggot", -- [1]
        "Waycrest Manor", -- [2]
      },
      [136006] = {
        "Rowdy Reveler", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [131823] = {
        "Sister Malady", -- [1]
        "Waycrest Manor", -- [2]
      },
      [134284] = {
        "Fallen Deathspeaker", -- [1]
        "The Underrot", -- [2]
      },
      [161746] = {
        "Ossirat", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [129208] = {
        "Dread Captain Lockwood", -- [1]
        "Siege of Boralus", -- [2]
      },
      [131819] = {
        "Coven Diviner", -- [1]
        "Waycrest Manor", -- [2]
      },
      [160404] = {
        "Angry Bear Rug Spirit", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [157594] = {
        "Lesser Void Elemental", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [133007] = {
        "Unbound Abomination", -- [1]
        "The Underrot", -- [2]
      },
      [133870] = {
        "Diseased Lasher", -- [1]
        "The Underrot", -- [2]
      },
      [137233] = {
        "Plague Toad", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [127484] = {
        "Jes Howlis", -- [1]
        "Tol Dagor", -- [2]
      },
      [131009] = {
        "Spirit of Gold", -- [1]
        "Atal'Dazar", -- [2]
      },
      [151773] = {
        "Junkyard D.0.G.", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [138019] = {
        "Kul Tiran Vanguard", -- [1]
        "Siege of Boralus", -- [2]
      },
      [155098] = {
        "Rexxar", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [160341] = {
        "Sewer Beastling", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [126845] = {
        "Captain Jolly", -- [1]
        "Freehold", -- [2]
      },
      [158158] = {
        "Forge-Guard Hurrul", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [144286] = {
        "Asset Manager", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [152669] = {
        "Void Globule", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [135406] = {
        "Animated Gold", -- [1]
        "Kings' Rest", -- [2]
      },
      [139626] = {
        "Dredged Sailor", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [152332] = {
        "Lara Moore", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [150297] = {
        "Mechagon Renormalizer", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [153097] = {
        "Voidbound Shaman", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [163841] = {
        "Amalgamation of Flesh", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [127485] = {
        "Bilge Rat Looter", -- [1]
        "Tol Dagor", -- [2]
      },
      [133912] = {
        "Bloodsworn Defiler", -- [1]
        "The Underrot", -- [2]
      },
      [133361] = {
        "Wasting Servant", -- [1]
        "Waycrest Manor", -- [2]
      },
      [158041] = {
        "N'Zoth the Corruptor", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [155657] = {
        "Huffer", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [159320] = {
        "Amahtet", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [158343] = {
        "Organ of Corruption", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [144231] = {
        "Rowdy Reveler", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [127879] = {
        "Shieldbearer of Zul", -- [1]
        "Atal'Dazar", -- [2]
      },
      [108624] = {
        "Floorpov", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [150168] = {
        "Toxic Monstrosity", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [134994] = {
        "Spectral Headhunter", -- [1]
        "Kings' Rest", -- [2]
      },
      [144301] = {
        "Living Waste", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [159321] = {
        "Khateph", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [126918] = {
        "Irontide Crackshot", -- [1]
        "Freehold", -- [2]
      },
      [144232] = {
        "Rowdy Reveler", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [127486] = {
        "Ashvane Officer", -- [1]
        "Tol Dagor", -- [2]
      },
      [151649] = {
        "Defense Bot Mk I", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [145185] = {
        "Gnomercy 4.U.", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [131445] = {
        "Block Warden", -- [1]
        "Tol Dagor", -- [2]
      },
      [152033] = {
        "Inconspicuous Plant", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [129788] = {
        "Irontide Bonesaw", -- [1]
        "Freehold", -- [2]
      },
      [126847] = {
        "Captain Raoul", -- [1]
        "Freehold", -- [2]
      },
      [162647] = {
        "Willing Sacrifice", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [157608] = {
        "Faceless Willbreaker", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [126919] = {
        "Irontide Stormcaller", -- [1]
        "Freehold", -- [2]
      },
      [131318] = {
        "Elder Leaxa", -- [1]
        "The Underrot", -- [2]
      },
      [133593] = {
        "Expert Technician", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [161241] = {
        "Voidweaver Mal'thir", -- [1]
        "Tol Dagor", -- [2]
      },
      [144168] = {
        "Ashvane Destroyer", -- [1]
        "Siege of Boralus", -- [2]
      },
      [132481] = {
        "Kul Tiran Vanguard", -- [1]
        "Siege of Boralus", -- [2]
      },
      [72336] = {
        "Small Illusionary Ripper", -- [1]
        "Proving Grounds", -- [2]
      },
      [65310] = {
        "Turnip Punching Bag", -- [1]
        "The Underrot", -- [2]
      },
      [134331] = {
        "King Rahu'ai", -- [1]
        "Kings' Rest", -- [2]
      },
      [134388] = {
        "A Knot of Snakes", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [132051] = {
        "Blood Tick", -- [1]
        "The Underrot", -- [2]
      },
      [137713] = {
        "Big Money Crab", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [129367] = {
        "Bilge Rat Tempest", -- [1]
        "Siege of Boralus", -- [2]
      },
      [126848] = {
        "Captain Eudora", -- [1]
        "Freehold", -- [2]
      },
      [134005] = {
        "Shalebiter", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [160420] = {
        "Angry Book Spirit", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [138187] = {
        "Grotesque Horror", -- [1]
        "The Underrot", -- [2]
      },
      [137458] = {
        "Rotting Spore", -- [1]
        "The Underrot", -- [2]
      },
      [129214] = {
        "Coin-Operated Crowd Pummeler", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [161243] = {
        "Samh'rek, Beckoner of Chaos", -- [1]
        "Tol Dagor", -- [2]
      },
      [137516] = {
        "Ashvane Invader", -- [1]
        "Siege of Boralus", -- [2]
      },
      [144299] = {
        "Workshop Defender", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [129559] = {
        "Cutwater Duelist", -- [1]
        "Freehold", -- [2]
      },
      [127488] = {
        "Ashvane Flamecaster", -- [1]
        "Tol Dagor", -- [2]
      },
      [129598] = {
        "Freehold Pack Mule", -- [1]
        "Freehold", -- [2]
      },
      [134390] = {
        "Sand-crusted Striker", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [136976] = {
        "T'zala", -- [1]
        "Kings' Rest", -- [2]
      },
      [161244] = {
        "Blood of the Corruptor", -- [1]
        "Tol Dagor", -- [2]
      },
      [152324] = {
        "Aldwin Laughlin", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [131864] = {
        "Gorak Tul", -- [1]
        "Waycrest Manor", -- [2]
      },
      [154524] = {
        "K'thir Mindcarver", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [137204] = {
        "Hoodoo Hexer", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [151654] = {
        "Deuce Mecha-Buffer", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [125977] = {
        "Reanimation Totem", -- [1]
        "Atal'Dazar", -- [2]
      },
      [158452] = {
        "Mindtwist Tendril", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [137716] = {
        "Bottom Feeder", -- [1]
        "The MOTHERLODE!!", -- [2]
      },
      [33502] = {
        "Billy", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [162715] = {
        "Fanatical Cultist", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [127757] = {
        "Reanimated Honor Guard", -- [1]
        "Atal'Dazar", -- [2]
      },
      [156642] = {
        "Enthralled Laborer", -- [1]
        "Horrific Vision of Stormwind", -- [2]
      },
      [10981] = {
        "Wolf", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [160990] = {
        "Image of Absolution", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [130087] = {
        "Irontide Raider", -- [1]
        "Tol Dagor", -- [2]
      },
      [157467] = {
        "Void Ascendant", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [135241] = {
        "Bilge Rat Pillager", -- [1]
        "Siege of Boralus", -- [2]
      },
      [161502] = {
        "Ravenous Fleshfiend", -- [1]
        "Tol Dagor", -- [2]
      },
      [133389] = {
        "Galvazzt", -- [1]
        "Temple of Sethraliss", -- [2]
      },
      [134137] = {
        "Temple Attendant", -- [1]
        "Shrine of the Storm", -- [2]
      },
      [131669] = {
        "Jagged Hound", -- [1]
        "Waycrest Manor", -- [2]
      },
      [127106] = {
        "Irontide Officer", -- [1]
        "Freehold", -- [2]
      },
      [164188] = {
        "Horrific Figment", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [69946] = {
        "Hygge", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [141938] = {
        "Ashvane Sniper", -- [1]
        "Siege of Boralus", -- [2]
      },
      [150250] = {
        "Pistonhead Blaster", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [135245] = {
        "Bilge Rat Demolisher", -- [1]
        "Siege of Boralus", -- [2]
      },
      [127490] = {
        "Knight Captain Valyri", -- [1]
        "Tol Dagor", -- [2]
      },
      [151657] = {
        "Bomb Tonk", -- [1]
        "Operation: Mechagon", -- [2]
      },
      [160291] = {
        "Ashwalker Assassin", -- [1]
        "Ny'alotha, the Waking City", -- [2]
      },
      [137591] = {
        "Healing Tide Totem", -- [1]
        "Kings' Rest", -- [2]
      },
      [152704] = {
        "Crawling Corruption", -- [1]
        "Horrific Vision of Orgrimmar", -- [2]
      },
      [141939] = {
        "Ashvane Spotter", -- [1]
        "Siege of Boralus", -- [2]
      },
      [125828] = {
        "Fatsamurai", -- [1]
        "Atal'Dazar", -- [2]
      },
      [120651] = {
        "Explosives", -- [1]
        "Temple of Sethraliss", -- [2]
      },
    },
    ["indicator_scale"] = 2,
    ["aggro_modifies"] = {
      ["health_bar_color"] = false,
    },
    ["hook_data"] = {
      {
        ["Enabled"] = false,
        ["Revision"] = 50,
        ["HooksTemp"] = {
        },
        ["Author"] = "Kastfall-Azralon",
        ["Desc"] = "Easy way to change the color of an unit. Open the constructor script and follow the examples.",
        ["Hooks"] = {
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    --list of npcs and their colors, can be inserted:\n    --name of the unit\n    --name of the unit in lower case\n    --npcID of the unit\n    \n    --color can be added as:\n    --color names: \"red\", \"yellow\"\n    --color hex: \"#FF0000\", \"#FFFF00\"\n    --color table: {1, 0, 0}, {1, 1, 0}    \n    \n    envTable.NpcColors = {\n        \n        --examples, using the unit name in lower case, regular unit name and the unitID:\n        \n        [\"Thunderlord Windreader\"] = \"red\", --using regular mob name and color it as red\n        [\"thunderlord crag-leaper\"] = {1, 1, 0}, --using lower case and coloring it yellow\n        [75790] = \"#00FF00\", --using the ID of the unit and using green as color\n        \n        --insert the new mobs here:\n        \n        \n        \n        \n        \n        \n        \n        \n        \n        \n        \n        \n        \n    } --close custom color bracket\n    \nend\n\n\n\n\n",
          ["Nameplate Updated"] = "function (self, unitId, unitFrame, envTable)\n    \n    --attempt to get the color from the unit color list\n    local color = envTable.NpcColors [unitFrame.namePlateUnitNameLower] or envTable.NpcColors [unitFrame.namePlateUnitName] or envTable.NpcColors [unitFrame.namePlateNpcId]\n    \n    --if the color exists, set the health bar color\n    if (color) then\n        Plater.SetNameplateColor (unitFrame, color)\n    end\n    \nend\n\n\n\n\n\n\n\n\n\n\n\n",
        },
        ["Time"] = 1547392935,
        ["PlaterCore"] = 1,
        ["Name"] = "Color Automation [Plater]",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["role"] = {
          },
          ["pvptalent"] = {
          },
          ["spec"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
          },
          ["race"] = {
          },
        },
        ["Icon"] = "Interface\\AddOns\\Plater\\images\\color_bar",
        ["LastHookEdited"] = "",
      }, -- [1]
      {
        ["Enabled"] = false,
        ["Revision"] = 59,
        ["HooksTemp"] = {
        },
        ["Author"] = "Izimode-Azralon",
        ["Desc"] = "Change the nameplate color when a nameplate does not have the auras set in the constructor script.",
        ["Hooks"] = {
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    --Important: lines starting with double dashes are comments and are not part of the script\n    \n    --set this to true if you are not using threat colors in the health bar\n    envTable.ForceRefreshNameplateColor = true\n    \n    --if the unit does not have any of the following auras, it will be painted with the color listed below\n    --list of spells to track, can be the spell name (case-sensitive) or the spellID\n    envTable.TrackingAuras = {\n        --[\"Nightblade\"] = true, --this is an example using the spell name\n        --[195452] = true, --this is an example using the spellID\n        \n    }\n    \n    --which color the nameplate wil be changed\n    --color can be added as:\n    --color names: \"red\", \"yellow\"\n    --color hex: \"#FF0000\", \"#FFFF00\"\n    --color table: {1, 0, 0}, {1, 1, 0}    \n    --you may also use /plater colors\n    envTable.NameplateColor = \"pink\"\n    \nend",
          ["Nameplate Updated"] = "function (self, unitId, unitFrame, envTable)\n    \n    --do nothing if the player isn't in combat\n    if (not Plater.IsInCombat()) then\n        return \n    end\n    \n    --do nothing if the unit isn't in combat\n    if (not unitFrame.InCombat) then\n        return\n    end\n    \n    --do nothing if the unit is the player it self\n    if (unitFrame.IsSelf) then\n        return\n    end\n    \n    --check the auras\n    local hasAura = false\n    \n    for auraName, _ in pairs (envTable.TrackingAuras) do\n        if (Plater.NameplateHasAura (unitFrame, auraName)) then\n            hasAura = true\n            break\n        end\n    end\n    \n    if (not hasAura) then\n        Plater.SetNameplateColor (unitFrame, envTable.NameplateColor)\n    else\n        if (envTable.ForceRefreshNameplateColor) then\n            Plater.RefreshNameplateColor (unitFrame) \n        end\n    end    \n    \nend",
        },
        ["Time"] = 1554138845,
        ["PlaterCore"] = 1,
        ["Name"] = "Don't Have Aura [Plater]",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["role"] = {
          },
          ["pvptalent"] = {
          },
          ["race"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
          },
          ["spec"] = {
          },
        },
        ["Icon"] = 136207,
        ["LastHookEdited"] = "",
      }, -- [2]
      {
        ["Enabled"] = false,
        ["Revision"] = 176,
        ["HooksTemp"] = {
        },
        ["Author"] = "Tecno-Azralon",
        ["Desc"] = "Add another border with more customizations. This border can also be manipulated by other scripts.",
        ["Hooks"] = {
          ["Nameplate Created"] = "function (self, unitId, unitFrame, envTable)\n    \n    --run constructor!\n    \nend\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n",
          ["Nameplate Added"] = "function (self, unitId, unitFrame, envTable)\n    if (envTable.IsEnabled) then\n        if (unitFrame.IsSelf) then\n            if (envTable.ShowOnPersonalBar) then\n                envTable.BorderFrame:Show()\n            else\n                envTable.BorderFrame:Hide() \n            end\n        else\n            envTable.BorderFrame:Show()\n        end   \n    end\n    \nend   \n\n\n\n",
          ["Nameplate Removed"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.BorderFrame:Hide()\n    \nend\n\n\n",
          ["Destructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.BorderFrame:Hide()\n    \nend\n\n\n",
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    --border color\n    local borderColor = \"yellow\"\n    \n    --size of the border\n    local borderSize = 1\n    \n    --transparency\n    local borderAlpha = 1\n    \n    --enabled (set to false it you only want to use the extra border in other scripts)\n    local isEnabled = true\n    \n    --export border (allow the border to be used by other scripts)\n    --other scripts can use:\n    --unitFrame.healthBar.extraBorder:Show()\n    --unitFrame.healthBar.extraBorder:SetVertexColor (r, g, b)\n    --unitFrame.healthBar.extraBorder:SetBorderSizes (borderSize)\n    local canExportBorder = true\n    \n    --do not add the border to personal bar\n    local noPersonalBar = true\n    \n    --private\n    do\n        \n        local newBorder = CreateFrame (\"frame\", nil, unitFrame.healthBar, \"NamePlateFullBorderTemplate\")\n        envTable.BorderFrame = newBorder\n        \n        newBorder:SetBorderSizes (borderSize, borderSize, borderSize, borderSize)\n        newBorder:UpdateSizes()\n        \n        local r, g, b = DetailsFramework:ParseColors (borderColor)\n        newBorder:SetVertexColor (r, g, b, borderAlpha)\n        \n        envTable.ShowOnPersonalBar = not noPersonalBar\n        \n        if (canExportBorder) then\n            unitFrame.healthBar.extraBorder = newBorder\n        end\n        \n        if (not isEnabled) then\n            envTable.IsEnabled = false\n        else\n            envTable.IsEnabled = true\n        end\n    end\n    \nend\n\n\n",
        },
        ["Time"] = 1547409079,
        ["PlaterCore"] = 1,
        ["Name"] = "Extra Border [Plater]",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["race"] = {
          },
          ["pvptalent"] = {
          },
          ["role"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
          },
          ["spec"] = {
          },
        },
        ["Icon"] = 133689,
        ["LastHookEdited"] = "",
      }, -- [3]
      {
        ["Enabled"] = false,
        ["Revision"] = 55,
        ["HooksTemp"] = {
        },
        ["Author"] = "Kastfall-Azralon",
        ["Desc"] = "Script for Stormwall Blockade encounter on Battle for Dazzar'alor",
        ["Hooks"] = {
          ["Nameplate Updated"] = "function (self, unitId, unitFrame, envTable)\n    \n    if (unitFrame.namePlateNpcId == envTable.NpcIDs.TemptingSiren) then\n        \n        if (envTable.Colors.TemptingSiren) then\n            Plater.SetNameplateColor (unitFrame, envTable.Colors.TemptingSiren)\n        end\n        \n    end\n    \n    \nend\n\n\n\n\n\n\n\n\n",
          ["Nameplate Added"] = "function (self, unitId, unitFrame, envTable)\n    \n    if (unitFrame.namePlateNpcId == envTable.NpcIDs.TemptingSiren) then\n        \n        if (envTable.NameplateHeight.TemptingSiren) then\n            \n            Plater.SetNameplateSize (unitFrame, nil, envTable.NameplateHeight.TemptingSiren)\n            \n        end\n        \n    end    \n    \nend\n\n\n",
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    --nameplate colors for unit\n    envTable.Colors = {}\n    envTable.Colors.TemptingSiren = \"orange\"\n    \n    --npcID\n    envTable.NpcIDs = {}\n    envTable.NpcIDs.TemptingSiren = 146436\n    \n    --nameplate height for each unit\n    envTable.NameplateHeight = {}\n    envTable.NameplateHeight.TemptingSiren = 18\n    \n    \n    \nend\n\n\n",
        },
        ["Time"] = 1548117267,
        ["PlaterCore"] = 1,
        ["Name"] = "Stormwall Encounter [Plater]",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["role"] = {
          },
          ["pvptalent"] = {
          },
          ["race"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
            2280, -- [1]
            ["Enabled"] = true,
          },
          ["spec"] = {
          },
        },
        ["Icon"] = "Interface\\AddOns\\Plater\\images\\encounter_stormwall_blockade",
        ["LastHookEdited"] = "",
      }, -- [4]
      {
        ["Enabled"] = false,
        ["Revision"] = 76,
        ["HooksTemp"] = {
        },
        ["Author"] = "Izimode-Azralon",
        ["Desc"] = "Hide neutral units, show when selected, see the constructor script for options.",
        ["Hooks"] = {
          ["Leave Combat"] = "function (self, unitId, unitFrame, envTable)\n    if (unitFrame.namePlateUnitReaction == envTable.REACTION_NEUTRAL) then\n        \n        --plater already handle this\n        if (unitFrame.PlayerCannotAttack) then\n            return\n        end    \n        \n        --check if is only open world\n        if (envTable.OnlyInOpenWorld and Plater.ZoneInstanceType ~= \"none\") then\n            return \n        end\n        \n        --check for only in combat\n        if (envTable.ShowInCombat) then\n            envTable.HideNameplate (unitFrame)\n        end\n    end\nend\n\n\n",
          ["Nameplate Added"] = "function (self, unitId, unitFrame, envTable)\n    \n    if (unitFrame.namePlateUnitReaction == envTable.REACTION_NEUTRAL) then\n        \n        --plater already handle this\n        if (unitFrame.PlayerCannotAttack) then\n            return\n        end\n        \n        --check if is only open world\n        if (envTable.OnlyInOpenWorld and Plater.ZoneInstanceType ~= \"none\") then\n            return \n        end\n        \n        --check for only in combat\n        if (envTable.ShowInCombat and InCombatLockdown()) then\n            return\n        end\n        \n        envTable.HideNameplate (unitFrame)\n    end\n    \nend\n\n\n\n\n\n\n",
          ["Target Changed"] = "function (self, unitId, unitFrame, envTable)\n    \n    if (unitFrame.namePlateUnitReaction == envTable.REACTION_NEUTRAL) then\n        \n        --plater already handle this\n        if (unitFrame.PlayerCannotAttack) then\n            return\n        end    \n        \n        --check if is only open world\n        if (envTable.OnlyInOpenWorld and Plater.ZoneInstanceType ~= \"none\") then\n            return \n        end\n        \n        --check for only in combat\n        if (envTable.ShowInCombat and InCombatLockdown()) then\n            return\n        end\n        \n        --check the unit reaction\n        if (unitFrame.namePlateIsTarget) then\n            envTable.ShowNameplate (unitFrame)\n            \n        else\n            envTable.HideNameplate (unitFrame)\n            \n        end    \n    end\n    \nend\n\n\n\n\n\n\n",
          ["Nameplate Removed"] = "function (self, unitId, unitFrame, envTable)\n    \n    if (unitFrame.namePlateUnitReaction == envTable.REACTION_NEUTRAL) then\n        envTable.ShowNameplate (unitFrame)\n    end\n    \nend\n\n\n\n\n",
          ["Nameplate Updated"] = "function (self, unitId, unitFrame, envTable)\n    \n    --when plater finishes an update on the nameplate\n    --check within the envTable if the healthBar of this nameplate should be hidden\n    if (envTable.IsHidden) then\n        if (unitFrame.healthBar:IsShown()) then\n            envTable.HideNameplate (unitFrame)\n        end\n    end\n    \nend\n\n\n\n\n",
          ["Enter Combat"] = "function (self, unitId, unitFrame, envTable)\n    \n    if (unitFrame.namePlateUnitReaction == envTable.REACTION_NEUTRAL) then\n        \n        --plater already handle this\n        if (unitFrame.PlayerCannotAttack) then\n            return\n        end    \n        \n        --check if is only open world\n        if (envTable.OnlyInOpenWorld and Plater.ZoneInstanceType ~= \"none\") then\n            return \n        end\n        \n        --check for only in combat\n        if (envTable.ShowInCombat) then\n            envTable.ShowNameplate (unitFrame)\n        end\n    end\nend\n\n\n",
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    --settings\n    envTable.OnlyInOpenWorld = true;\n    envTable.ShowInCombat = true;\n    \n    --consts\n    envTable.REACTION_NEUTRAL = 4;\n    \n    --functions to hide and show the healthBar\n    function envTable.HideNameplate (unitFrame)\n        Plater.HideHealthBar (unitFrame)\n        Plater.DisableHighlight (unitFrame)\n        envTable.IsHidden = true\n    end\n    \n    function envTable.ShowNameplate (unitFrame)\n        Plater.ShowHealthBar (unitFrame)\n        Plater.EnableHighlight (unitFrame)\n        envTable.IsHidden = false\n    end\n    \nend\n\n\n\n\n",
        },
        ["Prio"] = 99,
        ["Time"] = 1586243824,
        ["PlaterCore"] = 1,
        ["Name"] = "Hide Neutral Units [Plater]",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["race"] = {
          },
          ["pvptalent"] = {
          },
          ["spec"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
          },
          ["role"] = {
          },
        },
        ["Icon"] = 1990989,
        ["LastHookEdited"] = "Constructor",
      }, -- [5]
      {
        ["Enabled"] = false,
        ["Revision"] = 220,
        ["HooksTemp"] = {
        },
        ["Author"] = "Kastfall-Azralon",
        ["Desc"] = "Script for the Jaina encounter on Battle for Dazzar'alor",
        ["Hooks"] = {
          ["Nameplate Added"] = "function (self, unitId, unitFrame, envTable)\n    \n    --Unexploded Ordinance\n    if (unitFrame.namePlateNpcId == envTable.NpcIDs.UnexplodedOrdinance) then\n        \n        --make the life percent be bigger than the regular size\n        --so it's better to see the health percent of the barrel\n        local currentSize = Plater.db.profile.plate_config.enemynpc.percent_text_size\n        Plater:SetFontSize (unitFrame.healthBar.lifePercent, currentSize + envTable.UnexplodedOrdinanceTextSizeIncrease)\n    end\n    \n    if (envTable.IncreaseSize [unitFrame.namePlateNpcId]) then\n        local currentHeight = unitFrame.healthBar:GetHeight()\n        Plater.SetNameplateSize (unitFrame, nil, currentHeight + envTable.IncreaseSize [unitFrame.namePlateNpcId])\n    end\n    \nend\n\n\n\n\n\n\n",
          ["Cast Start"] = "function (self, unitId, unitFrame, envTable)\n    if (envTable.UnexplodedOrdinanceCast and envTable._SpellID == envTable.UnexplodedOrdinanceSpellID) then\n        Plater.SetCastBarSize (unitFrame, nil, envTable.UnexplodedOrdinanceCastBarHeight)\n        Plater:SetFontSize (unitFrame.castBar.percentText, envTable.UnexplodedOrdinanceTimerSize)\n    end\nend\n\n\n",
          ["Nameplate Removed"] = "function (self, unitId, unitFrame, envTable)\n    \n    --reset the health percent text size\n    local currentSize = Plater.db.profile.plate_config.enemynpc.percent_text_size\n    Plater:SetFontSize (unitFrame.healthBar.lifePercent, currentSize)    \n    \nend\n\n\n",
          ["Nameplate Updated"] = "function (self, unitId, unitFrame, envTable)\n    \n    --Override the color\n    if (envTable.Colors [unitFrame.namePlateNpcId]) then\n        Plater.SetNameplateColor (unitFrame, envTable.Colors [unitFrame.namePlateNpcId])\n    end    \n    \n    --Show the name of the unit in the Ice Block nameplate\n    if (unitFrame.namePlateNpcId == envTable.NpcIDs.IceBlock) then\n        --find which player this block are holding\n        for i = 1, GetNumGroupMembers() do\n            local unit = \"raid\" .. i\n            if (UnitExists (unit)) then\n                for debuffId = 1, 40 do\n                    local name, texture, count, debuffType, duration, expirationTime, caster = UnitDebuff (unit, debuffId)\n                    \n                    --cancel the loop if there's no more debuffs on the player\n                    if (not name) then \n                        break \n                    end                    \n                    \n                    --check if who casted this debuff is the unit shown on this nameplate\n                    if (UnitIsUnit (caster or \"\", unitId)) then\n                        local unitName = UnitName (unit)\n                        \n                        --color the text by the class\n                        unitName = Plater.SetTextColorByClass (unit, unitName)\n                        \n                        --add the role icon\n                        if (Details) then\n                            local role = UnitGroupRolesAssigned (unit)\n                            unitName = Details:AddRoleIcon (unitName, role, 12)\n                        end\n                        \n                        unitFrame.unitName:SetText (unitName)\n                        unitFrame.castBar.Text:SetText (unitName)\n                        break\n                    end\n                    \n                end\n            else\n                break\n            end\n        end\n    end\nend",
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    --nameplate colors for unit\n    envTable.Colors = {}\n    envTable.Colors [148631] = \"orange\" --Unexploded Ordinance\n    envTable.Colors [148522] = \"white\" --Ice Block\n    \n    --increase the nameplate height for these units\n    envTable.IncreaseSize = {}\n    envTable.IncreaseSize [148522] = 4 --Ice Block (increase in 4 pixels)\n    \n    --increase the size of the life percent for the nameplate of the barrel\n    envTable.UnexplodedOrdinanceTextSizeIncrease = 3\n    \n    --increase the castbar size for the unexploded ordinance explosion cast\n    envTable.UnexplodedOrdinanceCast = true\n    envTable.UnexplodedOrdinanceSpellID = 288221 --12058 --(debug)\n    envTable.UnexplodedOrdinanceCastBarHeight = 18\n    envTable.UnexplodedOrdinanceTimerSize = 18\n    \n    --npcIDs\n    envTable.NpcIDs = {}\n    envTable.NpcIDs.UnexplodedOrdinance = 148631\n    envTable.NpcIDs.IceBlock = 148522\nend\n\n--tests 126023 --harbor saurid - dazar'alor harbor\n--tests 3127 venomtail scorpid - durotar\n--tests 12058 dustwind storm witch - durotar\n--Load Condition: EncounterID 2281\n\n\n",
          ["Cast Stop"] = "function (self, unitId, unitFrame, envTable)\n    if (envTable.UnexplodedOrdinanceCast and envTable._SpellID == envTable.UnexplodedOrdinanceSpellID) then\n        Plater.SetCastBarSize (unitFrame)\n        Plater:SetFontSize (unitFrame.castBar.percentText, Plater.db.profile.plate_config.enemynpc.spellpercent_text_size)\n    end\nend\n\n\n",
        },
        ["Time"] = 1548612537,
        ["PlaterCore"] = 1,
        ["Name"] = "Jaina Encounter [Plater]",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["role"] = {
          },
          ["pvptalent"] = {
          },
          ["spec"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
            2281, -- [1]
            ["Enabled"] = true,
          },
          ["race"] = {
          },
        },
        ["Icon"] = "Interface\\AddOns\\Plater\\images\\encounter_jaina",
        ["LastHookEdited"] = "",
      }, -- [6]
      {
        ["Enabled"] = false,
        ["Revision"] = 84,
        ["HooksTemp"] = {
        },
        ["Author"] = "Ahwa-Azralon",
        ["Desc"] = "Add extra effects to execute range. See the constructor script for options.",
        ["Hooks"] = {
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    --execute detection, if true the script will handle the execute percent\n    --while false Plater will automatically trigger the execute range\n    --you only want to set this to true in case of Plater not detecting the execute range correctly\n    envTable.UseCustomExecutePercent = false\n    --execute percent, if not detecting automatic, this is the percent to active the execute range\n    --use from zero to one, 0.20 is equal to 20% of the unit life\n    envTable.ExecutePercent = 0.20\n    \n    --allow this script to change the nameplate color when the unit is in execute range\n    envTable.CanChangeColor = true\n    --change the health bar color to this color when the unit is in execute range\n    --color can be set as:\n    --color names: \"red\", \"yellow\"\n    --color hex: \"#FF0000\", \"#FFFF00\"\n    --color table: {1, 0, 0}, {1, 1, 0}\n    envTable.ExecuteColor = \"green\"\n    \n    --border color\n    envTable.CanChangeBorderColor = false\n    envTable.BorderColor = \"red\"\n    \n    --hide the default health divisor and the health execute indicator\n    envTable.HideHealthDivisor = false\n    --if not hidden, adjust the health divisor settings and the health execute indicator\n    envTable.HealthDivisorAlpha = 0.5\n    envTable.HealthDivisorColor = \"white\"\n    envTable.HealthExecuteIndicatorAlpha = 0.15\n    envTable.HealthExecuteIndicatorColor = \"darkred\"\n    \n    \n    --private (internal functions)\n    do\n        function envTable.UnitInExecuteRange (unitFrame)\n            --check if can change the execute color\n            if (envTable.CanChangeColor) then\n                Plater.SetNameplateColor (unitFrame, envTable.ExecuteColor)\n            end\n            \n            if (envTable.CanChangeBorderColor) then\n                Plater.SetBorderColor (unitFrame, envTable.BorderColor)\n            end\n            \n            if (envTable.HideHealthDivisor) then\n                unitFrame.healthBar.healthCutOff:Hide() \n                unitFrame.healthBar.executeRange:Hide()\n                \n            else\n                envTable.UpdateHealthDivisor (unitFrame)\n                \n            end\n        end\n        \n        function envTable.UpdateHealthDivisor (unitFrame)\n            local healthBar = unitFrame.healthBar\n            \n            healthBar.healthCutOff:Show()\n            healthBar.healthCutOff:SetVertexColor (DetailsFramework:ParseColors (envTable.HealthDivisorColor))\n            healthBar.healthCutOff:SetAlpha (envTable.HealthDivisorAlpha)\n            \n            healthBar.executeRange:Show()\n            healthBar.executeRange:SetVertexColor (DetailsFramework:ParseColors (envTable.HealthExecuteIndicatorColor))\n            healthBar.executeRange:SetAlpha (envTable.HealthExecuteIndicatorAlpha)\n            \n            if (envTable.UseCustomExecutePercent) then\n                healthBar.healthCutOff:ClearAllPoints()\n                healthBar.executeRange:ClearAllPoints()\n                \n                healthBar.healthCutOff:SetSize (healthBar:GetHeight(), healthBar:GetHeight())\n                healthBar.healthCutOff:SetPoint (\"center\", healthBar, \"left\", healthBar:GetWidth() * envTable.ExecutePercent, 0)\n                \n                healthBar.executeRange:SetTexCoord (0, envTable.ExecutePercent, 0, 1)\n                healthBar.executeRange:SetHeight (healthBar:GetHeight())\n                healthBar.executeRange:SetPoint (\"left\", healthBar, \"left\", 0, 0)\n                healthBar.executeRange:SetPoint (\"right\", healthBar.healthCutOff, \"center\")\n            end\n            \n        end\n    end\n    \nend",
          ["Nameplate Updated"] = "function (self, unitId, unitFrame, envTable)\n    \n    if (envTable.UseCustomExecutePercent) then\n        \n        --manual detection\n        local healthBar = unitFrame.healthBar\n        if (healthBar.CurrentHealth / healthBar.CurrentHealthMax <= envTable.ExecutePercent) then\n            envTable.UnitInExecuteRange (unitFrame)\n        end        \n        \n    else\n        \n        --auto detection\n        if (unitFrame.InExecuteRange) then\n            envTable.UnitInExecuteRange (unitFrame)\n        end\n        \n    end\n    \nend\n\n\n\n\n\n\n\n\n\n\n\n\n",
        },
        ["Time"] = 1547406548,
        ["PlaterCore"] = 1,
        ["Name"] = "Execute Range [Plater]",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["role"] = {
          },
          ["pvptalent"] = {
          },
          ["race"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
          },
          ["spec"] = {
          },
        },
        ["Icon"] = 135358,
        ["LastHookEdited"] = "",
      }, -- [7]
      {
        ["Enabled"] = false,
        ["Revision"] = 222,
        ["HooksTemp"] = {
        },
        ["Author"] = "Kastfall-Azralon",
        ["Desc"] = "Change the nameplate color if the unit is attacking a specific unit like Monk's Ox Statue or Druid's Treants. You may edit which units it track in the constructor script.",
        ["Hooks"] = {
          ["Nameplate Updated"] = "function (self, unitId, unitFrame, envTable)\n    \n    --get the GUID of the target of the unit\n    local targetGUID = UnitGUID (unitId .. \"target\")\n    \n    if (targetGUID) then\n        \n        --get the npcID of the target\n        local npcID = Plater.GetNpcIDFromGUID (targetGUID)\n        --check if the npcID of this unit is in the npc list \n        if (envTable.ListOfNpcs [npcID]) then\n            Plater.SetNameplateColor (unitFrame, envTable.ListOfNpcs [npcID])\n            \n        else\n            --check if the name of ths unit is in the list\n            local unitName = UnitName (unitId .. \"target\")\n            if (envTable.ListOfNpcs [unitName]) then\n                Plater.SetNameplateColor (unitFrame, envTable.ListOfNpcs [unitName])\n                \n            else\n                --check if the name of the unit in lower case is in the npc list\n                unitName = string.lower (unitName)\n                if (envTable.ListOfNpcs [unitName]) then\n                    Plater.SetNameplateColor (unitFrame, envTable.ListOfNpcs [unitName])                \n                    \n                end\n            end\n        end\n        \n    end\nend\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n",
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    --list of npcs and their colors, can be inserted:\n    --name of the unit\n    --name of the unit in lower case\n    --npcID of the unit\n    \n    --color can be added as:\n    --color names: \"red\", \"yellow\"\n    --color hex: \"#FF0000\", \"#FFFF00\"\n    --color table: {1, 0, 0}, {1, 1, 0}    \n    \n    envTable.ListOfNpcs = {\n        [61146] = \"olive\", --monk statue npcID\n        [103822] = \"olive\", --druid treant npcID\n        \n    }\n    \n    \nend\n\n\n\n\n",
        },
        ["Time"] = 1547993111,
        ["PlaterCore"] = 1,
        ["Name"] = "Attacking Specific Unit [Plater]",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["role"] = {
          },
          ["pvptalent"] = {
          },
          ["spec"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
          },
          ["race"] = {
          },
        },
        ["Icon"] = "Interface\\AddOns\\Plater\\images\\icon_attacking_unit",
        ["LastHookEdited"] = "",
      }, -- [8]
      {
        ["Enabled"] = false,
        ["Revision"] = 88,
        ["HooksTemp"] = {
        },
        ["Author"] = "Kastfall-Azralon",
        ["Desc"] = "Function Plater.UpdatePlateSize from Plater.lua exported to scritps.",
        ["Hooks"] = {
          ["Nameplate Added"] = "\n\n-- exported function Plater.UpdatePlateSize() from Plater.lua\n--this is for advanced users which wants to reorder the nameplate frame at their desire\n\n\n\nfunction (self, unitId, unitFrame, envTable)\n    \n    --check if there's a type of unit on this nameplate\n    local plateFrame = unitFrame:GetParent()\n    if (not plateFrame.actorType) then\n        return\n    end\n    \n    --get all the frames and cache some variables\n    local ACTORTYPE_ENEMY_PLAYER = \"enemyplayer\"\n    local profile = Plater.db.profile\n    local DB_PLATE_CONFIG = profile.plate_config\n    local isInCombat = Plater.IsInCombat()\n    local actorType = plateFrame.actorType\n    \n    local unitFrame = plateFrame.unitFrame\n    local healthBar = unitFrame.healthBar\n    local castBar = unitFrame.castBar\n    local powerBar = unitFrame.powerBar\n    local buffFrame1 = unitFrame.BuffFrame\n    local buffFrame2 = unitFrame.BuffFrame2\n    \n    --use in combat bars when in pvp\n    if (plateFrame.actorType == ACTORTYPE_ENEMY_PLAYER) then\n        if ((Plater.ZoneInstanceType == \"pvp\" or Plater.ZoneInstanceType == \"arena\") and DB_PLATE_CONFIG.player.pvp_always_incombat) then\n            isInCombat = true\n        end\n    end\n    \n    --get the config for this actor type\n    local plateConfigs = DB_PLATE_CONFIG [actorType]\n    --get the config key based if the player is in combat\n    local castBarConfigKey, healthBarConfigKey, manaConfigKey = Plater.GetHashKey (isInCombat)\n    \n    --get the width and height from what the user set in the options panel\n    local healthBarWidth, healthBarHeight = unitFrame.customHealthBarWidth or plateConfigs [healthBarConfigKey][1], unitFrame.customHealthBarHeight or plateConfigs [healthBarConfigKey][2]\n    local castBarWidth, castBarHeight = unitFrame.customCastBarWidth or plateConfigs [castBarConfigKey][1], unitFrame.customCastBarHeight or plateConfigs [castBarConfigKey][2]\n    local powerBarWidth, powerBarHeight = unitFrame.customPowerBarHeight or plateConfigs [manaConfigKey][1], unitFrame.customPowerBarHeight or plateConfigs [manaConfigKey][2]\n    \n    --calculate the offset for the cast bar, this is done due to the cast bar be anchored to topleft and topright\n    local castBarOffSetX = (healthBarWidth - castBarWidth) / 2\n    local castBarOffSetY = plateConfigs.castbar_offset\n    \n    --calculate offsets for the power bar\n    local powerBarOffSetX = (healthBarWidth - powerBarWidth) / 2\n    local powerBarOffSetY = 0\n    \n    --calculate the size deviation for pets\n    local unitType = Plater.GetUnitType (plateFrame)\n    if (unitType == \"pet\") then\n        healthBarHeight = healthBarHeight * Plater.db.profile.pet_height_scale\n        healthBarWidth = healthBarWidth * Plater.db.profile.pet_width_scale\n        \n    elseif (unitType == \"minus\") then\n        healthBarHeight = healthBarHeight * Plater.db.profile.minor_height_scale\n        healthBarWidth = healthBarWidth * Plater.db.profile.minor_width_scale\n    end\n    \n    --unit frame - is set to be the same size as the plateFrame\n    unitFrame:ClearAllPoints()\n    unitFrame:SetAllPoints()\n    \n    --calculates the health bar anchor points\n    --it will always be placed in the center of the nameplate area (where it accepts mouse clicks) \n    local xOffSet = (plateFrame:GetWidth() - healthBarWidth) / 2\n    local yOffSet = (plateFrame:GetHeight() - healthBarHeight) / 2\n    \n    --set the health bar point\n    healthBar:ClearAllPoints()\n    PixelUtil.SetPoint (healthBar, \"topleft\", unitFrame, \"topleft\", xOffSet + profile.global_offset_x, -yOffSet + profile.global_offset_y)\n    PixelUtil.SetPoint (healthBar, \"bottomright\", unitFrame, \"bottomright\", -xOffSet + profile.global_offset_x, yOffSet + profile.global_offset_y)\n    \n    --set the cast bar point and size\n    castBar:ClearAllPoints()\n    PixelUtil.SetPoint (castBar, \"topleft\", healthBar, \"bottomleft\", castBarOffSetX, castBarOffSetY)\n    PixelUtil.SetPoint (castBar, \"topright\", healthBar, \"bottomright\", -castBarOffSetX, castBarOffSetY)\n    PixelUtil.SetHeight (castBar, castBarHeight)\n    PixelUtil.SetSize (castBar.Icon, castBarHeight, castBarHeight)\n    PixelUtil.SetSize (castBar.BorderShield, castBarHeight * 1.4, castBarHeight * 1.4)\n    \n    --set the power bar point and size\n    powerBar:ClearAllPoints()\n    PixelUtil.SetPoint (powerBar, \"topleft\", healthBar, \"bottomleft\", powerBarOffSetX, powerBarOffSetY)\n    PixelUtil.SetPoint (powerBar, \"topright\", healthBar, \"bottomright\", -powerBarOffSetX, powerBarOffSetY)\n    PixelUtil.SetHeight (powerBar, powerBarHeight)\n    \n    --power bar are hidden by default, show it if there's a custom size for it\n    if (unitFrame.customPowerBarWidth and unitFrame.customPowerBarHeight) then\n        powerBar:SetUnit (unitFrame.unit)\n    end\n    \n    --aura frames\n    buffFrame1:ClearAllPoints()\n    PixelUtil.SetPoint (buffFrame1, \"bottom\", unitFrame, \"top\", profile.aura_x_offset,  plateConfigs.buff_frame_y_offset + profile.aura_y_offset)\n    \n    buffFrame2:ClearAllPoints()\n    PixelUtil.SetPoint (buffFrame2, \"bottom\", unitFrame, \"top\", profile.aura2_x_offset,  plateConfigs.buff_frame_y_offset + profile.aura2_y_offset)    \n    \nend\n\n\n",
        },
        ["Time"] = 1548077443,
        ["PlaterCore"] = 1,
        ["Name"] = "Reorder Nameplate [Plater]",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["race"] = {
          },
          ["pvptalent"] = {
          },
          ["spec"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
          },
          ["role"] = {
          },
        },
        ["Icon"] = 574574,
        ["LastHookEdited"] = "",
      }, -- [9]
      {
        ["Enabled"] = false,
        ["Revision"] = 37,
        ["HooksTemp"] = {
        },
        ["Author"] = "Izimode-Azralon",
        ["Desc"] = "Tint nameplates of Reaping Soul units (Mythic Dungeon Affix) depending on its target and role of the player",
        ["Hooks"] = {
          ["Nameplate Updated"] = "function (self, unitId, unitFrame, envTable)\n    \n    --can detect the reaping souls aggro?\n    if (envTable.detectAggro and Plater.IsInCombat()) then\n        \n        --is this npc a reaping soul?\n        if (envTable.npcIDs [unitFrame.namePlateNpcId]) then\n            \n            --check if the mob is attacking the player\n            if (UnitIsUnit (unitFrame.targetUnitID, \"player\")) then\n                Plater.SetNameplateColor (unitFrame, envTable.NameplateAggroColor)\n                \n            else\n                Plater.SetNameplateColor (unitFrame, envTable.NameplateNoAggroColor)\n            end\n            \n        end\n        \n    end\n    \nend",
          ["Nameplate Added"] = "function (self, unitId, unitFrame, envTable)\n    \n    --when the nameplate is added and the npcID matches, cache the color for the nameplate\n    if (envTable.detectAggro) then\n        if (envTable.npcIDs [unitFrame.namePlateNpcId]) then\n            local profile = Plater.db.profile\n            local role = Plater:GetPlayerRole()\n            \n            if (role == \"TANK\") then\n                envTable.NameplateAggroColor = profile.tank.colors.aggro\n                envTable.NameplateNoAggroColor = profile.tank.colors.noaggro\n            else\n                envTable.NameplateAggroColor = profile.dps.colors.aggro\n                envTable.NameplateNoAggroColor = profile.dps.colors.noaggro\n            end\n            \n        end\n    end\n    \n    \nend\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n",
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    --npcs affected by this script\n    \n    envTable.npcIDs = {\n        [148716] = true, --risen soul\n        [148893] = true, --tormented soul\n        [148894] = true, --lost soul\n        \n        [127278] = true, --skittering feeder (tests and debug, also need to disable the load conditions)\n    }\n    \n    --detect aggro, if true it will see which group member the soul is attacking and override the color\n    envTable.detectAggro = true\n    \n    \nend\n\n\n",
        },
        ["Time"] = 1549827281,
        ["PlaterCore"] = 1,
        ["Name"] = "M+ Bwonsamdi Reaping",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
            ["Enabled"] = true,
            ["party"] = true,
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["race"] = {
          },
          ["pvptalent"] = {
          },
          ["spec"] = {
          },
          ["affix"] = {
            [117] = true,
            ["Enabled"] = true,
          },
          ["encounter_ids"] = {
          },
          ["role"] = {
          },
        },
        ["Icon"] = 2446016,
        ["LastHookEdited"] = "",
      }, -- [10]
      {
        ["Enabled"] = true,
        ["Revision"] = 189,
        ["HooksTemp"] = {
        },
        ["Author"] = "Izimode-Azralon",
        ["Desc"] = "Show combo points above the nameplate for Druid Feral and Rogues.",
        ["Hooks"] = {
          ["Nameplate Created"] = "function (self, unitId, unitFrame, envTable)\n    \n    --run constructor!\n    --constructor is executed only once when any script of the hook runs.\n    \nend\n\n\n",
          ["Nameplate Added"] = "function (self, unitId, unitFrame, envTable)\n    \n    --check if need update the amount of combo points shown\n    if (envTable.LastPlayerTalentUpdate > envTable.LastUpdate) then\n        envTable.UpdateComboPointAmount()\n    end    \n    \n    if (unitFrame.namePlateIsTarget and not unitFrame.IsSelf) then\n        envTable.ComboPointFrame:Show()\n        envTable.UpdateComboPoints()\n        \n    else\n        envTable.ComboPointFrame:Hide()\n    end    \n    \nend\n\n\n",
          ["Target Changed"] = "function (self, unitId, unitFrame, envTable)\n    \n    --check if this nameplate is the current target\n    if (unitFrame.namePlateIsTarget and not unitFrame.IsSelf) then\n        envTable.ComboPointFrame:Show()\n        \n    else\n        envTable.ComboPointFrame:Hide()\n    end\n    \nend\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n",
          ["Player Power Update"] = "function (self, unitId, unitFrame, envTable)\n    \n    if (unitFrame.namePlateIsTarget and not unitFrame.IsSelf) then\n        envTable.UpdateComboPoints()\n    end\n    \n    \nend\n\n\n\n\n\n\n",
          ["Player Talent Update"] = "function (self, unitId, unitFrame, envTable)\n    \n    --update the amount of comboo points shown when the player changes talents or specialization\n    envTable.UpdateComboPointAmount()\n    \n    --save the time of the last talent change\n    envTable.LastPlayerTalentUpdate = GetTime()\n    \n    \nend\n\n\n",
          ["Destructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.ComboPointFrame:Hide()\n    \nend\n\n\n\n\n",
          ["Nameplate Removed"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.ComboPointFrame:Hide()\n    \nend\n\n\n",
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    --settings\n    local anchors = {\n        {\"bottom\", unitFrame.healthBar, \"top\", 0, 24},\n    }\n    \n    local sizes = {\n        width = 12,\n        height = 12,\n        scale = 1,\n    }\n    \n    local textures = {\n        backgroundTexture = [[Interface\\PLAYERFRAME\\ClassOverlayComboPoints]],\n        backgroundTexCoords = {78/128, 98/128, 21/64, 41/64},\n        \n        comboPointTexture = [[Interface\\PLAYERFRAME\\ClassOverlayComboPoints]],\n        comboPointTexCoords = {100/128, 120/128, 21/64, 41/64},\n    }\n    \n    local frameLevel = 1000\n    local frameStrata = \"high\"    \n    \n    --private\n    do\n        --store combo points frames on this table\n        envTable.ComboPoints = {}\n        --save when the player changed talents or spec\n        envTable.LastPlayerTalentUpdate = GetTime()\n        --save when this nameplate got a combo point amount and alignment update        \n        \n        --build combo points frame anchor (combo point are anchored to this)\n        if (not unitFrame.PlaterComboPointFrame) then\n            local hostFrame = CreateFrame (\"frame\", nil, unitFrame)\n            hostFrame.ComboPointFramesPool = {}\n            unitFrame.PlaterComboPointFrame = hostFrame\n            envTable.ComboPointFrame = hostFrame\n            \n            --DetailsFramework:ApplyStandardBackdrop (envTable.ComboPointFrame) --debug anchor size\n            \n            --animations\n            local onPlayShowAnimation = function (animation)\n                --stop the hide animation if it's playing\n                if (animation:GetParent():GetParent().HideAnimation:IsPlaying()) then\n                    animation:GetParent():GetParent().HideAnimation:Stop()\n                end\n                \n                animation:GetParent():Show()\n            end\n            \n            local onPlayHideAnimation = function (animation)\n                --stop the show animation if it's playing\n                if (animation:GetParent():GetParent().ShowAnimation:IsPlaying()) then\n                    animation:GetParent():GetParent().ShowAnimation:Stop()\n                end\n            end        \n            local onStopHideAnimation = function (animation)\n                animation:GetParent():Hide()       \n            end\n            \n            local createAnimations = function (comboPoint)\n                --on show\n                comboPoint.ShowAnimation = Plater:CreateAnimationHub (comboPoint.comboPointTexture, onPlayShowAnimation, nil)\n                Plater:CreateAnimation (comboPoint.ShowAnimation, \"scale\", 1, 0.1, 0, 0, 1, 1)\n                Plater:CreateAnimation (comboPoint.ShowAnimation, \"alpha\", 1, 0.1, .5, 1)\n                Plater:CreateAnimation (comboPoint.ShowAnimation, \"scale\", 2, 0.1, 1.2, 1.2, 1, 1)\n                \n                --on hide\n                comboPoint.HideAnimation = Plater:CreateAnimationHub (comboPoint.comboPointTexture, onPlayHideAnimation, onStopHideAnimation)\n                Plater:CreateAnimation (comboPoint.HideAnimation, \"scale\", 1, 0.1, 1, 1, 0, 0)\n                Plater:CreateAnimation (comboPoint.HideAnimation, \"alpha\", 1, 0.1, 1, 0)\n            end\n            \n            --build combo point frame        \n            for i =1, 10 do \n                local f = CreateFrame (\"frame\", nil, envTable.ComboPointFrame)\n                f:SetSize (sizes.width, sizes.height)\n                tinsert (envTable.ComboPoints, f)\n                tinsert (unitFrame.PlaterComboPointFrame.ComboPointFramesPool, f)\n                \n                local backgroundTexture = f:CreateTexture (nil, \"background\")\n                backgroundTexture:SetTexture (textures.backgroundTexture)\n                backgroundTexture:SetTexCoord (unpack (textures.backgroundTexCoords))\n                backgroundTexture:SetSize (sizes.width, sizes.height)\n                backgroundTexture:SetPoint (\"center\")\n                \n                local comboPointTexture = f:CreateTexture (nil, \"artwork\")\n                comboPointTexture:SetTexture (textures.comboPointTexture)\n                comboPointTexture:SetTexCoord (unpack (textures.comboPointTexCoords))\n                \n                comboPointTexture:SetSize (sizes.width, sizes.height)\n                comboPointTexture:SetPoint (\"center\")\n                comboPointTexture:Hide()            \n                \n                f.IsActive = false\n                \n                f.backgroundTexture = backgroundTexture\n                f.comboPointTexture = comboPointTexture\n                \n                createAnimations (f)\n            end\n            \n        else\n            envTable.ComboPointFrame = unitFrame.PlaterComboPointFrame\n            envTable.ComboPointFrame:SetScale (sizes.scale)\n            envTable.ComboPoints = unitFrame.PlaterComboPointFrame.ComboPointFramesPool\n            \n        end            \n        \n        envTable.ComboPointFrame:SetFrameLevel (frameLevel)\n        envTable.ComboPointFrame:SetFrameStrata (frameStrata)\n        \n        function envTable.UpdateComboPoints()\n            local comboPoints = UnitPower (\"player\", Enum.PowerType.ComboPoints)\n            \n            for i = 1, envTable.TotalComboPoints do\n                local thisComboPoint = envTable.ComboPoints [i]\n                \n                if (i <= comboPoints ) then\n                    --combo point enabled\n                    if (not thisComboPoint.IsActive) then\n                        thisComboPoint.ShowAnimation:Play()\n                        thisComboPoint.IsActive = true\n                        \n                    end\n                    \n                else\n                    --combo point disabled\n                    if (thisComboPoint.IsActive) then\n                        thisComboPoint.HideAnimation:Play()\n                        thisComboPoint.IsActive = false\n                        \n                    end\n                end\n            end\n            \n            \n        end\n        \n        function envTable.UpdateComboPointAmount()\n            local namePlateWidth = Plater.db.profile.plate_config.enemynpc.health_incombat[1]\n            local comboPoints = UnitPowerMax (\"player\", Enum.PowerType.ComboPoints)\n            local reservedSpace = namePlateWidth / comboPoints\n            \n            --store the total amount of combo points\n            envTable.TotalComboPoints = comboPoints\n            \n            --update anchor frame\n            envTable.ComboPointFrame:SetWidth (namePlateWidth)\n            envTable.ComboPointFrame:SetHeight (20)\n            envTable.ComboPointFrame:ClearAllPoints()\n            for i = 1, #anchors do\n                local anchor = anchors[i]\n                envTable.ComboPointFrame:SetPoint (unpack (anchor))\n            end        \n            \n            --\n            for i = 1, #envTable.ComboPoints do\n                envTable.ComboPoints[i]:Hide()\n                envTable.ComboPoints[i]:ClearAllPoints()\n            end\n            \n            for i = 1, comboPoints do\n                local comboPoint = envTable.ComboPoints[i]\n                comboPoint:SetPoint (\"left\", envTable.ComboPointFrame, \"left\", reservedSpace * (i-1), 0)\n                comboPoint:Show()\n            end\n            \n            envTable.LastUpdate = GetTime()\n            \n            envTable.UpdateComboPoints()\n        end\n        \n        --initialize\n        envTable.UpdateComboPointAmount()\n        envTable.ComboPointFrame:Hide()\n    end\n    \n    \nend",
        },
        ["Time"] = 1548354524,
        ["PlaterCore"] = 1,
        ["Name"] = "Combo Points [Plater]",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
            ["Enabled"] = true,
            ["DRUID"] = true,
            ["ROGUE"] = true,
          },
          ["map_ids"] = {
          },
          ["race"] = {
          },
          ["pvptalent"] = {
          },
          ["role"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
          },
          ["spec"] = {
            [103] = true,
            ["Enabled"] = true,
          },
        },
        ["Icon"] = 135426,
        ["LastHookEdited"] = "",
      }, -- [11]
      {
        ["Enabled"] = false,
        ["Revision"] = 182,
        ["HooksTemp"] = {
        },
        ["Author"] = "Izimode-Azralon",
        ["Desc"] = "Show how many raid members are targeting the unit",
        ["Hooks"] = {
          ["Leave Combat"] = "function (self, unitId, unitFrame, envTable)\n    envTable.CanShow = false;\n    envTable.TargetAmount:SetText (\"\")\nend\n\n\n",
          ["Nameplate Added"] = "function (self, unitId, unitFrame, envTable)\n    \n    --when a nameplate is added to the screen check if the player is in combat\n    if (InCombatLockdown()) then\n        --player is in combat, check if can check amount of targets\n        envTable.CanShow = envTable.CanShowTargetAmount();\n        \n    else\n        envTable.CanShow = false; \n    end\n    \n    envTable.TargetAmount:SetText (\"\");\n    \nend",
          ["Nameplate Removed"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.TargetAmount:SetText (\"\");\n    envTable.CanShow = false;\n    \nend\n\n\n",
          ["Nameplate Updated"] = "function (self, unitId, unitFrame, envTable)\n    \n    --if the script is allowed to show the amount of targets\n    --also check if the unit is in combat\n    if (envTable.CanShow and UnitAffectingCombat (unitId)) then\n        \n        --check if can update the amount of targets following the cooldown set in the constructor script\n        --by default Plater updates the nameplate every 250ms, by default the cooldown is 2, so it'll update the amuont of target every 1/2 of a second\n        envTable.UpdateCooldown = envTable.UpdateCooldown + 1\n        if (envTable.UpdateCooldown < envTable.UpdateInterval) then\n            return\n        else\n            \n            --reset the cooldown interval to check the amount of target again\n            envTable.UpdateCooldown = 0\n            \n            --get the amount of targets\n            local amount;\n            if (envTable.InRaid) then\n                amount = envTable.NumTargetsInRaid (unitFrame)      \n                \n            elseif (envTable.InParty) then\n                amount = envTable.NumTargetsInParty (unitFrame)   \n                \n            else\n                envTable.TargetAmount:SetText (\"\")\n                return\n            end\n            \n            --update the amount text\n            if (amount == 0) then\n                envTable.TargetAmount:SetText (\"\")\n            else\n                envTable.TargetAmount:SetText (amount)\n            end\n            \n        end\n    end\nend\n\n\n",
          ["Enter Combat"] = "function (self, unitId, unitFrame, envTable)\n    \n    --check if can show the amount of targets\n    envTable.CanShow = envTable.CanShowTargetAmount();\n    \n    if (not envTable.CanShow) then\n        envTable.TargetAmount:SetText (\"\") \n    end\nend\n\n\n\n\n",
          ["Constructor"] = "--all gray text like this are comments and do not run as code\n--build the settings and basic functions for the hook\n\nfunction (self, unitId, unitFrame, envTable)\n    \n    --declare setting variables:\n    local textColor = \"orange\";\n    local textSize = 12;\n    \n    local showInRaid = true;\n    local showInDungeon = true;\n    local showInArena = false;\n    local showInBattleground = false;\n    local showInOpenWorld = true;\n    \n    envTable.UpdateInterval = 2; --each 2 updates in the nameplate it'll update the amount of targets\n    \n    local anchor = {\n        side = 6, --1 = topleft 2 = left 3 = bottomleft 4 = bottom 5 = bottom right 6 = right 7 = topright 8 = top\n        x = 4, --x offset\n        y = 0, --y offset\n    };\n    \n    \n    ---------------------------------------------------------------------------------------------------------------------------------------------\n    \n    \n    --frames:\n    \n    --create the text that will show the amount of people targeting the unit\n    if (not  unitFrame.healthBar.TargetAmount) then\n        envTable.TargetAmount = Plater:CreateLabel (unitFrame.healthBar, \"\", textSize, textColor);\n        Plater.SetAnchor (envTable.TargetAmount, anchor);\n        unitFrame.healthBar.TargetAmount = envTable.TargetAmount\n    end\n    \n    --in case Plater wipes the envTable\n    envTable.TargetAmount = unitFrame.healthBar.TargetAmount\n    \n    ---------------------------------------------------------------------------------------------------------------------------------------------           \n    --private variables (they will be used in the other scripts within this hook)\n    envTable.CanShow = false;\n    envTable.UpdateCooldown = 0;\n    envTable.InRaid = false;\n    envTable.InParty = false;\n    \n    ---------------------------------------------------------------------------------------------------------------------------------------------           \n    --functions\n    \n    --update the InRaid or InParty proprieties\n    function envTable.UpdateGroupType()\n        if (IsInRaid()) then\n            envTable.InRaid = true;\n            envTable.InParty = false;     \n            \n        elseif (IsInGroup()) then\n            envTable.InRaid = false;\n            envTable.InParty = true;   \n            \n        else\n            envTable.InRaid = false;            \n            envTable.InParty = false;\n        end\n    end\n    \n    --this function controls if the amount of targets can show following the settings in the top of this script\n    function envTable.CanShowTargetAmount()\n        \n        local _, instanceType, difficultyID, _, _, _, _, instanceMapID, instanceGroupSize = GetInstanceInfo()\n        \n        if (showInRaid and instanceType == \"raid\") then\n            envTable.UpdateGroupType()\n            return true\n        end\n        \n        if (showInDungeon and instanceType == \"party\") then\n            envTable.UpdateGroupType()\n            return true\n        end\n        \n        if (showInArena and instanceType == \"arena\") then\n            envTable.UpdateGroupType()\n            return true\n        end\n        \n        if (showInBattleground and instanceType == \"pvp\") then\n            envTable.UpdateGroupType()\n            return true\n        end\n        \n        if (showInOpenWorld and instanceType == \"none\") then\n            envTable.UpdateGroupType()\n            if (envTable.InRaid or envTable.InParty) then\n                return true\n            end\n        end\n        \n        return false\n    end\n    \n    --get the amount of player targetting the unit in raid or party\n    function envTable.NumTargetsInRaid (unitFrame)\n        local amount = 0\n        for i = 1, GetNumGroupMembers() do\n            local unit = \"raid\" .. i .. \"target\"\n            if (UnitGUID (unit) == unitFrame.namePlateUnitGUID) then\n                amount = amount + 1\n            end\n        end\n        \n        return amount\n    end\n    \n    function envTable.NumTargetsInParty()\n        local amount = 0\n        for i = 1, GetNumGroupMembers() - 1 do\n            local unit = \"party\" .. i .. \"target\"\n            if (UnitGUID (unit) == unitFrame.namePlateUnitGUID) then\n                amount = amount + 1\n            end\n        end\n        \n        local unit = \"playertarget\"\n        if (UnitGUID (unit) == unitFrame.namePlateUnitGUID) then\n            amount = amount + 1\n        end        \n        \n        return amount\n    end\n    \nend",
        },
        ["Time"] = 1548278227,
        ["PlaterCore"] = 1,
        ["Name"] = "Players Targeting a Target [Plater]",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["role"] = {
          },
          ["pvptalent"] = {
          },
          ["spec"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
          },
          ["race"] = {
          },
        },
        ["Icon"] = 1966587,
        ["LastHookEdited"] = "",
      }, -- [12]
      {
        ["Enabled"] = false,
        ["Revision"] = 93,
        ["HooksTemp"] = {
        },
        ["Author"] = "Izimode-Azralon",
        ["Desc"] = "Changes the target color to the color set in the constructor script.",
        ["Hooks"] = {
          ["Nameplate Updated"] = "function (self, unitId, unitFrame, envTable)\n    envTable.UpdateColor (unitFrame)\nend",
          ["Nameplate Added"] = "function (self, unitId, unitFrame, envTable)\n    envTable.UpdateColor (unitFrame)\nend",
          ["Target Changed"] = "function (self, unitId, unitFrame, envTable)\n    envTable.UpdateColor (unitFrame)\nend\n\n\n\n\n\n\n\n\n\n\n",
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    --usage: color name e.g \"red\" \"yellow\"; color table e.g {1, 0, 0} {1, 1, 0}; hex string e.g. \"#FF0000\" \"FFFF00\"\n    \n    envTable.TargetColor = \"purple\"\n    --envTable.TargetColor = \"#FF00FF\"\n    --envTable.TargetColor = {252/255, 0/255, 254/255}\n    \n    function envTable.UpdateColor (unitFrame)\n        --do not change the color of the personal bar\n        if (not unitFrame.IsSelf) then\n            \n            --if this nameplate the current target of the player?\n            if (unitFrame.namePlateIsTarget) then\n                Plater.SetNameplateColor (unitFrame, envTable.TargetColor)  --rgb\n            else\n                --refresh the nameplate color\n                Plater.RefreshNameplateColor (unitFrame)\n            end\n        end\n    end\n    \nend\n\n\n\n\n",
        },
        ["Time"] = 1552354619,
        ["PlaterCore"] = 1,
        ["Name"] = "Current Target Color [Plater]",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["role"] = {
          },
          ["pvptalent"] = {
          },
          ["race"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
          },
          ["spec"] = {
          },
        },
        ["Icon"] = 878211,
        ["LastHookEdited"] = "",
      }, -- [13]
      {
        ["Enabled"] = true,
        ["Revision"] = 272,
        ["HooksTemp"] = {
        },
        ["Author"] = "Ditador-Azralon",
        ["Desc"] = "Reorder buffs and debuffs following the settings set in the constructor.",
        ["Hooks"] = {
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    --ATTENTION: after enabling this script, you may have to adjust the anchor point at the Buff Settings tab\n    \n    --space between each aura icon\n    envTable.padding = 2\n    \n    --space between each row of icons\n    envTable.rowPadding = 12\n    \n    --amount of icons in the row, it'll breakline and start a new row after reach the threshold\n    envTable.maxAurasPerRow = 5\n    \n    --stack auras of the same name that arent stacked by default from the game\n    envTable.consolidadeRepeatedAuras = true    \n    \n    --which auras goes first, assign a value (any number), bigger value goes first\n    envTable.priority = {\n        [\"Vampiric Touch\"] = 50,\n        [\"Shadow Word: Pain\"] = 22,\n        [\"Mind Flay\"] = 5,\n    }\n    \nend \n\n\n\n\n",
          ["Nameplate Updated"] = "function (self, unitId, unitFrame, envTable)\n    \n    local auraContainers = {unitFrame.BuffFrame.PlaterBuffList}\n\n    if (Plater.db.profile.buffs_on_aura2) then\n        auraContainers [2] = unitFrame.BuffFrame2.PlaterBuffList\n    end\n    \n    for containerID = 1, #auraContainers do\n        \n        local auraContainer = auraContainers [containerID]\n        local aurasShown = {}\n        local aurasDuplicated = {}\n        \n        --build the list of auras shown in the buff frame and check for each aura priority\n        --also check if the consolidate (stack) auras with the same name is enabled\n        for index, auraIcon in ipairs (auraContainer) do\n            if (auraIcon:IsShown()) then\n                if (envTable.consolidadeRepeatedAuras) then\n                    --is this aura already shown?\n                    local iconShownIndex = aurasDuplicated [auraIcon.SpellName]\n                    if (iconShownIndex) then\n                        --get the table with information about the shown icon\n                        local auraShownTable = aurasShown [iconShownIndex]\n                        --get the icon already in the table\n                        local icon = auraShownTable[1]\n                        --increase the amount of stacks\n                        auraShownTable[3] = auraShownTable[3] + 1\n                        \n                        --check if the remaining time of the icon already added in the table is lower than the current\n                        if (auraIcon.RemainingTime > icon.RemainingTime) then\n                            --replace the icon for the icon with bigger duration\n                            auraShownTable[1] = auraIcon\n                            icon:Hide()\n                        else\n                            auraIcon:Hide()\n                        end\n                    else    \n                        local priority = envTable.priority[auraIcon.SpellName] or envTable.priority[auraIcon.spellId] or 1\n                        tinsert (aurasShown, {auraIcon, priority, 1}) --icon frame, priority, stack amount\n                        aurasDuplicated [auraIcon.SpellName] = #aurasShown\n                    end\n                else\n                    --not stacking similar auras\n                    local priority = envTable.priority[auraIcon.SpellName] or envTable.priority[auraIcon.spellId] or 1\n                    tinsert (aurasShown, {auraIcon, priority})\n                    \n                end           \n            end\n        end\n        \n        --sort auras by priority\n        table.sort (aurasShown, DetailsFramework.SortOrder2)\n        \n        local growDirection\n        if (containerID == 1) then --debuff container\n            growDirection = Plater.db.profile.aura_grow_direction\n            --force to grow to right if it is anchored to center\n            if (growDirection == 2) then\n                growDirection = 3\n            end\n            -- \"Left\", \"Center\", \"Right\" - 1  2  3\n            \n        elseif (containerID == 2) then --buff container\n            growDirection = Plater.db.profile.aura2_grow_direction\n            --force to grow to left if it is anchored to center\n            if (growDirection == 2) then\n                growDirection = 1\n            end\n            \n        end\n        \n        local padding = envTable.padding\n        local framersPerRow = envTable.maxAurasPerRow + 1\n        \n        --first icon is where the row starts\n        local firstIcon = aurasShown[1] and aurasShown[1][1]\n        \n        if (firstIcon) then\n            local anchorPoint = firstIcon:GetParent() --anchor point is the BuffFrame\n            anchorPoint:SetSize (1, 1)\n            \n            firstIcon:ClearAllPoints()\n            firstIcon:SetPoint (\"center\", anchorPoint, \"center\", 0, 5)\n            \n            --check the consolidaded stacks, this is not the regular buff stacks\n            local firstIconStacks = aurasShown[1][3]\n            if (firstIconStacks and firstIconStacks > 1) then\n                firstIcon.StackText:SetText (firstIconStacks)\n                firstIcon.StackText:Show()\n            end\n            \n            --> left to right\n            if (growDirection == 3) then\n                --> iterate among all aura icons\n                for i = 2, #aurasShown do\n                    local auraIcon = aurasShown [i][1]\n                    auraIcon:ClearAllPoints()\n                    \n                    if (i == framersPerRow) then\n                        auraIcon:SetPoint (\"bottomleft\", firstIcon, \"topleft\", 0, envTable.rowPadding)\n                        framersPerRow = framersPerRow + framersPerRow\n                        \n                    else\n                        auraIcon:SetPoint (\"topleft\", aurasShown [i-1][1], \"topright\", padding, 0)\n                    end\n                    \n                    local stacks = aurasShown[i][3]\n                    if (stacks and stacks > 1) then\n                        auraIcon.StackText:SetText (stacks)\n                        auraIcon.StackText:Show()\n                    end\n                end        \n                \n                --right to left\n            elseif (growDirection == 1) then\n                --> iterate among all aura icons\n                for i = 2, #aurasShown do\n                    local auraIcon = aurasShown [i][1]\n                    auraIcon:ClearAllPoints()\n                    \n                    if (i == framersPerRow) then\n                        auraIcon:SetPoint (\"bottomright\", firstIcon, \"topright\", 0, envTable.rowPadding)\n                        framersPerRow = framersPerRow + framersPerRow\n                        \n                    else\n                        auraIcon:SetPoint (\"topright\", aurasShown [i-1][1], \"topleft\", -padding, 0)\n                    end\n                    \n                    local stacks = aurasShown[i][3]\n                    if (stacks and stacks > 1) then\n                        auraIcon.StackText:SetText (stacks)\n                        auraIcon.StackText:Show()\n                    end\n                    \n                end                    \n            end\n            \n        end\n    end\nend\n\n\n",
        },
        ["Time"] = 1553450957,
        ["PlaterCore"] = 1,
        ["Name"] = "Aura Reorder [Plater]",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["race"] = {
          },
          ["pvptalent"] = {
          },
          ["spec"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
          },
          ["role"] = {
          },
        },
        ["Icon"] = "Interface\\AddOns\\Plater\\images\\icon_aura_reorder",
        ["LastHookEdited"] = "",
      }, -- [14]
      {
        ["Enabled"] = true,
        ["Revision"] = 84,
        ["HooksTemp"] = {
        },
        ["Author"] = "Izimode-Azralon",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["role"] = {
          },
          ["pvptalent"] = {
          },
          ["affix"] = {
          },
          ["race"] = {
          },
          ["encounter_ids"] = {
          },
          ["spec"] = {
          },
        },
        ["Desc"] = "Adds a pixels perfect border around the cast bar.",
        ["Hooks"] = {
          ["Cast Start"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.UpdateBorder (unitFrame)\n    \nend\n\n\n",
          ["Destructor"] = "function (self, unitId, unitFrame, envTable)\n    if (unitFrame.castBar.CastBarBorder) then\n        unitFrame.castBar.CastBarBorder:Hide()\n    end    \nend",
          ["Cast Update"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.UpdateBorder (unitFrame)\n    \nend\n\n\n",
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    --settings\n    \n    --hide the icon of the spell, may require /reload after changing\n    envTable.HideIcon = false\n    \n    --border settings\n    envTable.BorderThickness = 1\n    envTable.BorderColor = \"black\"\n    \n    --private\n    --create the border\n    if (not unitFrame.castBar.CastBarBorder) then\n        unitFrame.castBar.CastBarBorder = CreateFrame (\"frame\", nil, unitFrame.castBar, \"NamePlateFullBorderTemplate\")\n    end    \n    \n    --update the border\n    function envTable.UpdateBorder (unitFrame)\n        local castBar = unitFrame.castBar\n        \n        local r, g, b, a = DetailsFramework:ParseColors (envTable.BorderColor)\n        castBar.CastBarBorder:SetVertexColor (r, g, b, a)\n        \n        local size = envTable.BorderThickness\n        castBar.CastBarBorder:SetBorderSizes (size, size, size, size)\n        castBar.CastBarBorder:UpdateSizes()        \n        \n        if (envTable.HideIcon) then\n            castBar.Icon:Hide()\n        end\n        \n        castBar.CastBarBorder:Show()\n    end\n    \nend\n\n\n\n\n",
        },
        ["Prio"] = 99,
        ["Time"] = 1584275881,
        ["PlaterCore"] = 1,
        ["LastHookEdited"] = "Constructor",
        ["Url"] = "https://wago.io/c0XUQAYJq/1",
        ["Icon"] = 133004,
        ["Name"] = "Cast Bar Border",
      }, -- [15]
      {
        ["Enabled"] = true,
        ["Revision"] = 40,
        ["HooksTemp"] = {
        },
        ["Author"] = "Izimode-Azralon",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["race"] = {
          },
          ["pvptalent"] = {
          },
          ["role"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
          },
          ["spec"] = {
          },
        },
        ["Desc"] = "Adds a pixels perfect border around the cast bar spell icon.",
        ["Hooks"] = {
          ["Cast Start"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.BuildFrames (unitFrame)\n    \nend\n\n\n",
          ["Destructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    if (unitFrame.castBar.IconOverlayFrame) then\n        unitFrame.castBar.IconOverlayFrame:Hide()\n    end\n    \nend\n\n\n\n\n",
          ["Cast Update"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.BuildFrames (unitFrame)\n    \nend\n\n\n",
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    --settings\n    envTable.AnchorSide = \"left\"\n    envTable.BorderThickness = 1\n    envTable.BorderColor = \"black\"\n    \n    --private\n    function envTable.BuildFrames (unitFrame)\n        local castBar = unitFrame.castBar\n        \n        local r, g, b, a = DetailsFramework:ParseColors (envTable.BorderColor)\n        castBar.IconBorder:SetVertexColor (r, g, b, a)\n        \n        local size = envTable.BorderThickness\n        castBar.IconBorder:SetBorderSizes (size, size, size, size)\n        castBar.IconBorder:UpdateSizes()\n        \n        local icon = castBar.Icon\n        if (envTable.AnchorSide == \"left\") then\n            icon:ClearAllPoints()\n            icon:SetPoint (\"topright\", unitFrame.healthBar, \"topleft\")\n            icon:SetPoint (\"bottomright\", castBar, \"bottomleft\")\n            icon:SetWidth (icon:GetHeight())\n            \n        elseif (envTable.AnchorSide == \"right\") then\n            icon:ClearAllPoints()\n            icon:SetPoint (\"topleft\", unitFrame.healthBar, \"topright\")\n            icon:SetPoint (\"bottomleft\", castBar, \"bottomright\")\n            icon:SetWidth (icon:GetHeight())\n            \n        end\n        \n        icon:Show()\n        castBar.IconOverlayFrame:Show()\n    end\n    \n    if (not unitFrame.castBar.IconOverlayFrame) then\n        --icon support frame\n        unitFrame.castBar.IconOverlayFrame = CreateFrame (\"frame\", nil, unitFrame.castBar)\n        unitFrame.castBar.IconOverlayFrame:SetPoint (\"topleft\", unitFrame.castBar.Icon, \"topleft\")\n        unitFrame.castBar.IconOverlayFrame:SetPoint (\"bottomright\", unitFrame.castBar.Icon, \"bottomright\")\n        \n        unitFrame.castBar.IconBorder = CreateFrame (\"frame\", nil,  unitFrame.castBar.IconOverlayFrame, \"NamePlateFullBorderTemplate\")\n    end    \n    \nend\n\n\n\n\n",
        },
        ["Prio"] = 99,
        ["Time"] = 1584460814,
        ["PlaterCore"] = 1,
        ["LastHookEdited"] = "Constructor",
        ["Url"] = "https://wago.io/T37kZgkmc/1",
        ["Icon"] = 133004,
        ["Name"] = "Cast Icon Border",
      }, -- [16]
      {
        ["Enabled"] = true,
        ["Revision"] = 158,
        ["HooksTemp"] = {
          ["Cast Stop"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.UpdateBorder (unitFrame, false)\n    \nend\n\n\n",
          ["Cast Update"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.UpdateIconPosition (unitFrame)\n    self.ThrottleUpdate = -1\n    \nend\n\n\n",
          ["Cast Start"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.UpdateIconPosition (unitFrame)\n    envTable.UpdateBorder (unitFrame, true)\n    \nend\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n",
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    --settings:\n    --show cast icon\n    envTable.ShowIcon = true\n    --anchor icon on what side\n    envTable.IconAnchor = \"left\" --accep 'left' 'right'\n    --fine tune the size of the icon\n    envTable.IconSizeOffset = 0\n    \n    --shield for non interruptible casts\n    envTable.ShowShield = true\n    envTable.ShieldTexture = [[Interface\\GROUPFRAME\\UI-GROUP-MAINTANKICON]]\n    envTable.ShieldDesaturated = true\n    envTable.ShieldColor = {1, 1, 1 ,1}\n    envTable.ShieldSize = {10, 12}\n    \n    --private:\n    function envTable.UpdateIconPosition (unitFrame)\n        local castBar = unitFrame.castBar\n        local icon = castBar.Icon\n        local shield = castBar.BorderShield\n        \n        if (envTable.ShowIcon) then\n            icon:ClearAllPoints()\n            \n            if (envTable.IconAnchor == \"left\") then\n                icon:SetPoint (\"topright\", unitFrame.healthBar, \"topleft\", 0, envTable.IconSizeOffset)\n                icon:SetPoint (\"bottomright\", unitFrame.castBar, \"bottomleft\", 0, 0)    \n                \n            elseif (envTable.IconAnchor == \"right\") then\n                icon:SetPoint (\"topleft\", unitFrame.healthBar, \"topright\", 0, envTable.IconSizeOffset)\n                icon:SetPoint (\"bottomleft\", unitFrame.castBar, \"bottomright\", 0, 0)\n                \n            end\n            \n            icon:SetWidth (icon:GetHeight())\n            icon:Show()\n            \n        else\n            icon:Hide()\n            \n        end\n        \n        if (envTable.ShowShield and not castBar.canInterrupt) then\n            shield:Show()\n            shield:SetAlpha (1)\n            shield:SetTexCoord (0, 1, 0, 1)\n            shield:SetVertexColor (1, 1, 1, 1)\n            \n            shield:SetTexture (envTable.ShieldTexture)\n            shield:SetDesaturated (envTable.ShieldDesaturated)\n            \n            if (not envTable.ShieldDesaturated) then\n                shield:SetVertexColor (DetailsFramework:ParseColors (envTable.ShieldColor))\n            end\n            \n            shield:SetSize (unpack (envTable.ShieldSize))\n            \n            shield:ClearAllPoints()\n            shield:SetPoint (\"center\", castBar, \"left\", 0, 0)\n            \n        else\n            shield:Hide()\n            \n        end\n        \n    end\n    \n    function envTable.UpdateBorder (unitFrame, casting)\n        local healthBar = unitFrame.healthBar\n        local castBar = unitFrame.castBar\n        \n        if casting then    \n            if envTable.IconAnchor == \"left\" then\n                healthBar.border:SetPoint(\"TOPLEFT\", castBar.Icon, \"TOPLEFT\", 0, 0)\n                healthBar.border:SetPoint(\"BOTTOMRIGHT\", castBar, \"BOTTOMRIGHT\", 0, 0)\n            elseif envTable.IconAnchor == \"right\" then\n                healthBar.border:SetPoint(\"TOPRIGHT\", castBar.Icon, \"TOPRIGHT\", 0, 0)\n                healthBar.border:SetPoint(\"BOTTOMLEFT\", castBar, \"BOTTOMLEFT\", 0, 0) \n            end\n        else\n            healthBar.border:SetPoint(\"TOPLEFT\", healthBar, \"TOPLEFT\", 0, 0)\n            healthBar.border:SetPoint(\"BOTTOMRIGHT\", healthBar, \"BOTTOMRIGHT\", 0, 0)\n        end\n        \n    end\n    \nend",
        },
        ["Author"] = "Viashi-Antonidas",
        ["PlaterCore"] = 1,
        ["Desc"] = "Move the icon of the spell cast to the left or right side of the nameplate and extend the border around icon + cast bar.",
        ["Hooks"] = {
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    --settings:\n    --show cast icon\n    envTable.ShowIcon = true\n    --anchor icon on what side\n    envTable.IconAnchor = \"left\" --accep 'left' 'right'\n    --fine tune the size of the icon\n    envTable.IconSizeOffset = 0\n    \n    --shield for non interruptible casts\n    envTable.ShowShield = true\n    envTable.ShieldTexture = [[Interface\\GROUPFRAME\\UI-GROUP-MAINTANKICON]]\n    envTable.ShieldDesaturated = true\n    envTable.ShieldColor = {1, 1, 1 ,1}\n    envTable.ShieldSize = {10, 12}\n    \n    --private:\n    function envTable.UpdateIconPosition (unitFrame)\n        local castBar = unitFrame.castBar\n        local icon = castBar.Icon\n        local shield = castBar.BorderShield\n        \n        if (envTable.ShowIcon) then\n            icon:ClearAllPoints()\n            \n            if (envTable.IconAnchor == \"left\") then\n                icon:SetPoint (\"topright\", unitFrame.healthBar, \"topleft\", 0, envTable.IconSizeOffset)\n                icon:SetPoint (\"bottomright\", unitFrame.castBar, \"bottomleft\", 0, 0)    \n                \n            elseif (envTable.IconAnchor == \"right\") then\n                icon:SetPoint (\"topleft\", unitFrame.healthBar, \"topright\", 0, envTable.IconSizeOffset)\n                icon:SetPoint (\"bottomleft\", unitFrame.castBar, \"bottomright\", 0, 0)\n                \n            end\n            \n            icon:SetWidth (icon:GetHeight())\n            icon:Show()\n            \n        else\n            icon:Hide()\n            \n        end\n        \n        if (envTable.ShowShield and not castBar.canInterrupt) then\n            shield:Show()\n            shield:SetAlpha (1)\n            shield:SetTexCoord (0, 1, 0, 1)\n            shield:SetVertexColor (1, 1, 1, 1)\n            \n            shield:SetTexture (envTable.ShieldTexture)\n            shield:SetDesaturated (envTable.ShieldDesaturated)\n            \n            if (not envTable.ShieldDesaturated) then\n                shield:SetVertexColor (DetailsFramework:ParseColors (envTable.ShieldColor))\n            end\n            \n            shield:SetSize (unpack (envTable.ShieldSize))\n            \n            shield:ClearAllPoints()\n            shield:SetPoint (\"center\", castBar, \"left\", 0, 0)\n            \n        else\n            shield:Hide()\n            \n        end\n        \n    end\n    \n    function envTable.UpdateBorder (unitFrame, casting)\n        local healthBar = unitFrame.healthBar\n        local castBar = unitFrame.castBar\n        \n        if casting then    \n            if envTable.IconAnchor == \"left\" then\n                healthBar.border:SetPoint(\"TOPLEFT\", castBar.Icon, \"TOPLEFT\", 0, 0)\n                healthBar.border:SetPoint(\"BOTTOMRIGHT\", castBar, \"BOTTOMRIGHT\", 0, 0)\n            elseif envTable.IconAnchor == \"right\" then\n                healthBar.border:SetPoint(\"TOPRIGHT\", castBar.Icon, \"TOPRIGHT\", 0, 0)\n                healthBar.border:SetPoint(\"BOTTOMLEFT\", castBar, \"BOTTOMLEFT\", 0, 0) \n            end\n        else\n            healthBar.border:SetPoint(\"TOPLEFT\", healthBar, \"TOPLEFT\", 0, 0)\n            healthBar.border:SetPoint(\"BOTTOMRIGHT\", healthBar, \"BOTTOMRIGHT\", 0, 0)\n        end\n        \n    end\n    \nend",
          ["Cast Start"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.UpdateIconPosition (unitFrame)\n    envTable.UpdateBorder (unitFrame, true)\n    \nend\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n",
          ["Cast Update"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.UpdateIconPosition (unitFrame)\n    self.ThrottleUpdate = -1\n    \nend\n\n\n",
          ["Cast Stop"] = "function (self, unitId, unitFrame, envTable)\n    \n    envTable.UpdateBorder (unitFrame, false)\n    \nend\n\n\n",
        },
        ["Prio"] = 99,
        ["Name"] = "Cast Icon Anchor + Border",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["race"] = {
          },
          ["pvptalent"] = {
          },
          ["role"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
          },
          ["spec"] = {
          },
        },
        ["LastHookEdited"] = "Constructor",
        ["Url"] = "https://wago.io/mEN8Q6zUa/1",
        ["Icon"] = "Interface\\Buttons\\UI-Quickslot2",
        ["Time"] = 1591979119,
      }, -- [17]
      {
        ["Enabled"] = false,
        ["Revision"] = 210,
        ["HooksTemp"] = {
        },
        ["Author"] = "Izimode-Azralon",
        ["PlaterCore"] = 1,
        ["Desc"] = "Hides your pet nameplates, show when selected, see the constructor script for options.",
        ["Hooks"] = {
          ["Nameplate Removed"] = "function (self, unitId, unitFrame, envTable)\n    \n    if (UnitIsUnit(\"pet\", unitFrame.namePlateUnitToken)) then\n        envTable.ShowNameplate (unitFrame)\n    end\n    \nend\n\n\n\n\n",
          ["Nameplate Updated"] = "function (self, unitId, unitFrame, envTable)\n    \n    --when plater finishes an update on the nameplate\n    --check within the envTable if the healthBar of this nameplate should be hidden\n    if (envTable.IsHidden) then\n        if (unitFrame.healthBar:IsShown()) then\n            envTable.HideNameplate (unitFrame)\n        end\n    end\n    \nend\n\n\n\n\n",
          ["Nameplate Added"] = "function (self, unitId, unitFrame, envTable)\n    \n    if (UnitIsUnit(\"pet\", unitFrame.namePlateUnitToken)) then\n        envTable.HideNameplate (unitFrame)\n    end\n    \nend\n\n\n\n\n\n\n\n\n",
          ["Constructor"] = "function (self, unitId, unitFrame, envTable)\n    \n    --functions to hide and show the healthBar\n    function envTable.HideNameplate (unitFrame)\n        unitFrame.PlateFrame:Hide()\n        envTable.IsHidden = true\n    end\n    \n    function envTable.ShowNameplate (unitFrame)\n        unitFrame.PlateFrame:Show()\n        envTable.IsHidden = false\n    end\n    \nend\n\n\n\n\n\n\n",
        },
        ["Prio"] = 99,
        ["Name"] = "Hide Your Pets",
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["race"] = {
          },
          ["pvptalent"] = {
          },
          ["role"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
          },
          ["spec"] = {
          },
        },
        ["LastHookEdited"] = "Constructor",
        ["Url"] = "https://wago.io/KlLYwJ3aD/1",
        ["Icon"] = 1990989,
        ["Time"] = 1584461151,
      }, -- [18]
      {
        ["Enabled"] = true,
        ["Revision"] = 125,
        ["HooksTemp"] = {
        },
        ["Author"] = "�r��ne-Kel'thuzad",
        ["Name"] = "Double Arrow Target",
        ["Desc"] = "Adds double arrow as a target indicators option",
        ["Hooks"] = {
          ["Nameplate Created"] = "function (self, unitId, unitFrame, envTable)\n    \n    -- /RELOAD AFTER IMPORTING OR CHANGING THE SCRIPT\n    -- SELECT THE INDICATOR AT THE TARGET TAB\n    \n    Plater.TargetIndicators    [\"Double Arrows\"] = {\n        path = [[Interface\\AddOns\\Plater\\media\\arrow_double_right_64]],\n        coords = {\n            {0, 1, 0, 1}, \n            {1, 0, 0, 1}\n        },\n        desaturated = false,\n        width = 20,\n        height = 16,\n        x = 20,\n        y = 0,\n        blend = \"ADD\",\n        color = \"white\",\n    }    \n    \nend\n\n\n",
          ["Nameplate Updated"] = "function (self, unitId, unitFrame, envTable)\n    \n    --border thickness\n    local size = 1 \n    \n    for index, auraIcon in ipairs (unitFrame.BuffFrame.PlaterBuffList) do\n        if (auraIcon:IsShown()) then\n            \n            if (not auraIcon.PixelPerfectBorder) then\n                auraIcon.PixelPerfectBorder = CreateFrame (\"frame\", nil, auraIcon, \"NamePlateFullBorderTemplate\")\n            end\n            \n            local r, g, b = auraIcon:GetBackdropBorderColor()\n            auraIcon:SetBackdropBorderColor (0, 0, 0, 0)\n            \n            auraIcon.PixelPerfectBorder:SetVertexColor (r, g, b)\n            auraIcon.PixelPerfectBorder:SetBorderSizes (size, size, size, size)\n            auraIcon.PixelPerfectBorder:UpdateSizes()\n            \n            auraIcon.Icon:ClearAllPoints()\n            auraIcon.Icon:SetAllPoints()\n            \n            auraIcon.Border:Hide() --hide plater default border\n        end\n    end\n    \n    for index, auraIcon in ipairs (unitFrame.BuffFrame2.PlaterBuffList) do\n        if (auraIcon:IsShown()) then\n            \n            if (not auraIcon.PixelPerfectBorder) then\n                auraIcon.PixelPerfectBorder = CreateFrame (\"frame\", nil, auraIcon, \"NamePlateFullBorderTemplate\")\n            end\n            \n            local r, g, b = auraIcon:GetBackdropBorderColor()\n            auraIcon:SetBackdropBorderColor (0, 0, 0, 0)\n            \n            auraIcon.PixelPerfectBorder:SetVertexColor (r, g, b)\n            auraIcon.PixelPerfectBorder:SetBorderSizes (size, size, size, size)\n            auraIcon.PixelPerfectBorder:UpdateSizes()            \n            \n            auraIcon.Icon:ClearAllPoints()\n            auraIcon.Icon:SetAllPoints()\n            \n            auraIcon.Border:Hide() --hide plater default border\n        end\n    end    \nend",
        },
        ["Prio"] = 99,
        ["Time"] = 1584663450,
        ["LoadConditions"] = {
          ["talent"] = {
          },
          ["group"] = {
          },
          ["class"] = {
          },
          ["map_ids"] = {
          },
          ["race"] = {
          },
          ["pvptalent"] = {
          },
          ["spec"] = {
          },
          ["affix"] = {
          },
          ["encounter_ids"] = {
          },
          ["role"] = {
          },
        },
        ["LastHookEdited"] = "Nameplate Created",
        ["Url"] = "https://wago.io/HRb0qZ8I9/1",
        ["Icon"] = "Interface\\AddOns\\Plater\\media\\arrow_double_right_64",
        ["PlaterCore"] = 1,
      }, -- [19]
    },
    ["health_statusbar_bgcolor"] = {
      0.2980392156862745, -- [1]
      0.2980392156862745, -- [2]
      0.2980392156862745, -- [3]
      0.8900000005960464, -- [4]
    },
    ["script_auto_imported"] = {
      ["Cast - Small Alert"] = 4,
      ["Aura - Invalidate Unit"] = 1,
      ["Aura - Buff Alert"] = 4,
      ["Unit - Important"] = 5,
      ["Explosion Affix M+"] = 3,
      ["Cast - Very Important"] = 2,
      ["Aura Border Color"] = 1,
      ["Color Change"] = 1,
      ["Aura - Debuff Alert"] = 3,
      ["Cast - Frontal Cone"] = 2,
      ["Fixate"] = 3,
      ["Aura - Blink Time Left"] = 1,
      ["Unit Power"] = 1,
      ["Cast - Big Alert"] = 5,
      ["Fixate On You"] = 2,
    },
    ["extra_icon_auras"] = {
      "wicked frenzy", -- [1]
      "bolstering shout", -- [2]
      "enraged", -- [3]
      "riot shield", -- [4]
      "bwonsamdi's mantle", -- [5]
      "gathered souls", -- [6]
      256849, -- [7]
      "bulwark of juju", -- [8]
      257476, -- [9]
      "blind rage", -- [10]
      "painful motivation", -- [11]
      "ancestral fury", -- [12]
      "seduction", -- [13]
      "healing tide totem", -- [14]
      "protective aura", -- [15]
      "mending rapids", -- [16]
      "reinforcing ward", -- [17]
      "watertight shell", -- [18]
      "electrified scales", -- [19]
      "embryonic vigor", -- [20]
      "inhale vapors", -- [21]
      263275, -- [22]
      "azerite injection", -- [23]
      "warcry", -- [24]
      265091, -- [25]
      "soul fetish", -- [26]
      "warding candles", -- [27]
      "overclock", -- [28]
      "enlarge", -- [29]
      "turbo boost", -- [30]
      "gilded claws", -- [31]
      "loaded dice: man-o-war", -- [32]
      "lightning shield", -- [33]
      "pay to win", -- [34]
      "motivated", -- [35]
    },
    ["indicator_faction"] = false,
    ["first_run3"] = true,
    ["ui_parent_scale_tune"] = 1.562500034924597,
    ["aura_tracker"] = {
      ["buff"] = {
        258153, -- [1]
        255824, -- [2]
        290586, -- [3]
        268375, -- [4]
        276767, -- [5]
        272888, -- [6]
        257397, -- [7]
        269129, -- [8]
        268130, -- [9]
        269302, -- [10]
        268710, -- [11]
        263215, -- [12]
        291502, -- [13]
        290880, -- [14]
        290542, -- [15]
        297152, -- [16]
        264396, -- [17]
        265368, -- [18]
        264111, -- [19]
        299428, -- [20]
        299869, -- [21]
        293926, -- [22]
        276105, -- [23]
      },
      ["debuff"] = {
        287286, -- [1]
        295671, -- [2]
        292611, -- [3]
        292626, -- [4]
        278946, -- [5]
        292711, -- [6]
        212463, -- [7]
        318272, -- [8]
        113746, -- [9]
      },
      ["track_method"] = 2,
      ["buff_tracked"] = {
        [209859] = true,
      },
    },
    ["hook_auto_imported"] = {
      ["Targetting Alpha"] = 3,
      ["Dont Have Aura"] = 1,
      ["Players Targetting Amount"] = 4,
      ["Color Automation"] = 1,
      ["Bwonsamdi Reaping"] = 1,
      ["Blockade Encounter"] = 1,
      ["Jaina Encounter"] = 6,
      ["Execute Range"] = 1,
      ["Attacking Specific Unit"] = 1,
      ["Hide Neutral Units"] = 1,
      ["Extra Border"] = 2,
      ["Combo Points"] = 3,
      ["Target Color"] = 3,
      ["Aura Reorder"] = 1,
      ["Reorder Nameplate"] = 3,
    },
    ["castbar_target_show"] = true,
    ["indicator_rare"] = false,
    ["target_indicator"] = "Double Arrows",
    ["aura_show_buff_by_the_unit"] = false,
    ["captured_spells"] = {
      [269279] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [259388] = {
        ["type"] = "BUFF",
        ["source"] = "Chaosshot",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [268899] = {
        ["type"] = "BUFF",
        ["source"] = "Noblemonk-Blackmoore",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [278736] = {
        ["type"] = "BUFF",
        ["source"] = "Intensoo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [318211] = {
        ["type"] = "BUFF",
        ["source"] = "Noblemonk-Blackmoore",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [264173] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [257410] = {
        ["type"] = "BUFF",
        ["source"] = "Håm",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [271711] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [295856] = {
        ["source"] = "Guardian of Azeroth",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 152396,
      },
      [266091] = {
        ["type"] = "BUFF",
        ["source"] = "Chromeo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [186254] = {
        ["type"] = "BUFF",
        ["source"] = "Pmzz",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [259391] = {
        ["source"] = "Chaosshot",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [315787] = {
        ["type"] = "BUFF",
        ["source"] = "Pmzz",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [317065] = {
        ["type"] = "BUFF",
        ["source"] = "Mamoush",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [319237] = {
        ["type"] = "BUFF",
        ["source"] = "Zlobnik",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [264689] = {
        ["type"] = "DEBUFF",
        ["source"] = "Pmzz",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [268905] = {
        ["type"] = "BUFF",
        ["source"] = "Heavenguard",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [768] = {
        ["type"] = "BUFF",
        ["source"] = "Lilbeerus",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [116858] = {
        ["source"] = "Chromeo",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [118455] = {
        ["type"] = "BUFF",
        ["source"] = "Pmzz",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [318219] = {
        ["type"] = "BUFF",
        ["source"] = "Pmzz",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [186257] = {
        ["type"] = "BUFF",
        ["source"] = "Duzoo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [8679] = {
        ["type"] = "BUFF",
        ["source"] = "Nerfmefast",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [5116] = {
        ["source"] = "Pmzz",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [80354] = {
        ["type"] = "DEBUFF",
        ["source"] = "Loonaxi-Draenor",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [186258] = {
        ["type"] = "BUFF",
        ["source"] = "Duzoo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [44457] = {
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [32752] = {
        ["type"] = "BUFF",
        ["source"] = "Chromeo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [264057] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [30213] = {
        ["source"] = "Jhuukorak",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 17252,
      },
      [774] = {
        ["type"] = "BUFF",
        ["source"] = "Druwomi",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [2580] = {
        ["type"] = "BUFF",
        ["source"] = "Heavenguard",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [157644] = {
        ["type"] = "BUFF",
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [205231] = {
        ["source"] = "Darkglare",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 103673,
      },
      [113858] = {
        ["type"] = "BUFF",
        ["source"] = "Chromeo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [315161] = {
        ["type"] = "DEBUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [11366] = {
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [43308] = {
        ["type"] = "BUFF",
        ["source"] = "Bobbydh",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [3110] = {
        ["source"] = "Gobnip",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 416,
      },
      [108366] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [287062] = {
        ["type"] = "BUFF",
        ["source"] = "Noblemonk-Blackmoore",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [289362] = {
        ["type"] = "DEBUFF",
        ["source"] = "Marcø",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [295367] = {
        ["type"] = "DEBUFF",
        ["source"] = "Chromeo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [113860] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [297412] = {
        ["type"] = "BUFF",
        ["source"] = "Arucardo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [315681] = {
        ["type"] = "DEBUFF",
        ["source"] = "Bajerogstreg",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [304056] = {
        ["type"] = "BUFF",
        ["source"] = "Aldarana",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [260425] = {
        ["source"] = "Muddy Riverbeast",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 130757,
      },
      [235450] = {
        ["type"] = "BUFF",
        ["source"] = "Macrìna",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [105174] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [312107] = {
        ["type"] = "BUFF",
        ["source"] = "Zlobnik",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [19483] = {
        ["type"] = "BUFF",
        ["source"] = "Infernal",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 89,
      },
      [217694] = {
        ["type"] = "DEBUFF",
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [297034] = {
        ["type"] = "BUFF",
        ["source"] = "Nerfmefast",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [297162] = {
        ["type"] = "BUFF",
        ["source"] = "Pmzz",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [259277] = {
        ["type"] = "DEBUFF",
        ["source"] = "Wolf",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 76597,
      },
      [227723] = {
        ["type"] = "BUFF",
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [2649] = {
        ["source"] = "Wolf",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 76597,
      },
      [317859] = {
        ["type"] = "BUFF",
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [295248] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [272893] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [315179] = {
        ["type"] = "DEBUFF",
        ["source"] = "Milkmë",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [295378] = {
        ["type"] = "BUFF",
        ["source"] = "Macrìna",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [297039] = {
        ["type"] = "BUFF",
        ["source"] = "Nelleniel",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [54680] = {
        ["source"] = "oneclickdps",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 130713,
      },
      [280177] = {
        ["type"] = "BUFF",
        ["source"] = "Loonaxi-Draenor",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [48181] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [205179] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [89751] = {
        ["type"] = "BUFF",
        ["source"] = "Jhuukorak",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 17252,
      },
      [108211] = {
        ["type"] = "BUFF",
        ["source"] = "Virry",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [212653] = {
        ["type"] = "BUFF",
        ["source"] = "Loonaxi-Draenor",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [205180] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [165961] = {
        ["type"] = "BUFF",
        ["source"] = "Eshleear-Silvermoon",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [30151] = {
        ["type"] = "BUFF",
        ["source"] = "Jhuukorak",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 17252,
      },
      [279673] = {
        ["type"] = "BUFF",
        ["source"] = "Chromeo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [298836] = {
        ["type"] = "BUFF",
        ["source"] = "Átomos",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [298837] = {
        ["type"] = "BUFF",
        ["source"] = "Ratsa",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [260881] = {
        ["type"] = "BUFF",
        ["source"] = "Marcø",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [104316] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [259285] = {
        ["type"] = "BUFF",
        ["source"] = "Chaosshot",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [89753] = {
        ["source"] = "Jhuukorak",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 17252,
      },
      [313148] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [319919] = {
        ["type"] = "BUFF",
        ["source"] = "Zintama",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [298841] = {
        ["type"] = "BUFF",
        ["source"] = "Chakaron",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [242551] = {
        ["type"] = "BUFF",
        ["source"] = "Haereticus",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [295137] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [146739] = {
        ["type"] = "DEBUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [274443] = {
        ["type"] = "BUFF",
        ["source"] = "Aldarana",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [313918] = {
        ["type"] = "BUFF",
        ["source"] = "Nelleniel",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [257946] = {
        ["type"] = "BUFF",
        ["source"] = "Aldarana",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [279810] = {
        ["type"] = "BUFF",
        ["source"] = "Aldarana",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [108853] = {
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [172] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [104318] = {
        ["source"] = "Wild Imp",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 55659,
      },
      [131347] = {
        ["source"] = "Tpzed",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [134477] = {
        ["type"] = "BUFF",
        ["source"] = "Jhuukorak",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 17252,
      },
      [688] = {
        ["source"] = "Chromeo",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [268955] = {
        ["type"] = "BUFF",
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [1122] = {
        ["source"] = "Chromeo",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [8936] = {
        ["type"] = "BUFF",
        ["source"] = "Druwomi",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [691] = {
        ["source"] = "Chromeo",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [260249] = {
        ["type"] = "BUFF",
        ["source"] = "Chaosshot",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [272790] = {
        ["source"] = "Aldarana",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [245686] = {
        ["type"] = "BUFF",
        ["source"] = "Intensoo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [267171] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [280713] = {
        ["type"] = "BUFF",
        ["source"] = "Chromeo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [264106] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [348] = {
        ["source"] = "Chromeo",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [40120] = {
        ["type"] = "BUFF",
        ["source"] = "Laidacow",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [697] = {
        ["source"] = "Chromeo",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [256739] = {
        ["type"] = "BUFF",
        ["source"] = "Flinched",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [80240] = {
        ["source"] = "Chromeo",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [289277] = {
        ["type"] = "BUFF",
        ["source"] = "Jahovah",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [233490] = {
        ["type"] = "DEBUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [266030] = {
        ["type"] = "BUFF",
        ["source"] = "Chromeo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [308188] = {
        ["type"] = "BUFF",
        ["source"] = "Vaggem",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [312915] = {
        ["type"] = "BUFF",
        ["source"] = "Nelleniel",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [308189] = {
        ["type"] = "BUFF",
        ["source"] = "Arcänist",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [157736] = {
        ["type"] = "DEBUFF",
        ["source"] = "Chromeo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [259489] = {
        ["source"] = "Chaosshot",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [48792] = {
        ["type"] = "BUFF",
        ["source"] = "Denzai",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [258915] = {
        ["source"] = "Vicious Diemetradon",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 130756,
      },
      [16827] = {
        ["source"] = "YEET",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 50051,
      },
      [259618] = {
        ["source"] = "Vicious Diemetradon",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 130756,
      },
      [272678] = {
        ["source"] = "Pmzz",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [2823] = {
        ["type"] = "BUFF",
        ["source"] = "Átomos",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [303211] = {
        ["type"] = "BUFF",
        ["source"] = "Wârrîor",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [259491] = {
        ["source"] = "Chaosshot",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [260322] = {
        ["source"] = "Nol'ixwan",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 130741,
      },
      [281240] = {
        ["type"] = "BUFF",
        ["source"] = "Nebularay",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [29722] = {
        ["source"] = "Chromeo",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [21562] = {
        ["type"] = "BUFF",
        ["source"] = "Ðraakje",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [274598] = {
        ["type"] = "BUFF",
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [265273] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [712] = {
        ["source"] = "Chromeo",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [233496] = {
        ["type"] = "DEBUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [303344] = {
        ["type"] = "BUFF",
        ["source"] = "Aldarana",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [272940] = {
        ["type"] = "BUFF",
        ["source"] = "Saraziy",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [303345] = {
        ["type"] = "BUFF",
        ["source"] = "Aldarana",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [233497] = {
        ["type"] = "DEBUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [26297] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [167898] = {
        ["type"] = "BUFF",
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [259495] = {
        ["source"] = "Chaosshot",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [233498] = {
        ["type"] = "DEBUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [292361] = {
        ["type"] = "BUFF",
        ["source"] = "Vegettaa",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [101643] = {
        ["type"] = "BUFF",
        ["source"] = "Noblemonk-Blackmoore",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [288146] = {
        ["type"] = "BUFF",
        ["source"] = "Nerfmefast",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [48107] = {
        ["type"] = "BUFF",
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [279715] = {
        ["type"] = "BUFF",
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [233499] = {
        ["type"] = "DEBUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [259241] = {
        ["type"] = "BUFF",
        ["source"] = "Nepatalo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 131265,
      },
      [317020] = {
        ["type"] = "BUFF",
        ["source"] = "Strongx",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [201754] = {
        ["source"] = "YEET",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 50051,
      },
      [5740] = {
        ["source"] = "Chromeo",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [232670] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [252216] = {
        ["type"] = "BUFF",
        ["source"] = "Mimi",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [246851] = {
        ["type"] = "BUFF",
        ["source"] = "Pmzz",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [285981] = {
        ["type"] = "BUFF",
        ["source"] = "Mollymorph-Ragnaros",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [275378] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [246852] = {
        ["type"] = "BUFF",
        ["source"] = "Pmzz",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [2383] = {
        ["type"] = "BUFF",
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [48108] = {
        ["type"] = "BUFF",
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [17962] = {
        ["source"] = "Chromeo",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [3408] = {
        ["type"] = "BUFF",
        ["source"] = "Átomos",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [246853] = {
        ["type"] = "BUFF",
        ["source"] = "Pmzz",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [267971] = {
        ["source"] = "Demonic Tyrant",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 135002,
      },
      [119914] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [193455] = {
        ["source"] = "Pmzz",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [246152] = {
        ["type"] = "BUFF",
        ["source"] = "Aldarana",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [251837] = {
        ["type"] = "BUFF",
        ["source"] = "Vaggem",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [1459] = {
        ["type"] = "BUFF",
        ["source"] = "Macrìna",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [79865] = {
        ["type"] = "BUFF",
        ["source"] = "Nerog",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 46716,
      },
      [12654] = {
        ["type"] = "DEBUFF",
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [7870] = {
        ["type"] = "BUFF",
        ["source"] = "Aezwyn",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 1863,
      },
      [200546] = {
        ["source"] = "Chromeo",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [316522] = {
        ["type"] = "BUFF",
        ["source"] = "Pmzz",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [205146] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [119085] = {
        ["type"] = "BUFF",
        ["source"] = "Scottblexy-Blackrock",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [270661] = {
        ["type"] = "BUFF",
        ["source"] = "Chromeo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [215111] = {
        ["source"] = "Dreadstalker",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 98035,
      },
      [244813] = {
        ["type"] = "DEBUFF",
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [187645] = {
        ["source"] = "Wild Sabertusk",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 123135,
      },
      [72968] = {
        ["type"] = "BUFF",
        ["source"] = "Kuncain",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [297108] = {
        ["source"] = "Pmzz",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [8690] = {
        ["source"] = "Guacaholy",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [2948] = {
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [203233] = {
        ["type"] = "BUFF",
        ["source"] = "Duzoo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [279902] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [187647] = {
        ["source"] = "Wild Sabertusk",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 123135,
      },
      [260349] = {
        ["source"] = "Ten'gor",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 130713,
      },
      [296003] = {
        ["type"] = "BUFF",
        ["source"] = "Xelinia",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [270150] = {
        ["source"] = "Feathered Viper",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 132410,
      },
      [260355] = {
        ["type"] = "BUFF",
        ["source"] = "Ten'gor",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 130713,
      },
      [6673] = {
        ["type"] = "BUFF",
        ["source"] = "Klv",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [231390] = {
        ["type"] = "BUFF",
        ["source"] = "Mamoush",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [293664] = {
        ["type"] = "BUFF",
        ["source"] = "Nebularay",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [108446] = {
        ["type"] = "BUFF",
        ["source"] = "Unknown",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 17252,
      },
      [260070] = {
        ["type"] = "BUFF",
        ["source"] = "Pa'kura Priest",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 131834,
      },
      [300761] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [264764] = {
        ["type"] = "BUFF",
        ["source"] = "Nebularay",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [270481] = {
        ["source"] = "Demonic Tyrant",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 135002,
      },
      [268854] = {
        ["type"] = "BUFF",
        ["source"] = "Noblemonk-Blackmoore",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [104773] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [686] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [268954] = {
        ["type"] = "BUFF",
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [318391] = {
        ["type"] = "DEBUFF",
        ["source"] = "Great Worm From Beyond",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 152550,
      },
      [300693] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [34026] = {
        ["source"] = "Aldarana",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [316814] = {
        ["type"] = "BUFF",
        ["source"] = "Håm",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [236502] = {
        ["type"] = "BUFF",
        ["source"] = "Marcø",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [1490] = {
        ["type"] = "DEBUFF",
        ["source"] = "Xazin-Draenor",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [61684] = {
        ["type"] = "BUFF",
        ["source"] = "oneclickdps",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 130713,
      },
      [295840] = {
        ["type"] = "BUFF",
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [112042] = {
        ["type"] = "BUFF",
        ["source"] = "Maknar",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 1860,
      },
      [205025] = {
        ["type"] = "BUFF",
        ["source"] = "Macrìna",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [260069] = {
        ["type"] = "BUFF",
        ["source"] = "Priest of Gonk",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 131809,
      },
      [164273] = {
        ["type"] = "BUFF",
        ["source"] = "Yukehunter",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [264178] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [133] = {
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [117828] = {
        ["type"] = "BUFF",
        ["source"] = "Chromeo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [295842] = {
        ["type"] = "BUFF",
        ["source"] = "Wârrîor",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [115804] = {
        ["type"] = "DEBUFF",
        ["source"] = "oneclickdps",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 130713,
      },
      [256456] = {
        ["type"] = "BUFF",
        ["source"] = "Duzoo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [90328] = {
        ["type"] = "BUFF",
        ["source"] = "Unknown",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 69946,
      },
      [269747] = {
        ["type"] = "DEBUFF",
        ["source"] = "Chaosshot",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [217200] = {
        ["source"] = "Aldarana",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [48707] = {
        ["type"] = "BUFF",
        ["source"] = "Denzai",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [24858] = {
        ["type"] = "BUFF",
        ["source"] = "Vegettaa",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [294966] = {
        ["type"] = "BUFF",
        ["source"] = "Zlobnik",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [264314] = {
        ["type"] = "BUFF",
        ["source"] = "Kylary-Nemesis",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [232698] = {
        ["type"] = "BUFF",
        ["source"] = "Ðraakje",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [267612] = {
        ["type"] = "BUFF",
        ["source"] = "Pmzz",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [980] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [17253] = {
        ["source"] = "Wolf",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 76597,
      },
      [193530] = {
        ["type"] = "BUFF",
        ["source"] = "Aldarana",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [225787] = {
        ["type"] = "BUFF",
        ["source"] = "Chromeo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [63106] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [196277] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [235313] = {
        ["type"] = "BUFF",
        ["source"] = "Bajerogstreg",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [199483] = {
        ["type"] = "BUFF",
        ["source"] = "Chaosshot",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [265187] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [326419] = {
        ["type"] = "BUFF",
        ["source"] = "Kekwopepèg",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [204262] = {
        ["type"] = "BUFF",
        ["source"] = "Yasskween-Silvermoon",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [30108] = {
        ["source"] = "Benkeyq",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [2645] = {
        ["type"] = "BUFF",
        ["source"] = "Arucardo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [272934] = {
        ["type"] = "BUFF",
        ["source"] = "Pýropussaý",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [297126] = {
        ["type"] = "BUFF",
        ["source"] = "Pmzz",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [280205] = {
        ["type"] = "BUFF",
        ["source"] = "Marcø",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [202602] = {
        ["type"] = "BUFF",
        ["source"] = "Strongx",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [259387] = {
        ["source"] = "Chaosshot",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [79849] = {
        ["type"] = "BUFF",
        ["source"] = "Moraka",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 46718,
      },
      [19574] = {
        ["type"] = "BUFF",
        ["source"] = "Pmzz",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [2643] = {
        ["source"] = "Pmzz",
        ["event"] = "SPELL_CAST_SUCCESS",
        ["npcID"] = 0,
      },
      [233582] = {
        ["type"] = "DEBUFF",
        ["source"] = "Chromeo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [275429] = {
        ["type"] = "BUFF",
        ["source"] = "Chromeo",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [224001] = {
        ["type"] = "BUFF",
        ["source"] = "Loonaxi-Draenor",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
      [268877] = {
        ["type"] = "BUFF",
        ["source"] = "Pmzz",
        ["event"] = "SPELL_AURA_APPLIED",
        ["npcID"] = 0,
      },
    },
    ["saved_cvars"] = {
      ["ShowClassColorInNameplate"] = "1",
      ["nameplateOverlapV"] = "1.1",
      ["nameplateShowSelf"] = "0",
      ["nameplateShowEnemyMinus"] = "1",
      ["nameplatePersonalShowAlways"] = "0",
      ["nameplateMotionSpeed"] = "0.05",
      ["nameplateGlobalScale"] = "1",
      ["nameplateShowFriendlyTotems"] = "0",
      ["nameplateShowEnemyMinions"] = "1",
      ["nameplateShowFriendlyPets"] = "0",
      ["nameplateShowFriendlyNPCs"] = "0",
      ["nameplateSelectedScale"] = "1.15",
      ["nameplatePersonalShowInCombat"] = "1",
      ["nameplatePersonalShowWithTarget"] = "0",
      ["ShowNamePlateLoseAggroFlash"] = "1",
      ["nameplateShowFriendlyMinions"] = "0",
      ["nameplateResourceOnTarget"] = "0",
      ["nameplateMotion"] = "1",
      ["nameplateSelfAlpha"] = "1",
      ["nameplateMinScale"] = "1",
      ["nameplateMaxDistance"] = "100",
      ["nameplateOtherTopInset"] = "0.085",
      ["nameplateSelfScale"] = "1",
      ["nameplateSelfBottomInset"] = "0.2",
      ["NamePlateHorizontalScale"] = "1",
      ["nameplateShowFriendlyGuardians"] = "0",
      ["nameplateOccludedAlphaMult"] = "1",
      ["nameplateShowAll"] = "1",
      ["nameplatePersonalHideDelaySeconds"] = "0.2",
      ["nameplateSelfTopInset"] = "0.5",
      ["NamePlateVerticalScale"] = "1",
    },
    ["login_counter"] = 8684,
    ["patch_version"] = 9,
    ["aura_timer_text_size"] = 10,
    ["number_region_first_run"] = true,
    ["aura_cooldown_edge_texture"] = "Interface\\GLUES\\loadingOld",
    ["npc_colors"] = {
      [154524] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [131383] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [145185] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [122972] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [134284] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [130909] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [144293] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [133870] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [150222] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [139799] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [151657] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [144294] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [144071] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [131863] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [139800] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [122973] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [134828] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [150160] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [131864] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [134701] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [136295] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [127484] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [137029] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [126847] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [136934] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [135245] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [136297] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [141939] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [144298] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [138465] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [134417] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [134991] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [129366] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [126832] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [126848] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [128649] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [137478] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [133685] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [131677] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [152874] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [136076] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [161895] = {
        true, -- [1]
        false, -- [2]
        "hotpink", -- [3]
      },
      [127757] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [133463] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [137830] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [129367] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [134069] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [152009] = {
        true, -- [1]
        false, -- [2]
        "hotpink", -- [3]
      },
      [134388] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [136100] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [133432] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [134739] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [128969] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [131670] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [134063] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [129527] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [127503] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [129559] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [150253] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [150295] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [126845] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [139097] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [130404] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [150168] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [134390] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [133912] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [133944] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [135470] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [134158] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [137474] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [134993] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [127488] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [131586] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [122971] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [135475] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [134232] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [127486] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [163746] = {
        true, -- [1]
        false, -- [2]
        "hotpink", -- [3]
      },
      [138489] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [127106] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [134137] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [135699] = {
        true, -- [1]
        false, -- [2]
        "lavenderblush", -- [3]
      },
      [133436] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [130485] = {
        true, -- [1]
        false, -- [2]
        "salmon", -- [3]
      },
      [139110] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [131492] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [138187] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [129529] = {
        true, -- [1]
        false, -- [2]
        "salmon", -- [3]
      },
      [136688] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [133835] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [154758] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [122963] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [150297] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [129370] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [130661] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [131812] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [134139] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [137486] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [150712] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [150142] = {
        true, -- [1]
        false, -- [2]
        "hotpink", -- [3]
      },
      [129227] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [144244] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [150396] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [127490] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [136549] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [131545] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [132056] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [154744] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [136391] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [161124] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [131527] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [129699] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [133389] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [137233] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [135204] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [132532] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [153755] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [144246] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [150143] = {
        true, -- [1]
        false, -- [2]
        "hotpink", -- [3]
      },
      [133345] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [134331] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [134174] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [137103] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [122965] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [150190] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [135322] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [136976] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [131849] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [130488] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [138255] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [141284] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [152089] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [131824] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [144248] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [130026] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [131817] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [134144] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [133379] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [129602] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [136160] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [139946] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [144249] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [150146] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [141285] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [131850] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [131436] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [134150] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [129214] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [151613] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [122967] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [126969] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [126983] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [136186] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [161241] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [136250] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [135007] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [128434] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [132713] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [134338] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [127111] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [135167] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [139949] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [135231] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [135263] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [129231] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [153942] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [130028] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [136083] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [157609] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [135169] = {
        true, -- [1]
        false, -- [2]
        "salmon", -- [3]
      },
      [122984] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [122968] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [132491] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [153377] = {
        true, -- [1]
        false, -- [2]
        "hotpink", -- [3]
      },
      [153401] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [137591] = {
        true, -- [1]
        false, -- [2]
        "hotpink", -- [3]
      },
      [133384] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [138281] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [161243] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [130012] = {
        true, -- [1]
        false, -- [2]
        "hotpink", -- [3]
      },
      [129232] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [135329] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [127479] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [131823] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [157610] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [126919] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [161244] = {
        true, -- [1]
        false, -- [2]
        "DEATHKNIGHT", -- [3]
      },
      [122969] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [134612] = {
        true, -- [1]
        false, -- [2]
        "hotpink", -- [3]
      },
      [137484] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [128651] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [134629] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [137204] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [152033] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [135235] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [136470] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [136214] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [134056] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [136643] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [131825] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [130025] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [139284] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [131666] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [129600] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [150397] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [129208] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [134599] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [127879] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [128652] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [150250] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [131667] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [134012] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [150159] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [134058] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [134600] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [150292] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [129553] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [135764] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [136139] = {
        true, -- [1]
        false, -- [2]
        "salmon", -- [3]
      },
      [129601] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [133007] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [135472] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [151476] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [131318] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [130655] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [150293] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [133430] = {
        true, -- [1]
        false, -- [2]
        "blueviolet", -- [3]
      },
      [134251] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
      [137473] = {
        true, -- [1]
        false, -- [2]
        "fuchsia", -- [3]
      },
      [134060] = {
        true, -- [1]
        false, -- [2]
        "aqua", -- [3]
      },
      [134418] = {
        true, -- [1]
        false, -- [2]
        "MAGE", -- [3]
      },
    },
    ["OptionsPanelDB"] = {
      ["PlaterOptionsPanelFrame"] = {
        ["scale"] = 1.013466954231262,
      },
    },
    ["aura_show_enrage"] = true,
    ["health_statusbar_texture"] = "Blizzard",
    ["indicator_enemyclass"] = true,
  }

end
