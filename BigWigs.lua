local  BUI = select(2, ...):unpack()

--Don't touch this function unless you know what it does
local function BigWigsFresh()
  --BigWigs sucks and takes forever to create it's database for new users
  BigWigs3DB = BigWigs3DB or {}
  BigWigs3DB["namespaces"] = BigWigs3DB["namespaces"] or {}
  BigWigs3DB["profiles"] = BigWigs3DB["profiles"] or {}
  --These namespaces are for individual bosses and aren't created for a profile unless a setting is changed
  local BigWigsNameSpaces = {
    "BigWigs_Bosses_Lord Stormsong",
    "BigWigs_Bosses_Adderis and Aspix",
    "BigWigs_Bosses_Viq'Goth",
    "BigWigs_Bosses_Overseer Korgus",
    "BigWigs_Bosses_K.U.-J.0.",
    "BigWigs_Bosses_Hadal Darkfathom",
    "BigWigs_Bosses_Mogul Razzdunk",
    "BigWigs_Bosses_Harlan Sweete",
    "BigWigs_Bosses_King's Rest Trash",
    "BigWigs_Bosses_Ring of Booty",
    "BigWigs_Bosses_Aqu'sirr",
    "BigWigs_Bosses_The Council of Tribes",
    "BigWigs_Bosses_Jes Howlis",
    "BigWigs_Bosses_Tussle Tonks",
    "BigWigs_Bosses_Freehold Trash",
    "BigWigs_Bosses_The Golden Serpent",
    "BigWigs_Bosses_Temple of Sethraliss Trash",
    "BigWigs_Bosses_Shrine of the Storm Trash",
    "BigWigs_Bosses_Mchimba the Embalmer",
    "BigWigs_Bosses_Tidesage Coucil",
    "BigWigs_Bosses_Skycap'n Kragg",
    "BigWigs_Bosses_King Gobbamak",
    "BigWigs_Bosses_Elder Leaxa",
    "BigWigs_Bosses_Atal'Dazar Trash",
    "BigWigs_Bosses_Avatar of Sethraliss",
    "BigWigs_Bosses_Underrot Trash",
    "BigWigs_Bosses_Waycrest Manor Trash",
    "BigWigs_Bosses_King Mechagon",
    "BigWigs_Bosses_Siege of Boralus Trash",
    "BigWigs_Bosses_Rezan",
    "BigWigs_Bosses_Coin-Operated Crowd Pummeler",
    "BigWigs_Bosses_Unbound Abomination",
    "BigWigs_Bosses_Gunker",
    "BigWigs_Bosses_Vol'kaal",
    "BigWigs_Bosses_Dazar, The First King",
    "BigWigs_Bosses_The Sand Queen",
    "BigWigs_Bosses_Tol Dagor Trash",
    "BigWigs_Bosses_Machinist's Garden",
    "BigWigs_Bosses_Soulbound Goliath",
    "BigWigs_Bosses_Operation: Mechagon Trash",
    "BigWigs_Bosses_Knight Captain Valyri",
    "BigWigs_Bosses_Infested Crawg",
    "BigWigs_Bosses_Gorak Tul",
    "BigWigs_Bosses_Yazma",
    "BigWigs_Bosses_Rixxa Fluxflame",
    "BigWigs_Bosses_Galvazzt",
    "BigWigs_Bosses_Trixie & Naeno",
    "BigWigs_Bosses_Merektha",
    "BigWigs_Bosses_HK-8 Aerial Oppression Unit",
    "BigWigs_Bosses_The MOTHERLODE!! Trash",
    "BigWigs_Bosses_Sergeant Bainbridge",
    "BigWigs_Bosses_Raal the Gluttonous",
    "BigWigs_Bosses_Council o' Captains",
    "BigWigs_Bosses_Priestess Alun'za",
    "BigWigs_Bosses_Dread Captain Lockwood",
    "BigWigs_Bosses_Vol'zith the Whisperer",
    "BigWigs_Bosses_Heartsbane Triad",
    "BigWigs_Bosses_Lord and Lady Waycrest",
    "BigWigs_Bosses_Sporecaller Zancha",
  }

  --These namespaces will exist if the BigWigs menu has been opened/a profile created
  --They aren't fully created until a setting under the namespace has been changed
  --They take extra care to ensure settings from existing profiles aren't erased
  local BigWigsAnnoyingNameSpaces = {
    "BigWigs_Plugins_Alt Power",
    "BigWigs_Plugins_Messages",
    "BigWigs_Plugins_Sounds",
    "BigWigs_Plugins_Colors",
    "BigWigs_Plugins_Proximity",
    "BigWigs_Plugins_InfoBox",
    "BigWigs_Plugins_Bars",
    "BigWigs_Plugins_Super Emphasize",
    "BigWigs_Plugins_Pull",
  }

  --Run through the easy namespaces and create their tables if they don't already exist
  for i, setting in pairs(BigWigsNameSpaces) do
    BigWigs3DB["namespaces"][setting] = BigWigs3DB["namespaces"][setting] or {
      ["profiles"] = {
        profile = {},
      },
    }
  end

  --For the annyoing namespaces see if the setting exists then check if it has any profile data
  for i, setting in pairs(BigWigsAnnoyingNameSpaces) do
    BigWigs3DB["namespaces"][setting] = BigWigs3DB["namespaces"][setting] or {}
    BigWigs3DB["namespaces"][setting]["profiles"] = BigWigs3DB["namespaces"][setting]["profiles"] or {}
  end
end

--This is the function that adds and applies the BigWigs profile
function BUI:ImportBigWigs()
  -- Make sure all of the BigWigs module profiles exist
  BigWigsFresh()

	--Create Bob's Profile
  local profileName = "Bob's BigWigs"
	BigWigs3DB["profiles"][profileName] = BigWigs3DB["profiles"][profileName] or {}

  --Its important that all of these follow the format BigWigs3DB["namespaces"][*BigWigs Plugin Name*]["profiles"][*Profile Name*] = {}
	--If not it will probably wipe out all other BigWigs profiles
  --These are pretty annoying to copy out of the WTF files and easy to mess up, be careful or ask for help

  --These settings won't be in the same order in the BigWigs WTF file, watch out for that
  --Currently they're arraged by dungeon and then boss order with trash at the end

  --Shrine of the Storm
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Aqu'sirr"]["profiles"][profileName] = {
    [264526] = 0,
    [264101] = 3064053,
    [264166] = 3064053,
    [265001] = 3064053,
    [264560] = 3072245,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Tidesage Coucil"]["profiles"][profileName] = {
    [267830] = 0,
    [267818] = 0,
    [267899] = 3064053,
    [267901] = 0,
    [267891] = 3064053,
    [267905] = 3064053,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Lord Stormsong"]["profiles"][profileName] = {
    [269097] = 0,
    [268896] = 3137781,
    [268347] = 0,
    [269131] = 3064053,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Vol'zith the Whisperer"]["profiles"][profileName] = {
    [267037] = 3064053,
    [269399] = 0,
    [267360] = 3129589,
    [267385] = 3064053,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Shrine of the Storm Trash"]["profiles"][profileName] = {
    [276268] = 3064053,
    [268030] = 0,
    [276292] = 0,
    [267977] = 0,
    [268050] = 0,
    [268027] = 0,
    [267981] = 0,
    [274631] = 0,
    [276265] = 0,
    [268184] = 0,
    [268177] = 0,
    [274633] = 0,
  }

  --Temple of Setrallis
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Adderis and Aspix"]["profiles"][profileName] = {
    [263371] = 2539765,
    [263424] = 0,
    [263246] = 3064037,
    [263257] = 3129589,
    [263309] = 3064053,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Merektha"]["profiles"][profileName] = {
    [264239] = 3064037,
    [263912] = 3064037,
    [263914] = 3064053,
    [263958] = 3064053,
    [263927] = 3064053,
    [264206] = 3064037,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Galvazzt"]["profiles"][profileName] = {
    [266923] = 0,
    [266512] = 0,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Avatar of Sethraliss"]["profiles"][profileName] = {
    [268007] = 0,
    [269686] = 0,
    [268008] = 3072245,
    [268024] = 3064037,
    [269688] = 3064053,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Temple of Sethraliss Trash"]["profiles"][profileName] = {
    [268008] = 3064053,
    [265912] = 0,
    [272700] = 0,
    [272659] = 0,
    [273563] = 0,
    [258908] = 3064053,
    [273995] = 3064053,
    [265968] = 0,
    [272657] = 3064053,
    [267237] = 0,
    [264574] = 3064053,
  }

  --Siege of Boralus
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Sergeant Bainbridge"]["profiles"][profileName] = {
    [277965] = 0,
    [257585] = 0,
    ["adds"] = 3064053,
    [260924] = 0,
    [279761] = 0,
    [260954] = 3064053,
    [261428] = 3064053,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Dread Captain Lockwood"]["profiles"][profileName] = {
    [272471] = 0,
    [269029] = 0,
    [268963] = 0,
    [268230] = 0,
    [268260] = 3064053,
    [273470] = 3064053,
    [268752] = 3129589,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Hadal Darkfathom"]["profiles"][profileName] = {
    [276068] = 3129589,
    [257882] = 3064053,
    [261563] = 3064037,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Viq'Goth"]["profiles"][profileName] = {
    [270590] = 3064053,
    [269266] = 3064053,
    [269366] = 3064037,
    [275014] = 3063989,
    [270185] = 3064037,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Siege of Boralus Trash"]["profiles"][profileName] = {
    [275826] = 3064053,
    [272827] = 3064053,
    [272711] = 3064053,
    [272874] = 3064053,
    [275835] = 0,
    [256640] = 0,
    [257036] = 0,
    [257288] = 3064053,
    [257641] = 0,
    [272421] = 3064309,
    [256673] = 0,
    [268260] = 3064053,
    [272546] = 262145,
    [274569] = 0,
    [256627] = 3064053,
    [257169] = 3064053,
    [256866] = 0,
    [257170] = 0,
    [256897] = 0,
  }

  --Tol Dagor
  BigWigs3DB["namespaces"]["BigWigs_Bosses_The Sand Queen"]["profiles"][profileName] = {
    [257092] = 3064037,
    [257495] = 3064037,
    [257609] = 0,
    [257608] = 4178135,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Jes Howlis"]["profiles"][profileName] = {
    [257791] = 3064037,
    [260067] = 3064037,
    [257793] = 0,
    [257785] = 3064053,
    [257827] = 3064037,
    [257777] = 3064053,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Knight Captain Valyri"]["profiles"][profileName] = {
    [257028] = 3064053,
    [256955] = 3064053,
    [256970] = 3064037,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Overseer Korgus"]["profiles"][profileName] = {
    [256083] = 3064037,
    [256105] = 2539669,
    [256198] = 3064037,
    [256199] = 3064037,
    [263345] = 3064037,
    [256038] = 2932965,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Tol Dagor Trash"]["profiles"][profileName] = {
    [258128] = 3064053,
    [258864] = 3064053,
    [258935] = 3064037,
    [258634] = 0,
    [258153] = 3064053,
    [258917] = 3064037,
    [258079] = 0,
  }

  --The MOTHERLODE!!
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Coin-Operated Crowd Pummeler"]["profiles"][profileName] = {
    [257337] = 3064053,
    [269493] = 3064053,
    [271784] = 0,
    [256493] = 0,
    [262347] = 3129589,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Rixxa Fluxflame"]["profiles"][profileName] = {
    [270028] = 3064037,
    [259853] = 3129589,
    [260669] = 3063893,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Mogul Razzdunk"]["profiles"][profileName] = {
    [271456] = 3064037,
    [260280] = 3064053,
    [276229] = 3129589,
    [260829] = 3064001,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_The MOTHERLODE!! Trash"]["profiles"][profileName] = {
    [267433] = 0,
    [263628] = 3064037,
    [263202] = 3064037,
    [262412] = 3064037,
    [262947] = 0,
    [263103] = 0,
    [268846] = 0,
    [281621] = 0,
    [262092] = 3064053,
    [269429] = 3064055,
    [262554] = 0,
    [280604] = 3064037,
    [263066] = 0,
    [263601] = 0,
    [269313] = 3064053,
    [268709] = 3064037,
    [269090] = 3064309,
    [268415] = 3064053,
    [268702] = 3064037,
    [268865] = 3064053,
    [268129] = 3064037,
    [263215] = 3064037,
    [268362] = 0,
    [262540] = 0,
    [269302] = 0,
    [268797] = 0,
  }

  --Freehold
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Skycap'n Kragg"]["profiles"][profileName] = {
    [255952] = 3064053,
    [256005] = 0,
    [256016] = 0,
    [256060] = 0,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Council o' Captains"]["profiles"][profileName] = {
    [258338] = 3064053,
    [272902] = 0,
    [264608] = 0,
    [265168] = 0,
    [265088] = 0,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Ring of Booty"]["profiles"][profileName] = {
    [257904] = 3064053,
    [257829] = 0,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Harlan Sweete"]["profiles"][profileName] = {
    [257305] = 3064053,
    [257316] = 3064053,
    [257278] = 3064053,
    [257314] = 3064053,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Freehold Trash"]["profiles"][profileName] = {
    [274400] = 3064053,
    [257736] = 0,
    [257899] = 0,
    [272402] = 0,
    [257272] = 3064053,
    [257908] = 3064053,
    [258199] = 3064053,
    [257397] = 0,
    [258672] = 0,
    [257870] = 0,
    [258777] = 0,
    [257756] = 3064053,
    [274555] = 0,
    [274507] = 0,
    [274383] = 0,
    [257437] = 0,
    [258181] = 3064053,
    [257739] = 3064309,
    [257426] = 3064053,
    [257775] = 0,
    [258323] = 3064053,
  }

  --King's Rest
  BigWigs3DB["namespaces"]["BigWigs_Bosses_The Golden Serpent"]["profiles"][profileName] = {
    [265773] = 3064053,
    [265781] = 3064037,
    [265923] = 3064037,
    [265910] = 3068133,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Mchimba the Embalmer"]["profiles"][profileName] = {
    [267639] = 3129589,
    [267618] = 3064037,
    [267702] = 3064053,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_The Council of Tribes"]["profiles"][profileName] = {
    [267060] = 3064053,
    [266231] = 3064053,
    [266237] = 3065061,
    [267273] = 3064037,
    [266951] = 3064037,
    [266206] = 3064037,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Dazar, The First King"]["profiles"][profileName] = {
    [268586] = 3068149,
    [268403] = 3064037,
    [269231] = 3064037,
    [268932] = 3064037,
    [269369] = 3064037,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_King's Rest Trash"]["profiles"][profileName] = {
    [270928] = 3064053,
    [271564] = 0,
    [270084] = 3064053,
    [269976] = 3064053,
    [270891] = 3064053,
    [270016] = 0,
    [269931] = 3064037,
    [270931] = 0,
    [270503] = 3064309,
    [270482] = 3064037,
    [269936] = 3064053,
    [270901] = 0,
    [270506] = 4112629,
    [270514] = 3064037,
    [269972] = 3064037,
    [270499] = 3064037,
    [270507] = 3064053,
    [270284] = 3064037,
    [270492] = 0,
    [270872] = 0,
    [270865] = 3064037,
    [270003] = 3064053,
    [271640] = 3064037,
    ["healing_tide_totem"] = 0,
    [271555] = 3064037,
    [270920] = 3064037,
    [270487] = 3068149,
  }

  --Junkyard
  BigWigs3DB["namespaces"]["BigWigs_Bosses_King Gobbamak"]["profiles"][profileName] = {
    [297257] = 0,
    [297261] = 3129589,
    [297254] = 3064053,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Gunker"]["profiles"][profileName] = {
    [297985] = 0,
    [298259] = 3064053,
    [297834] = 3064053,
    [297835] = 3064053,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Trixie & Naeno"]["profiles"][profileName] = {
    [298651] = 0,
    [298940] = 3064053,
    [298571] = 0,
    [302682] = 4112629,
    [298946] = 3064053,
    [299153] = 0,
    [298898] = 0,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_HK-8 Aerial Oppression Unit"]["profiles"][profileName] = {
    [295536] = 3064053,
    [296080] = 0,
    ["stages"] = 0,
    [296522] = 0,
    [303885] = 3064053,
    [302274] = 3064053,
    [301351] = 3064053,
    [295445] = 3133685,
  }

  --Workshop
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Tussle Tonks"]["profiles"][profileName] = {
    [285344] = 0,
    [285388] = 3129589,
    [285152] = 0,
    [283422] = 3064053,
    [285020] = 0,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_K.U.-J.0."]["profiles"][profileName] = {
    [291946] = 3129589,
    [294929] = 3068149,
    [291918] = 0,
    [291973] = 0,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Machinist's Garden"]["profiles"][profileName] = {
    [294853] = 3064053,
    [285440] = 3064053,
    [294855] = 0,
    [285454] = 0,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_King Mechagon"]["profiles"][profileName] = {
    [291865] = 3064053,
    [291626] = 0,
    [292290] = 0,
    ["stages"] = 0,
    ["button"] = 0,
    [291613] = 0,
    [291928] = 3064053,
    [283551] = 3064053,
    ["hardmode"] = 0,
  }

  --Operation: Mechagon
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Operation: Mechagon Trash"]["profiles"][profileName] = {
    [299502] = 0,
    [294195] = 0,
    [294290] = 0,
    [300102] = 0,
    [300764] = 0,
    [300087] = 0,
    [294103] = 0,
    [299474] = 0,
    [294324] = 3064053,
    [300514] = 0,
    [297128] = 3129589,
    [299475] = 3064053,
    [300436] = 0,
    [301681] = 3064053,
    [299525] = 0,
    [300171] = 0,
    [294180] = 0,
    [293683] = 0,
    [300188] = 3064053,
    [300424] = 3129589,
    [297133] = 0,
    [293729] = 0,
    [294015] = 0,
    [293930] = 0,
    [284219] = 0,
    [293670] = 0,
    [293827] = 0,
    [301088] = 0,
    [300159] = 3064053,
    [295169] = 0,
    [303941] = 0,
    [300207] = 3064053,
    [293986] = 3064053,
    [294884] = 3064053,
    [300129] = 0,
    [294349] = 0,
    [293861] = 0,
    [300177] = 0,
    [301667] = 0,
    [300650] = 3072245,
    [300414] = 3072245,
    [299588] = 0,
    [299438] = 0,
    [300777] = 3064053,
    [300687] = 0,
  }

  --Underrot
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Elder Leaxa"]["profiles"][profileName] = {
    [264757] = 3064037,
    [260894] = 3064053,
    [260879] = 0,
    [264603] = 3064053,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Infested Crawg"]["profiles"][profileName] = {
    ["random_cast"] = 3064037,
    [260793] = 3064053,
    [260333] = 3129589,
    [260292] = 3064053,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Sporecaller Zancha"]["profiles"][profileName] = {
    [272457] = 3064037,
    [273285] = 3064053,
    [259718] = 2539717,
    [259732] = 3129589,
    [259830] = 0,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Unbound Abomination"]["profiles"][profileName] = {
    [269843] = 3064053,
    [269310] = 3063953,
    [269301] = 2932965,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Underrot Trash"]["profiles"][profileName] = {
    [266209] = 3064053,
    [265668] = 0,
    [265568] = 3064021,
    [278961] = 3064053,
    [265081] = 3064053,
    [265089] = 0,
    [272592] = 3064037,
    [265019] = 0,
    [265540] = 0,
    [266106] = 16,
    [265091] = 3064037,
    [266107] = 3064309,
    [272609] = 3064053,
    [265487] = 3064037,
    [265433] = 3064037,
    [272183] = 0,
  }

  --Atal'Dazar
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Rezan"]["profiles"][profileName] = {
    [257407] = 3064311,
    [255434] = 3129591,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Vol'kaal"]["profiles"][profileName] = {
    [250258] = 3064037,
    [259572] = 3129591,
    [250241] = 0,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Priestess Alun'za"]["profiles"][profileName] = {
    [255579] = 3065061,
    [258709] = 0,
    [255582] = 3064037,
    [255577] = 3129591,
    [255558] = 0,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Yazma"]["profiles"][profileName] = {
    [250036] = 0,
    [259187] = 3129591,
    [250096] = 0,
    [250050] = 0,
    [249919] = 3064037,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Atal'Dazar Trash"]["profiles"][profileName] = {
    [253517] = 0,
    [256849] = 0,
    [255567] = 0,
    [252781] = 0,
    [255041] = 0,
    [252687] = 0,
  }

  --Waycrest Manor
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Waycrest Manor Trash"]["profiles"][profileName] = {
    [263891] = 3064053,
    [265759] = 3064037,
    [265876] = 3064037,
    [264396] = 3064037,
    [265760] = 3064053,
    [265352] = 0,
    [264390] = 3064037,
    [264150] = 0,
    [264105] = 2539717,
    [265741] = 3064037,
    [264050] = 3064037,
    [271174] = 3064053,
    [265368] = 3064037,
    [265407] = 3064037,
    [265880] = 2539717,
    [278474] = 3064037,
    [263943] = 3064037,
    [265346] = 0,
    [265881] = 3064053,
    [264556] = 3064037,
    [263959] = 3064037,
    [263905] = 3064037,
    [264456] = 3064037,
    [264525] = 3064037,
    [264038] = 3064005,
    [264520] = 3064037,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Raal the Gluttonous"]["profiles"][profileName] = {
    [264694] = 3064053,
    [264923] = 3064053,
    [265005] = 0,
    [264931] = 3064037,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Soulbound Goliath"]["profiles"][profileName] = {
    [267907] = 3129589,
    [260508] = 3065077,
    [260541] = 0,
    [260512] = 0,
    [260569] = 0,
    ["custom_on_267907"] = false,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Heartsbane Triad"]["profiles"][profileName] = {
    [260773] = 3129589,
    [260703] = 2539541,
    [268086] = 0,
    [260741] = 3064053,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Lord and Lady Waycrest"]["profiles"][profileName] = {
    [268278] = 3064037,
    [261440] = 2539669,
    [261447] = 0,
    [268306] = 3064037,
    [261438] = 3133685,
  }
  BigWigs3DB["namespaces"]["BigWigs_Bosses_Gorak Tul"]["profiles"][profileName] = {
    [266266] = 3064053,
    [266198] = 3064037,
    [266181] = 3129589,
    [266225] = 3064037,
  }



  BigWigs3DB["namespaces"]["BigWigs_Plugins_Proximity"]["profiles"][profileName] = {
    ["width"] = 139.9999847412109,
    ["posy"] = 300.0890263636939,
    ["height"] = 120.0000076293945,
    ["posx"] = 1164.089003882145,
  }
  BigWigs3DB["namespaces"]["BigWigs_Plugins_Sounds"]["profiles"][profileName] = {
    ["Long"] = {
      ["BigWigs_Bosses_Tol Dagor Trash"] = {
        [258864] = "BigWigs: [DBM] Beware (Algalon)",
      },
    },
    ["Warning"] = {
      ["BigWigs_Bosses_Lord Stormsong"] = {
        [269131] = "BigWigs: Info",
      },
      ["BigWigs_Bosses_Jes Howlis"] = {
        [257785] = "BigWigs: [DBM] Run Away Little Girl (Big Bad Wolf)",
      },
      ["BigWigs_Bosses_Gunker"] = {
        [298259] = "BigWigs: [DBM] Beware (Algalon)",
      },
      ["BigWigs_Bosses_Infested Crawg"] = {
        [260793] = "Air Horn",
      },
      ["BigWigs_Bosses_Underrot Trash"] = {
        [266106] = "Air Horn",
      },
    },
    ["Info"] = {
      ["BigWigs_Bosses_Underrot Trash"] = {
        [266209] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Operation: Mechagon Trash"] = {
        [300207] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Temple of Sethraliss Trash"] = {
        [268008] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Shrine of the Storm Trash"] = {
        [268211] = "Bike Horn",
      },
      ["BigWigs_Bosses_HK-8 Aerial Oppression Unit"] = {
        [302274] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Siege of Boralus Trash"] = {
        [272874] = "BigWigs: [DBM] Beware (Algalon)",
      },
    },
    ["Alarm"] = {
      ["BigWigs_Bosses_Operation: Mechagon Trash"] = {
        [300424] = "Air Horn",
        [293986] = "BigWigs: [DBM] Beware (Algalon)",
        [294884] = "Banana Peel Slip",
        [300159] = "BigWigs: [DBM] Beware (Algalon)",
        [300777] = "Air Horn",
        [297128] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Temple of Sethraliss Trash"] = {
        [264574] = "BigWigs: [DBM] Beware (Algalon)",
        [272657] = "BigWigs: [DBM] Beware (Algalon)",
        [273995] = "BigWigs: [DBM] Beware (Algalon)",
      },
      ["BigWigs_Bosses_Shrine of the Storm Trash"] = {
        [276268] = "Air Horn",
      },
      ["BigWigs_Bosses_K.U.-J.0."] = {
        [291946] = "BigWigs: Alert",
      },
      ["BigWigs_Bosses_Underrot Trash"] = {
        [266209] = "BigWigs: Raid Warning",
        [272609] = "BigWigs: [DBM] Beware (Algalon)",
        [265523] = "Air Horn",
        [266107] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Tik'ali"] = {
        [275907] = "BigWigs: [DBM] Beware (Algalon)",
      },
      ["BigWigs_Bosses_Heartsbane Triad"] = {
        [260741] = "BigWigs: Raid Warning",
        [260703] = "BigWigs: Alarm",
      },
      ["BigWigs_Bosses_Tidesage Coucil"] = {
        [267899] = "BigWigs: [DBM] Beware (Algalon)",
      },
      ["BigWigs_Bosses_King's Rest Trash"] = {
        [270003] = "BigWigs: Alert",
        [270928] = "BigWigs: [DBM] Run Away Little Girl (Big Bad Wolf)",
      },
      ["BigWigs_Bosses_The MOTHERLODE!! Trash"] = {
        [262092] = "BigWigs: Raid Warning",
        [268415] = "BigWigs: [DBM] Beware (Algalon)",
        [269429] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Raal the Gluttonous"] = {
        [264694] = "BigWigs: [DBM] Beware (Algalon)",
      },
      ["BigWigs_Bosses_Dread Captain Lockwood"] = {
        [268260] = "BigWigs: [DBM] Beware (Algalon)",
      },
      ["BigWigs_Bosses_Vol'zith the Whisperer"] = {
        [267385] = "Air Horn",
      },
      ["BigWigs_Bosses_Rezan"] = {
        [257407] = "BigWigs: [DBM] Run Away Little Girl (Big Bad Wolf)",
      },
      ["BigWigs_Bosses_The Council of Tribes"] = {
        [266231] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Siege of Boralus Trash"] = {
        [268260] = "BigWigs: [DBM] Beware (Algalon)",
      },
      ["BigWigs_Bosses_Vol'kaal"] = {
        [250585] = "BigWigs: Raid Boss Whisper",
      },
      ["BigWigs_Bosses_Machinist's Garden"] = {
        [285440] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Freehold Trash"] = {
        [257426] = "BigWigs: [DBM] Beware (Algalon)",
        [257739] = "BigWigs: [DBM] Run Away Little Girl (Big Bad Wolf)",
      },
      ["BigWigs_Bosses_Atal'Dazar Trash"] = {
        [253583] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_King Mechagon"] = {
        [291928] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Coin-Operated Crowd Pummeler"] = {
        [257337] = "Air Horn",
      },
    },
    ["Alert"] = {
      ["BigWigs_Bosses_Lord Stormsong"] = {
        [268896] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Adderis and Aspix"] = {
        [263309] = "BigWigs: [DBM] Beware (Algalon)",
      },
      ["BigWigs_Bosses_Temple of Sethraliss Trash"] = {
        [268008] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Infested Crawg"] = {
        [260292] = "Air Horn",
      },
      ["BigWigs_Bosses_K.U.-J.0."] = {
        [294929] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Underrot Trash"] = {
        [266209] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Trixie & Naeno"] = {
        [298946] = "BigWigs: Raid Warning",
        [298940] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_King's Rest Trash"] = {
        [270891] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Aqu'sirr"] = {
        [264101] = "BigWigs: [DBM] Beware (Algalon)",
      },
      ["BigWigs_Bosses_Waycrest Manor Trash"] = {
        [271174] = "BigWigs: [DBM] Beware (Algalon)",
        [265881] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_King Gobbamak"] = {
        [297254] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Council o' Captains"] = {
        [258338] = "BigWigs: [DBM] Flag Taken (PvP)",
      },
      ["BigWigs_Bosses_Tussle Tonks"] = {
        [283422] = "BigWigs: [DBM] Beware (Algalon)",
      },
      ["BigWigs_Bosses_Freehold Trash"] = {
        [258323] = "Bike Horn",
      },
      ["BigWigs_Bosses_Avatar of Sethraliss"] = {
        [268008] = "BigWigs: Raid Warning",
      },
      ["BigWigs_Bosses_Siege of Boralus Trash"] = {
        [256627] = "BigWigs: [DBM] Beware (Algalon)",
        [257288] = "BigWigs: [DBM] Beware (Algalon)",
      },
      ["BigWigs_Bosses_Shrine of the Storm Trash"] = {
        [268187] = "Banana Peel Slip",
        [274437] = "Bike Horn",
        [268214] = "BigWigs: Raid Boss Whisper",
      },
    },
  }
  BigWigs3DB["namespaces"]["BigWigs_Plugins_Bars"]["profiles"][profileName] = {
    ["BigWigsEmphasizeAnchor_y"] = 327.111162434685,
    ["BigWigsAnchor_width"] = 219.9999389648438,
    ["BigWigsAnchor_x"] = 886.7558374444707,
    ["BigWigsEmphasizeAnchor_height"] = 22.00003242492676,
    ["BigWigsAnchor_height"] = 15.99998760223389,
    ["BigWigsAnchor_y"] = 184.1777171333615,
    ["BigWigsEmphasizeAnchor_width"] = 320.0000305175781,
    ["BigWigsEmphasizeAnchor_x"] = 869.6890835589875,
  }
  BigWigs3DB["namespaces"]["BigWigs_Plugins_Alt Power"]["profiles"][profileName] = {
    ["posx"] = 1108.622943511291,
    ["posy"] = 196.2668667316484,
  }

  --Apply the BigWigs Profile
  local BigWigs = LibStub("AceDB-3.0"):New(BigWigs3DB)
  BigWigs:SetProfile(profileName)
end
