local  BUI = select(2, ...):unpack()

function BUI:ImportBlizzardSettings()
  SetCVar("useCompactPartyFrames", 1)

  ----------------------------------------------
  --           Part Frame Profile
  ----------------------------------------------
  local profileName = "Bob's Party Frames"
  --if the profile doesn't exist create it
  if not RaidProfileExists(profileName) then
    CreateNewRaidProfile(profileName)
  end
  --Set our new profile to the active profile
  SetActiveRaidProfile(profileName)

  SetRaidProfileOption(profileName, "shown", true)

  SetRaidProfileOption(profileName, "autoActivate2Players", false)
  SetRaidProfileOption(profileName, "autoActivate3Players", false)
  SetRaidProfileOption(profileName, "autoActivate5Players", false)
  SetRaidProfileOption(profileName, "autoActivate10Players", false)
  SetRaidProfileOption(profileName, "autoActivate15Players", false)
  SetRaidProfileOption(profileName, "autoActivate25Players", false)
  SetRaidProfileOption(profileName, "autoActivate40Players", false)

  SetRaidProfileOption(profileName, "autoActivatePvP", false)
  SetRaidProfileOption(profileName, "autoActivatePvE", true)

  SetRaidProfileOption(profileName, "autoActivateSpec1", false)
  SetRaidProfileOption(profileName, "autoActivateSpec2", false)
  SetRaidProfileOption(profileName, "autoActivateSpec3", false)
  SetRaidProfileOption(profileName, "autoActivateSpec4", false)

  SetRaidProfileOption(profileName, "keepGroupsTogether", false)
  SetRaidProfileOption(profileName, "sortBy", "role")
  SetRaidProfileOption(profileName, "displayHealPrediction", true)
  SetRaidProfileOption(profileName, "displayPowerBar", true)
  SetRaidProfileOption(profileName, "displayAggroHighlight", true)
  SetRaidProfileOption(profileName, "useClassColors", true)
  SetRaidProfileOption(profileName, "displayPets", true)
  SetRaidProfileOption(profileName, "displayMainTankAndAssist", true)
  SetRaidProfileOption(profileName, "displayBorder", false)
  SetRaidProfileOption(profileName, "displayNonBossDebuffs", true)
  SetRaidProfileOption(profileName, "displayOnlyDispellableDebuffs", false)
  SetRaidProfileOption(profileName, "healthText", "none")

  SetRaidProfileOption(profileName, "frameHeight", 72)
  SetRaidProfileOption(profileName, "frameWidth", 144)
  SetRaidProfileOption(profileName, "horizontalGroups", false)
  SetRaidProfileOption(profileName, "locked", true)

  SetRaidProfileSavedPosition(profileName, false, "TOP", 233, "BOTTOM", 379, "LEFT", 390)

  ----------------------------------------------
  --           Raid Frame Profile
  ----------------------------------------------

  local profileNameRaid = "Bob's Raid Frames"
  --if the profile doesn't exist create it
  if not RaidProfileExists(profileNameRaid) then
    CreateNewRaidProfile(profileNameRaid)
  end
  SetRaidProfileOption(profileNameRaid, "shown", true)

  SetRaidProfileOption(profileNameRaid, "autoActivate2Players", false)
  SetRaidProfileOption(profileNameRaid, "autoActivate3Players", false)
  SetRaidProfileOption(profileNameRaid, "autoActivate5Players", false)
  SetRaidProfileOption(profileNameRaid, "autoActivate10Players", false)
  SetRaidProfileOption(profileNameRaid, "autoActivate15Players", false)
  SetRaidProfileOption(profileNameRaid, "autoActivate25Players", false)
  SetRaidProfileOption(profileNameRaid, "autoActivate40Players", false)

  SetRaidProfileOption(profileNameRaid, "autoActivatePvP", false)
  SetRaidProfileOption(profileNameRaid, "autoActivatePvE", true)

  SetRaidProfileOption(profileNameRaid, "autoActivateSpec1", false)
  SetRaidProfileOption(profileNameRaid, "autoActivateSpec2", false)
  SetRaidProfileOption(profileNameRaid, "autoActivateSpec3", false)
  SetRaidProfileOption(profileNameRaid, "autoActivateSpec4", false)

  SetRaidProfileOption(profileNameRaid, "keepGroupsTogether", false)
  SetRaidProfileOption(profileNameRaid, "sortBy", "role")
  SetRaidProfileOption(profileNameRaid, "displayHealPrediction", true)
  SetRaidProfileOption(profileNameRaid, "displayPowerBar", true)
  SetRaidProfileOption(profileNameRaid, "displayAggroHighlight", true)
  SetRaidProfileOption(profileNameRaid, "useClassColors", true)
  SetRaidProfileOption(profileNameRaid, "displayPets", false)
  SetRaidProfileOption(profileNameRaid, "displayMainTankAndAssist", true)
  SetRaidProfileOption(profileNameRaid, "displayBorder", false)
  SetRaidProfileOption(profileNameRaid, "displayNonBossDebuffs", true)
  SetRaidProfileOption(profileNameRaid, "displayOnlyDispellableDebuffs", false)
  SetRaidProfileOption(profileNameRaid, "healthText", "none")

  SetRaidProfileOption(profileNameRaid, "frameHeight", 36)
  SetRaidProfileOption(profileNameRaid, "frameWidth", 75)
  SetRaidProfileOption(profileNameRaid, "horizontalGroups", false)
  SetRaidProfileOption(profileNameRaid, "locked", true)

  SetRaidProfileSavedPosition(profileNameRaid, false, "TOP", 233, "BOTTOM", 382, "LEFT", 339)

end
