local addon, engine = ...
engine[1] = {}
--store the toc version for use later
local Version = GetAddOnMetadata(addon, "Version")

BobUI = LibStub("AceAddon-3.0"):NewAddon("BobUI Installer");

function engine:unpack()
	return self[1]
end

BUI = engine [1]
local f

--make all our frames that we'll be filling later
local function createFrame()

  --save the default font here for now incalse we want to change it later
  local font = "Fonts\\FRIZQT__.TTF"
  f = CreateFrame("Button", "InstallerFrame", BobUI_EventFrame)
  f:SetSize(400,300)
  f:SetBackdrop({bgFile = "Interface\\Buttons\\WHITE8x8", edgeFile = "Interface\\Buttons\\WHITE8x8", edgeSize = 1})
  f:SetBackdropColor(.1,.1,.1,.8)
  f:SetBackdropBorderColor(1,1,1,1)
  f:SetPoint('CENTER')
  f:SetFrameStrata('TOOLTIP')
  f:SetMovable(true)

  f.MoveFrame = CreateFrame('Frame', nil, f, 'TitleDragAreaTemplate')
  f.MoveFrame:SetSize(350, 50)
  f.MoveFrame:SetPoint('TOP', f, 'TOP')

  f.Title = f:CreateFontString(nil, 'OVERLAY')
  f.Title:SetFont(font, 17, "OUTLINE")
  f.Title:SetPoint('TOP', 0, -5)
  f.Title:SetText("")

  f.Next = CreateFrame('Button', 'InstallerNextButton', f, 'UIPanelButtonTemplate')
  f.Next:SetSize(110, 25)
  f.Next:SetPoint('BOTTOMRIGHT', -5, 5)
	f.Next:SetText("Next")
  f.Next:Disable()
  f.Next:Hide()

  f.Prev = CreateFrame('Button', 'InstallerPrevButton', f, 'UIPanelButtonTemplate')
	f.Prev:SetSize(110, 25)
	f.Prev:SetPoint('BOTTOMLEFT', 5, 5)
	f.Prev:SetText("Back")
  f.Prev:Disable()
  f.Prev:Hide()

  f.ProgressText = f:CreateFontString(nil, 'OVERLAY')
  f.ProgressText:SetFont(font, 17, "OUTLINE")
  f.ProgressText:SetPoint('BOTTOM', 0, 10)
  f.ProgressText:SetText("")

  f.Button1 = CreateFrame('Button', 'InstallerButton1', f, 'UIPanelButtonTemplate')
  f.Button1:SetSize(160,30)
  f.Button1:SetPoint('BOTTOM', 0, 45)
  f.Button1:SetText('')
  f.Button1:Hide()

  f.Button2 = CreateFrame('Button', 'InstallerButton1', f, 'UIPanelButtonTemplate')
  f.Button2:SetSize(110,30)
  f.Button2:SetPoint('BOTTOMLEFT', f, 'BOTTOM', 4, 45)
  f.Button2:SetText('')
  --when Button2 is showing adjust size and position of Button1 so they look nice
  f.Button2:SetScript('OnShow', function()
    f.Button1:SetWidth(110,30)
    f.Button1:ClearAllPoints()
    f.Button1:SetPoint('BOTTOMRIGHT', f, 'BOTTOM', -4, 45)
  end)
  --when Button2 is hidden put Button1 back to normal
  f.Button2:SetScript('OnHide', function()
    f.Button1:SetWidth(160,30)
    f.Button1:ClearAllPoints()
    f.Button1:SetPoint('BOTTOM', 0, 45)
  end)
  f.Button2:Hide()

  f.PageTitle = f:CreateFontString(nil, 'OVERLAY')
  f.PageTitle:SetFont(font, 15, "OUTLINE")
  f.PageTitle:SetPoint('TOP', 0, -40)
  f.PageTitle:SetText('Page Title')

  f.PageText1 = f:CreateFontString(nil, 'OVERLAY')
  f.PageText1:SetFont(font, 15, "OUTLINE")
  f.PageText1:SetPoint('TOPLEFT', 20, -75)
  f.PageText1:SetWidth(f:GetWidth() - 40)

  f.PageText2 = f:CreateFontString(nil, 'OVERLAY')
  f.PageText2:SetFont(font, 15, "OUTLINE")
  f.PageText2:SetPoint('TOP', f.PageText1, 0, -20)
  f.PageText2:SetWidth(f:GetWidth() - 40)

  local close = CreateFrame('Button', 'InstallCloseButton', f, 'UIPanelCloseButton')
  close:SetPoint('TOPRIGHT', f, 'TOPRIGHT')
  close:SetScript('OnClick', function() f:Hide() end)

  f:Hide()

end

--demo function for testing as we go
local function demo()
  f:Show()
  f.Title:SetText("Welcome to Bob's UI!")
  f.Next:Enable()
  f.Prev:Enable()
  f.PageText1:SetText('Sample Text 1')
  f.PageText2:SetText('Sample Text 2')
  f.Button1:Show()
  f.Button2:Show()
end

local function runInstaller()
	--pull the installer data table from InstallerData.lua and use it to fill the installer frame
  BUI.installerData[1]()
  f:Show()
end

local function main()
	--create the Ace3 DB we'll be using
	BobUI.db = LibStub("AceDB-3.0"):New("BobUI_DB");

	--Go make sure the installer frame exists
  createFrame()

	--Parse the version variable from the toc file
	local _, _ , major, minor, build = string.find(Version, "(%d+).(%d+).(%d+)")

	--If the installer has been run before check the saved version variable
	local majorUser, minorUser, buildUser
	if BobUI.db.profile.install_version then
		_, _ ,majorUser, minorUser, buildUser = string.find(BobUI.db.profile.install_version, "(%d+).(%d+).(%d+)")
	end
	--Check and see if the isntaller has run before, if it has check and see if there has been an update
	if not BobUI.db.profile.install_version or (majorUser ~= major or minorUser ~= minor) then
  	runInstaller()
	end
end

--Register our slash command
SLASH_BOBUI1 = "/bobui"
local function Commands(msg, editbox)
	runInstaller()
end
SlashCmdList["BOBUI"] = Commands

--initialize our addon
local BobUI_EventFrame = CreateFrame ("Frame")
BobUI_EventFrame:RegisterEvent("PLAYER_ENTERING_WORLD")
BobUI_EventFrame:SetScript("OnEvent", function() main() end)
