local  BUI = select(2, ...):unpack()

function BUI:ImportTellMeWhen()

  local druidProfileName = "Bob's Druid TMW"
  --A copy paste of the "Gigabob - Kazzak" profile from the TMW WTF file
  TellMeWhenDB["profiles"][druidProfileName] = {
    ["Groups"] = {
      {
        ["GUID"] = "TMW:group:1SBj0thtNmIH",
        ["Point"] = {
          ["y"] = -93.2352016242447,
          ["x"] = 155.882278442383,
          ["point"] = "LEFT",
          ["relativePoint"] = "LEFT",
        },
        ["Scale"] = 1.70000195503235,
        ["Role"] = 3,
        ["EnabledSpecs"] = {
          [103] = false,
          [104] = false,
          [102] = false,
        },
        ["Columns"] = 7,
        ["Icons"] = {
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Innervate",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [1]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Tranquility",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [2]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Berserking",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [3]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Rebirth",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [4]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Heart Essence",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.92,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [5]
          {
            ["ShowTimer"] = true,
            ["Type"] = "item",
            ["Name"] = "Forbidden Obsidian Claw",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.92,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [6]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Thorns",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [7]
        },
      }, -- [1]
      {
        ["Point"] = {
          ["y"] = 159.5863494873047,
          ["x"] = 7.027857835806656,
          ["point"] = "BOTTOM",
          ["relativePoint"] = "BOTTOM",
        },
        ["Scale"] = 1.49135684967041,
        ["Role"] = 3,
        ["Level"] = 5,
        ["EnabledSpecs"] = {
          [103] = false,
          [104] = false,
          [102] = false,
        },
        ["Columns"] = 5,
        ["Icons"] = {
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["Name"] = "Swiftmend",
            ["Events"] = {
              {
                ["Type"] = "Sound",
                ["Sound"] = "TMW - Pling 4",
                ["Event"] = "OnFinish",
              }, -- [1]
              ["n"] = 1,
            },
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.85,
              }, -- [2]
              {
                ["Alpha"] = 0.37,
              }, -- [3]
              {
              }, -- [4]
            },
            ["RangeCheck"] = true,
            ["Enabled"] = true,
          }, -- [1]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["Name"] = "Wild Growth",
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.86,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["RangeCheck"] = true,
            ["Enabled"] = true,
          }, -- [2]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Nature's Cure",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.86,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [3]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["Name"] = "Ironbark",
            ["Events"] = {
              {
                ["Type"] = "Sound",
                ["Sound"] = "Bam",
                ["Event"] = "OnFinish",
              }, -- [1]
              ["n"] = 1,
            },
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.85,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["RangeCheck"] = true,
            ["Enabled"] = true,
          }, -- [4]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["Name"] = "Cenarion Ward",
            ["Events"] = {
              {
                ["Type"] = "Sound",
                ["Sound"] = "TMW - Ding 3",
                ["Event"] = "OnFinish",
              }, -- [1]
              ["n"] = 1,
            },
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.86,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["RangeCheck"] = true,
            ["Enabled"] = true,
          }, -- [5]
        },
        ["GUID"] = "TMW:group:1SBj47nEa7vo",
      }, -- [2]
      {
        ["GUID"] = "TMW:group:1SDf3GqeMloG",
        ["Point"] = {
          ["y"] = -18.49978637695313,
          ["x"] = -197.4995727539063,
          ["point"] = "RIGHT",
          ["relativePoint"] = "RIGHT",
        },
        ["EnabledSpecs"] = {
          [103] = false,
          [104] = false,
          [105] = false,
        },
        ["Columns"] = 5,
        ["Icons"] = {
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["Name"] = "Celestial Alignment",
            ["Events"] = {
              {
                ["Type"] = "Sound",
                ["Sound"] = "TMW - Ding 6",
                ["Event"] = "OnFinish",
              }, -- [1]
              ["n"] = 1,
            },
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.91,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [1]
          {
            ["ShowTimer"] = true,
            ["Type"] = "item",
            ["ShowTimerText"] = true,
            ["Name"] = "Forbidden Obsidian Claw",
            ["Events"] = {
              {
                ["Type"] = "Sound",
                ["Sound"] = "Bam",
                ["Event"] = "OnFinish",
              }, -- [1]
              ["n"] = 1,
            },
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.89,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [2]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["Name"] = "Solar Beam",
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [3]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["Name"] = "Force of Nature",
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [4]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Guardian of Azeroth",
            ["Events"] = {
              {
                ["Type"] = "Sound",
                ["Sound"] = "Bite",
                ["Event"] = "OnFinish",
              }, -- [1]
              ["n"] = 1,
            },
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [5]
        },
      }, -- [3]
      {
        ["GUID"] = "TMW:group:1SLNxAIwavem",
        ["Scale"] = 1.5523,
        ["Rows"] = 2,
        ["Role"] = 3,
        ["Icons"] = {
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Ursol's Vortex",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.84,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [1]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Typhoon",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.85,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [2]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Barkskin",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.85,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [3]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Frenzied Regeneration",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.84,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [4]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["Name"] = "Soothe",
            ["Events"] = {
              {
                ["Type"] = "Sound",
                ["Sound"] = "TMW - Ding 9",
                ["Event"] = "OnFinish",
              }, -- [1]
              ["n"] = 1,
            },
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.85,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [5]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Wild Charge",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.86,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [6]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Dash",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.85,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [7]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Prowl",
            ["ShowTimerText"] = true,
            ["ManaCheck"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.85,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [8]
        },
        ["Point"] = {
          ["y"] = 8.00846290588379,
          ["x"] = 3.4,
          ["point"] = "BOTTOM",
          ["relativePoint"] = "BOTTOM",
        },
      }, -- [4]
      {
        ["GUID"] = "TMW:group:1SLV8aq00Awd",
        ["Point"] = {
          ["y"] = 38.0661544799805,
          ["x"] = -11.6142111193393,
          ["point"] = "BOTTOM",
          ["relativePoint"] = "BOTTOM",
        },
        ["Scale"] = 1.5499,
        ["Role"] = 3,
        ["Columns"] = 1,
        ["Icons"] = {
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Mighty Bash",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.81,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [1]
        },
      }, -- [5]
      {
        ["GUID"] = "TMW:group:1SLk95rjbRZd",
        ["Point"] = {
          ["y"] = -93.5292931291418,
          ["x"] = 217.275329589844,
          ["point"] = "LEFT",
          ["relativePoint"] = "LEFT",
        },
        ["Scale"] = 1.7,
        ["Role"] = 3,
        ["Columns"] = 1,
        ["Icons"] = {
          {
            ["Type"] = "cooldown",
            ["Name"] = "Shadowmeld",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [1]
        },
      }, -- [6]
      {
        ["GUID"] = "TMW:group:1SM1NFM5ybce",
        ["Point"] = {
          ["y"] = -92.4623049944404,
          ["x"] = 153.32389831543,
          ["point"] = "LEFT",
          ["relativePoint"] = "LEFT",
        },
        ["Scale"] = 1.7088,
        ["Role"] = 1,
        ["Columns"] = 6,
        ["Icons"] = {
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Innervate",
            ["ShowTimerText"] = true,
            ["Events"] = {
              {
                ["Type"] = "Sound",
                ["Sound"] = "Bike Horn",
                ["Event"] = "OnFinish",
              }, -- [1]
              ["n"] = 1,
            },
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [1]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Rebirth",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.91,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [2]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Swiftmend",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.89,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [3]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Wild Growth",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.91,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [4]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Focused Azerite Beam",
            ["ShowTimerText"] = true,
            ["Events"] = {
              {
                ["Type"] = "Sound",
                ["Sound"] = "TMW - Ding 9",
                ["Event"] = "OnFinish",
              }, -- [1]
              ["n"] = 1,
            },
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.83,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [5]
          {
            ["Type"] = "cooldown",
            ["Enabled"] = true,
            ["CustomTex"] = "Fury of Elune",
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [6]
        },
      }, -- [7]
      {
        ["Point"] = {
          ["y"] = -69.19770892251569,
          ["x"] = 209.8985284499862,
        },
        ["Scale"] = 2.1677,
        ["Enabled"] = false,
        ["Columns"] = 1,
        ["Icons"] = {
          {
            ["ShowTimer"] = true,
            ["Type"] = "item",
            ["Name"] = "Potion of Unbridled Fury",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [1]
        },
        ["GUID"] = "TMW:group:1SN1Yhg7WQVw",
      }, -- [8]
      {
        ["GUID"] = "TMW:group:1SbqyPi9VbF1",
        ["Point"] = {
          ["y"] = 7.96884202957153,
          ["x"] = -11.9536954212274,
          ["point"] = "BOTTOM",
          ["relativePoint"] = "BOTTOM",
        },
        ["Scale"] = 1.5058,
        ["Role"] = 3,
        ["Columns"] = 1,
        ["Icons"] = {
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Renewal",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.86,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [1]
        },
      }, -- [9]
      {
        ["GUID"] = "TMW:group:1TpAVIBbI8uu",
        ["Scale"] = 1.500015497207642,
        ["Rows"] = 2,
        ["Role"] = 4,
        ["Icons"] = {
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Mangle",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [1]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Thrash",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [2]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Skull Bash",
            ["ShowTimerText"] = true,
            ["Events"] = {
              {
                ["Type"] = "Sound",
                ["Sound"] = "TMW - Pling 4",
                ["Event"] = "OnFinish",
              }, -- [1]
              ["n"] = 1,
            },
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [3]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Growl",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [4]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Frenzied Regeneration",
            ["ShowTimerText"] = true,
            ["Events"] = {
              {
                ["Type"] = "Sound",
                ["Sound"] = "Bite",
                ["Event"] = "OnFinish",
              }, -- [1]
              ["n"] = 1,
            },
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [5]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Barkskin",
            ["ShowTimerText"] = true,
            ["Events"] = {
              {
                ["Type"] = "Sound",
                ["Sound"] = "Bam",
                ["Event"] = "OnFinish",
              }, -- [1]
              ["n"] = 1,
            },
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [6]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Survival Instincts",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [7]
          {
            ["ShowTimer"] = true,
            ["Type"] = "item",
            ["Name"] = "Abyssal Healing Potion",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [8]
        },
        ["Point"] = {
          ["y"] = -137.6645828028826,
          ["x"] = 0.6669985566164834,
        },
      }, -- [10]
      {
        ["Point"] = {
          ["y"] = 152.7680969238281,
          ["x"] = 4.845773836940166,
          ["point"] = "BOTTOM",
          ["relativePoint"] = "BOTTOM",
        },
        ["Scale"] = 1.444451093673706,
        ["Role"] = 4,
        ["Enabled"] = false,
        ["Columns"] = 3,
        ["Icons"] = {
          {
            ["Type"] = "cooldown",
            ["Name"] = "Frenzied Regeneration",
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [1]
          {
            ["Type"] = "cooldown",
            ["Name"] = "Barkskin",
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [2]
          {
            ["Type"] = "cooldown",
            ["Name"] = "Survival Instincts",
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [3]
        },
        ["GUID"] = "TMW:group:1TpAZ4NHaoSw",
      }, -- [11]
      {
        ["GUID"] = "TMW:group:1TpAbDr9c=eD",
        ["Scale"] = 1.666662931442261,
        ["Rows"] = 2,
        ["Role"] = 4,
        ["Icons"] = {
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Typhoon",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [1]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Ursol's Vortex",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [2]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Soothe",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [3]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Remove Corruption",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [4]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Stampeding Roar",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [5]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Incapacitating Roar",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [6]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Swiftmend",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [7]
          {
            ["ShowTimer"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Wild Growth",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [8]
        },
        ["Point"] = {
          ["y"] = 9.600004196166992,
          ["x"] = 5.400510189085183,
          ["point"] = "BOTTOM",
          ["relativePoint"] = "BOTTOM",
        },
      }, -- [12]
      {
        ["GUID"] = "TMW:group:1UC207Rd9eSj",
        ["Point"] = {
          ["y"] = -43.99990844726563,
          ["x"] = -188.5000610351563,
        },
        ["Columns"] = 1,
        ["Icons"] = {
          {
            ["ShowTimer"] = true,
            ["Type"] = "buff",
            ["Name"] = "Racing Pulse",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [1]
        },
      }, -- [13]
      {
        ["GUID"] = "TMW:group:1UC20Obccha_",
        ["Point"] = {
          ["y"] = -157.6483909291737,
          ["x"] = -251.0481328652624,
        },
        ["Scale"] = 1.497,
        ["Role"] = 2,
        ["Columns"] = 1,
        ["Icons"] = {
          {
            ["ShowTimer"] = true,
            ["Type"] = "buff",
            ["Name"] = "Devout Spirit",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Enabled"] = true,
          }, -- [1]
        },
      }, -- [14]
    },
    ["NumGroups"] = 14,
    ["Version"] = 87505,
  }

  local paladinProfileName = "Bob's Paladin TMW"
  --A copy paste of the "Bobra - Kazzak" profile from the TMW WTF file
  TellMeWhenDB["profiles"][paladinProfileName] = {
    ["Locked"] = true,
    ["NumGroups"] = 12,
    ["Groups"] = {
      {
        ["GUID"] = "TMW:group:1SCgPi0aB=B8",
        ["Columns"] = 5,
        ["Scale"] = 1.79999899864197,
        ["Icons"] = {
          {
            ["ClockGCD"] = true,
            ["ShowTimer"] = true,
            ["ShowTimerText"] = true,
            ["Enabled"] = true,
            ["Name"] = "Holy Shock",
            ["Type"] = "cooldown",
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.8,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [1]
          {
            ["ClockGCD"] = true,
            ["ShowTimer"] = true,
            ["ShowTimerText"] = true,
            ["Enabled"] = true,
            ["Name"] = "Light of Dawn",
            ["Type"] = "cooldown",
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.84,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [2]
          {
            ["ClockGCD"] = true,
            ["ShowTimer"] = true,
            ["ShowTimerText"] = true,
            ["Enabled"] = true,
            ["Name"] = "Crusader Strike",
            ["Type"] = "cooldown",
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.83,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [3]
          {
            ["ClockGCD"] = true,
            ["ShowTimer"] = true,
            ["ShowTimerText"] = true,
            ["Enabled"] = true,
            ["Name"] = "Judgment",
            ["Type"] = "cooldown",
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.8,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [4]
          {
            ["ClockGCD"] = true,
            ["ShowTimer"] = true,
            ["ShowTimerText"] = true,
            ["Enabled"] = true,
            ["Name"] = "Cleanse",
            ["Type"] = "cooldown",
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.8,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [5]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Beacon of Virtue",
            ["Type"] = "cooldown",
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.81,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [6]
        },
        ["EnabledSpecs"] = {
          [66] = false,
          [70] = false,
        },
        ["Point"] = {
          ["y"] = -129.4441962979482,
        },
      }, -- [1]
      {
        ["GUID"] = "TMW:group:1SmrRR21y2hT",
        ["Locked"] = true,
        ["Point"] = {
          ["y"] = -93.06769754446142,
          ["x"] = 152.8412628173828,
          ["point"] = "LEFT",
          ["relativePoint"] = "LEFT",
        },
        ["EnabledSpecs"] = {
          [70] = false,
          [66] = false,
        },
        ["Scale"] = 1.7599983215332,
        ["Rows"] = 2,
        ["Icons"] = {
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Holy Avenger",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.85,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [1]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Avenging Wrath",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.83,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [2]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Aura Mastery",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.82,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [3]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Blessing of Sacrifice",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.82,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [4]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Blessing of Protection",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.81,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [5]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Divine Shield",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.8,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [6]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Divine Protection",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.8,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [7]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Lay on Hands",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.81,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [8]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Arcane Torrent",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.8,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [9]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Type"] = "item",
            ["Name"] = "Manifesto of Madness",
            ["OnlyEquipped"] = true,
            ["OnlyInBags"] = true,
          }, -- [10]
        },
        ["Columns"] = 5,
      }, -- [2]
      {
        ["GUID"] = "TMW:group:1SmrXkC=WevX",
        ["Columns"] = 6,
        ["Scale"] = 1.67222118377686,
        ["Icons"] = {
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Blessing of Freedom",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.83,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [1]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Blinding Light",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.8,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [2]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Hand of Reckoning",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.81,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [3]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Hammer of Justice",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.82,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [4]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Divine Steed",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.81,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [5]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Word of Glory",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.8,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [6]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [7]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [8]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [9]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [10]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [11]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [12]
        },
        ["Point"] = {
          ["y"] = 29.80047607421875,
          ["point"] = "BOTTOM",
          ["relativePoint"] = "BOTTOM",
        },
      }, -- [3]
      {
        ["GUID"] = "TMW:group:1TxMEdUeF0KC",
        ["Columns"] = 5,
        ["Scale"] = 1.6911,
        ["Icons"] = {
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["ShowTimerText"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Bestow Faith",
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [1]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["ShowTimerText"] = true,
            ["Type"] = "cooldown",
            ["Name"] = "Divine Favor",
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [2]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Gladiator's Medallion",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [3]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Repentance",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [4]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Heart Essence",
            ["Type"] = "cooldown",
            ["Conditions"] = {
              {
                ["Type"] = "AZESSLEARNED_MAJOR",
                ["Name"] = "Vision of Perfection",
                ["Level"] = 1,
              }, -- [1]
              ["n"] = 1,
            },
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [5]
        },
        ["Point"] = {
          ["y"] = -141.3281703227468,
          ["x"] = -236.8482034285916,
          ["point"] = "RIGHT",
          ["relativePoint"] = "RIGHT",
        },
      }, -- [4]
      {
        ["GUID"] = "TMW:group:1U5_scQVqHeB",
        ["Role"] = 1,
        ["Columns"] = 6,
        ["Scale"] = 1.766673445701599,
        ["Icons"] = {
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Blade of Justice",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [1]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Hammer of wrath",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [2]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Judgment",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [3]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Crusader strike",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [4]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Wake of ashes",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [5]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Rebuke",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [6]
        },
        ["Point"] = {
          ["y"] = -104.9993604401135,
          ["x"] = 2.264496529256044,
        },
      }, -- [5]
      {
        ["GUID"] = "TMW:group:1U8ueLwCthCY",
        ["Columns"] = 7,
        ["Scale"] = 1.933330178260803,
        ["Icons"] = {
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Divine shield",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [1]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Lay on hands",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.89,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [2]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Blessing of protection",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [3]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Shield of vengeance",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [4]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "avenging wrath",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [5]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Arcane Torrent",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [6]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["ShowTimerText"] = true,
            ["OnlyInBags"] = true,
            ["Type"] = "item",
            ["Name"] = "Vial of Animated Blood",
            ["OnlyEquipped"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [7]
        },
        ["EnabledSpecs"] = {
          [66] = false,
          [65] = false,
        },
        ["Point"] = {
          ["y"] = -70.34479647901419,
          ["x"] = 132.9310455322266,
          ["point"] = "LEFT",
          ["relativePoint"] = "LEFT",
        },
      }, -- [6]
      {
        ["GUID"] = "TMW:group:1UG=0oRMABaw",
        ["Icons"] = {
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["ShowTimerText"] = true,
            ["Type"] = "buff",
            ["Name"] = "Draconic Empowerment",
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [1]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Honed Mind",
            ["Type"] = "buff",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [2]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [3]
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Rule of Law",
            ["Type"] = "cooldown",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.9,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [4]
        },
        ["Point"] = {
          ["y"] = 120.4999160766602,
          ["x"] = 141.4998626708984,
          ["point"] = "BOTTOMLEFT",
          ["relativePoint"] = "BOTTOMLEFT",
        },
      }, -- [7]
      {
        ["GUID"] = "TMW:group:1UJdaCFBAX4T",
        ["Role"] = 2,
        ["Columns"] = 1,
        ["Icons"] = {
          {
            ["Enabled"] = true,
            ["Type"] = "buff",
            ["Name"] = "Spirit of Preservation",
            ["Conditions"] = {
              {
                ["Name"] = "Spirit of Preservation",
                ["Type"] = "AZESSLEARNED",
              }, -- [1]
              ["n"] = 1,
            },
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [1]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [2]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [3]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [4]
        },
        ["Point"] = {
          ["y"] = 112.0000228881836,
          ["x"] = -210.0001525878906,
          ["point"] = "BOTTOM",
          ["relativePoint"] = "BOTTOM",
        },
      }, -- [8]
      {
        ["GUID"] = "TMW:group:1UlOf2mE0845",
        ["Icons"] = {
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Ineffable Truth",
            ["Type"] = "buff",
            ["ShowTimerText"] = true,
            ["Events"] = {
              {
                ["Type"] = "Sound",
                ["Sound"] = "Electrical Spark",
                ["Event"] = "OnShow",
              }, -- [1]
              ["n"] = 1,
            },
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [1]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [2]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [3]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [4]
        },
        ["Point"] = {
          ["y"] = 0.0001220703125,
          ["x"] = -106.4998779296875,
        },
        ["Columns"] = 1,
      }, -- [9]
      {
        ["GUID"] = "TMW:group:1Un=GeMmvti6",
        ["Columns"] = 1,
        ["Scale"] = 1.800021529197693,
        ["Icons"] = {
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Forbidden Obsidian Claw",
            ["States"] = {
              {
              }, -- [1]
              {
                ["Alpha"] = 0.8,
              }, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
            ["Type"] = "item",
            ["ShowTimerText"] = true,
            ["OnlyEquipped"] = true,
            ["OnlyInBags"] = true,
          }, -- [1]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [2]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [3]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [4]
        },
        ["Point"] = {
          ["y"] = -111.6649702468648,
          ["x"] = -217.2195288708268,
        },
      }, -- [10]
      {
        ["GUID"] = "TMW:group:1UpLyDlRscik",
        ["Columns"] = 1,
        ["Scale"] = 1.70000684261322,
        ["Icons"] = {
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "Empyrean Power",
            ["SettingsPerView"] = {
              ["icon"] = {
                ["Texts"] = {
                  "", -- [1]
                  "[Stacks:Hide(0)]", -- [2]
                },
              },
            },
            ["Type"] = "buff",
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [1]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [2]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [3]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [4]
        },
        ["Point"] = {
          ["y"] = -6.176386146699819,
          ["x"] = 0.8825074478667299,
        },
      }, -- [11]
      {
        ["GUID"] = "TMW:group:1UqDJrt5_duU",
        ["Icons"] = {
          {
            ["Enabled"] = true,
            ["ShowTimer"] = true,
            ["Name"] = "SpeedBoosts",
            ["Type"] = "buff",
            ["ShowTimerText"] = true,
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [1]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [2]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [3]
          {
            ["States"] = {
              {
              }, -- [1]
              nil, -- [2]
              {
              }, -- [3]
              {
              }, -- [4]
            },
          }, -- [4]
        },
        ["Point"] = {
          ["y"] = 55.00015258789063,
          ["x"] = 1,
        },
        ["Columns"] = 1,
      }, -- [12]
    },
    ["Version"] = 87401,
  }

  TMW.db:SetProfile(druidProfileName)
end
