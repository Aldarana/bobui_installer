local  BUI = select(2, ...):unpack()

function BUI:ImportGottaGoFast()
  --GottaGoFast is using Ace3 profiles but doesn't seem to allow swapping at all so we'll just change the default one
  GottaGoFastDB["profiles"]["Default"] = {
    ["FrameX"] = -97.00032806396484,
    ["DeathInObjectives"] = true,
    ["FrameY"] = 29.9999942779541,
    ["Version"] = 31100,
    ["MobPoints"] = true,
  }
end
