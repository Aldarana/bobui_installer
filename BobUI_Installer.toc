## Interface: 80300
## Author: Aldarana
## Version: 0.0.1
## Title: BobUI Installer
## Notes: Bob's UI!
## RequiredDeps:
## DefaultState: enabled
## SavedVariables: BobUI_DB

Core.lua
InstallerData.lua
BobsWeakAuras.lua
BigWigs.lua
Plater.lua
Details.lua
TellMeWhen.lua
Bartender.lua
Blizzard.lua
BigDebuffs.lua
EnhancedRaidFrames.lua
GottaGoFast.lua
