local  BUI = select(2, ...):unpack()

function BUI:ImportBartender()

  --Its important that all of these follow the format Bartender4DB["namespaces"][*Bartender Plugin Name*]["profiles"][*Profile Name*] = {}
	--If not it will probably wipe out all other Bartender profiles

  local profileName = "Bob's Bartender"
  Bartender4DB["namespaces"]["StatusTrackingBar"]["profiles"][profileName] = {
    ["enabled"] = true,
    ["position"] = {
      ["y"] = 16,
      ["x"] = -405.500061035156,
      ["point"] = "BOTTOM",
    },
    ["version"] = 3,
  }
  Bartender4DB["namespaces"]["ActionBars"]["profiles"][profileName] = {
    ["actionbars"] = {
      {
        ["fadeout"] = true,
        ["fadeoutalpha"] = 0,
        ["position"] = {
          ["y"] = 91,
          ["x"] = 300,
          ["point"] = "BOTTOM",
        },
        ["version"] = 3,
        ["padding"] = 6,
        ["states"] = {
          ["stance"] = {
            ["DRUID"] = {
              ["prowl"] = 7,
            },
          },
        },
      }, -- [1]
      {
        ["enabled"] = false,
        ["version"] = 3,
        ["position"] = {
          ["y"] = -227.499847412109,
          ["x"] = -231.500183105469,
          ["point"] = "CENTER",
        },
      }, -- [2]
      {
        ["rows"] = 12,
        ["version"] = 3,
        ["position"] = {
          ["y"] = 610,
          ["x"] = -82,
          ["point"] = "BOTTOMRIGHT",
        },
        ["padding"] = 5,
      }, -- [3]
      {
        ["rows"] = 12,
        ["version"] = 3,
        ["position"] = {
          ["y"] = 610,
          ["x"] = -42,
          ["point"] = "BOTTOMRIGHT",
        },
        ["padding"] = 5,
      }, -- [4]
      {
        ["fadeout"] = true,
        ["fadeoutalpha"] = 0,
        ["version"] = 3,
        ["position"] = {
          ["y"] = 146,
          ["x"] = 300,
          ["point"] = "BOTTOM",
        },
        ["padding"] = 6,
      }, -- [5]
      {
        ["fadeout"] = true,
        ["version"] = 3,
        ["fadeoutalpha"] = 0,
        ["position"] = {
          ["y"] = 196,
          ["x"] = 300,
          ["point"] = "BOTTOM",
        },
        ["padding"] = 6,
      }, -- [6]
      {
        ["version"] = 3,
        ["position"] = {
          ["y"] = -227.499847412109,
          ["x"] = -231.500183105469,
          ["point"] = "CENTER",
        },
        ["states"] = {
          ["stance"] = {
            ["DRUID"] = {
              ["cat"] = 0,
            },
          },
        },
      }, -- [7]
      {
        ["version"] = 3,
        ["position"] = {
          ["y"] = -227.499847412109,
          ["x"] = -231.500183105469,
          ["point"] = "CENTER",
        },
      }, -- [8]
      {
        ["version"] = 3,
        ["position"] = {
          ["y"] = -227.499847412109,
          ["x"] = -231.500183105469,
          ["point"] = "CENTER",
        },
      }, -- [9]
      {
        ["version"] = 3,
        ["position"] = {
          ["y"] = -227.499847412109,
          ["x"] = -231.500183105469,
          ["point"] = "CENTER",
        },
      }, -- [10]
    },
  }
  Bartender4DB["namespaces"]["ExtraActionBar"]["profiles"][profileName] = {
    ["position"] = {
      ["y"] = 223.000030517578,
      ["x"] = -31.5000610351563,
      ["point"] = "BOTTOM",
    },
    ["version"] = 3,
  }
  Bartender4DB["namespaces"]["MicroMenu"]["profiles"][profileName] = {
    ["position"] = {
      ["y"] = 40,
      ["x"] = -292,
      ["point"] = "BOTTOMRIGHT",
      ["scale"] = 1,
    },
    ["version"] = 3,
    ["padding"] = -2,
  }
  Bartender4DB["namespaces"]["BagBar"]["profiles"][profileName] = {
    ["enabled"] = false,
    ["version"] = 3,
    ["position"] = {
      ["y"] = 200,
      ["x"] = -84.00000762939453,
      ["point"] = "CENTER",
    },
  }
  Bartender4DB["namespaces"]["BlizzardArt"]["profiles"][profileName] = {
    ["position"] = {
      ["y"] = 47,
      ["x"] = -512,
      ["point"] = "BOTTOM",
    },
    ["version"] = 3,
  }
  Bartender4DB["namespaces"]["ZoneAbilityBar"]["profiles"][profileName] = {
    ["position"] = {
      ["y"] = 223.000045776367,
      ["x"] = -31.5000610351563,
      ["point"] = "BOTTOM",
    },
    ["version"] = 3,
  }
  Bartender4DB["namespaces"]["StanceBar"]["profiles"][profileName] = {
    ["enabled"] = false,
    ["position"] = {
      ["y"] = 127,
      ["x"] = -460,
      ["point"] = "BOTTOM",
      ["scale"] = 1,
    },
    ["version"] = 3,
  }
  Bartender4DB["namespaces"]["PetBar"]["profiles"][profileName] = {
    ["version"] = 3,
    ["position"] = {
      ["y"] = 127,
      ["x"] = -120,
      ["point"] = "BOTTOM",
    },
  }
  Bartender4DB["namespaces"]["Vehicle"]["profiles"][profileName] = {
    ["position"] = {
      ["y"] = 47.5000305175781,
      ["x"] = 99.5,
      ["point"] = "CENTER",
    },
    ["version"] = 3,
  }

  Bartender4DB["profiles"][profileName] = {
    ["blizzardVehicle"] = true,
    ["focuscastmodifier"] = false,
    ["minimapIcon"] = {
      ["minimapPos"] = 115.5884292817226,
    },
    ["outofrange"] = "hotkey",
  }

  Bartender4.db:SetProfile(profileName)
end
